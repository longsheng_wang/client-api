<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%
	String LTF_LOGIN_URL = "/login/ltf";
	String REUNIFY_LOGIN_URL = "/login/reunify";
	String companyName = "reunify";
	if (session != null) {
		session.invalidate();
	}
	if (request.getCookies() != null) {
		for (Cookie cookie : request.getCookies()) {
			if (cookie.getName().equals("company")) {
				if (cookie.getValue().equals("reunify")) {

					response.sendRedirect(request.getContextPath() + REUNIFY_LOGIN_URL);
				} else {
					response.sendRedirect(request.getContextPath() + LTF_LOGIN_URL);

				}
				return;
			}
		}

	}
	response.sendRedirect(request.getContextPath() + LTF_LOGIN_URL);
%>

