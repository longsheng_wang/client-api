-- To support user roles
CREATE TABLE user_roles_lookup (
record_id serial PRIMARY KEY,
role_name varchar(50)
);

-- Adding roles
INSERT INTO user_roles_lookup (role_name) VALUES ('Admin');
INSERT INTO user_roles_lookup (role_name) VALUES ('Manager');
INSERT INTO user_roles_lookup (role_name) VALUES ('Customer Service');

-- Modifying users table to add support for roles;
ALTER TABLE users ADD COLUMN user_role INT REFERENCES user_roles_lookup (record_id) NOT NULL DEFAULT 1;

-- Index creation 
CREATE INDEX IX_users_user_role on users(user_role);

-- Reading permissions
GRANT SELECT
	ON user_roles_lookup
	TO ltfdashboard;	