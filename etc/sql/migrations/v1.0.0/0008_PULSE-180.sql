CREATE INDEX IX_member_check_in_location_id ON member_check_in(location_id) ;
CREATE INDEX IX_member_gym_buddy_location_id ON member_gym_buddy(location_id) ;
CREATE INDEX IX_member_intercept_impact_on_attrition_location_id ON member_intercept_impact_on_attrition(location_id) ;
CREATE INDEX IX_member_intercept_impact_on_usage_location_id ON member_intercept_impact_on_usage(location_id) ;
CREATE INDEX IX_member_intercepts_location_id ON member_intercepts(location_id) ;
CREATE INDEX IX_statistics_per_location_history_location_id ON statistics_per_location_history(location_id) ;
CREATE INDEX IX_statistics_per_location_today_location_id ON statistics_per_location_today(location_id) ;
CREATE INDEX IX_users_location_id ON users(location_id) ;

-- big tables
--  ~ 281 seconds
CREATE INDEX IX_member_check_in_history_location_id ON member_check_in_history(location_id) ;
--  ~ 141 seconds
CREATE INDEX IX_member_swipes_pattern_location_id ON member_swipes_pattern(location_id) ;
--  ~ 144 seconds
CREATE INDEX IX_members_location_id ON members(location_id) ;