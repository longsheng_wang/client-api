/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.traffic;

import java.util.List;

import org.springframework.stereotype.Component;

/**
 * DAO for traffic data. The class is implemented separately for different data
 * sources
 * 
 * @author kiana.baradaran
 * 
 */
@Component
public interface TrafficDAO {
	/**
	 * Gets the count and traffic information for all the zones in a location.
	 * 
	 * @param locatioId
	 * @return List<TrafficZone>
	 */
	public List<TrafficZone> getLocationTraffic(String locatioId);

}
