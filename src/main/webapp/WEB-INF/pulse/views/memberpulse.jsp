<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<ui:baseview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Trace - Member Pulse">
	<jsp:attribute name="headTag">
		<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/memberpulse/memberpulse.css"
			rel="stylesheet">
		<script
			src="${pageContext.servletContext.contextPath}/static/pulse/js/memberpulse/memberpulse.js"
			type="text/javascript"></script>
	</jsp:attribute>
	<jsp:body>
<div id="pulsepage_content" class="col-md-12 col-sm-12"
			style="height: 100%; padding: 0;">
	<div class="memberpulsepage_button_bar row-col-sm-12">
		<table class="memberpulsepage_button_bar_table">
			<tr>
				<td></td>

				<td>
					<p>RISK</p>
				</td>
				<td>
					<p>STARS</p>
				</td>
				<td><p>NEW (31-90 Days)</p></td>
			</tr>
			<tr>
				<td></td>

				<td>
					<div id="memberpulse-risk-choice" class="btn-group">
						<button type="button" id="memberpulse_risk-choice-2" value="1"
									class="btn btn-default">High</button>
						<button type="button" id="memberpulse_risk-choice-3" value="2"
									class="btn btn-default">Rest</button>
					</div>
				</td>
				<td><div id="memberpulse-value-choice" class="btn-group">
						<button type="button" id="memberpulse-value-choice-2" value="1"
									class="btn btn-default">5 Star</button>
						<button type="button" id="memberpulse-value-choice-3" value="2"
									class="btn btn-default">Rest</button>
					</div></td>
				<td>
					<div id="memberpulse-new-choice" class="btn-group">
						<button type="button" id="memberpulse-new-choice-2" value="1"
									class="btn btn-default">New</button>
					</div>
			
					
					
					</tr>

		</table>
	</div>
	
	<div class="alert alert-warning new-members-alert" role="alert" id="tap_to_see_new_comers" style="display:none">
		New Member(s) Came in, Tap <span style="text-decoration:underline;"> here</span> to See  
	
	
	</div>
	<div class="memberpulsepage_member_table_container col-md-12 row-col-sm-12" >

		<table id="memberpulsepage_member_table" class="table" id="table-container">
			<thead>

				<tr>

					<th id="memberpulsepage_member_table_col_0">Risk</th>
					<th id="memberpulsepage_member_table_col_1">Star</th>
					<th id="memberpulsepage_member_table_col_2"><img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/interception/interception-flag-white.png"
								width="23" alt=""></th>

					<th id="memberpulsepage_member_table_col_3">Name</th>
					<th id="memberpulsepage_member_table_col_4">Last Action</th>
					<th id="memberpulsepage_member_table_col_5">Age</th>
					<th id="memberpulsepage_member_table_col_6">Tenure</th>
					<th id="memberpulsepage_member_table_col_7">Swiped</th>
					<th><img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/pulse/MEMBERPULSE_BIRTHDAY_WHITE.png"
								width="20" alt=""></th>
					<th id="memberpulsepage_member_table_col_9">New</th>
					<th id="memberpulsepage_member_table_col_10">Anniversary</th>

				</tr>
			</thead>
			<tbody>
			</tbody>



			<tfoot>
			</tfoot>
		</table>

	</div>
</div>
	</jsp:body>
</ui:baseview>