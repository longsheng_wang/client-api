/**
 * Handles all the universal events
 */


// Notification sound initialization
$.ionSound({
	sounds : [ "notification" ],
	path : ctx + "/static/pulse/sound/", // set path to sounds
	multiPlay : true, // playing only 1 sound at once
	volume : "0.3" // not so loud please
});



$(document).ready(function() {
	// Break out from the iframe (LTF login)
	if (window.top.location != window.location) {
		window.top.location = window.location
	}

	stop_page_specific_timeouts();
	stop_universal_timeouts();
// Making Short Notes
	
// Check cookies to display alert
	 if (are_cookies_enabled())
		{ 
		
	}else{
		 document.getElementById("Cookie_check").style.display = "block";
	}

	// getClubNotifications();
	// setclock();
	$(".reunify-logo").attr("href", "http://kb.reunify.net");
	
});
// Function to check cookies + testcookie
function are_cookies_enabled()
{
    var cookieEnabled = (navigator.cookieEnabled) ? true : false;

    if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled)
    { 
        document.cookie="testcookie";
        cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
    }
    return (cookieEnabled);
}

// Page navigation -- start
$(document).on('click', "#homepage", function() {
	window.parent.location = ctx + "/clubpulse";
});

$(document).on('click', "#trafficpage", function() {
	window.parent.location = ctx + "/clubtraffic";

});

$(document).on('click', "#pulsepage", function() {
	window.parent.location = ctx + "/memberpulse";

});
$(document).on('click tap', ".memberprofile", function() {
	window.parent.location = ctx + "/memberprofile?member_id=" + $(this).attr("member-id");
});


$(document).on('click', "#searchpage", function() {
	window.parent.location = ctx + "/search";

});


$(document).on('click', "#analyticspage", function() {
	window.parent.location = ctx + "/clubanalytics";

});

$(document).on('click tap', ".memberprofile", function() {
window.parent.location = ctx + "/memberprofile/" + $(this).attr("member-id");
});

// Page navigation -- end

// Top right navbar event handlers -- start

$(document).on('click', ".edit-client", function() {
	window.parent.location = ctx + "/client/edit";

});
$(document).on('click', ".delete-client", function() {
	window.parent.location = ctx + "/client/remove";

});



$(document).on('click', ".view-users", function() {
	window.parent.location = ctx + "/users";

});

$(document).on('click', ".view-locations", function() {
	window.parent.location = ctx + "/client/locations";

});


$(document).on('click', ".refresh_button", function() {
	location.reload();
});

$(document).on('click', ".logout_button", function() {

	$('.logout_modal').modal('show');
	$(document).on("click", ".confirm-logout", function() {

		$('.logout_modal').modal('hide');
		stop_page_specific_timeouts();
		stop_universal_timeouts();
		window.parent.location = ctx + "/logout";
	});
	$(document).on("click", ".cancel-logout", function() {

		$('.logout_modal').modal('hide');
	});

});


$(document).on(
		"click",
		".navbar_birthday_notification_button",
		function() {
			window.parent.location = ctx + "/memberpulse?filter=birthday";
			$('.birthday-notification').attr("src",
					ctx + "/static/pulse/img/navbar/TOPBAR_BIRTHDAY_OFF_2.0x.png");
		});

$(document).on(
		"click",
		".navbar_attention_notification_button",
		function() {
			window.parent.location = ctx + "/memberpulse?filter=notification";
			$('.attention-notification').attr("src",
					ctx + "/static/pulse/img/navbar/TOPBAR_FLAG_OFF_2.0x.png");
		});
$(document)
		.on(
				"click",
				".navbar_risk_notification_button",
				function() {
					window.parent.location = ctx + "/memberpulse?filter=risk";


					$('.risk-notification')
							.attr(
									"src",
									ctx
											+ "/static/pulse/img/navbar/TOPBAR_ATRISK_OFF_2.0x.png");
				});
$(document)
		.on(
				"click",
				".navbar_new_notification_button",
				function() {
					window.parent.location = ctx + "/memberpulse?filter=new";

					$('.new-notification')
							.attr(
									"src",
									ctx
											+ "/static/pulse/img/navbar/TOPBAR_NEWCOMER_OFF_2.0x.png");
				});

// Top right navbar event handlers -- end


// Get the offset of the current location timezone
function getTimeZone() {
	$.ajax({
		type : 'GET',
		async : true,
		url : ctx+'/jsp/GetTime.jsp',
		dataType : 'json',
		success : function(data) {
			if (jQuery.isEmptyObject(data)) {
			} else {

				setCookie("offset", data.offset);

			}
		}
	});
}


// Setting the local time using the timezone offset
function setclock() {
	var offset = getCookie("offset");

	if (offset != null && offset != "") {
		$(".navbar_date_time_field").html(formatDate(offset));
	} else {
		getTimeZone();
	}
	clock_interval = setTimeout(function() {
		setclock();
	}, 30000);

}

// Date Time formatting
function formatDate(offset) {
	var months = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
			"Aug", "Sep", "Oct", "Nov", "Dec");
	var daysOfWeek = new Array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
	var localDate = new Date();
	var utcOffsetFromLocal = localDate.getTimezoneOffset() * 60 * 1000;
	var timeInUTC = localDate.getTime() + utcOffsetFromLocal;
	var timeInLocation = timeInUTC + parseInt(offset);
	var dateInLocation = new Date(timeInLocation);
	var month = months[dateInLocation.getMonth()];
	var dayOfWeek = daysOfWeek[dateInLocation.getDay()];
	var minutes = dateInLocation.getMinutes();
	var hours = dateInLocation.getHours();
	var ampm = hours >= 12 ? 'pm' : 'am';
	  hours = hours % 12;
	  hours = hours ? hours : 12; // the hour '0' should be '12';
	minutes = minutes < 10 ? '0'+minutes : minutes;// the minute '1' should be
													// '01';

	var result = month + " " + dateInLocation.getDate() + ", " + dayOfWeek
			+ " " + hours + ":"
			+ minutes+ ' ' + ampm;
	return result;

}


function stop_page_specific_timeouts() {
	clearTimeout(member_pulse_interval);
	clearTimeout(floorpage_count_interval);
	clearTimeout(homepage_survey_info_interval);
	clearTimeout(homepage_interception_info_interval);
	clearTimeout(homepage_pulse_interval);
	clearTimeout(homepage_interception_info_interval);
	clearTimeout(analytics_table_interval);
}

function stop_universal_timeouts() {
	clearTimeout(clock_interval);
	clearTimeout(notification_pulse_interval);
}



function getClubNotifications() {
    $.ajax({
        type: 'GET',
        async: true,
        url: ctx + '/jsp/getClubNotificationPulse.jsp',
        dataType: 'json',
        success: function(data) {
            if (jQuery.isEmptyObject(data)) {

            } else {

                var risk_notification_num = data.risky;
                var new_notification_num = data.new;
                var attention_notification_num = data.attention;
                var birthday_notification_num = data.birthday;
                
                if(risk_notification_num > 0 || new_notification_num > 0 || attention_notification_num > 0 ||birthday_notification_num > 0 ){
             	   $.ionSound.play("notification");
                }

                if (risk_notification_num > 0) {
                    clearTimeout(risky_notification_timeout);
                    $('.risk-notification').attr("src", ctx+"/static/pulse/img/navbar/TOPBAR_ATRISK_ON_2.0x.png");
                    risky_notification_timeout = setTimeout("$('.risk-notification').attr(\"src\", ctx+\"/static/pulse/img/navbar/TOPBAR_ATRISK_OFF_2.0x.png\");", 120000);
                    $('.risk-notification').addClass('animating'); // animate
																	// it
                    setTimeout(function() {
                        $('.risk-notification').removeClass('animating');
                    }, 1000);

                }


                if (new_notification_num > 0) {
                    clearTimeout(new_notification_timeout);
                    $('.new-notification').attr("src", ctx+"/static/pulse/img/navbar/TOPBAR_NEWCOMER_ON_2.0x.png");
                    new_notification_timeout = setTimeout("$('.new-notification').attr(\"src\", ctx+\"/static/pulse/img/navbar/TOPBAR_NEWCOMER_OFF_2.0x.png\");", 120000);
                    $('.new-notification').addClass('animating'); // animate
																	// it
                    setTimeout(function() {
                        $('.new-notification').removeClass('animating');
                    }, 1000);

                }


                if (attention_notification_num > 0) {
                    clearTimeout(attention_notification_timeout);
                    $('.attention-notification').attr("src", ctx+"/static/pulse/img/navbar/TOPBAR_FLAG_ON_2.0x.png");
                    attention_notification_timeout = setTimeout("$('.attention-notification').attr(\"src\", ctx+\"/static/pulse/img/navbar/TOPBAR_FLAG_OFF_2.0x.png\");", 120000);
                    $('.attention-notification').addClass('animating'); // animate
																		// it
                    setTimeout(function() {
                        $('.attention-notification').removeClass('animating');
                    }, 1000);

                }


                if (birthday_notification_num > 0) {
                    clearTimeout(birthday_notification_timeout);
                    $('.birthday-notification').attr("src", ctx+"/static/pulse/img/navbar/TOPBAR_BIRTHDAY_ON_2.0x.png");
                    birthday_notification_timeout = setTimeout("$('.birthday-notification').attr(\"src\", ctx+\"/static/pulse/img/navbar/TOPBAR_BIRTHDAY_OFF_2.0x.png\");", 120000);
                    $('.birthday-notification').addClass('animating'); // animate
																		// it
                    setTimeout(function() {
                        $('.birthday-notification').removeClass('animating');
                    }, 1000);
                }


            }

        }
    });
    notification_pulse_interval = setTimeout(function() {
        getClubNotifications();
    }, 5000);
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for ( var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1);
		if (c.indexOf(name) == 0)
			return c.substring(name.length, c.length);
	}
	return "";
}

function setCookie(cname, cvalue) {
	document.cookie = cname + "=" + cvalue + ";path= "+ctx;
	
}

function getURLParameter(name) {
	  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
	}

