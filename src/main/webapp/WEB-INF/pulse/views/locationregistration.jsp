<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<ui:loginview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Pulse - New User Sign-up">
	<jsp:attribute name="headTag">
		<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/login/registration.css"
			rel="stylesheet">
	</jsp:attribute>
	<jsp:body>
	
<div class="wrapper">

<form:form method="POST"
				action="${pageContext.servletContext.contextPath}/client/location/addorupdate"
				commandName="location">
             <table>

        		<tr>
                    <td><form:label path="clientLocationId">Location ID</form:label></td>
                    <td><form:input path="clientLocationId" /></td>
                   <td class="error"><form:errors
								path="clientLocationId" element="div" /></td>
                </tr>
       	 		<tr>
                    <td><form:label path="name">Location Name</form:label></td>
                    <td><form:input path="name" /></td>
                   <td class="error"><form:errors path="name"
								element="div" /></td>
                </tr>
                     <tr>
                    <td><form:label path="timeZone">Location Time Zone</form:label></td>
                    <td><form:select path="timeZone">
							<form:option value="NONE" label="--- Select ---" />
							<form:option value="US/Pacific" label="US Pacific" />
							<form:option value="US/Eastern" label="US Eastern" />
							<form:option value="US/Mountain" label="US Mountain" />
							<form:option value="US/Central" label="US Central" />
   						</form:select></td>
                   <td class="error"><form:errors path="timeZone"
								element="div" /></td>
                </tr>
                	<tr>
                    <td><form:label path="activeUsers">Number of Active Users</form:label></td>
                    <td><form:input path="activeUsers" /></td>
                   <td class="error"><form:errors
								path="activeUsers" element="div" /></td>
                </tr>
                	<tr>
                    <td><form:label path="performanceGoal">Performance Goal</form:label></td>
                    <td><form:input path="performanceGoal" /></td>
                   <td class="error"><form:errors
								path="performanceGoal" element="div" /></td>
                </tr>
             <c:if test="${not empty location.locationId}">
                <form:hidden path="locationId"
							value="${location.locationId}" />
               </c:if>
                <tr>
                    <td><input type="submit" value="Submit" /></td>
                </tr>
            </table>
        </form:form>
  </div>
	 
	</jsp:body>
</ui:loginview>