/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members.interception;

/**
 * Object representing the intercept later queue entry
 * 
 * @author kiana.baradaran
 * 
 */
public class InterceptQueueEntry {
	private int recordId;
	private int memberId;
	private int interceptorId;
	private int locationId;
	private String comment;
	private boolean isDeleted;
	private String insertTime;
	private String remindInInterval;
	private String reminderTime;

	public InterceptQueueEntry() {
		// TODO Auto-generated constructor stub
	}

	public InterceptQueueEntry(int memberId, int interceptorId, String comment, String remindInInterval, int locationId) {
		this.memberId = memberId;
		this.interceptorId = interceptorId;
		this.comment = comment;
		this.remindInInterval = remindInInterval;
		this.locationId = locationId;
	}

	public InterceptQueueEntry(int recordId, int memberId, int interceptorId, String comment, String reminderTime) {
		this.recordId = recordId;
		this.memberId = memberId;
		this.interceptorId = interceptorId;
		this.comment = comment;
		this.reminderTime = reminderTime;
	}

	public int getRecordId() {
		return recordId;
	}

	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}

	public int getInterceptorId() {
		return interceptorId;
	}

	public void setInterceptorId(int interceptorId) {
		this.interceptorId = interceptorId;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(String insertTime) {
		this.insertTime = insertTime;
	}

	public String getReminderTime() {
		return reminderTime;
	}

	public void setReminderTime(String reminderTime) {
		this.reminderTime = reminderTime;
	}

	public String getRemindInInterval() {
		return remindInInterval;
	}

	public void setRemindInInterval(String remindInInterval) {
		this.remindInInterval = remindInInterval;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
}
