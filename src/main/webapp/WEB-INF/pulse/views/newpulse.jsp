<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui-v2"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<ui:baseview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Pulse">
	<jsp:attribute name="headTag">

	</jsp:attribute>
	<jsp:body>
		
      <div class="row" id="memperPulse-top-row">
      
      	<div class="col-xs-4 left-third">
      		<div class="col-xs-4" id="walking-icon-time">
      			<div id="walking-icon-container">
							<img id="walking-icon"
							src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/walking-icon.png" />
						</div>
      			<div id="last-swipe-time">8:22AM </div>
      		</div>
      		<div class="col-xs-8" id="filter-parent">
      			<div id="filter-container">
      					<div id="filter">Filter</div>
      					<div class="notification-icon-filter">
								<img id="red-notification"
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/notification-red.png" />
							</div>
      					<div class="notification-icon-filter">
								<img id="yellow-notification"
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/notification-yellow.png" />
							</div>
      					<div class="notification-icon-filter">
								<img id="green-notification"
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/notification-green.png" />
							</div>
      				
      				
      			</div>
      		</div>
      	</div>
      	<div class="col-xs-4 "></div>
      	<div class="col-xs-4 right-third">
      		<div id="memberPulse-queue">Queue &nbsp <span
						id="memberPulse-queue-number">(4)</span>
					</div>
      		<div id="memberPulse-intercept-today">Intercepts Today &nbsp  <span
						id="memberPulse-intercept-today-number">(3)</span>
					</div>
      		
      	</div>
      	
       

      </div>
      <div class="row" id="second-row-memberpulse">
      		<div class="col-xs-4" id="member-cards-holder">
      			<div class="member-card">
      				<div class="col-xs-4" id="member-card-picture">
      					<div id="member-picture">
      						<img id="picture"
								src="${pageContext.servletContext.contextPath}/static/pulse/img/img-v2/member-picture.png" />
      					</div>
      				</div>
      				
      				<div class="col-xs-8" id="member-card-info">
      					<div class="row" id="member-card-first-row">
      						<div class="col-xs-8" id="member-card-name">Susan G. Harris</div>
      						<div class="col-xs-4" id="swiped-in-past-minutes">
      							<div id="swipe-in-minutes">3m</div>
      							<div id="walking-icon-member-card-container">
      								<img
										src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/walking-icon.png"
										id="walking-icon-member-card" />
      							</div>
      							
      						</div>
      						
      					</div>
      					<div class="row" id="member-card-second-row">Last intercept: Jan 23 by John B.</div>
      					<div class="row" id="member-card-third-row">
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/birthday-icon.png"
								id="birthday-icon" />
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/star-icon.png"
								id="star-icon" />	
      					</div>
      				
      				</div>
      			
      			</div>
      			<div class="member-card">
      				<div class="col-xs-4" id="member-card-picture">
      					<div id="member-picture">
      						<img id="picture"
								src="${pageContext.servletContext.contextPath}/static/pulse/img/img-v2/member-picture.png" />
      					</div>
      				</div>
      				
      				<div class="col-xs-8" id="member-card-info">
      					<div class="row" id="member-card-first-row">
      						<div class="col-xs-8" id="member-card-name">Susan G. Harris</div>
      						<div class="col-xs-4" id="swiped-in-past-minutes">
      							<div id="swipe-in-minutes">3m</div>
      							<div id="walking-icon-member-card-container">
      								<img
										src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/walking-icon.png"
										id="walking-icon-member-card" />
      							</div>
      							
      						</div>
      						
      					</div>
      					<div class="row" id="member-card-second-row">Last intercept: Jan 23 by John B.</div>
      					<div class="row" id="member-card-third-row">
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/birthday-icon.png"
								id="birthday-icon" />
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/star-icon.png"
								id="star-icon" />	
      					</div>
      				
      				</div>
      			
      			</div>
      			<div class="member-card">
      				<div class="col-xs-4" id="member-card-picture">
      					<div id="member-picture">
      						<img id="picture"
								src="${pageContext.servletContext.contextPath}/static/pulse/img/img-v2/member-picture.png" />
      					</div>
      				</div>
      				
      				<div class="col-xs-8" id="member-card-info">
      					<div class="row" id="member-card-first-row">
      						<div class="col-xs-8" id="member-card-name">Susan G. Harris</div>
      						<div class="col-xs-4" id="swiped-in-past-minutes">
      							<div id="swipe-in-minutes">3m</div>
      							<div id="walking-icon-member-card-container">
      								<img
										src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/walking-icon.png"
										id="walking-icon-member-card" />
      							</div>
      							
      						</div>
      						
      					</div>
      					<div class="row" id="member-card-second-row">Last intercept: Jan 23 by John B.</div>
      					<div class="row" id="member-card-third-row">
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/birthday-icon.png"
								id="birthday-icon" />
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/star-icon.png"
								id="star-icon" />	
      					</div>
      				
      				</div>
      			
      			</div>
      			<div class="member-card">
      				<div class="col-xs-4" id="member-card-picture">
      					<div id="member-picture">
      						<img id="picture"
								src="${pageContext.servletContext.contextPath}/static/pulse/img/img-v2/member-picture.png" />
      					</div>
      				</div>
      				
      				<div class="col-xs-8" id="member-card-info">
      					<div class="row" id="member-card-first-row">
      						<div class="col-xs-8" id="member-card-name">Susan G. Harris</div>
      						<div class="col-xs-4" id="swiped-in-past-minutes">
      							<div id="swipe-in-minutes">3m</div>
      							<div id="walking-icon-member-card-container">
      								<img
										src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/walking-icon.png"
										id="walking-icon-member-card" />
      							</div>
      							
      						</div>
      						
      					</div>
      					<div class="row" id="member-card-second-row">Last intercept: Jan 23 by John B.</div>
      					<div class="row" id="member-card-third-row">
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/birthday-icon.png"
								id="birthday-icon" />
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/star-icon.png"
								id="star-icon" />	
      					</div>
      				
      				</div>
      			
      			</div>
      			<div class="member-card">
      				<div class="col-xs-4" id="member-card-picture">
      					<div id="member-picture">
      						<img id="picture"
								src="${pageContext.servletContext.contextPath}/static/pulse/img/img-v2/member-picture.png" />
      					</div>
      				</div>
      				
      				<div class="col-xs-8" id="member-card-info">
      					<div class="row" id="member-card-first-row">
      						<div class="col-xs-8" id="member-card-name">Susan G. Harris</div>
      						<div class="col-xs-4" id="swiped-in-past-minutes">
      							<div id="swipe-in-minutes">3m</div>
      							<div id="walking-icon-member-card-container">
      								<img
										src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/walking-icon.png"
										id="walking-icon-member-card" />
      							</div>
      							
      						</div>
      						
      					</div>
      					<div class="row" id="member-card-second-row">Last intercept: Jan 23 by John B.</div>
      					<div class="row" id="member-card-third-row">
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/birthday-icon.png"
								id="birthday-icon" />
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/star-icon.png"
								id="star-icon" />	
      					</div>
      				
      				</div>
      			
      			</div>
      			<div class="member-card">
      				<div class="col-xs-4" id="member-card-picture">
      					<div id="member-picture">
      						<img id="picture"
								src="${pageContext.servletContext.contextPath}/static/pulse/img/img-v2/member-picture.png" />
      					</div>
      				</div>
      				
      				<div class="col-xs-8" id="member-card-info">
      					<div class="row" id="member-card-first-row">
      						<div class="col-xs-8" id="member-card-name">Susan G. Harris</div>
      						<div class="col-xs-4" id="swiped-in-past-minutes">
      							<div id="swipe-in-minutes">3m</div>
      							<div id="walking-icon-member-card-container">
      								<img
										src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/walking-icon.png"
										id="walking-icon-member-card" />
      							</div>
      							
      						</div>
      						
      					</div>
      					<div class="row" id="member-card-second-row">Last intercept: Jan 23 by John B.</div>
      					<div class="row" id="member-card-third-row">
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/birthday-icon.png"
								id="birthday-icon" />
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/star-icon.png"
								id="star-icon" />	
      					</div>
      				
      				</div>
      			
      			</div>
      			<div class="member-card">
      				<div class="col-xs-4" id="member-card-picture">
      					<div id="member-picture">
      						<img id="picture"
								src="${pageContext.servletContext.contextPath}/static/pulse/img/img-v2/member-picture.png" />
      					</div>
      				</div>
      				
      				<div class="col-xs-8" id="member-card-info">
      					<div class="row" id="member-card-first-row">
      						<div class="col-xs-8" id="member-card-name">Susan G. Harris</div>
      						<div class="col-xs-4" id="swiped-in-past-minutes">
      							<div id="swipe-in-minutes">3m</div>
      							<div id="walking-icon-member-card-container">
      								<img
										src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/walking-icon.png"
										id="walking-icon-member-card" />
      							</div>
      							
      						</div>
      						
      					</div>
      					<div class="row" id="member-card-second-row">Last intercept: Jan 23 by John B.</div>
      					<div class="row" id="member-card-third-row">
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/birthday-icon.png"
								id="birthday-icon" />
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/star-icon.png"
								id="star-icon" />	
      					</div>
      				
      				</div>
      			
      			</div>
      			<div class="member-card">
      				<div class="col-xs-4" id="member-card-picture">
      					<div id="member-picture">
      						<img id="picture"
								src="${pageContext.servletContext.contextPath}/static/pulse/img/img-v2/member-picture.png" />
      					</div>
      				</div>
      				
      				<div class="col-xs-8" id="member-card-info">
      					<div class="row" id="member-card-first-row">
      						<div class="col-xs-8" id="member-card-name">Susan G. Harris</div>
      						<div class="col-xs-4" id="swiped-in-past-minutes">
      							<div id="swipe-in-minutes">3m</div>
      							<div id="walking-icon-member-card-container">
      								<img
										src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/walking-icon.png"
										id="walking-icon-member-card" />
      							</div>
      							
      						</div>
      						
      					</div>
      					<div class="row" id="member-card-second-row">Last intercept: Jan 23 by John B.</div>
      					<div class="row" id="member-card-third-row">
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/birthday-icon.png"
								id="birthday-icon" />
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/star-icon.png"
								id="star-icon" />	
      					</div>
      				
      				</div>
      			
      			</div>
      			<div class="member-card">
      				<div class="col-xs-4" id="member-card-picture">
      					<div id="member-picture">
      						<img id="picture"
								src="${pageContext.servletContext.contextPath}/static/pulse/img/img-v2/member-picture.png" />
      					</div>
      				</div>
      				
      				<div class="col-xs-8" id="member-card-info">
      					<div class="row" id="member-card-first-row">
      						<div class="col-xs-8" id="member-card-name">Susan G. Harris</div>
      						<div class="col-xs-4" id="swiped-in-past-minutes">
      							<div id="swipe-in-minutes">3m</div>
      							<div id="walking-icon-member-card-container">
      								<img
										src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/walking-icon.png"
										id="walking-icon-member-card" />
      							</div>
      							
      						</div>
      						
      					</div>
      					<div class="row" id="member-card-second-row">Last intercept: Jan 23 by John B.</div>
      					<div class="row" id="member-card-third-row">
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/birthday-icon.png"
								id="birthday-icon" />
      						<img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/icons/icon-v2/star-icon.png"
								id="star-icon" />	
      					</div>
      				
      				</div>
      			
      			</div>
      			
      			

      			
      			
      		
      		</div>
      		<div class="col-xs-8" id="right-side-content"> </div>
      
      
    
    </div>
	</jsp:body>
</ui:baseview>
