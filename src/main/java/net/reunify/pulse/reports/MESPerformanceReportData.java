/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Container class for an MES Performance Report. A List of MESPerformanceReportDataRow
 * 
 * @author Vivek Hungund
 *
 */

public class MESPerformanceReportData {
	private static final Logger logger = Logger.getLogger(MESPerformanceReportData.class);
	
	private List<MESPerformanceReportDataRow> reportData;
	
	/**
	 * @param
	 */
	public MESPerformanceReportData() {
		reportData = new ArrayList<MESPerformanceReportDataRow>();
	}
	
	/**
	 * @param reportData
	 */
	public MESPerformanceReportData(List<MESPerformanceReportDataRow> reportData) {
		this.reportData = reportData;
	}
	
	/**
	 * 
	 * @param row
	 */
	public void addRow(MESPerformanceReportDataRow row) {
		reportData.add(row);
	}
	

	/**
	 * @return the reportData
	 */
	public List<MESPerformanceReportDataRow> getReportData() {
		return reportData;
	}

	/**
	 * @param reportData the reportData to set
	 */
	public void setReportData(List<MESPerformanceReportDataRow> reportData) {
		this.reportData = reportData;
	}
	
}
