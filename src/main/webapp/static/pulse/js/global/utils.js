/**
 * 
 */

var addUrlParameter = function(url, param, val) {
	var result = url;
	if (url && param && val) {
		if (url.indexOf('?') > -1) {
			// drop param if it already exists
			var pattern = param + "\=[^&]*";
			var re = new RegExp(pattern);
			result = url.replace(re, param+"="+encodeURIComponent(val));
		} else {
			result = url + '?' + param + '=' + encodeURIComponent(val);
		}
	}
	return result;
};