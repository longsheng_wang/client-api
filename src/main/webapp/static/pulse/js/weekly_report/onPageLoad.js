/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).keypress(function(e) {// logs in by pressing enter on the keyboard
	if (e.which === 13) {
		if ($("#loginbtn").is(':visible')) {
			$("#loginbtn").click();
		}
	}
});


$(document).on("click", ".logout_button", function() {// header logout button
														// functionality
	logout();
});

$(document).on(
		'pagebeforeshow',
		'#login_page',
		function() {

			$(document).on(
					'click',
					'#loginbtn',
					function() { // catch the form's submit event
						localStorage.setItem("lang", $(
								"#language-options option:selected").val());
						sessionStorage.setItem("lang", $(
								"#language-options option:selected").val());
						if ($('#username').val().length > 0
								&& $('#password').val().length > 0) {
							// Send data to server through ajax call
							// action is functionality we want to call and
							// outputJSON is our data
							login();

						} else {
							$("#errorpopup").popup("open");
						}
						return false; // cancel original event to prevent form
										// submitting
					});
		});


$(document).on('pagebeforeshow', '#homepage', function() {
        getDailyReports();    
        var interval = setInterval(function(){
        $.mobile.loading('show');
        clearInterval(interval);
    },1); 
});




$(document).on('pageshow', '#homepage', function() {
       check_status();
    setValues(); 
   var interval = setInterval(function(){
        $.mobile.loading('hide');
        clearInterval(interval);
    },2000);
	
});

function logout() {

	window.location.replace("#login_page", {
		transition : "fade"
	});
	location.reload();
	localStorage.clear();
	sessionStorage.clear();
}
