/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members;

import java.util.List;

import org.apache.log4j.Logger;

/**
 * data from members_fitness and other members_XXX tables
 * 
 * @author Shakti Shrivastava
 * @author kiana.baradaran
 * 
 */
public class MemberAttributes {
	@Override
	public String toString() {
		return "MemberAttributes [attributes=" + attributes + "]";
	}

	private static final Logger logger = Logger.getLogger(MemberAttributes.class);

	private List<MemberAttribute<?>> attributes;

	public MemberAttributes() {
		// TODO Auto-generated constructor stub
	}

	public MemberAttributes(List<MemberAttribute<?>> attributes) {
		setAttributes(attributes);
	}

	public List<MemberAttribute<?>> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<MemberAttribute<?>> attributes) {
		this.attributes = attributes;

	}

}
