/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members.interception;

import java.util.List;

/**
 * Response object for MemberInterceptionService
 * 
 * @author kiana.baradaran
 * 
 */

public class MemberInterceptResponse {

	private List<MemberIntercept> intercepts;

	public MemberInterceptResponse(List<MemberIntercept> intercepts) {
		this.intercepts = intercepts;
	}

	public MemberInterceptResponse() {
		// TODO Auto-generated constructor stub
	}

	public List<MemberIntercept> getIntercepts() {
		return intercepts;
	}

	public void setIntercepts(List<MemberIntercept> intercepts) {
		this.intercepts = intercepts;
	}

}
