-- creating the pulse db on remote server
CREATE DATABASE pulse;
-- switch database to pulse
\connect pulse

-- create tables
BEGIN;
-- clients table
CREATE TABLE clients (
	client_id serial PRIMARY KEY
	,industry_name VARCHAR(50)
	,client_name VARCHAR(100)
	,is_active BOOLEAN DEFAULT FALSE NOT NULL
	);
-- locations table
CREATE TABLE client_locations (
	location_id serial PRIMARY KEY
	,client_id INT REFERENCES clients(client_id)
	,client_location_id VARCHAR(36)
	,location_name VARCHAR(100)
	,location_timezone VARCHAR(20)
	,is_active BOOLEAN DEFAULT FALSE NOT NULL
	);	
-- users table
CREATE TABLE users (
	user_id serial PRIMARY KEY
	,location_id INT REFERENCES client_locations(location_id)
	,client_user_id VARCHAR(36)
	,firstname VARCHAR(50)
	,lastname VARCHAR(50)
	,email_address VARCHAR(100) NOT NULL
	,username VARCHAR(100) NOT NULL 
	,password VARCHAR(50) NOT NULL
	,account_creation_date timestamptz DEFAULT now() NOT NULL
	,is_active BOOLEAN DEFAULT FALSE NOT NULL
	,UNIQUE (email_address, location_id)
	);
-- members table
CREATE TABLE members (
	member_id serial PRIMARY KEY
	,location_id INT REFERENCES client_locations(location_id)
	,client_member_id VARCHAR(36) NOT NULL
	,firstname VARCHAR(50)
	,lastname VARCHAR(50)
	,birthdate DATE
	,gender VARCHAR(1)
	,email_address VARCHAR(100)
	,address VARCHAR(100)
	,city VARCHAR(50)
	,zipcode VARCHAR(10)
	,attrition_score INT
	,value_score INT
	,last_score_date DATE
	,last_check_in_id INT REFERENCES member_check_in(record_id)
	);
-- score history table
CREATE TABLE members_score_history (
	record_id serial
	,member_id INT REFERENCES members(member_id)
	,attrition_score INT
	,value_score DOUBLE PRECISION
	,score_date DATE
	,insert_time timestamptz DEFAULT now()
	,PRIMARY KEY (
		member_id
		,score_date
		)
	);
-- check-in table (UNIQUE is required for record_id as it is referenced from members)
CREATE TABLE member_check_in (
	record_id serial UNIQUE
	,client_member_id VARCHAR(36)
	,location_id INT REFERENCES client_locations(location_id)
	,check_in_time timestamptz
	,insert_time timestamptz default now() not null
	,PRIMARY KEY (
		client_member_id
		,location_id
		,check_in_time
		)
	);
-- intercept table
CREATE TABLE member_intercepts (
	intercept_id serial
	,member_id INT REFERENCES members(member_id)
	,interceptor_id INT REFERENCES users(user_id)
	,location_id INT REFERENCES client_locations(location_id)
	,intercept_note VARCHAR(1000)
	,intercept_status INT REFERENCES intercept_status_lookup(record_id)
	,intercept_time timestamptz DEFAULT now()
	,member_attrition_score_at_time INT
	,PRIMARY KEY (
		member_id
		,intercept_time
		)
	);
--  interest/persona table
CREATE TABLE member_personal_interests (
	record_id serial
	,member_id INT REFERENCES members(member_id)
	,type VARCHAR(10)
	,score_name VARCHAR(50)
	,score_value DOUBLE PRECISION
	,update_time timestamptz DEFAULT now()
	,PRIMARY KEY (
		member_id
		,score_name
		)
	);
-- social updates table
CREATE TABLE member_social_media_updates (
	record_id serial
	,member_id INT REFERENCES members(member_id)
	,social_media_source VARCHAR(50)
	,update_time timestamptz DEFAULT now()
	);
-- Intercept status	guideline table
CREATE TABLE intercept_status_lookup(
	record_id serial PRIMARY KEY,
	status VARCHAR(20) UNIQUE
);
-- Gym buddy table
CREATE TABLE member_gym_buddy(
 	record_id serial
	,location_id INT REFERENCES client_locations(location_id)
	,member_id INT REFERENCES members(member_id)
	,client_membership_id INT
 	,gymbuddy_member_id INT REFERENCES members(member_id)
 	,gymbuddy_membership_id INT
 	,score DOUBLE PRECISION
 	,score_date DATE NOT NULL
	,insert_time timestamptz DEFAULT now() NOT NULL
	,PRIMARY KEY (
	location_id
	,member_id
	,gymbuddy_member_id
	,score_date
	)
	);
-- Analytics: member_intercept_impact_on_attrition	
CREATE TABLE member_intercept_impact_on_attrition(
	record_id serial
	,location_id INT REFERENCES client_locations(location_id)
	,score_date DATE
	,attrition_group INT
	,time_window_in_months INT
	,distinct_checked_in_members INT
	,distinct_checked_in_members_cancellation_requested INT
	,distinct_intercepted_members INT
	,distinct_intercepted_members_cancellation_requested INT
	,checked_in_members_attrition_rate DOUBLE PRECISION
	,intercepted_members_attrition_rate DOUBLE PRECISION
	,intercepted_members_expected_cancellation INT
	,intercepted_members_saved INT
	,insert_time timestamptz DEFAULT now()
	,PRIMARY KEY (
		score_date, 
		location_id, 
		attrition_group, 
		time_window_in_months)
);
-- Analytics: member_intercept_impact_on_usage
CREATE TABLE member_intercept_impact_on_usage(
	record_id serial
	,location_id INT REFERENCES client_locations(location_id)
	,score_date DATE
	,time_window_in_days INT
	,intercepted_members INT
	,checked_in_members INT
	,intercepted_members_usage_increase_rate DOUBLE PRECISION
	,checked_in_members_usage_increase_rate DOUBLE PRECISION
	,insert_time timestamptz DEFAULT now()
	,PRIMARY KEY (
		score_date, 
		location_id, 
		time_window_in_days)
);
-- Swipe pattern table
CREATE TABLE member_swipes_pattern(
	record_id serial
	,location_id INT REFERENCES client_locations(location_id)
	,member_id INT REFERENCES members(member_id)
	,recency INT
	,frequency INT
	,sun  DOUBLE PRECISION
	,mon  DOUBLE PRECISION
	,tue  DOUBLE PRECISION
	,wed  DOUBLE PRECISION
	,thu  DOUBLE PRECISION
	,fri  DOUBLE PRECISION
	,sat  DOUBLE PRECISION
	,dawn  DOUBLE PRECISION
	,morning  DOUBLE PRECISION
	,afternoon  DOUBLE PRECISION
	,evening  DOUBLE PRECISION
	,score_date DATE
	,insert_time timestamptz DEFAULT now()
	,PRIMARY KEY (
	member_id
	,score_date)	
);


CREATE TABLE members_fitness(
	member_id INT PRIMARY KEY REFERENCES members(member_id)
	,membership_id VARCHAR(36) not null
	,membership_status VARCHAR(30)
	,membership_non_junior_members INT
	,membership_junior_members INT
	,membership_type VARCHAR(50)
	,membership_type_description VARCHAR(50)
	,membership_creation_date DATE
	,membership_activation_date DATE
	,membership_cancellation_request_date DATE
	,membership_termination_date DATE
	,cancellation_reason VARCHAR(100)
	,member_join_date DATE
	,monthly_dues DOUBLE PRECISION
	,member_type VARCHAR(10)
	,member_status VARCHAR(20)
);

CREATE TABLE statistics_per_location_history(
location_id INT REFERENCES client_locations(location_id)
,report_date DATE 
,total_check_ins INT
,high_risk_check_ins INT
,med_risk_check_ins INT
,low_risk_check_ins INT
,total_intercepts INT
,high_risk_intercepts INT
,med_risk_intercepts INT
,low_risk_intercepts INT
,good_to_go_intercepts INT
,need_follow_up_intercepts INT
,need_attention_intercepts INT
, PRIMARY KEY(
location_id
,report_date
)
);

CREATE TABLE statistics_per_location_today(
location_id INT PRIMARY KEY REFERENCES client_locations(location_id)
,report_date DATE NOT NULL
,total_check_ins INT
,high_risk_check_ins INT
,med_risk_check_ins INT
,low_risk_check_ins INT
,total_intercepts INT
,high_risk_intercepts INT
,med_risk_intercepts INT
,low_risk_intercepts INT
,good_to_go_intercepts INT
,need_follow_up_intercepts INT
,need_attention_intercepts INT
);
COMMIT;

-- Granting permissions
GRANT SELECT
	,INSERT
	,UPDATE
	ON ALL TABLES IN SCHEMA PUBLIC
	TO ltfdashboard;
	
GRANT ALL PRIVILEGES 
	ON ALL sequences IN SCHEMA PUBLIC
	TO ltfdashboard;
