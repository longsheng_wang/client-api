<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="${pageContext.servletContext.contextPath}/static/pulse/js/jquery/jquery-1.11.0.min.js"
	type="text/javascript"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/static/pulse/js/global/utils.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/static/pulse/js/swipesimulator/swipesimulator.js"></script>
<title>Pulse Swipe Simulator</title>
<link
	href="${pageContext.servletContext.contextPath}/static/pulse/css/swipesimulator/swipeSimulator.css"
	rel="stylesheet">
</head>
<body>
<div class="wrapper">
	<c:set var="clientId" value="${param.clientId == null ? '' : param.clientId}" />
	<c:set var="locationId" value="${param.locationId == null ? '' : param.locationId}" />
	<c:set var="numSwipes" value="${param.numSwipes == null ? '' : param.numSwipes}" />
	
	
	<strong>Swipe Simulator</strong>
	<form:form method="POST" action="${pageContext.servletContext.contextPath}/simulators/swipesimulator" commandName="SwipeSimulatorForm">
	
	<table>
		<tr>
			<td>
				<form:label path="clientId">Client</form:label>
			</td>
			<td>
				<form:select name="clientId" id="clientList" path="clientId">
					<form:option value="0" label="- Choose Client -" />
					<c:forEach items="${clients}" var="c">
			            <c:choose>
			               <c:when test="${param.clientId eq c.getClientId()}">
			                    <form:option value="${c.getClientId()}" selected="true" label="${c.getCompanyName()}" />
			                </c:when>
			                <c:otherwise>
			                    <form:option value="${c.getClientId()}" label="${c.getCompanyName()}" />
			                </c:otherwise>
			            </c:choose>
			        </c:forEach>
		       </form:select>
	        </td>
	        <td class="error">
	        	<form:errors path="clientId" element="div" cssClass="error" />
	        </td>
		</tr>
		
		<tr>
			<td>
				<form:label path="locationId">Location</form:label>
			</td>
			<td>
				<form:select name="locationId" path="locationId">
					<form:option value="0" label="- Choose Location -" />
					<c:forEach items="${locations}" var="l">
			            <c:choose>
			                <c:when test="${param.locationId eq l.getLocationId()}">
			                    <form:option value="${l.getLocationId()}" selected="true" label="${l.getName()}" />
			                </c:when>
			                <c:otherwise>
			                    <form:option value="${l.getLocationId()}" label="${l.getName()}" />
			                </c:otherwise>
			            </c:choose>
			        </c:forEach>	
	        	</form:select>
	        </td>
	        <td class="error">
	        	<form:errors path="locationId" element="div" cssClass="error" />
	        </td>
		</tr>
		
		<tr>
			<td>
				<form:label path="numSwipes">Number of Swipes<br />(not to exceed 5000)</form:label>
			</td>
			<td>
				<form:input type="text" name="numSwipes" value="${numSwipes}" path="numSwipes" />
			</td>
			<td class="error">
	        	<form:errors path="numSwipes" element="div" cssClass="error" />
	        </td>
		</tr>
		<tr>
			<td><input type="submit" value="Start Swipe Simulator" /></td>
		</tr>
	</table>
	</form:form>
	
	<c:if test="${pageContext.request.method=='POST'}">
		<br /><br /><strong>Simulating ${latestSimulator.getNumSwipesRequested()} swipes for ${latestSimulator.getClientName()} at the ${latestSimulator.getLocationName()} location</strong>
	</c:if>
	
	<br /><br />
	<c:choose>
		<c:when test="${runningSimulators.size() == 0 }">
			No simulators currently running<br />
		</c:when>
		<c:otherwise>
			<strong>Running Simulators</strong><br />
			<table border="1">
				<tr>
					<th width="15%">Client</th>
					<th width="25%">Location</th>
					<th width="10%">Number of Swipes Requested</th>
					<th width="10%">Number of Swipes Recorded So Far</th>
					<th width="40%">Started At</th>
				</tr>
				<c:forEach items="${runningSimulators}" var="sim">
					<tr>
						<td>${sim.getClientName()}</td>
						<td>${sim.getLocationName()}</td>
						<td>${sim.getNumSwipesRequested()}</td>
						<td>${sim.getNumSwipesInserted()}</td>
						<td>${sim.getStartedAt()}</td>
					</tr>
				</c:forEach>
			</table>
		</c:otherwise>
	</c:choose>
	<br />
	<button type="button" onClick="window.location='${pageContext.servletContext.contextPath}/simulators/swipesimulator'">Refresh Status</button>
</div>
</body>
</html>