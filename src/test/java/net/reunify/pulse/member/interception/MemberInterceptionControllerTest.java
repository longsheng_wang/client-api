/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.member.interception;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import javax.inject.Inject;
import javax.inject.Named;

import net.reunify.pulse.members.interception.MemberIntercept;
import net.reunify.pulse.members.interception.MemberInterceptionSearchCriteria;
import net.reunify.pulse.members.interception.MemberInterceptionService;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Assert;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Functional testing for member interception controllers
 * 
 * @author kiana.baradaran
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/spring-security.xml",
		"file:src/test/webapp/WEB-INF/mvc-dispatcher-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class MemberInterceptionControllerTest {

	private static final Logger logger = Logger.getLogger(MemberInterceptionControllerTest.class);

	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private FilterChainProxy springSecurityFilter;
	private MockMvc mockMvc;
	private MockHttpSession session;
	@Inject
	@Named("InterceptionService")
	private MemberInterceptionService interceptionServiceHandler;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		mockMvc = webAppContextSetup(wac).addFilters(springSecurityFilter).alwaysDo(print()).build();
		// enable authentication for all tests
		Authentication authentication = new UsernamePasswordAuthenticationToken("reunifyuser", "user@reunify278");
		SecurityContext securityContext = SecurityContextHolder.getContext();
		securityContext.setAuthentication(authentication);
		session = new MockHttpSession();
		session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, securityContext);
		session.setAttribute("locationId", "36");
	}

	@After
	public void tearDown() throws Exception {
		mockMvc = null;
	}

	@Test
	public void testMemberInterceptionSearchByLocationIdValidId() throws Exception {
		//@formatter:off
		Gson gson = new Gson();	
		MemberInterceptionSearchCriteria searchCriteria = new MemberInterceptionSearchCriteria();
		searchCriteria.setFrom("2015-01-13 00:00:00");
		searchCriteria.setTo("2015-01-14 00:00:00");
		MockHttpServletResponse response =mockMvc.perform(post("/memberinterceptionsearch")
				 .contentType(MediaType.APPLICATION_JSON).content(gson.toJson(searchCriteria))
				 .session(session))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8")).andReturn().getResponse();
		String expected = "{\"interceptId\":8,\"interceptDate\":\"2015-01-13 19:26:43\",\"interceptStatus\":2,\"locationId\":36,\"interceptorId\":2,\"memberId\":6537200,\"interceptNote\":\"test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 \",\"attritionScore\":24}";
		JsonParser parser = new JsonParser();
		JsonObject o = (JsonObject)parser.parse(response.getContentAsString());
		assertEquals(expected, o.getAsJsonArray("intercepts").get(0).getAsJsonObject().toString());
		//@formatter:on
	}

	@Test
	public void testMemberInterceptionSearchByLocationIdEmptyResponse() throws Exception {
		//@formatter:off
		Gson gson = new Gson();	
		MemberInterceptionSearchCriteria searchCriteria = new MemberInterceptionSearchCriteria();
		searchCriteria.setLocationId("152");
		searchCriteria.setFrom("2014-11-25 00:00:00");
		searchCriteria.setTo("2014-11-26 00:00:00");
		MockHttpServletResponse response =mockMvc.perform(post("/memberinterceptionsearch")
				 .contentType(MediaType.APPLICATION_JSON).content(gson.toJson(searchCriteria))
				 .session(session))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8")).andReturn().getResponse();
		String expected = "{\"intercepts\":[]}";
		 assertEquals(expected, response.getContentAsString());
		//@formatter:on
	}

	@Test
	public void testMemberInterceptionSearchByLocationIdInvalidDate() throws Exception {
		//@formatter:off
		Gson gson = new Gson();	
		MemberInterceptionSearchCriteria searchCriteria = new MemberInterceptionSearchCriteria();
		searchCriteria.setFrom("2014-11-25 00:00:00");
		searchCriteria.setTo("2014-11-260:00:00");
		mockMvc.perform(post("/memberinterceptionsearch")
				 .contentType(MediaType.APPLICATION_JSON).content(gson.toJson(searchCriteria))
				 .session(session))
				.andExpect(status().isBadRequest());
		//@formatter:on
	}

	@Test
	public void testMemberInterceptionSearchByMemberIdValidId() throws Exception {
		//@formatter:off
		Gson gson = new Gson();	
		MemberInterceptionSearchCriteria searchCriteria = new MemberInterceptionSearchCriteria();
		searchCriteria.setMemberId("1928843");
		MockHttpServletResponse response =mockMvc.perform(post("/memberinterceptionsearch")
				 .contentType(MediaType.APPLICATION_JSON).content(gson.toJson(searchCriteria))
				 .session(session))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8")).andReturn().getResponse();
		String expected = "{\"interceptId\":116,\"interceptDate\":\"2015-02-12 18:15:57\",\"interceptStatus\":1,\"locationId\":36,\"interceptorId\":2,\"memberId\":1928843,\"interceptNote\":\"Test From Reunify\",\"attritionScore\":0}";
		JsonParser parser = new JsonParser();
		JsonObject o = (JsonObject)parser.parse(response.getContentAsString());
		assertEquals(expected, o.getAsJsonArray("intercepts").get(0).getAsJsonObject().toString());
		//@formatter:on
	}

	@Test
	public void testMemberInterceptionSearchByMemberIdInvalidId() throws Exception {
		//@formatter:off
		Gson gson = new Gson();	
		MemberInterceptionSearchCriteria searchCriteria = new MemberInterceptionSearchCriteria();
		searchCriteria.setMemberId("102052649b");
		mockMvc.perform(post("/memberinterceptionsearch")
				 .contentType(MediaType.APPLICATION_JSON).content(gson.toJson(searchCriteria))
				 .session(session))
					.andExpect(status().isBadRequest());
		//@formatter:on
	}

	@Test
	public void testMemberInterceptionSearchByMemberIdEmptyResponse() throws Exception {
		//@formatter:off
		Gson gson = new Gson();	
		MemberInterceptionSearchCriteria searchCriteria = new MemberInterceptionSearchCriteria();
		searchCriteria.setMemberId("102052649");
		MockHttpServletResponse response =mockMvc.perform(post("/memberinterceptionsearch")
				 .contentType(MediaType.APPLICATION_JSON).content(gson.toJson(searchCriteria))
				 .session(session))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8")).andReturn().getResponse();		
		 String expected = "{\"intercepts\":[]}";
		 assertEquals(expected, response.getContentAsString());
		//@formatter:on
	}

	@Test
	public void testMemberInterceptionSearchByEmployeeIdValidId() throws Exception {
		//@formatter:off
		Gson gson = new Gson();	
		MemberInterceptionSearchCriteria searchCriteria = new MemberInterceptionSearchCriteria();
		searchCriteria.setEmployeeId("2");
		searchCriteria.setFrom("2015-01-13 00:00:00");
		searchCriteria.setTo("2015-01-14 00:00:00");
		MockHttpServletResponse response =mockMvc.perform(post("/memberinterceptionsearch")
				 .contentType(MediaType.APPLICATION_JSON).content(gson.toJson(searchCriteria))
				 .session(session))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8")).andReturn().getResponse();
		String expected = "{\"interceptId\":8,\"interceptDate\":\"2015-01-13 19:26:43\",\"interceptStatus\":2,\"locationId\":36,\"interceptorId\":2,\"memberId\":6537200,\"interceptNote\":\"test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 test note 4 \",\"attritionScore\":24}";
		JsonParser parser = new JsonParser();
		JsonObject o = (JsonObject)parser.parse(response.getContentAsString());
		assertEquals(expected, o.getAsJsonArray("intercepts").get(0).getAsJsonObject().toString());
		//@formatter:on
	}

	@Test
	public void testMemberInterceptionSearchByEmployeeIdEmptyResponse() throws Exception {
		//@formatter:off
		Gson gson = new Gson();	
		MemberInterceptionSearchCriteria searchCriteria = new MemberInterceptionSearchCriteria();
		searchCriteria.setEmployeeId("148");
		searchCriteria.setFrom("2014-11-25 00:00:00");
		searchCriteria.setTo("2014-11-26 00:00:00");
		MockHttpServletResponse response =mockMvc.perform(post("/memberinterceptionsearch")
				 .contentType(MediaType.APPLICATION_JSON).content(gson.toJson(searchCriteria))
				 .session(session))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8")).andReturn().getResponse();
		 String expected = "{\"intercepts\":[]}";
		 assertEquals(expected, response.getContentAsString());
		//@formatter:on
	}

	@Test
	public void testMemberInterceptionSearchByEmployeeIdInvalidId() throws Exception {
		//@formatter:off
		Gson gson = new Gson();	
		MemberInterceptionSearchCriteria searchCriteria = new MemberInterceptionSearchCriteria();
		searchCriteria.setEmployeeId("148f");
		searchCriteria.setFrom("2014-11-25 00:00:00");
		searchCriteria.setTo("2014-11-26 00:00:00");
		mockMvc.perform(post("/memberinterceptionsearch")
				 .contentType(MediaType.APPLICATION_JSON).content(gson.toJson(searchCriteria))
				 .session(session))
				.andExpect(status().isBadRequest());
		//@formatter:on
	}

	@Test
	public void testMemberInterceptionSearchByEmployeeIdInvalidDate() throws Exception {
		//@formatter:off
		Gson gson = new Gson();	
		MemberInterceptionSearchCriteria searchCriteria = new MemberInterceptionSearchCriteria();
		searchCriteria.setEmployeeId("2");
		searchCriteria.setFrom("2014-11-25 00:00:00");
		searchCriteria.setTo("2014-11-260:00:00");
		mockMvc.perform(post("/memberinterceptionsearch")
				 .contentType(MediaType.APPLICATION_JSON).content(gson.toJson(searchCriteria))
				 .session(session))
				.andExpect(status().isBadRequest());
		//@formatter:on
	}

	@Test
	public void testGetInterceptDetailsByValidId() throws Exception {
		//@formatter:off
		MockHttpServletResponse response = mockMvc.perform(get("/memberintercepts/10")
				.session(session))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8")).andReturn().getResponse();
		
		String expected = "{\"interceptId\":10,\"interceptDate\":\"2015-01-13 19:15:34\",\"interceptStatus\":3,\"locationId\":36,\"interceptorId\":2,\"memberId\":6537200,\"interceptNote\":\"test note ,test note ,test note ,test note ,test note ,test note ,test note ,test note ,test note ,test note ,test note ,test note ,test note ,test note ,test note ,test note ,test note ,test note ,test note ,test note ,\",\"attritionScore\":24}";
		Assert.notNull(response);
		assertEquals(expected,response.getContentAsString());	
		//@formatter:on
	}

	@Test
	public void testGetInterceptDetailsByInValidId() throws Exception {
		//@formatter:off
		 mockMvc.perform(get("/memberintercepts/0f")
				.session(session)).andExpect(status().isBadRequest());
		//@formatter:on
	}

	@Test
	public void testGetInterceptDetailsByEmptyResponse() throws Exception {
		//@formatter:off
		mockMvc.perform(get("/memberintercepts/0")
				.session(session)).andExpect(status().isNotFound());
		//@formatter:on
	}

	@Test
	public void testInsertMemberIntercept() throws Exception {
		//@formatter:off
		Gson gson = new Gson();
		MemberIntercept intercept = new MemberIntercept();
		intercept.setInterceptorId(2);
		intercept.setInterceptNote("Test From Reunify");
		intercept.setLocationId(36);
		intercept.setMemberId(1928843);
		intercept.setInterceptStatus(1);
		mockMvc.perform(post("/memberintercepts").contentType(MediaType.APPLICATION_JSON).content(gson.toJson(intercept)).session(session))
				.andExpect(status().isOk());		
		//@formatter:on
	}

	@Test
	public void testInsertMemberInterceptAfterItsDone() throws Exception {
		//@formatter:off
		Gson gson = new Gson();
		MemberIntercept intercept = new MemberIntercept();
		intercept.setInterceptorId(2);
		intercept.setInterceptNote("Test From Reunify");
		intercept.setLocationId(36);
		intercept.setMemberId(1928843);
		intercept.setInterceptStatus(1);
		intercept.setInterceptDuration(369);
		mockMvc.perform(post("/memberintercepts").contentType(MediaType.APPLICATION_JSON).content(gson.toJson(intercept)).session(session))
				.andExpect(status().isOk());		
		//@formatter:on
	}

	@Test
	public void testEndMemberIntercept() throws Exception {
		//@formatter:off
		Gson gson = new Gson();
		MemberIntercept intercept = new MemberIntercept();
		intercept.setInterceptorId(2);
		intercept.setInterceptNote("Test From Reunify");
		intercept.setLocationId(36);
		intercept.setMemberId(1928843);
		intercept.setInterceptStatus(1);
		int interceptId = interceptionServiceHandler.insertMemberIntercept(intercept);
		intercept.setInterceptDuration(369);
		mockMvc.perform(put("/memberintercepts/"+interceptId).contentType(MediaType.APPLICATION_JSON).content(gson.toJson(intercept)).session(session))
				.andExpect(status().isOk());		
		//@formatter:on
	}
}
