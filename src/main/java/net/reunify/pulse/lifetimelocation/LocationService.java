/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.lifetimelocation;
import java.util.Map;

/**
 * Service for Lifetime Locations
 * 
 * @author Vivek Hungund
 *
 */
public interface LocationService {

	public Map<String,Location> getLocations();
}
