/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service to handle MemberController requests
 * 
 * @author kiana.baradaran
 * 
 */
@Service("MemberService")
@Transactional(value = "pulseTxManager")
public class MemberServiceImpl implements MemberService {

	@Inject
	private MemberAnalyticsDAO memberAnalyticsDAO;

	@Inject
	private MemberProfileDAO memberProfileDAO;

	@Inject
	private MemberCheckInDAO memberCheckInDAO;

	@Override
	public void insertMemberCheckIn(MemberCheckInDTO memberCheckInDTO) {
		memberCheckInDAO.insertMemberCheckIn(memberCheckInDTO);
	}

	@Override
	public MemberProfile getMemberProfile(int memberId, int clientId) {
		return memberProfileDAO.getMemberProfile(memberId, clientId);

	}

	@Override
	public void updateMemberProfile(MemberProfile memberProfile, String memberId, int clientId) {
		memberProfileDAO.updateMemberProfile(memberProfile, memberId, clientId);

	}

	@Override
	public void insertMemberProfile(MemberProfile memberProfile, int clientId) {
		memberProfileDAO.insertMemberProfile(memberProfile, clientId);
	}

	@Override
	public MemberAnalytics getMemberAnalytics(int memberId, int clientId) {
		return memberAnalyticsDAO.getMemberAnalytics(memberId, clientId);
	}

}
