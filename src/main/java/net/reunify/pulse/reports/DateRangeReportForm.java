/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;
import org.apache.log4j.Logger;

/**
 * Form that can be used for reports that only require a date range
 * 
 * @author Vivek Hungund
 *
 */
public class DateRangeReportForm {
	private static final Logger logger = Logger.getLogger(DateRangeReportForm.class);
	
	protected String startDate;
	
	protected String endDate;

	/**
	 * @param startDate
	 * @param endDate
	 */
	public DateRangeReportForm(String startDate, String endDate) {
		this.startDate = startDate;
		this.endDate = endDate;
	}

	/**
	 * 
	 */
	public DateRangeReportForm() {
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
