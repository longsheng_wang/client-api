<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<ui:loginview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Pulse - Change Password">
	<jsp:attribute name="headTag">
		<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/login/registration.css"
			rel="stylesheet">
	</jsp:attribute>
	<jsp:body>
	
<div class="wrapper">

<form:form method="POST"
				action="${pageContext.servletContext.contextPath}/user/changepassword"
				commandName="user">
             <table>
                    <tr>
                          <td><form:label path="oldPassword">Old Password</form:label></td>
                    <td><form:password path="oldPassword" /></td>
                         <td class="error"><form:errors
								path="oldPassword" element="div" /></td>
                    
                </tr>
                 <tr>
                          <td><form:label path="password">New Password</form:label></td>
                    <td><form:password path="password" /></td>
                         <td class="error"><form:errors
								path="password" element="div" /></td>
                    
                </tr>
                 <tr>
                    <td><form:label path="matchingPassword">Confirm Password</form:label></td>
                    <td><form:password path="matchingPassword" /></td>
                         <td class="error"><form:errors
								path="matchingPassword" element="div" /></td>
                    
                </tr>
  				<form:hidden path="userId" value="${userId}" />
 
                <tr>
                    <td><input type="submit" value="Submit" /></td>
                </tr>
            </table>
        </form:form>
  </div>
	 
	</jsp:body>
</ui:loginview>