/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members.interception;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import net.reunify.pulse.base.InvalidInputFormat;
import net.reunify.pulse.base.RecordNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 * JDBC implementation of MemberInterceptionDAO
 * 
 * @author kiana.baradaran
 * 
 */

@Component
public class JdbcMemberInterceptionDAO implements MemberInterceptionDAO {

	private static final Logger logger = LoggerFactory.getLogger(JdbcMemberInterceptionDAO.class);

	@Inject
	@Named("pulseJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<MemberIntercept> getLatestMemberIntercept(String memberId) {
		List<MemberIntercept> interceptions = new ArrayList<MemberIntercept>();
		try {
			String selectMemberInterception =
					"select intercept_id,member_id,interceptor_id,member_intercepts.location_id,intercept_note,intercept_status,to_char(intercept_time at time zone location_timezone,'yyyy-mm-dd hh24:mi:ss')::text as intercept_time,member_attrition_score_at_time from member_intercepts join client_locations on member_intercepts.location_id=client_locations.location_id  where member_id = ?::int order by intercept_time desc limit 1;";

			List<Map<String, Object>> rows =
					this.jdbcTemplate.queryForList(selectMemberInterception, new Object[] { memberId });

			for (Map<String, Object> row : rows) {

				MemberIntercept interception =
						new MemberIntercept((int) row.get("intercept_id"), (String) row.get("intercept_time"),
											(int) row.get("intercept_status"), (int) row.get("location_id"),
											(int) row.get("interceptor_id"), (int) row.get("member_id"),
											(String) row.get("intercept_note"),
											(int) row.get("member_attrition_score_at_time"));

				interceptions.add(interception);
			}

		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat("No member with id " + memberId
					+ " was found. Please check the input data and try again. StackTrace: " + e.getMessage());
		}
		return interceptions;
	}

	@Override
	public List<MemberIntercept> getMemberInterceptsByEmployeeId(String employeeId, Timestamp from, Timestamp to) {
		List<MemberIntercept> interceptions = new ArrayList<MemberIntercept>();

		String selectEmployeeInterception =
				"select intercept_id,member_id,interceptor_id,member_intercepts.location_id,intercept_note,intercept_status,to_char(intercept_time at time zone location_timezone,'yyyy-mm-dd hh24:mi:ss')::text as intercept_time,member_attrition_score_at_time from member_intercepts join client_locations on member_intercepts.location_id=client_locations.location_id  where interceptor_id = ?::int and intercept_time at time zone location_timezone between ?::timestamp and ?::timestamp ";
		try {
			List<Map<String, Object>> rows =
					this.jdbcTemplate.queryForList(selectEmployeeInterception, new Object[] { employeeId, from, to });

			for (Map<String, Object> row : rows) {

				MemberIntercept interception =
						new MemberIntercept((int) row.get("intercept_id"), (String) row.get("intercept_time"),
											(int) row.get("intercept_status"), (int) row.get("location_id"),
											(int) row.get("interceptor_id"), (int) row.get("member_id"),
											(String) row.get("intercept_note"),
											(int) row.get("member_attrition_score_at_time"));

				interceptions.add(interception);

			}

		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat("No employee with id " + employeeId
					+ " was found. Please check the input data and try again. StackTrace: " + e.getMessage());
		}
		return interceptions;
	}

	@Override
	public List<MemberIntercept> getMemberInterceptsByLocationId(String locationId, Timestamp from, Timestamp to) {
		List<MemberIntercept> interceptions = new ArrayList<MemberIntercept>();

		String selectLocationInterception =
				"select intercept_id,member_id,interceptor_id,member_intercepts.location_id,intercept_note,intercept_status,to_char(intercept_time at time zone location_timezone,'yyyy-mm-dd hh24:mi:ss')::text as intercept_time,member_attrition_score_at_time from member_intercepts join client_locations on member_intercepts.location_id=client_locations.location_id  where member_intercepts.location_id = ?::int and intercept_time at time zone location_timezone between ?::timestamp and ?::timestamp ";
		try {
			List<Map<String, Object>> rows =
					this.jdbcTemplate.queryForList(selectLocationInterception, new Object[] { locationId, from, to });

			for (Map<String, Object> row : rows) {

				MemberIntercept interception =
						new MemberIntercept((int) row.get("intercept_id"), (String) row.get("intercept_time"),
											(int) row.get("intercept_status"), (int) row.get("location_id"),
											(int) row.get("interceptor_id"), (int) row.get("member_id"),
											(String) row.get("intercept_note"),
											(int) row.get("member_attrition_score_at_time"));

				interceptions.add(interception);
			}

		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat("No location with id " + locationId
					+ " was found. Please check the input data and try again. StackTrace: " + e.getMessage());
		}
		return interceptions;
	}

	@Override
	public MemberIntercept getInterceptDetails(int interceptId) {
		String selectIntercept =
				"select intercept_id,member_id,interceptor_id,member_intercepts.location_id,intercept_note,intercept_status,to_char(intercept_time at time zone location_timezone,'yyyy-mm-dd hh24:mi:ss')::text as intercept_time,member_attrition_score_at_time from member_intercepts join client_locations on member_intercepts.location_id=client_locations.location_id  where intercept_id=?";
		MemberIntercept intercept = null;
		try {
			intercept =
					this.jdbcTemplate.queryForObject(selectIntercept, new Object[] { interceptId },
							new RowMapper<MemberIntercept>() {
								public MemberIntercept mapRow(ResultSet rs, int rowNum) throws SQLException {
									MemberIntercept intercept =
											new MemberIntercept(rs.getInt("intercept_id"),
																rs.getString("intercept_time"),
																rs.getInt("intercept_status"),
																rs.getInt("location_id"), rs.getInt("interceptor_id"),
																rs.getInt("member_id"), rs.getString("intercept_note"),
																rs.getInt("member_attrition_score_at_time"));

									return intercept;
								}
							});
		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat("No intercept with id " + interceptId
					+ " was found. Please check the input data and try again. StackTrace: " + e.getMessage());
		} catch (EmptyResultDataAccessException e) {
			throw new RecordNotFoundException("No intercept with id " + interceptId
					+ " was found. Please check the input data and try again. StackTrace: " + e.getMessage());
		}
		return intercept;
	}

	@Override
	public int insertMemberIntercept(MemberIntercept intercept) {
		int insertedRowId = 0;
		String insertIntercept =
				"insert into member_intercepts(member_id,interceptor_id,location_id,intercept_note,intercept_status,member_attrition_score_at_time) values (?,?,?,?,?,?) returning intercept_id;";

		String insertInterceptAfterItsDone =
				"insert into member_intercepts(member_id,interceptor_id,location_id,intercept_note,intercept_status,member_attrition_score_at_time,intercept_duration) values (?,?,?,?,?,?,?) returning intercept_id;";
		if (intercept.getInterceptDuration() > 0) {
			insertedRowId =
					jdbcTemplate.queryForObject(
							insertInterceptAfterItsDone,
							new Object[] { intercept.getMemberId(), intercept.getInterceptorId(),
									intercept.getLocationId(), intercept.getInterceptNote(),
									intercept.getInterceptStatus(), intercept.getAttritionScore(),
									intercept.getInterceptDuration() }, new RowMapper<Integer>() {
								public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
									return rs.getInt(1);
								}
							});
		} else {

			insertedRowId =
					jdbcTemplate.queryForObject(
							insertIntercept,
							new Object[] { intercept.getMemberId(), intercept.getInterceptorId(),
									intercept.getLocationId(), intercept.getInterceptNote(),
									intercept.getInterceptStatus(), intercept.getAttritionScore() },
							new RowMapper<Integer>() {
								public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
									return rs.getInt(1);

								}
							});
		}
		if (insertedRowId > 0) {
			logger.info("Inserting intercept: " + intercept + " was successful");
		} else {
			logger.error("Inserting intercept: " + intercept + " failed");
		}
		return insertedRowId;
	}

	@Override
	public int enqueueMemberIntercept(InterceptQueueEntry interceptQueueEntry) {
		String insertSql =
				"INSERT INTO member_intercepts_queue (member_id, interceptor_id,comment,reminder_time,location_id) VALUES (?,?,?,statement_timestamp()+?::interval,?) returning record_id;";

		int recordId = 0;
		try {
			recordId =
					this.jdbcTemplate.queryForObject(insertSql, new Object[] { interceptQueueEntry.getMemberId(),
							interceptQueueEntry.getInterceptorId(), interceptQueueEntry.getComment(),
							interceptQueueEntry.getRemindInInterval(), interceptQueueEntry.getLocationId() },
							new RowMapper<Integer>() {
								public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
									return rs.getInt(1);
								}

							});

			if (recordId > 0) {
				logger.info("Enqueuing intercept was successful");
			} else {
				logger.error("Enqueuing intercept failed");
			}
		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat(
											"The input is not valid. Please check the input data and try again. StackTrace: "
													+ e.getMessage());
		}

		return recordId;
	}

	@Override
	public boolean removeMemberInterceptFromQueue(int recordId) {
		String deleteSql = "UPDATE member_intercepts_queue SET is_deleted = true WHERE record_id = ?";
		boolean success = false;
		try {
			int out = jdbcTemplate.update(deleteSql, new Object[] { recordId });
			if (out != 0) {
				success = true;
				logger.info("Removing intercept: " + recordId + " from queue was successful.");
			} else {
				logger.error("Removing intercept: " + recordId + " from queue failed.");
			}
		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat(
											"The input is not valid. Please check the input data and try again. StackTrace: "
													+ e.getMessage());
		}
		return success;
	}

	@Override
	public List<InterceptQueueEntry> getMemberInterceptQueue(int userId) {
		List<InterceptQueueEntry> entries = new ArrayList<InterceptQueueEntry>();
		String deleteSql =
				"SELECT record_id,member_id,interceptor_id,(reminder_time at time zone location_timezone)::text as reminder_time,comment FROM member_intercepts_queue join client_locations on member_intercepts_queue.location_id = client_locations.location_id  where interceptor_id = ? and is_deleted = false and date_trunc('day',reminder_time at time zone location_timezone) = date_trunc('day',statement_timestamp() at time zone location_timezone);";
		List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(deleteSql, new Object[] { userId });

		for (Map<String, Object> row : rows) {

			InterceptQueueEntry entry =
					new InterceptQueueEntry((int) row.get("record_id"), (int) row.get("member_id"),
											(int) row.get("interceptor_id"), (String) row.get("comment"),
											(String) row.get("reminder_time"));

			entries.add(entry);
		}

		return entries;
	}

	@Override
	public boolean updateUserStatsWithQueueInsert(int interceptorId, int locationId) {
		boolean success = false;
		int updatedRows = 0;
		int insertedRows = 0;
		String updateSQL =
				"WITH timezone AS ("
						+ "SELECT location_timezone "
						+ "FROM client_locations "
						+ "WHERE location_id = ?"
						+ ")"
						+ "UPDATE users_daily_stats "
						+ "SET added_to_queue = added_to_queue + 1 "
						+ "WHERE user_id = ? "
						+ "AND report_date = (statement_timestamp() at TIME zone(SELECT location_timezone FROM timezone))::DATE";
		String insertSQL =
				"INSERT INTO users_daily_stats (" + "user_id" + ",report_date" + ",added_to_queue) "
						+ "SELECT ?,(statement_timestamp() at TIME zone location_timezone)::DATE,1 "
						+ "FROM client_locations WHERE location_id = ?;";

		updatedRows = jdbcTemplate.update(updateSQL, new Object[] { locationId, interceptorId });

		if (updatedRows == 0) {
			insertedRows = jdbcTemplate.update(insertSQL, new Object[] { interceptorId, locationId });
		}

		if (updatedRows > 0 || insertedRows > 0) {
			success = true;
			logger.info("Updating the record for location " + locationId + " and user " + interceptorId
					+ " was successful.");
		} else {
			logger.error("Updating the record for location " + locationId + " and user " + interceptorId + " failed.");
		}
		return success;

	}

	@Override
	public boolean updateUserStatsWithQueueDelete(int recordId) {
		boolean success = false;
		int updatedRows = 0;
		int insertedRows = 0;

		String updateSQL =
				"WITH intercept_info AS ("
						+ "SELECT interceptor_id"
						+ ",location_timezone "
						+ "FROM member_intercepts_queue "
						+ "INNER JOIN client_locations ON member_intercepts_queue.location_id = client_locations.location_id "
						+ "WHERE member_intercepts_queue.record_id = ? "
						+ ") "
						+ "UPDATE users_daily_stats "
						+ "SET removed_from_queue = removed_from_queue + 1 "
						+ "WHERE user_id = ("
						+ "SELECT interceptor_id "
						+ "FROM intercept_info "
						+ ") AND report_date = (statement_timestamp() at TIME zone(SELECT location_timezone FROM intercept_info))::DATE;";

		updatedRows = jdbcTemplate.update(updateSQL, new Object[] { recordId });

		String insertSQL =
				"WITH intercept_info AS ("
						+ "SELECT interceptor_id"
						+ ",location_timezone "
						+ "FROM member_intercepts_queue "
						+ "INNER JOIN client_locations ON member_intercepts_queue.location_id = client_locations.location_id "
						+ "WHERE member_intercepts_queue.record_id = ? "
						+ ") "
						+ "INSERT INTO users_daily_stats ( user_id ,report_date ,removed_from_queue ) SELECT interceptor_id"
						+ ",(statement_timestamp() at TIME zone location_timezone)::DATE,1 FROM intercept_info ";

		if (updatedRows == 0) {
			insertedRows = jdbcTemplate.update(insertSQL, new Object[] { recordId });
		}

		if (updatedRows > 0 || insertedRows > 0) {
			success = true;
			logger.info("Updating the record with updateUserStatsWithQueueDelete for record id " + recordId
					+ " was successful.");
		} else {
			logger.error("Updating the record with updateUserStatsWithQueueDelete for record id " + recordId
					+ " failed.");
		}
		return success;

	}

	@Override
	public boolean updateUserStatsWithIntercept(int interceptorId, int locationId) {
		boolean success = false;
		int updatedRows = 0;
		int insertedRows = 0;
		String updateSQL =
				"WITH timezone AS ("
						+ "SELECT location_timezone "
						+ "FROM client_locations "
						+ "WHERE location_id = ?"
						+ ") UPDATE users_daily_stats "
						+ "SET intercepts_done = intercepts_done + 1 "
						+ "WHERE user_id = ? "
						+ "AND report_date = (statement_timestamp() at TIME zone(SELECT location_timezone FROM timezone))::DATE";

		String insertSQL =
				"INSERT INTO users_daily_stats ( user_id ,report_date ,intercepts_done) "
						+ "SELECT ?,(statement_timestamp() at TIME zone location_timezone)::DATE,1 FROM client_locations WHERE location_id = ?;";

		updatedRows = jdbcTemplate.update(updateSQL, new Object[] { locationId, interceptorId });

		if (updatedRows == 0) {
			insertedRows = jdbcTemplate.update(insertSQL, new Object[] { interceptorId, locationId });
		}
		if (updatedRows > 0 || insertedRows > 0) {
			success = true;
			logger.info("Updating the record with updateUserStatsWithIntercept for location " + locationId
					+ " and user " + interceptorId + " was successful.");
		} else {
			logger.error("Updating the record with updateUserStatsWithIntercept for location " + locationId
					+ " and user " + interceptorId + " failed.");
		}
		return success;
	}

	@Override
	public void endMemberIntercept(MemberIntercept intercept, int clientId) {
		int updatedRows = 0;
		String endIntercept =
				"UPDATE member_intercepts SET intercept_end_time = statement_timestamp() , intercept_duration = EXTRACT(EPOCH FROM (statement_timestamp() - intercept_time)) WHERE intercept_id =? and "
						+ "EXISTS (SELECT 1 FROM client_locations WHERE location_id = ? and client_id = ?); ";
		updatedRows =
				jdbcTemplate.update(endIntercept, new Object[] { intercept.getInterceptId(), intercept.getLocationId(),
						clientId });
		if (updatedRows > 0) {
			logger.info("Updating the intercept " + intercept + " was successful.");

		} else {
			logger.error("Updating the intercept " + intercept + " failed.");
		}

	}
}
