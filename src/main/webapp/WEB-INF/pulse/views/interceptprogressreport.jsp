<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Life Time Intercept/Survey Progress Report</title>
<link
	href="${pageContext.servletContext.contextPath}/static/pulse/css/error/error.css"
	rel="stylesheet">
</head>
<body>

	<c:set var="startDate" value="${param.startDate == null ? '' : param.startDate}" />
	<c:set var="endDate" value="${param.endDate == null ? '' : param.endDate}" />

	<strong>Intercept/Survey Progress Report</strong>
	<form:form method="POST"
		action="${pageContext.servletContext.contextPath}/reports/interceptprogressreport" commandName="DateRangeReportForm">
		<table>
			<tr>
				<td>
					<form:label path="startDate">Start Date (e.g. 2015-01-15)</form:label>
				</td>
				<td>
					<form:input type="text" name="startDate" value="${startDate}" path="startDate" />
				</td>
				<td class="error">
					<form:errors path="startDate" element="div" cssClass="error" />
				</td>
			</tr>
			
			<tr>
				<td>
					<form:label path="endDate">End Date (e.g. 2015-02-01)</form:label>
				</td>
				<td>
					<form:input type="text" name="endDate" value="${endDate}" path="endDate" />
				</td>
				<td class="error">
					<form:errors path="endDate" element="div" cssClass="error" />
				</td>
			</tr>
			
			<tr>
				<td><input type="submit" value="Get Report" name="dateRangeReport"></td>
			</tr>
		</table>
	</form:form> <br />
	<strong>OR</strong> click below for today's report<br />
	<form method="POST"
		action="${pageContext.servletContext.contextPath}/reports/interceptprogressreport">
	<table>
			<tr>
				<td><input type="submit" value="Get Today's Report" name="dailyReport"></td>
			</tr>
	</table>
	</form>
	
	<c:if test="${pageContext.request.method=='POST'}">
		<div>
		<c:if test="${report.getReportData().size() > 0 }">
			<table style="display:inline-block;padding:0;margin:0;border:0;border-collapse:collapse">
				<thead>
				<tr>
					<th style="width:105px;text-align:center;font-weight:bolder;font-size:larger;font-family:Helvetica Neue,sans-serif;border:1px solid #99ca3c;color:white;background-color:#99ca3c" scope="col"><span style="font-family:Helvetica Neue,sans-serif;margin-left:5px;font-size:larger">Intercepts</span></th>
					<c:if test="${reportType == 'daily' }">						
						<th style="height:50px;width:65px;font-weight:bolder;text-align:center;font-size:larger;font-family:Helvetica Neue,sans-serif;border:1px solid #99ca3c;color:#99ca3c;background-color:white" scope="col">Weekly Goal</th>
					</c:if>
					<c:set value="${report.getReportData()[0]}" var="firstRow" />
					<c:forEach items="${firstRow.getInterceptCount()}" var="date" varStatus="loop">
						<c:choose>
							<c:when test="${loop.last && reportType == 'daily'}">
								<th style="height:50px;width:75px;text-align:center;font-size:larger;font-family:Helvetica Neue,sans-serif;border:1px solid green;color:white;background-color:green" scope="col"><fmt:formatDate value="${date.key}" dateStyle="MEDIUM" /></th>
							</c:when>
							<c:otherwise>
								<th style="height:50px;width:75px;text-align:center;font-size:larger;font-family:Helvetica Neue,sans-serif;border:1px solid #4d4d4d;color:white;background-color:#4d4d4d" scope="col"><fmt:formatDate value="${date.key}" dateStyle="MEDIUM" /></th>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<c:remove var="firstRow" />
					<c:remove var="date" />
					<c:choose>
						<c:when test="${reportType == 'daily' }" >
							<th style="height:50px;font-weight:bolder;width:65px;text-align:center;font-size:larger;font-family:Helvetica Neue,sans-serif;border:1px solid #99ca3c;color:#99ca3c;background-color:white" scope="col">Distance To Goal</th>
						</c:when>
						<c:otherwise>
							<th style="height:50px;font-weight:bolder;width:65px;text-align:center;font-size:larger;font-family:Helvetica Neue,sans-serif;border:1px solid #99ca3c;color:#99ca3c;background-color:white" scope="col">Total</th>							
						</c:otherwise>
					</c:choose>
				</tr>
				</thead>
				<tbody>
					<c:forEach items="${report.getReportData()}" var="row">
						<tr>
							<th style="height:30px;text-align:center;border:solid 1px #4d4d4d;width:175px;color:white;padding-right:5px;font-weight:bolder;font-size:larger;font-family:Helvetica Neue,sans-serif;background-color:#4d4d4d" scope="row"><span style="margin-left:5px">${row.getLocationName()}</span></th>
							<c:choose>
								<c:when test="${reportType == 'daily' }" >
									<td style="text-align:center;border:1px solid #99ca3c;height:30px;color:#99ca3c;font-family:Helvetica Neue,sans-serif;font-weight:bolder;font-size:larger;background-color:white">${row.getWeeklyInterceptGoal()}</td>
								</c:when>
								<c:otherwise>
									<c:set var="total" value="0" />
								</c:otherwise>
							</c:choose>
							<c:forEach items="${row.getInterceptCount()}" var="intercepts" varStatus="loop">
								<c:choose>
									<c:when test="${loop.last && reportType == 'daily'}">
										<td style="text-align:center;height:30px;border:1px solid #99ca3c;color:green;font-weight:bolder;font-family:Helvetica Neue,sans-serif;font-size:larger;background-color:white">${intercepts.value }</td>
									</c:when>
									<c:otherwise>
										<td style="text-align:center;height:30px;border:1px solid #99ca3c;font-family:Helvetica Neue,sans-serif;font-size:larger;background-color:white">${intercepts.value}</td>
										<c:set var="total" value="${total + intercepts.value}" />
									</c:otherwise>
								</c:choose>
							</c:forEach>
							<c:choose>
								<c:when test="${reportType == 'daily' }" >
									<c:choose>
										<c:when test="${row.getRemainingIntercepts() < 0 }" >
											<td style="text-align:center;height:30px;border:1px solid #99ca3c;font-weight:bold;color:#99ca3c;font-size:larger;font-family:Helvetica Neue,sans-serif;background-color:white">+${0 - row.getRemainingIntercepts()} ✓</td>
										</c:when>
										<c:otherwise>
											<td style="text-align:center;height:30px;border:1px solid #99ca3c;font-weight:bold;color:#99ca3c;font-size:larger;font-family:Helvetica Neue,sans-serif;background-color:white">${row.getRemainingIntercepts()} </td>
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<td style="text-align:center;height:30px;border:1px solid #99ca3c;font-weight:bold;color:#99ca3c;font-size:larger;font-family:Helvetica Neue,sans-serif;background-color:white">${total}</td>
								</c:otherwise>
							</c:choose>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
		<div style="min-height:30px"></div>
		<c:if test="${report.getReportData().size() > 0}">
			<table style="display:inline-block;padding:0;margin:0;border:0;border-collapse:collapse" class="data" border="1">
				<thead>
					<tr>
						<th style="width:105px;text-align:center;font-weight:bolder;font-size:larger;font-family:Helvetica Neue,sans-serif;border:1px solid #99ca3c;color:white;background-color:#99ca3c" scope="col"><span style="font-family:Helvetica Neue,sans-serif;margin-left:5px;font-size:larger">Surveys</span></th>
						<c:if test="${reportType == 'daily' }" >
							<th style="height:50px;width:65px;font-weight:bolder;text-align:center;font-size:larger;font-family:Helvetica Neue,sans-serif;border:1px solid #99ca3c;color:#99ca3c;background-color:white" scope="col">Weekly Goal</th>
						</c:if>
						<c:set value="${report.getReportData()[0] }" var="firstRow" />
						<c:forEach items="${firstRow.getSurveyCount()}" var="date" varStatus="loop">
							<c:choose>
								<c:when test="${loop.last && reportType == 'daily'}">
									<th style="height:50px;width:75px;text-align:center;font-size:larger;font-family:Helvetica Neue,sans-serif;border:1px solid green;color:white;background-color:green"><fmt:formatDate value="${date.key }" dateStyle="MEDIUM" /></th>
								</c:when>
								<c:otherwise>
									<th style="height:50px;width:75px;text-align:center;font-size:larger;font-family:Helvetica Neue,sans-serif;border:1px solid #4d4d4d;color:white;background-color:#4d4d4d" scope="col"><fmt:formatDate value="${date.key }" dateStyle="MEDIUM" /></th>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<c:remove var="firstRow" />
						<c:remove var="date" />
						<c:choose>
							<c:when test="${reportType == 'daily' }" >
								<th style="height:50px;font-weight:bolder;width:65px;text-align:center;font-size:larger;font-family:Helvetica Neue,sans-serif;border:1px solid #99ca3c;color:#99ca3c;background-color:white" scope="col">Distance To Goal</th>
							</c:when>
							<c:otherwise>
								<th style="height:50px;font-weight:bolder;width:65px;text-align:center;font-size:larger;font-family:Helvetica Neue,sans-serif;border:1px solid #99ca3c;color:#99ca3c;background-color:white" scope="col">Total</th>
							</c:otherwise>
						</c:choose>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${report.getReportData() }" var="row">
						<c:if test="${row.hasKiosk()}">
							<tr>
								<th style="height:30px;border:solid 1px #4d4d4d;text-align:center;color:white;font-weight:bolder;padding-right:5px;width:175px;font-size:larger;font-family:Helvetica Neue,sans-serif;background-color:#4d4d4d" scope="row"><span style="margin-left:5px">${row.getLocationName()}</span></th>
								<c:choose>
									<c:when test="${reportType == 'daily' }" >
										<td style="height:30px;color:#99ca3c;border:1px solid #99ca3c;font-weight:bolder;font-size:larger;font-size:larger;font-family:Helvetica Neue,sans-serif;text-align:center;background-color:white">${row.getWeeklySurveyGoal() } </td>
									</c:when>
									<c:otherwise>
										<c:set var="total" value="0" />
									</c:otherwise>
								</c:choose>
								<c:forEach items="${row.getSurveyCount() }" var="surveys" varStatus="loop">
									<c:choose>
										<c:when test="${loop.last && reportType == 'daily'}">
											<td style="text-align:center;height:30px;border:1px solid #99ca3c;color:green;font-weight:bolder;font-size:larger;font-family:Helvetica Neue,sans-serif;background-color:white">${surveys.value}</td>
										</c:when>
										<c:otherwise>
											<td style="text-align:center;height:30px;border:1px solid #99ca3c;font-size:larger;font-family:Helvetica Neue,sans-serif;background-color:white">${surveys.value}</td>
											<c:set var="total" value="${total + surveys.value}" />
										</c:otherwise>
									</c:choose>
								</c:forEach>
								<c:choose>
									<c:when test="${reportType == 'daily' }" >
										<c:choose>
											<c:when test="${row.getRemainingSurveys() < 0 }" >
												<td style="text-align:center;height:30px;border:1px solid #99ca3c;font-weight:bold;color:#99ca3c;font-size:larger;font-family:Helvetica Neue,sans-serif;background-color:white">+${0 - row.getRemainingSurveys()} ✓</td>
											</c:when>
											<c:otherwise>
												<td style="text-align:center;height:30px;border:1px solid #99ca3c;font-weight:bold;color:#99ca3c;font-size:larger;font-family:Helvetica Neue,sans-serif;background-color:white">${row.getRemainingSurveys()} </td>
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<td style="text-align:center;height:30px;border:1px solid #99ca3c;font-weight:bold;color:#99ca3c;font-size:larger;font-family:Helvetica Neue,sans-serif;background-color:white">${total}</td>
									</c:otherwise>
								</c:choose>
							</tr>
						</c:if>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
		</div>
	</c:if>
</body>
</html>