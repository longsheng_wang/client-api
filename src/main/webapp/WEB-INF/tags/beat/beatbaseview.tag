<%@ tag language="java" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@attribute name="contextPath" required="true"%>
<%@attribute name="pageTitle" required="true"%>
<%@attribute name="pageName" required="true"%>
<%@attribute name="headTag" fragment="true"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style"
	content="black-translucent" />
<!-- start: Meta -->
<meta charset="utf-8">
<!-- end: Meta -->

<!-- start: Mobile Specific -->
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<meta name="format-detection" content="telephone=no">
<!-- end: Mobile Specific -->

<!-- google fonts:start-->
<link
	href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,300italic'
	rel='stylesheet' type='text/css'>
<!-- google fonts: ends -->
<!-- start: Widget Icons -->
<link rel="apple-touch-icon"
	href="${contextPath}img/icons/apple-touch-icon-iphone.png" />
<link rel="apple-touch-icon" sizes="72x72"
	href="${contextPath}img/icons/apple-touch-icon-ipad.png" />
<link rel="apple-touch-icon" sizes="114x114"
	href="${contextPath}img/icons/apple-touch-icon-iphone4.png" />
<link rel="apple-touch-icon" sizes="144x144"
	href="${contextPath}img/icons/apple-touch-icon-ipad3.png" />
<!-- end: Widget Icons -->

<!-- start: CSS -->
<link href="${contextPath}layout/css/global/trace.css" rel="stylesheet">
<link type="text/css"
	href="${contextPath}layout/css/jquery/jquery.mobile-1.3.2.min.css"
	rel="stylesheet" />
<link type="text/css"
	href="${contextPath}layout/css/tables/jquery.dataTables.mod.min.css"
	rel="stylesheet" />
<link href="${contextPath}layout/css/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${contextPath}layout/css/bootstrap/css/font-awesome.min.css"
	rel="stylesheet">
<link href="${contextPath}layout/css/bootstrap/css/style.min.css"
	rel="stylesheet">

<link href="layout/css/homepage/homepage-2.0.css" rel="stylesheet">

<!-- Aarti's CSS -->
<link href="css/presentation.css" rel="stylesheet">
<link href="css/beat_homepage.css" rel="stylesheet">
<link href="css/charts.css" rel="stylesheet">
<!-- end CSS -->

<!-- start: Scripts -->
<script src="${contextPath}js/jquery/jquery-1.11.0.min.js"
	type="text/javascript"></script>
<script src="${contextPath}js/jquery/jquery.mobile-1.3.2.min.js"
	type="text/javascript"></script>
<script src="${contextPath}js/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="${contextPath}js/jquery/tables/jquery.dataTables.min.js"
	type="text/javascript"></script>
<script src="${contextPath}js/bootstrap/bootstrap.min.js"></script>

<!--Aarti's added jquery for charts testing -->
<script src="js/animatedpiechart.js"></script>

<jsp:invoke fragment="headTag" />

<title>${pageTitle}</title>
</head>
<body>
	<!--main container-->
	<div id="homepage" data-role="page"
		style="overflow: hidden; overflow-y: hidden; overflow-x: hidden;">
		<div class="row container-fluid container-fluid-ext">
			<!--topbar - starts-->

			<div class="row navbar topnav-container" role="navigation">
				<div class="row col-lg-12"
					style="background-color: #d65c35; height: 10px; margin: 0px"></div>
				<div class="row col-lg-5 topnav"></div>

				<div class="row col-lg-2 beat-brand">
					<p id="brand-text">beat</p>
				</div>

				<div class="row col-lg-5 topnav">
					<div class="row col-lg-6"></div>
					<div class="row col-lg-4">
						<p id="top-nav-name">LoggedIn Name</p>
					</div>
					<div class="row col-lg-2" style="padding-top: 5px">
						<img src="layout/img/navbar/RS_LogOut@2.0x.png" width="30px"
							height="30px" align="right" vertical-align="middle" />
					</div>
				</div>
			</div>
			<!--topbar - ends-->
			<!--timebar-->
			<div class="row navbar timebar-ext" role="navigation">
				<div class="row col-lg-4 timebar "></div>

				<div class="row col-lg-4 timebar">
					<!--buttons-->
					<div class="row col-lg-12 btn-row">

						<button type="button" class="btn btn-default btn-lg btn-timeline"
							onclick="window.location.href='${pageName}?time=today'">Td</button>
						<button type="button" class="btn btn-default btn-lg btn-timeline"
							onclick="window.location.href='${pageName}?time=yesterday'">Yd</button>
						<button type="button" class="btn btn-default btn-lg btn-timeline"
							onclick="window.location.href='${pageName}?time=week'">W</button>
						<button type="button" class="btn btn-default btn-lg btn-timeline"
							onclick="window.location.href='${pageName}?time=month'">M</button>
						<button type="button" class="btn btn-default btn-lg btn-timeline"
							onclick="window.location.href='${pageName}?time=year'">Y</button>

					</div>
					<!--buttons ends-->
				</div>
				<div class="row col-lg-4 timebar"></div>
			</div>
			<!--timebar       nav-stacked-->
		</div>
		<div class="container-fluid" style="padding-left: 0px;">
			<div class="row col-md-offset-1 col-sm-offset-1">
				<!--sidebar -start-->
				<div class="sidebar" role="complementary">
					<ul class="nav  nav-pills  col-md-12 col-sm-12">
						<c:choose>
							<c:when test="${pageName eq 'homePage'}">
								<li class="active">
							</c:when>
							<c:otherwise>
								<li>
							</c:otherwise>
						</c:choose>
						<a onclick="location.href='test_homepage.html'"><img
							height="75">HOME</a>
						</li>
						<c:choose>
							<c:when test="${pageName eq 'pulsePageView'}">
								<li class="active">
							</c:when>
							<c:otherwise>
								<li>
							</c:otherwise>
						</c:choose>
						<a onclick="location.href='pulsePageView'"><img height="75">PULSE</a>
						</li>
						<c:choose>
							<c:when test="${pageName eq 'whatPageView'}">
								<li class="active">
							</c:when>
							<c:otherwise>
								<li>
							</c:otherwise>
						</c:choose>
						<a onclick="location.href='whatPageView'"><img height="75">WHAT</a>
						</li>
						<c:choose>
							<c:when test="${pageName eq 'whoPageView'}">
								<li class="active">
							</c:when>
							<c:otherwise>
								<li>
							</c:otherwise>
						</c:choose>
						<a onclick="location.href='Beat_page3.html'"><img alt=""
							height="75">WHO</a>
						</li>
						<c:choose>
							<c:when test="${pageName eq 'wherePageView'}">
								<li class="active">
							</c:when>
							<c:otherwise>
								<li>
							</c:otherwise>
						</c:choose>
						<a onclick="location.href='wherePageView.html'"><img alt=""
							height="75">WHERE</a>
						</li>
						<c:choose>
							<c:when test="${pageName eq 'whenPageView'}">
								<li class="active">
							</c:when>
							<c:otherwise>
								<li>
							</c:otherwise>
						</c:choose>
						<a onclick="location.href='Beat_page5.html'"><img alt=""
							height="75">WHEN</a>
						</li>
						<c:choose>
							<c:when test="${pageName eq 'whichPageView'}">
								<li class="active">
							</c:when>
							<c:otherwise>
								<li>
							</c:otherwise>
						</c:choose>
						<a onclick="location.href='whichPageView'"><img alt=""
							height="75">WHICH</a>
						</li>
					</ul>
				</div>
				<!--sidebar -ends-->
				<jsp:doBody />
			</div>
		</div>
	</div>
	<!--main container-->
</body>
</html>
