/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/spring-security.xml",
		"file:src/test/webapp/WEB-INF/mvc-dispatcher-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class ClientRegistrationValidatorTest {

	@Test
	public void testClientRegisterationValidatorForInvalidIndustryName() {
		ClientDTO client = new ClientDTO();
		client.setIndustryName("NONE");
		Errors errors = new BeanPropertyBindingResult(client, "client");
		ClientRegistrationValidator validator = new ClientRegistrationValidator();
		validator.validateIndustryName(client, errors, "industryName");

		assertTrue(errors.hasErrors());
		assertEquals("industryName_required", errors.getAllErrors().get(0).getCode());
	}

	@Test
	public void testClientRegisterationValidatorForCompanyNameTooLong() {
		ClientDTO client = new ClientDTO();
		client.setCompanyName("test test test test test test test test test test test test test test test test test test test test test");
		Errors errors = new BeanPropertyBindingResult(client, "client");
		ClientRegistrationValidator validator = new ClientRegistrationValidator();
		validator.validateCompanyName(client, errors, "companyName");

		assertTrue(errors.hasErrors());
		assertEquals("incorrect_companyName", errors.getAllErrors().get(0).getCode());
	}

	@Test
	public void testClientRegisterationValidatorForNoCompanyNameProvided() {
		ClientDTO client = new ClientDTO();
		client.setCompanyName("   ");
		Errors errors = new BeanPropertyBindingResult(client, "client");
		ClientRegistrationValidator validator = new ClientRegistrationValidator();
		validator.validateCompanyName(client, errors, "companyName");

		assertTrue(errors.hasErrors());
		assertEquals("companyName_required", errors.getAllErrors().get(0).getCode());
	}
}
