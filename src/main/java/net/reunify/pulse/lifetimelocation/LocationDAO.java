/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.lifetimelocation;
import java.util.Map;

/**
 * Interface for Lifetime Locations
 * @author Vivek Hungund
 *
 */
public interface LocationDAO {

	public Map<String, Location> getPilotClubLocations();
}
