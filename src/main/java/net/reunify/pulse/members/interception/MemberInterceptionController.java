/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members.interception;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import net.reunify.pulse.base.BaseController;
import net.reunify.pulse.base.InvalidSearchCriteriaException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Handles requests for intercept info.
 * 
 * @author kiana.baradaran
 * 
 */

@Controller
public class MemberInterceptionController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(MemberInterceptionController.class);

	@Inject
	@Named("InterceptionService")
	private MemberInterceptionService interceptionServiceHandler;

	/**
	 * 
	 * Searching the intercept records for the given criteria
	 * 
	 * @param searchCriteria
	 * @return
	 */
	@RequestMapping(value = "/memberinterceptionsearch", consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" }, method = RequestMethod.POST)
	@ResponseBody
	public MemberInterceptResponse searchMemberIntercepts(@RequestBody MemberInterceptionSearchCriteria searchCriteria,
			HttpServletRequest request) {
		if (logger.isDebugEnabled()) {
			logger.debug(searchCriteria.toString());
		}
		Assert.notNull(searchCriteria, "searchCriteria cannot be null");
		searchCriteria.setLocationId((String) request.getSession().getAttribute("locationId"));
		if (!searchCriteria.isValid()) {
			throw new InvalidSearchCriteriaException();
		}

		List<MemberIntercept> interceptions = interceptionServiceHandler.searchMemberIntercepts(searchCriteria);
		return new MemberInterceptResponse(interceptions);

	}

	/**
	 * Request for the complete record of the given intercept id
	 * 
	 * @param interceptId
	 * @return MemberIntercept
	 */

	@RequestMapping(value = "/memberintercepts/{interceptId}", method = RequestMethod.GET)
	@ResponseBody
	public MemberIntercept getNoteDetails(@PathVariable int interceptId) {
		if (logger.isDebugEnabled()) {
			logger.info("Requested Mem ID is: " + interceptId);
		}
		Assert.notNull(interceptId, "ID cannot be null");

		MemberIntercept intercept = interceptionServiceHandler.getInterceptDetails(interceptId);

		return intercept;
	}

	/**
	 * Post a request for intercept insertion
	 * 
	 * @param intercept
	 */
	@RequestMapping(value = "/memberintercepts", consumes = { "application/json;charset=UTF-8" }, method = RequestMethod.POST)
	@ResponseBody
	public void initiateMemberIntercept(@RequestBody MemberIntercept intercept) {
		Assert.notNull(intercept, "Note cannot be null");
		interceptionServiceHandler.insertMemberIntercept(intercept);
	}

	/**
	 * Post a request for ending an intercept
	 * 
	 * @param intercept
	 */
	@RequestMapping(value = "/memberintercepts/{interceptId}", consumes = { "application/json;charset=UTF-8" }, method = RequestMethod.PUT)
	@ResponseBody
	public void endMemberIntercept(HttpServletRequest request, @RequestBody MemberIntercept intercept,
			@PathVariable int interceptId) {
		Assert.notNull(intercept, "Intercept cannot be null");
		Assert.notNull(intercept.getInterceptId(), "Intercept ID cannot be null");
		intercept.setInterceptId(interceptId);
		int clientId = super.getClientId(request);
		interceptionServiceHandler.endMemberIntercept(intercept, clientId);
	}

	/**
	 * Post a request for insertion in queue
	 * 
	 * @param intercept
	 */
	@RequestMapping(value = "/interceptqueue", consumes = { "application/json;charset=UTF-8" }, method = RequestMethod.POST)
	@ResponseBody
	public void enqueueMemberIntercept(HttpServletRequest request, @RequestBody InterceptQueueEntry interceptQueueEntry) {
		Assert.notNull(interceptQueueEntry, "Note cannot be null");
		interceptQueueEntry.setLocationId(Integer.parseInt((String) request.getSession().getAttribute("locationId")));
		int recordId = interceptionServiceHandler.enqueueMemberIntercept(interceptQueueEntry);
		if (recordId > 0) {
			logger.info("Enqueuing was successful.");
		} else {
			logger.error("Enqueuing failed.");
		}
	}

	/**
	 * Request for removing an intercept reminder from the queue
	 * 
	 * @param intercept
	 */
	@RequestMapping(value = "/interceptqueue/{recordId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void removeMemberInterceptFromQueue(@PathVariable int recordId) {
		Assert.notNull(recordId, "Record ID cannot be null");
		boolean success = interceptionServiceHandler.removeMemberInterceptFromQueue(recordId);

		if (success) {
			logger.info("Removing from the queue was successful.");
		} else {
			logger.error("Removing from the queue failed.");
		}

	}

	/**
	 * Request for retrieving the queue for a user
	 * 
	 * @param intercept
	 */
	@RequestMapping(value = "/interceptqueue/{userId}", method = RequestMethod.GET)
	@ResponseBody
	public InterceptQueueResponse getMemberInterceptQueue(@PathVariable int userId) {
		Assert.notNull(userId, "Record ID cannot be null");
		return new InterceptQueueResponse(interceptionServiceHandler.getMemberInterceptQueue(userId));
	}

}
