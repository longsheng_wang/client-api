/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.client;

import java.util.List;

import net.reunify.pulse.user.UserDTO;

/**
 * 
 * @author kiana.baradaran
 * 
 */
public class ClientDTO {
	private int clientId;
	private String companyName;
	private String industryName;
	private List<LocationDTO> locations;
	private UserDTO accountAdmin;

	public ClientDTO() {
		// TODO Auto-generated constructor stub
	}

	public ClientDTO(String companyName, String industryName, List<LocationDTO> locations, UserDTO accountAdmin) {
		this.companyName = companyName;
		this.industryName = industryName;
		this.locations = locations;
		this.accountAdmin = accountAdmin;
	}
	
	public ClientDTO(int clientId, String companyName, List<LocationDTO> locations) {
		this.clientId = clientId;
		this.companyName = companyName;
		this.locations = locations;
	}
	
	public ClientDTO(int clientId, String companyName) {
		this.clientId = clientId;
		this.companyName = companyName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getIndustryName() {
		return industryName;
	}

	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

	public List<LocationDTO> getLocations() {
		return locations;
	}

	public void setLocations(List<LocationDTO> locations) {
		this.locations = locations;
	}

	public UserDTO getAccountAdmin() {
		return accountAdmin;
	}

	public void setAccountAdmin(UserDTO accountAdmin) {
		this.accountAdmin = accountAdmin;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
}
