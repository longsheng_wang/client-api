/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.internaltools;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * DAO for accessing info about simulators
 * 
 * @author Vivek Hungund
 *
 */
@Component
public class JdbcSwipeSimulatorDAO implements SwipeSimulatorDAO {
	private static final Logger logger = Logger.getLogger(JdbcSwipeSimulatorDAO.class);

	@Inject
	@Named("pulseJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	/**
	 * Get a list of running simulators
	 */
	@Override
	public List<SwipeSimulator> getRunningSwipeSimulators() {
		logger.info("Getting a list of running simulators");
		String query = "SELECT started_at, t1.client_id, t1.location_id, num_swipes_requested, num_swipes_inserted, client_name, location_name FROM swipe_simulator_status t1 "
				+ "join clients t2 on t1.client_id = t2.client_id join client_locations t3 on t2.client_id = t3.client_id and t1.location_id = t3.location_id WHERE is_running = true order by started_at";
		List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(query);

		List<SwipeSimulator> runningSimulators = new ArrayList<SwipeSimulator>();
		for (Map<String, Object> row : rows) {
			SwipeSimulator s = new SwipeSimulator((Date) row.get("started_at"), (Integer) row.get("client_id"),
										(Integer) row.get("location_id"), (Integer) row.get("num_swipes_requested"),
										(Integer) row.get("num_swipes_inserted"), (String) row.get("client_name"),
										(String) row.get("location_name"));
			runningSimulators.add(s);
		}

		return runningSimulators;
	}
}
