package net.reunify.pulse.user;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.inject.Inject;

import net.reunify.pulse.client.ClientService;
import net.reunify.pulse.client.LocationDTO;
import net.reunify.pulse.test.util.MockDatabaseRecordCreator;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/spring-security.xml",
		"file:src/test/webapp/WEB-INF/mvc-dispatcher-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@TransactionConfiguration(defaultRollback = true)
@Transactional(value = "pulseTxManager")
public class JdbcUserDAOTest {

	@Inject
	private UserDAO userDAO;

	@Inject
	private ClientService clientServiceHandler;

	@Inject
	MockDatabaseRecordCreator mockDatabaseRecordCreator;

	@After
	public void tearDown() throws Exception {
		mockDatabaseRecordCreator.cleanUp();
	}

	@Test
	public void testCreateUserAccount() {
		int testLocationId = mockDatabaseRecordCreator.createTestLocationRecord(0);
		UserDTO user = new UserDTO();
		user.setFirstName("firstNameForTest");
		user.setLastName("lastNameForTest");
		user.setEmailAddress("a@test.com");
		user.setPassword("Testing1");
		user.setMatchingPassword("Testing1");
		user.setLocationId(testLocationId);
		user.setRole(new UserRole(1, "Admin"));
		assertTrue(userDAO.createUserAccount(user) > 0);
	}

	@Test
	public void testRemoveUserAccount() {
		int clientId = mockDatabaseRecordCreator.createTestClientRecord();
		int testLocationId = mockDatabaseRecordCreator.createTestLocationRecord(clientId);
		int userId = mockDatabaseRecordCreator.createTestUserRecord(testLocationId);
		UserDTO user = new UserDTO();
		user.setFirstName("firstNameForTest");
		user.setLastName("lastNameForTest");
		user.setEmailAddress("b@test.com");
		user.setPassword("Testing1");
		user.setMatchingPassword("Testing1");
		user.setRole(new UserRole(1, "Admin"));
		user.setLocationId(testLocationId);
		userDAO.createUserAccount(user);
		assertTrue(userDAO.removeUserAccount(userId, clientId));
	}

	@Test
	public void testRemoveAdminAccount() {
		int clientId = mockDatabaseRecordCreator.createTestClientRecord();
		int testLocationId = mockDatabaseRecordCreator.createTestLocationRecord(clientId);
		int userId = mockDatabaseRecordCreator.createTestUserRecord(testLocationId);
		assertFalse(userDAO.removeUserAccount(userId, clientId));
	}

	@Test
	public void testChangePassword() {
		int clientId = mockDatabaseRecordCreator.createTestClientRecord();
		int testLocationId = mockDatabaseRecordCreator.createTestLocationRecord(clientId);
		int userId = mockDatabaseRecordCreator.createTestUserRecord(testLocationId);
		UserDTO user = new UserDTO();
		user.setOldPassword("Testing1");
		user.setPassword("Testing2");
		user.setMatchingPassword("Testing2");
		user.setUserId(userId);
		assertTrue(userDAO.changePassword(user, clientId));
	}

	@Test
	public void testChangePasswordInvalidClient() {
		int userId = mockDatabaseRecordCreator.createTestUserRecord(0);
		UserDTO user = new UserDTO();
		user.setOldPassword("Testing1");
		user.setPassword("Testing2");
		user.setMatchingPassword("Testing2");
		user.setUserId(userId);
		assertFalse(userDAO.changePassword(user, 0));
	}

	@Test
	public void testEditUserAccount() {
		int clientId = mockDatabaseRecordCreator.createTestClientRecord();
		int testLocationId = mockDatabaseRecordCreator.createTestLocationRecord(clientId);
		int userId = mockDatabaseRecordCreator.createTestUserRecord(testLocationId);
		UserDTO user = new UserDTO();
		user.setFirstName("firstNameForTest");
		user.setLastName("lastNameForTest");
		user.setEmailAddress("a@test.com");
		user.setLocationId(testLocationId);
		user.setUserId(userId);
		user.setRole(new UserRole(1, "Admin"));
		assertTrue(userDAO.editUserAccount(user, clientId));
	}

	@Test
	public void testGetUserAccounts() {
		int clientId = mockDatabaseRecordCreator.createTestClientRecord();
		LocationDTO locationDTO = new LocationDTO();
		locationDTO.setClientId(clientId);
		locationDTO.setName("TestLocationFromReunify");
		locationDTO.setTimeZone("US/PACIFIC");
		int testLocationId = clientServiceHandler.addLocation(locationDTO);
		UserDTO user = new UserDTO();
		user.setFirstName("firstNameForTest");
		user.setLastName("lastNameForTest");
		user.setEmailAddress("a@test.com");
		user.setPassword("Testing1");
		user.setMatchingPassword("Testing1");
		user.setRole(new UserRole(1, "Admin"));
		user.setLocationId(testLocationId);
		userDAO.createUserAccount(user);
		UserDTO user2 = new UserDTO();
		user2.setFirstName("firstNameForTest");
		user2.setLastName("lastNameForTest");
		user2.setEmailAddress("b@test.com");
		user2.setPassword("Testing1");
		user2.setMatchingPassword("Testing1");
		user2.setLocationId(testLocationId);
		user2.setRole(new UserRole(1, "Admin"));
		userDAO.createUserAccount(user2);
		List<UserDTO> users = userDAO.getUserAccounts(clientId);
		assertTrue(users.size() == 2);
	}

}
