/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.analytics.metrics;

import java.util.Date;

/**
 * A metric with an associated time stamp value
 *
 * @author Shakti Shrivastava
 *
 * @param <T>
 */
public interface TimestampMetric<T> extends Metric<T> {

	Date getDate();

}
