<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<ui:baseview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Trace - Search">
	<jsp:attribute name="headTag">
		<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/search/search.css"
			rel="stylesheet">
		<script
			src="${pageContext.servletContext.contextPath}/static/pulse/js/search/search.js"
			type="text/javascript"></script>
	</jsp:attribute>
	<jsp:body>
<div id="searchpage_content" class="col-md-12 col-sm-12"
			style="height: 100%; padding: 0;">

	<div class="row col-md-12 col-sm-12 search_page_search_bar">

		<div class="col-md-3 col-sm-3 ">
			<input type="text" id="search_page_input_field_1" data-role="none"
						class=" search_page_input_field" placeholder="First Name"
						spellcheck='false' autocomplete="off" />


		</div>
		<div class="col-md-3 col-sm-3 ">
			<input type="text" id="search_page_input_field_2" data-role="none"
						class=" search_page_input_field" placeholder="Last Name"
						spellcheck='false' autocomplete="off" />



		</div>
		<div class="col-md-2 col-sm-2 ">
			<input type="text" id="search_page_input_field_3" data-role="none"
						class=" search_page_input_field" placeholder="Member ID"
						spellcheck='false' autocomplete="off" />

		</div>
		<button class="col-md-1 col-sm-1 btn" id="search_page_clear_button"
					style="background-color: #4d4d4d; height: 30px; width: 30px; margin-left: 25px;">
			<img
						src="${pageContext.servletContext.contextPath}/static/pulse/img/search/RD_cross_CROSS_WHITE_2.0x.png"
						alt="" width="20" style="margin-left: -8.5px; margin-top: -2spx;">
		</button>

		<div class="col-md-2 col-sm-2  "
					style="height: 40px; margin-top: -4px; margin-left: 10px;">
			<div id="searchpulse-location-choice" class="btn-group">
				<button type="button" id="searchpulse-location-choice-1" value="1"
							class="btn btn-default">Club</button>
				<button type="button" id="searchpulse-location-choice-2" value="2"
							class="btn btn-default">All</button>
			</div>
		</div>
		<button class="col-md-1 col-sm-1  btn" id="search_page_go_button"
					style="background-color: #4d4d4d; margin-top: -10px; height: 40px;">
			<img
						src="${pageContext.servletContext.contextPath}/static/pulse/img/search/RD_SEARCH_WHITE_2.0x.png"
						alt="" width="30">
		</button>


	</div>
	<div class="memberpulsepage_member_table_container col-md-12 col-sm-12">

		<table id="searchpage_member_table" class="table">
			<thead>

				<tr>
					<th><img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/pulse/MEMBERPULSE_BIRTHDAY_WHITE.png"
								width="20" alt=""></th>

					<th id="searchpage_member_table_col_0"><img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/interception/interception-flag-white.png"
								width="23" alt=""></th>
					<th id="searchpage_member_table_col_1">ID</th>

					<th id="searchpage_member_table_col_2">First Name</th>
					<th id="searchpage_member_table_col_3">Last Name</th>
					<th id="searchpage_member_table_col_4"><img alt=""
								src="${pageContext.servletContext.contextPath}/static/pulse/img/search/RD_CLUB_WHITE_2.0x.png"
								width="25"></th>

					<th id="searchpage_member_table_col_5">Last Action</th>
					<th id="searchpage_member_table_col_6">Age</th>
					<th id="searchpage_member_table_col_7">Tenure</th>
					<th id="searchpage_member_table_col_8"><img alt=""
								src="${pageContext.servletContext.contextPath}/static/pulse/img/pulse/RD_AtRisk_white_2.0x.png"
								width="25"></th>
					<th id="searchpage_member_table_col_9"><img alt=""
								src="${pageContext.servletContext.contextPath}/static/pulse/img/search/RD_STAR_WHITE_2.0x.png"
								width="25"></th>
					<th id="searchpage_member_table_col_10"><img alt=""
								src="${pageContext.servletContext.contextPath}/static/pulse/img/pulse/newcomer_white.png"
								width="25"></th>


				</tr>
			</thead>
			<tbody>
			</tbody>



			<tfoot>
			</tfoot>
		</table>

	</div>
</div>
			
	</jsp:body>
</ui:baseview>