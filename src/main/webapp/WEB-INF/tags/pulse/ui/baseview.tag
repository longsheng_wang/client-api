<%@ tag language="java" pageEncoding="UTF-8"%>

<%@attribute name="contextPath" required="true"%>
<%@attribute name="pageTitle" required="true"%>
<%@attribute name="headTag" fragment="true"%>
<%@attribute name="modalTag" fragment="true"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style"
	content="black-translucent" />
<!-- start: Meta -->
<meta charset="utf-8">
<!-- end: Meta -->

<!-- start: Mobile Specific -->
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<meta name="format-detection" content="telephone=no">
<!-- end: Mobile Specific -->

<!-- start: Widget Icons -->
<link rel="apple-touch-icon"
	href="${contextPath}/static/pulse/img/icons/apple-touch-icon-iphone.png" />
<link rel="apple-touch-icon" sizes="72x72"
	href="${contextPath}/static/pulse/img/icons/apple-touch-icon-ipad.png" />
<link rel="apple-touch-icon" sizes="114x114"
	href="${contextPath}/static/pulse/img/icons/apple-touch-icon-iphone4.png" />
<link rel="apple-touch-icon" sizes="144x144"
	href="${contextPath}/static/pulse/img/icons/apple-touch-icon-ipad3.png" />
<!-- end: Widget Icons -->

<!-- start: CSS -->
<link type="text/css"
	href="${contextPath}/static/pulse/css/jquery/jquery.mobile-1.3.2.min.css"
	rel="stylesheet" />
<link type="text/css"
	href="${contextPath}/static/pulse/css/tables/jquery.dataTables.mod.min.css"
	rel="stylesheet" />
<link type="text/css"
	href="${contextPath}/static/pulse/js/jquery/tables/Scroller/css/dataTables.scroller.css"
	rel="stylesheet" />
<link
	href="${contextPath}/static/pulse/css/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${contextPath}/static/pulse/css/bootstrap/css/font-awesome.min.css"
	rel="stylesheet">
<link href="${contextPath}/static/pulse/css/global/shell.css"
	rel="stylesheet">
<link
	href="${contextPath}/static/pulse/css/global/progress-bars-style.css"
	rel="stylesheet">
<link href="${contextPath}/static/pulse/css/global/responsiveDesign.css"
	rel="stylesheet">


<!-- end CSS -->

<!-- start: Scripts -->

<script>
	var ctx = "${contextPath}";
</script>

<script src="${contextPath}/static/pulse/js/jquery/jquery-1.11.0.min.js"
	type="text/javascript"></script>
<script
	src="${contextPath}/static/pulse/js/jquery/jquery.mobile-1.3.2.min.js"
	type="text/javascript"></script>
<script
	src="${contextPath}/static/pulse/js/jquery/jquery-migrate-1.2.1.min.js"
	type="text/javascript"></script>


<script
	src="${contextPath}/static/pulse/js/jquery/tables/jquery.dataTables.min.js"
	type="text/javascript"></script>
<script
	src="${contextPath}/static/pulse/js/jquery/tables/Scroller/js/dataTables.scroller.js"
	type="text/javascript"></script>

<script src="${contextPath}/static/pulse/js/bootstrap/bootstrap.min.js"></script>
<script src="${contextPath}/static/pulse/js/global/globals.js"
	type="text/javascript"></script>
<script type="text/javascript"
	src="${contextPath}/static/pulse/js/ion-sound/ion.sound.min.js"></script>
<script type="text/javascript"
	src="${contextPath}/static/pulse/js/global/main.js"></script>
<noscript>
	<div class="alert alert-danger"
		style="width: 50%; margin-left: 25%; text-align: center; border: 2px solid red; font-size: 1.5em">
		<span class="glyphicon glyphicon-exclamation-sign"></span> For full
		functionality of this site it is necessary to enable JavaScript.</br> Here
		are the <a href="http://www.enable-javascript.com/" target="_blank">
			instructions how to enable JavaScript in your web browser</a>.
	</div>

</noscript>
<jsp:invoke fragment="headTag" />
<!-- end: Scripts -->

<title>Pulse</title>
</head>
<body>

	<div id="main_content">

		<div class="navbar navbar-static-top top-fixed-grey-nav-bar">

			<div class="container-fluid">

				<div class="navbar-header">
					<a class="navbar-brand" id="back-button" style="visibility: hidden"><input
						class="back-button"
						style="background: url(${contextPath}/static/pulse/img/navbar/Top_Bar_BackButton_0.5x.png)	no-repeat;"
						onClick="history.go(-1);return true;" Type="button"></a><a
						class="navbar-brand"><img alt=""
						src="${contextPath}/static/pulse/img/logo/lifetimelogo.png"
						height="30" style="display: inline-block;"></a> <a
						class="navbar-brand"
						style="width: 350px; overflow: hidden; margin-right: 0px; padding-right: 0px">
						<span class="header-club-name" id="clubnameLocal">${sessionScope.locationName}</span>
						<span class="header-user-name" id="loggedin_username">-
							${sessionScope.name}</span>
					</a>



				</div>

				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a class="edit-client" href="#">edit account</a></li>
						<li><a class="delete-client" href="#">delete account</a></li>
						<li><a class="view-locations" href="#">view locations</a></li>
						<li><a class="view-users" href="#">view users</a></li>
						<li><a style="margin-top: 2%"><span
								class="navbar_date_time_field"></span></a></li>
						<li><a href="#" class="navbar_birthday_notification_button"
							style="margin-top: -6%;"><img alt=""
								class=" birthday-notification"
								src="${contextPath}/static/pulse/img/navbar/TOPBAR_BIRTHDAY_OFF_2.0x.png"
								height="30"></a></li>
						<li><a href="#" class="navbar_new_notification_button"
							style="margin-top: -6%;"><img alt=""
								class=" new-notification"
								src="${contextPath}/static/pulse/img/navbar/TOPBAR_NEWCOMER_OFF_2.0x.png"
								height="30"></a></li>
						<li><a href="#" class="navbar_attention_notification_button"
							style="margin-top: -6%;"><img alt=""
								class=" attention-notification"
								src="${contextPath}/static/pulse/img/navbar/TOPBAR_FLAG_OFF_2.0x.png"
								height="30"></a></li>
						<li><a href="#" class="navbar_risk_notification_button"
							style="margin-top: -6%;"><img alt=""
								class=" risk-notification"
								src="${contextPath}/static/pulse/img/navbar/TOPBAR_ATRISK_OFF_2.0x.png"
								height="31"></a></li>
						<li><a class="refresh_button" href="#"><img alt=""
								src="${contextPath}/static/pulse/img/navbar/top-nav-bar-refresh.png"
								height="28"></a></li>
						<li><a class="logout_button" href="#"><img alt=""
								src="${contextPath}/static/pulse/img/navbar/top-nav-bar-log-out.png"
								height="28"></a></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="col-md-12 col-sm-12" style="height: 100%; padding: 0;">
			<div class="side-nav-pills col-md-1 col-sm-1">
				<ul class="nav nav-pills">
					<%
						if (request.getRequestURI().contains("clubpulse")) {
					%>
					<li id="homepage" class="active">
						<%
							} else {
						%>
					
					<li id="homepage">
						<%
							}
						%> <a href="#homepage"><img alt=""
							src="${contextPath}/static/pulse/img/menu/side-bar-home.png"
							height="75" style="margin-top: 10%"></a>
					</li>
					<%
						if (request.getRequestURI().contains("memberpulse")) {
					%>
					<li id="pulsepage" class="active">
						<%
							} else {
						%>
					
					<li id="pulsepage">
						<%
							}
						%> <a href="#pulsepage"><img alt=""
							src="${contextPath}/static/pulse/img/menu/side-bar-pulse.png"
							height="75" style="margin-top: 10%"></a>
					</li>
					<%
						if (session.getAttribute("hasCamera").equals("t") || session.getAttribute("hasCamera").equals("true")) {
							if (request.getRequestURI().contains("clubtraffic")) {
					%>

					<li id="trafficpage" class="active">
						<%
							} else {
						%>
					
					<li id="trafficpage">
						<%
							}
						%> <a href="#trafficpage"><img alt=""
							src="${contextPath}/static/pulse/img/menu/side-bar-floor.png"
							height="75" style="margin-top: 10%"></a>
					</li>
					<%
						} else {
							if (request.getRequestURI().contains("clubtraffic")) {
								response.sendRedirect(contextPath + "/clubpulse");
							}
						}

						if (request.getRequestURI().contains("clubanalytics")) {
					%>
					<li id="analyticspage" class="active">
						<%
							} else {
						%>
					
					<li id="analyticspage">
						<%
							}
						%> <a href="#analyticspage"><img alt=""
							src="${contextPath}/static/pulse/img/menu/side-bar-analytics.png"
							height="75" style="margin-top: 10%"></a>
					</li>
					<%
						if (request.getRequestURI().contains("search")) {
					%>
					<li id="searchpage" class="active">
						<%
							} else {
						%>
					
					<li id="searchpage">
						<%
							}
						%> <a href="#searchpage"><img alt=""
							src="${contextPath}/static/pulse/img/menu/side-bar-search.png"
							height="75"></a>
					</li>
				</ul>


				<div class="reunify-logo">
					<img alt=""
						src="${contextPath}/static/pulse/img/menu/side-bar-reunify-logo.png"
						height="75" style="display: block; margin: auto">
				</div>

				<div id="version" style="text-shadow: none">Version</div>

			</div>
			<div id="Cookie_check" class="alert alert-warning"
				style="text-align: center; width: 35%; margin-top: 1%; margin-left: 32%; display: none">
				<span class="glyphicon glyphicon-warning-sign"></span> Seems that
				your browser doesn't accept cookies</br> For complete functionality
				please enable it in browser settings.
			</div>
			<div id="page_content"
				class="inner-main-div container-fluid col-md-11 col-sm-11">

				<jsp:doBody />
				<div id="ajaxSpinnerContainer">
					<img src="${contextPath}/static/pulse/img/loader/716.GIF"
						id="ajaxSpinnerImage" title="working...">
				</div>

			</div>


		</div>

		<div class="modal logout_modal bs-modal-sm" tabindex="-1"
			role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
			style="margin-top: 10%;">
			                   
			<div class="modal-dialog modal-sm">
				                   
				<div class="modal-content">
					                   
					<div class="modal-header" style="height: 50px;">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">×</button>
						<h4 class="modal-title" id="mySmallModalLabel">Log Out Alert</h4>

					</div>
					<div class="modal-body" style="height: 20px;">Are you sure
						you want to log out?</div>
					                   
					<div class="modal-footer" style="height: 50px;">
						                                               
						<button style="width: 100px; float: right; margin-left: 30px;"
							class="btn btn-success confirm-logout">Yes</button>
						<button style="width: 100px; float: right;"
							class="btn btn-danger cancel-logout">No</button>
						                 
					</div>
					                   
				</div>
				                   
			</div>
			               
		</div>

		<jsp:invoke fragment="modalTag" />

	</div>

</body>

</html>