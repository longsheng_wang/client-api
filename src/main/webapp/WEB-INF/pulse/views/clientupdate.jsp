<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<ui:loginview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Pulse - New User Sign-up">
	<jsp:attribute name="headTag">
		<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/login/registration.css"
			rel="stylesheet">
	</jsp:attribute>
	<jsp:body>
	
<div class="wrapper">

<form:form method="POST"
				action="${pageContext.servletContext.contextPath}/client/updateaccount"
				commandName="client">
             <table>
                <tr>
                    <td><form:label path="companyName">Company Name</form:label></td>
                    <td><form:input path="companyName" /></td>
                      <td class="error">  <form:errors
								path="companyName" element="div" /></td>
                    
                </tr>
                
                 <tr>
                    <td><form:label path="industryName">Industry Name</form:label></td>
                    <td><form:select path="industryName">
							<form:option value="NONE" label="--- Select ---" />
							<form:option value="health & fitness" label="Fitness & Health" />
   						</form:select></td>
                   <td class="error"><form:errors
								path="industryName" element="div" /></td>
                </tr>
                        <form:hidden path="clientId" value="${clientId}" />
        
          	
                <tr>
                    <td><input type="submit" value="Submit" /></td>
                </tr>
            </table>
        </form:form>
  </div>
	 
	</jsp:body>
</ui:loginview>