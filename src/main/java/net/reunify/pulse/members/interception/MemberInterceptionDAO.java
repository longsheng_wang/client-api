/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members.interception;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.stereotype.Component;

/**
 * DAO for member intercept data. The class is implemented separately for
 * different data sources
 * 
 * @author kiana.baradaran
 * 
 */
@Component
public interface MemberInterceptionDAO {

	/**
	 * Get the detailed object for the given id
	 * 
	 * @param interceptId
	 * @return
	 */
	MemberIntercept getInterceptDetails(int interceptId);

	/**
	 * Insert the given intercept
	 * 
	 * @param intercept
	 * @return
	 */
	int insertMemberIntercept(MemberIntercept intercept);

	/**
	 * Gets all the intercepts that the specified employee has done in the given
	 * interval
	 * 
	 * @param employeeId
	 * @param fromTimestamp
	 * @param toTimestamp
	 * @return
	 */
	List<MemberIntercept> getMemberInterceptsByEmployeeId(String employeeId, Timestamp fromTimestamp,
			Timestamp toTimestamp);

	/**
	 * Gets all the intercepts that the have been done in the specified location
	 * in the given interval
	 * 
	 * @param locationId
	 * @param fromTimestamp
	 * @param toTimestamp
	 * @return
	 */
	List<MemberIntercept> getMemberInterceptsByLocationId(String locationId, Timestamp fromTimestamp,
			Timestamp toTimestamp);

	/**
	 * Gets the latest intercept for the specified member
	 * 
	 * @param memberId
	 * @return
	 */
	List<MemberIntercept> getLatestMemberIntercept(String memberId);

	/**
	 * Insert a member in the intercept queue to be intercepted later
	 * 
	 * @param interceptQueueEntry
	 * @return recordId of the newly inserted record
	 */
	int enqueueMemberIntercept(InterceptQueueEntry interceptQueueEntry);

	/**
	 * Request for removing an intercept reminder from the queue
	 * 
	 * @param recordId
	 * @return
	 */
	boolean removeMemberInterceptFromQueue(int recordId);

	/**
	 * Request for retrieving the queue for a user
	 * 
	 * @param intercept
	 * @return List<InterceptQueueEntry>
	 */
	List<InterceptQueueEntry> getMemberInterceptQueue(int userId);

	/**
	 * Increment number of intercepts/queued members for this user
	 * 
	 * @param interceptorId
	 * @param locationId
	 */

	boolean updateUserStatsWithQueueInsert(int interceptorId, int locationId);

	/**
	 * Decrement number of queued members for this user
	 * 
	 * @param recordId
	 */
	boolean updateUserStatsWithQueueDelete(int recordId);

	/**
	 * Increment number of intercepts for this user
	 * 
	 * @param interceptorId
	 * @param locationId
	 */

	boolean updateUserStatsWithIntercept(int interceptorId, int locationId);

	/**
	 * Request for ending an intercept
	 * 
	 * @param intercept
	 * @param clientId
	 */
	void endMemberIntercept(MemberIntercept intercept, int clientId);

}
