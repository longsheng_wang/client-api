/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.user;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import net.reunify.pulse.base.BaseController;
import net.reunify.pulse.client.ClientService;
import net.reunify.pulse.client.LocationDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Handles requests for creation of new users
 * 
 * @author kiana.baradaran
 * 
 */
@Controller
public class UserController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Inject
	@Named("UserService")
	private UserService userServiceHandler;

	@Inject
	@Named("ClientService")
	private ClientService clientServiceHandler;

	/**
	 * Populate the list of locations for a client
	 * 
	 * @param clientId
	 * @return
	 */
	protected Map<Integer, String> populateLocationList(int clientId) {
		List<LocationDTO> locations = clientServiceHandler.getClientLocations(clientId);

		Map<Integer, String> locationsList = new HashMap<Integer, String>();
		for (LocationDTO location : locations) {
			locationsList.put(location.getLocationId(), location.getName());
		}
		return locationsList;
	}

	/**
	 * Populate the list of roles for a user
	 * 
	 * @param clientId
	 * @return
	 */
	protected Map<Integer, String> populateUserList() {
		List<UserRole> roles = userServiceHandler.getUserRoles();

		Map<Integer, String> rolesList = new HashMap<Integer, String>();
		for (UserRole role : roles) {
			rolesList.put(role.getRoleId(), role.getRoleName());
		}
		return rolesList;
	}

	/**
	 * Provides the tabular view of users
	 * 
	 * @return
	 */
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ModelAndView usersView(HttpServletRequest request) {

		List<UserDTO> usersList =
				userServiceHandler.getUserAccounts(Integer.parseInt((String) request.getSession().getAttribute(
						"clientId")));

		ModelAndView usersView = new ModelAndView("usersView");
		usersView.addObject("usersList", usersList);

		return usersView;
	}

	/**
	 * Removes (deactivates) the account that matches the given information
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/user/removeaccount/{userId}", method = RequestMethod.GET)
	public String removeAccount(HttpServletRequest request, @PathVariable int userId, ModelMap model) {
		String view = "actionresult";
		int clientId = Integer.parseInt((String) request.getSession().getAttribute("clientId"));
		boolean success = userServiceHandler.removeUserAccount(userId, clientId);

		if (success) {
			model.addAttribute("message", "The account was successfully removed.");
			model.addAttribute("redirectUrl", "/users");
			model.addAttribute("redirectMessage", "Go back to users view.");
		} else {
			model.addAttribute("message", "The account could not be removed.");
			model.addAttribute("redirectUrl", "/users");
			model.addAttribute("redirectMessage", "Try Again");
		}
		return view;
	}

	/**
	 * Request for changing the password
	 * 
	 * @param user
	 * @param result
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/user/changepassword", produces = "text/html", method = { RequestMethod.GET,
			RequestMethod.POST })
	@ResponseBody
	public ModelAndView changePassword(@RequestParam(required = false) int userId,
			@ModelAttribute("user") UserDTO user, BindingResult result, HttpServletRequest request) {
		int clientId = Integer.parseInt((String) request.getSession().getAttribute("clientId"));
		ModelAndView changepasswordview = new ModelAndView("changepassword");
		if (userId > 0) {
			changepasswordview.addObject("userId", userId);
		}
		if (("POST").equals(request.getMethod())) {
			UserRegistrationValidator formValidator = new UserRegistrationValidator();
			formValidator.validatePassword(user, result, "password", "matchingPassword");
			user.setUserId(userId);
			if (!result.hasErrors()) {
				changepasswordview = new ModelAndView("actionresult");
				boolean success = userServiceHandler.changePassword(user, clientId);

				if (success) {
					changepasswordview.addObject("message", "The password has been successfully changed.");
					changepasswordview.addObject("redirectUrl", "/users");
					changepasswordview.addObject("redirectMessage", "Go back to users view.");

				} else {
					changepasswordview.addObject("message", "The password could not be changed at this time.");
					changepasswordview.addObject("redirecturl", "/users");
					changepasswordview.addObject("redirectMessage", "Try Again!");
				}
			}
		}
		changepasswordview.addObject("user", user);
		return changepasswordview;
	}

	/**
	 * Request for user information update
	 * 
	 * @param request
	 * @param user
	 * @param result
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/user/edit", produces = "text/html", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public ModelAndView updateUser(HttpServletRequest request, @RequestParam(required = false) int userId,
			@ModelAttribute("user") UserDTO user, BindingResult result) {
		int clientId = Integer.parseInt((String) request.getSession().getAttribute("clientId"));

		Map<Integer, String> locationsList =
				populateLocationList(Integer.parseInt((String) request.getSession().getAttribute("clientId")));
		Map<Integer, String> userRolesList = populateUserList();
		ModelAndView updateview = new ModelAndView("useraccountupdate");
		updateview.addObject("locationsList", locationsList);
		updateview.addObject("rolesList", userRolesList);

		if (userId > 0) {
			updateview.addObject("userId", userId);
		}
		if ((request.getMethod()).equals("POST")) {
			UserRegistrationValidator formValidator = new UserRegistrationValidator();
			formValidator.validateUpdateObject(user, result);
			user.setUserId(userId);
			if (!result.hasErrors()) {
				boolean success = userServiceHandler.editUserAccount(user, clientId);
				updateview = new ModelAndView("actionresult");
				if (success) {
					updateview.addObject("message", "The account has been successfully edited.");
					updateview.addObject("redirectUrl", "/users");
					updateview.addObject("redirectMessage", "Go back to users view.");

				} else {
					updateview.addObject("message", "The account could not be edited at this time.");
					updateview.addObject("redirecturl", "/users");
					updateview.addObject("redirectMessage", "Try Again!");
				}

			}
		}
		updateview.addObject("user", user);
		return updateview;
	}

	@RequestMapping(value = "/user/register", produces = "text/html", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public ModelAndView registerUser(HttpServletRequest request, @ModelAttribute("user") UserDTO user,
			BindingResult result) {
		Map<Integer, String> locationsList =
				populateLocationList(Integer.parseInt((String) request.getSession().getAttribute("clientId")));
		Map<Integer, String> userRolesList = populateUserList();
		ModelAndView registrationview = new ModelAndView("userregistration");
		registrationview.addObject("locationsList", locationsList);
		registrationview.addObject("rolesList", userRolesList);

		if ((request.getMethod()).equals("POST")) {

			UserRegistrationValidator formValidator = new UserRegistrationValidator();
			formValidator.validate(user, result);
			if (!result.hasErrors()) {
				logger.info("creating user...");
				int userId = userServiceHandler.createUserAccount(user);
				registrationview = new ModelAndView("actionresult");
				if (userId > 0) {
					registrationview.addObject("message",
							"The account has been successfully created for user " + user.getEmailAddress() + ".");
					registrationview.addObject("redirectUrl", "/login");
					registrationview.addObject("redirectMessage", "Login now!");

				} else {
					registrationview.addObject("message", "The account could not be created.");
					registrationview.addObject("redirecturl", "/user/registration");
					registrationview.addObject("redirectMessage", "Try Again!");
				}

			}
		}
		registrationview.addObject("user", user);
		return registrationview;
	}

}
