package net.reunify.pulse.client;

import static org.junit.Assert.assertTrue;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/spring-security.xml",
		"file:src/test/webapp/WEB-INF/mvc-dispatcher-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class JdbcClientDAOTest {

	private static final Logger logger = Logger.getLogger(JdbcClientDAOTest.class);

	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private ClientDAO clientDAO;

	@Inject
	@Named("pulseJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@After
	public void tearDown() throws Exception {
		cleanUp();
	}

	private void cleanUp() {
		String clientCleanUp =
				"delete from clients where client_name ='TestCompanyFromReunify' and industry_name = 'Test';";

		jdbcTemplate.update(clientCleanUp);
	}

	@Test
	public void testAddClientAccount() {
		ClientDTO client = new ClientDTO();
		client.setCompanyName("TestCompanyFromReunify");
		client.setIndustryName("Test");
		assertTrue(clientDAO.createClientAccount(client) > 0);
	}

}
