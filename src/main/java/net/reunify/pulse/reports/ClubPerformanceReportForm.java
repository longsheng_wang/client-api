/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;

import org.apache.log4j.Logger;


/**
 * Form data used as input for a ClubPerformanceReport
 * 
 * @author Vivek Hungund
 *
 */
public class ClubPerformanceReportForm extends DateRangeReportForm {
	private static final Logger logger = Logger.getLogger(ClubPerformanceReportForm.class);

	/**
	 * Location ID for this report
	 */
	private String locationId;

	/**
	 * Empty constructor
	 */
	public ClubPerformanceReportForm() {
		startDate = "";
		endDate = "";
		locationId = "0";
	}
	
	/**
	 * @param startDate
	 * @param endDate
	 * @param locationId
	 */
	public ClubPerformanceReportForm(String startDate, String endDate, String locationId) {
		super(startDate, endDate);
		this.locationId = locationId;
	}

	/**
	 * @return the locationId
	 */
	public String getLocationId() {
		return locationId;
	}

	/**
	 * @param locationId
	 *            the locationId to set
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
}
