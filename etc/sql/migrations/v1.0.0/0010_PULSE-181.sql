-- create indices for user_id
CREATE INDEX IX_member_intercepts_interceptor_id ON member_intercepts(interceptor_id);
CREATE INDEX IX_member_intercepts_queue_interceptor_id ON member_intercepts_queue(interceptor_id);

-- create indices for member_id
CREATE INDEX IX_member_gym_buddy_gymbuddy_member_id ON member_gym_buddy(gymbuddy_member_id);
CREATE INDEX IX_member_gym_buddy_member_id ON member_gym_buddy(member_id);
CREATE INDEX IX_member_intercepts_member_id ON member_intercepts(member_id);
CREATE INDEX IX_member_intercepts_queue_member_id ON member_intercepts_queue(member_id);
CREATE INDEX IX_member_social_media_updates_member_id ON member_social_media_updates(member_id);
CREATE INDEX IX_members_score_history_member_id ON members_score_history(member_id);
-- big tables
--  ~ 104s
CREATE INDEX IX_member_personal_interests_member_id ON member_personal_interests(member_id);
--  ~ 122s
CREATE INDEX IX_member_swipes_pattern_member_id ON member_swipes_pattern(member_id);
--  ~ 40s
CREATE INDEX IX_members_fitness_member_id ON members_fitness(member_id);

-- create indices for client_id
CREATE INDEX IX_client_locations_client_id ON client_locations(client_id);