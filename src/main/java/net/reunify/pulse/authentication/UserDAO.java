/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.authentication;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * DAO for user account retrieval
 * 
 * @author kiana.baradaran
 * 
 */

public interface UserDAO {
	/**
	 * Finds the associated user for the given username
	 * 
	 * @param username
	 * @return UserModel
	 */
	UserDetails findUserByUsername(String username);
}
