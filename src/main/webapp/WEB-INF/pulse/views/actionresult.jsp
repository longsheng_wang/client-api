<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@page import="java.net.URLEncoder"%>

<ui:loginview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Pulse - Registration">
	<jsp:attribute name="headTag">
	<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/login/registration.css"
			rel="stylesheet">
	</jsp:attribute>
	<jsp:body>
<div class="wrapper">
<div class="main-div">
	${message}
	<br>
	<br>
	<A HREF="${pageContext.servletContext.contextPath}${redirectUrl}">${redirectMessage}</A>
	</div>
	</div>
	</jsp:body>
</ui:loginview>

