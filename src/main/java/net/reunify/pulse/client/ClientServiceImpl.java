/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.client;

import java.util.List;

import net.reunify.pulse.user.UserService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of ClientService
 * 
 * @author kiana.baradaran
 * 
 */
@Service("ClientService")
@Transactional(value = "pulseTxManager")
public class ClientServiceImpl implements ClientService {
	private static final Logger logger = LoggerFactory.getLogger(ClientServiceImpl.class);

	@Autowired
	private ClientDAO clientDAO;

	@Autowired
	private UserService userServiceHandler;

	@Override
	public List<ClientDTO> getActiveClients() {
		return clientDAO.getActiveClients();
	}

	@Override
	public List<LocationDTO> getClientLocations(int clientId) {
		return clientDAO.getClientLocations(clientId);
	}

	@Override
	public int createClientAccount(ClientDTO client) {
		int userId = 0;
		try {
			int clientId = clientDAO.createClientAccount(client);
			client.getLocations().get(0).setClientId(clientId);
			int locationId = addLocation(client.getLocations().get(0));
			client.getAccountAdmin().setLocationId(locationId);
			userId = userServiceHandler.createUserAccount(client.getAccountAdmin());
		} catch (NullPointerException e) {
			logger.error("Account could not be created.", e);
		}

		return userId;
	}

	@Override
	public int addLocation(LocationDTO location) {
		return clientDAO.addLocation(location);
	}

	@Override
	public boolean removeLocation(int locationId, int clientId) {
		return clientDAO.removeLocation(locationId, clientId);
	}

	@Override
	public boolean updateLocation(LocationDTO location) {
		return clientDAO.updateLocation(location);
	}

	@Override
	public boolean updateClientAccount(ClientDTO client) {
		return clientDAO.updateClientAccount(client);
	}

	@Override
	public boolean removeClientAccount(int clientId) {
		return clientDAO.removeClientAccount(clientId);
	}
}
