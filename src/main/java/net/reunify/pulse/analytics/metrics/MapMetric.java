/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.analytics.metrics;

import java.util.Date;
import java.util.Map;

import net.reunify.pulse.members.MemberMetric;

import org.apache.log4j.Logger;

/**
 * Map valued metric
 * 
 * @author Shakti Shrivastava
 * 
 * @param <T>
 */
public class MapMetric<T extends Map<?, ?>> implements TimestampMetric<T> {

	private static final Logger logger = Logger.getLogger(MapMetric.class);

	private MemberMetric metricName;
	private T value;
	private Date date;

	public MapMetric() {
		// TODO Auto-generated constructor stub
	}

	public MapMetric(MemberMetric metricName, T value, Date date) {
		this.date = date;
		this.metricName = metricName;
		this.value = value;
	}

	public static Logger getLogger() {
		return logger;
	}

	public void setMetricName(MemberMetric metricName) {
		this.metricName = metricName;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public MemberMetric getMetricName() {
		return metricName;
	}

	@Override
	public T getValue() {
		return value;
	}

	@Override
	public Date getDate() {
		return date;
	}

}
