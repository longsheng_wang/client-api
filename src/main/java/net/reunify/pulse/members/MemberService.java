package net.reunify.pulse.members;

/**
 * Service to handle MemberController requests
 * 
 * @author kiana.baradaran
 * 
 */
public interface MemberService {

	/**
	 * Insert a member check-in entry
	 * 
	 * @param memberCheckInDTO
	 */
	public void insertMemberCheckIn(MemberCheckInDTO memberCheckInDTO);

	/**
	 * Get a member profile by id
	 * 
	 * @param memberId
	 * @param clientId
	 * @return
	 */
	public MemberProfile getMemberProfile(int memberId, int clientId);

	/**
	 * Update an existing member profile
	 * 
	 * @param memberProfile
	 * @param memberId
	 * @param clientId
	 */

	public void updateMemberProfile(MemberProfile memberProfile, String memberId, int clientId);

	/**
	 * Insert a new member profile
	 * 
	 * @param memberProfile
	 */
	public void insertMemberProfile(MemberProfile memberProfile, int clientId);

	/**
	 * Get member analytics info
	 * 
	 * @param memberId
	 * @param clientId
	 * @return
	 */
	public MemberAnalytics getMemberAnalytics(int memberId, int clientId);
}
