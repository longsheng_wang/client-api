/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members;

import java.util.Date;

import org.apache.log4j.Logger;

/**
 * data from members, common field for all members
 * 
 * @author Shakti Shrivastava
 * @author kiana.baradaran
 * 
 */
public class MemberPersonalInfo {
	private static final Logger logger = Logger.getLogger(MemberPersonalInfo.class);

	private String firstName;
	private String lastName;
	private String locationId;
	private Date birthdate;
	private String gender;
	private String address;
	private String city;
	private String zipeCode;

	public MemberPersonalInfo() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "MemberPersonalInfo [firstName=" + firstName + ", lastName=" + lastName + ", locationId=" + locationId
				+ ", birthdate=" + birthdate + ", gender=" + gender + ", address=" + address + ", city=" + city
				+ ", zipeCode=" + zipeCode + "]";
	}

	public MemberPersonalInfo(String firstName, String lastName, Date birthdate, String gender, String address,
								String city, String zipeCode, String locationId) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthdate = birthdate;
		this.gender = gender;
		this.address = address;
		this.city = city;
		this.zipeCode = zipeCode;
		this.locationId = locationId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipeCode() {
		return zipeCode;
	}

	public void setZipeCode(String zipeCode) {
		this.zipeCode = zipeCode;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
}
