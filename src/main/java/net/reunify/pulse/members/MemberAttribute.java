/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members;

/**
 * @author kiana.baradaran
 * 
 * @param <T>
 */
public class MemberAttribute<T> {

	@Override
	public String toString() {
		return "MemberAttribute [attributeName=" + attributeName + ", attributeValue=" + attributeValue + "]";
	}

	private String attributeName;
	private T attributeValue;

	public MemberAttribute() {
		// TODO Auto-generated constructor stub
	}

	public MemberAttribute(String attributeName, T attributeValue) {
		this.attributeName = attributeName;
		this.attributeValue = attributeValue;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public void setAttributeValue(T attributeValue) {
		this.attributeValue = attributeValue;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public T getAttributeValue() {
		return attributeValue;
	}
}
