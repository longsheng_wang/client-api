<%@taglib prefix="ui" tagdir="/WEB-INF/tags/beat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<ui:beatbaseview contextPath="" pageTitle="ReunifyBeat"
	pageName="whatPageView">
	<jsp:attribute name="headTag">
	<!--  Add header files here  -->
	</jsp:attribute>
	<jsp:body>
	<div class="row col-md-12 col-sm-12 gridlines"
			style="height: 700px; padding-left: 5px; padding-right: 25px;">

		<!--Content(Aarti) starts-->
		<div class="row col-lg-12 gridlines" style="padding: 0px;">

			<!--issues-->
			<div class="col-lg-6 gridlines"
					style="padding-left: 5px; height: 300px; margin-top: 10px;">
				<!--header-->
				<div class="row col-lg-12 bootcolstyle">
					<div class="headerbox">
						<p class="customp" style="color: white;">Issues</p>
					</div>
				</div>
				<!--header ends-->

				<!--subheader 1-->
				<div class="row col-lg-12 bootcolstyle">
					<div class="headerbox" style="background: orange; border: none;">
						<div class="row col-lg-3">
							<p></p>
						</div>
						<div class="row col-lg-7">
							<p class="subheaderp">50 100 150 200 250</p>
						</div>
						<div class="row col-lg-2">
							<p></p>
						</div>
					</div>
				</div>
				<!--number row 1 ends-->
				
				<!-- Issue table right now showing top 5 issues. -->
				<c:forEach var="counter" begin="${1}" end="${3}">
				<c:set var="issue" value="${issueList[counter]}" />
				<c:if test="${issue != null}">
				<fmt:formatNumber var="issuePercentage"
								value="${issue.count/250*100}" maxFractionDigits="0" />									
					<div class="row col-lg-12 bootcolstyle">
                                <div class="rowboxoutline"
									style="border: 0px;">
                                    <div class="row col-lg-3">
                                        <p class="textp">${issue.item}</p>
                                    </div>
                                    <div class="row col-lg-7">
                                        <div class="progress">
                                            <div class="progress-bar"
												role="progressbar" aria-valuenow="${issue.count}"
												aria-valuemin="0" aria-valuemax="250"
												style="width: ${issuePercentage}%;">
                                                <span class="sr-only">${issuePercentage}% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row col-lg-2">
                                    </div>
                                </div>
                            </div>
                </c:if>
				</c:forEach>
			</div>
			<!--issues ends-->

			<!--people-->
			<div class="col-lg-6 gridlines"
					style="padding-left: 5px; height: 300px; margin-top: 10px;">
				<!--header-->
				<div class="row col-lg-12 bootcolstyle">
					<div class="headerbox">
						<p class="customp" style="color: white;">People</p>
					</div>
				</div>
				<!--header ends-->
				<div class="row col-lg-12 bootcolstyle">
                	<div class="col-lg-6 rowboxoutline" style="border: 0px; background-color: #F99;">
                    	<p class="subheaderp">Name</p>
                   </div>
					<div class="col-lg-6 rowboxoutline" style="border: 0px; background-color: #09C;">
	                   <p class="subheaderp">Name</p>
	               </div>
	            </div>
	            <c:forEach var="counter" begin="${1}" end="${3}">
	            <c:set var="person" value="${personList[counter]}" />
	            <c:if test="${person != null}">
		            <div class="row col-lg-12 gridline" style="padding:0px; margin:0px;">
	                    <div class="row col-lg-3 rowboxoutline" style="border:0px;">
	                        <p class="textp">${person.person}</p>
	                    </div>
	                    <div class="row col-lg-3 rowboxoutline" style="border:0px;">
	                        <p class="textp">${person.count}</p>
	                    </div>
	                    <div class="row col-lg-3 rowboxoutline" style="border:0px;">
	                        <p class="textp">${person.person}</p>
	                    </div>
	                    <div class="row col-lg-3 rowboxoutline" style="border:0px;">
	                        <p class="textp">${person.count}</p>
	                    </div>
	                </div>
                </c:if>
				</c:forEach>
			</div>
			<!--people ends-->
		</div>
		<!--Row1 in outside container ends-->

		<div class="row col-lg-12 gridlines" style="padding: 0px;">
			<!--departments-->
			<div class="col-lg-6 gridlines"
					style="padding-left: 5px; height: 300px;">
				<!--header-->
				<div class="row col-lg-12 bootcolstyle">
					<div class="headerbox">
						<p class="customp" style="color: white;">Department</p>
					</div>
				</div>
				<!--header ends-->

				<!--subheader 1-->
				<div class="row col-lg-12 bootcolstyle">
					<div class="headerbox" style="background: orange; border: none;">
						<div class="row col-lg-3">
							<p></p>
						</div>
						<div class="row col-lg-6">
							<p class="subheaderp">5 10 15 20 25</p>
						</div>
						<div class="row col-lg-3">
							<p></p>
						</div>
					</div>
				</div>
				<!--number row 1 ends-->

				<!--bar row 1-->
				<div class="row col-lg-12 bootcolstyle">
					<div class="rowboxoutline" style="border: 0px;">
						<div class="row col-lg-3">
							<p class="textp">Yoga</p>
						</div>
						<div class="row col-lg-6">
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="60"
										aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
									<span class="sr-only">60% Complete</span>
								</div>
							</div>
						</div>
						<div class="row col-lg-3">
							<p class="textp" style="color: orange;">13% &#x21e3;</p>
						</div>
					</div>
				</div>
				<!--bar row 1 ends-->

				<!--bar row 2-->
				<div class="row col-lg-12 bootcolstyle">
					<div class="rowboxoutline" style="border: 0px;">
						<div class="row col-lg-3">
							<p class="textp">Basketball</p>
						</div>
						<div class="row col-lg-6">
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="40"
										aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
									<span class="sr-only">60% Complete</span>
								</div>
							</div>
						</div>
						<div class="row col-lg-3">
							<p class="textp" style="color: orange;">13% &#x21e3;</p>
						</div>
					</div>
				</div>
				<!--bar row 2 ends-->

				<!--bar row 3-->
				<div class="row col-lg-12 bootcolstyle">
					<div class="rowboxoutline" style="border: 0px;">
						<div class="row col-lg-3">
							<p class="textp">Fitness</p>
						</div>
						<div class="row col-lg-6">
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="40"
										aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
									<span class="sr-only">60% Complete</span>
								</div>
							</div>
						</div>
						<div class="row col-lg-3">
							<p class="textp" style="color: orange;">13% &#x21e3;</p>
						</div>
					</div>
				</div>
				<!--bar row 3 ends-->
			</div>
			<!--departments ends-->

			<!--sentiments-->
			<div class="col-lg-6 gridlines"
					style="padding-left: 5px; height: 300px;">
				<!--header-->
				<div class="row col-lg-12 bootcolstyle">
					<div class="headerbox">
						<p class="customp" style="color: white;">Sentiments</p>
					</div>
				</div>
				<!--header ends-->

				<!--complaint row 1-->
				<c:set var="total"
						value="${sentimentCount.positive + sentimentCount.negative + sentimentCount.neutral}" />
				<c:if test="${total == 0}">
				<c:set var="total" value="1" />
			</c:if>
				<div class="row col-lg-12 gridline"
						style="padding: 0px; margin: 0px;">
					<div class="row col-lg-6 rowboxoutline" style="border: 0px;">
						<p class="textp">Positive</p>
					</div>
					<div class="row col-lg-6 rowboxoutline" style="border: 0px;">
						<c:set var="positiveRate"
								value="${sentimentCount.positive / total}" />
						<p class="textp">
						<fmt:formatNumber type="percent" value="${positiveRate}" />
					</p>
					</div>
				</div>
				<!--complaint row 1 ends-->
				<!--complaint row 2-->
				<div class="row col-lg-12 gridline"
						style="padding: 0px; margin: 0px;">
					<div class="row col-lg-6 rowboxoutline" style="border: 0px;">
						<p class="textp">Negative</p>
					</div>
					<div class="row col-lg-6 rowboxoutline" style="border: 0px;">
						<c:set var="negativeRate"
								value="${sentimentCount.negative / total}" />
						<p class="textp">
						<fmt:formatNumber type="percent" value="${negativeRate}" />
					</p>
					</div>
				</div>
				<!--complaint row 2 ends-->
				<!--complaint row 3-->
				<div class="row col-lg-12 gridline"
						style="padding: 0px; margin: 0px;">
					<div class="row col-lg-6 rowboxoutline" style="border: 0px;">
						<p class="textp">Neutral</p>
					</div>
					<div class="row col-lg-6 rowboxoutline" style="border: 0px;">
						<c:set var="neutralRate" value="${sentimentCount.neutral / total}" />
						<p class="textp">
						<fmt:formatNumber type="percent" value="${neutralRate}" />
					</p>
					</div>
				</div>
				<!--complaint row 3 ends-->
			</div>
			<!--sentiments ends-->




		</div>
		<!--Row2 in outside container ends-->

		<!--Content(Aarti) ends-->
	</div>
	<!--content container -ends-->
</jsp:body>
</ui:beatbaseview>
