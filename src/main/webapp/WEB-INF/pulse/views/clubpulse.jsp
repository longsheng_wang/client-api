<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<ui:baseview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Trace - Club Pulse">
	<jsp:attribute name="headTag">
		<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/clubpulse/clubpulse.css"
			rel="stylesheet">
		<script
			src="${pageContext.servletContext.contextPath}/static/pulse/js/clubpulse/clubpulse.js"
			type="text/javascript"></script>
	</jsp:attribute>
	<jsp:body>
		<div id="homepage_content" class="col-md-12 col-sm-12"
			style="height: 100%; padding: 0;">
		
			<div class="col-md-6 col-sm-6">
				<div class="col-md-12 col-sm-12 ">
		
					<h5 class="action-center-title-homepage">Pulse (Last 90 Minutes)</h5>
		
		
					<div class="row ">
						<div class="col-md-12 col-sm-12">
							<div class=" btn-group-vertical pulse-btn-homepage"
								id="homepage_inclub_members_button">
								<button style="background: #4d4d4d; line-height: 35px;"
									type="button" class="btn btn-default">
									<img alt=""
										src="${pageContext.servletContext.contextPath}/static/pulse/img/pulse/RD_MEMBER_WHITE_2.0x.png"
										style="float: left; margin-right: 10px;" height="35"> <span
										class="number-desc-left">Members</span>
								</button>
								<button style="background: #4d4d4d;" type="button"
									class="btn btn-default">
									<span class="big-number" id="homepage_inclub_members">0</span>
								</button>
							</div>
						</div>
					</div>
					<div class="row ">
						<div class="col-md-6 col-sm-6">
							<div class="btn-group-vertical pulse-btn-homepage"
								id="homepage_new_members_button" style="margin-top: 4%;">
								<button style="background: #99CC33; line-height: 35px;"
									type="button" class="btn btn-default">
									<img alt=""
										src="${pageContext.servletContext.contextPath}/static/pulse/img/pulse/newcomer_white.png"
										style="float: left; margin-right: 10px;" height="35"> <span
										class="number-desc-left">Newcomers</span>
								</button>
								<button style="background: #99CC33;" type="button"
									class="btn btn-default">
									<span class="big-number" id="homepage_new_members">0</span>
								</button>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="btn-group-vertical pulse-btn-homepage"
								id="homepage_inclub_atRisk_button" style="margin-top: 4%;">
								<button style="background: #f03224; line-height: 35px;"
									type="button" class="btn btn-default">
									<img alt=""
										src="${pageContext.servletContext.contextPath}/static/pulse/img/pulse/RD_AtRisk_white_2.0x.png"
										style="float: left; margin-right: 10px;" height="35"> <span
										class="number-desc-left">High Risk</span>
								</button>
								<button style="background: #f03224;" type="button"
									class="btn btn-default">
									<span class="big-number" id="homepage_inclub_atRisk">0</span>
								</button>
		
		
							</div>
		
						</div>
					</div>
				</div>
			</div>
		
			<div class="col-md-6 col-sm-6">
				<div class="col-md-12 col-sm-12 ">
		
					<h5 class="my-goals-title-text">My Goal and Performance</h5>
					<table class="table today-goal-table">
						<tr>
							<td>
		
		
								<div class="col-md-3 col-sm-3"
									style="display: inline; float: left; line-height: 70px;">
									<img alt=""
										src="${pageContext.servletContext.contextPath}/static/pulse/img/interception/interception-flag-grey.png"
										height="40">
								</div>
		
								<div class="col-md-4 col-sm-4"
									style="display: inline; float: left; margin-top: 4%;">
									<span class="sm-gray-bold">To Intercept Today</span>
								</div>
								<div class="col-md-5 col-sm-5"
									style="display: inline; float: left;">
									<span class="big-number-grey" id="homepage_need_to_intercept">0</span>
								</div>
		
		
		
							</td>
						</tr>
		
					</table>
		
		
		
					<table class="table table-bordered data-table"
						style="margin-top: 2%;">
						<thead>
							<tr>
								<th class=""></th>
								<th class="">Today</th>
								<th class="">Month</th>
							</tr>
						</thead>
						<tbody>
							<tr>
		
								<td>
									<div class=" btn-home-title">
										<span style="width: 50px; float: left"><img alt=""
											style="margin-left: -2%;"
											src="${pageContext.servletContext.contextPath}/static/pulse/img/pulse/general-member.png"
											height="35"></span>Members
									</div>
								</td>
								<td>
									<div class=" btn-activity-data-home-page"
										id="total_members_today">0</div>
								</td>
								<td>
									<div class=" btn-activity-data-home-page"
										id="total_members_this_month">0</div>
								</td>
		
							</tr>
							<tr>
								<td>
									<div class=" btn-home-title">
										<span style="width: 50px; float: left"><img alt=""
											style="margin-left: -2%;"
											src="${pageContext.servletContext.contextPath}/static/pulse/img/pulse/at-risk.png"
											height="35"></span>High Risk
									</div>
								</td>
								<td>
									<div class=" btn-activity-data-home-page"
										id="total_at_risk_members_today">0</div>
								</td>
								<td>
									<div class=" btn-activity-data-home-page"
										id="total_at_risk_members_this_month">0</div>
								</td>
		
							</tr>
		
							<tr>
								<td>
									<div class=" btn-home-title">
										<span style="width: 50px; float: left"><img alt=""
											style="margin-left: -2%;"
											src="${pageContext.servletContext.contextPath}/static/pulse/img/interception/interception-flag-grey.png"
											height="35"></span>Intercepted
									</div>
								</td>
								<td>
									<div class=" btn-activity-data-home-page"
										id="talked_to_members_today">0</div>
								</td>
								<td>
									<div class=" btn-activity-data-home-page"
										id="talked_to_members_this_month">0</div>
								</td>
		
							</tr>
		
		
		
						</tbody>
					</table>
		
		
					<div class="col-md-4 col-sm-4 btn-group-vertical vert-colored-div"
						id="homepage_interception_good_to_go">
						<button style="background: #27b24a;" type="button"
							class="btn btn-default">
							<img alt=""
								src="${pageContext.servletContext.contextPath}/static/pulse/img/interception/RD_GOODTOGO_WHITE_2.0x.png"
								height="35"> <span class="number-desc-left">Good to
								Go<br>&nbsp
							</span>
		
						</button>
						<button style="background: #27b24a;" type="button"
							class="btn btn-default">
							<span class="big-number" id="good_to_go_members_today">0</span> <span
								class="number-desc-right">Today</span>
						</button>
						<button style="background: #27b24a;" type="button"
							class="btn btn-default">
							<span class="big-number" id="good_to_go_members_this_month">0</span>
							<span class="number-desc-right">Month</span>
						</button>
		
					</div>
		
					<div class="col-md-4 col-sm-4  btn-group-vertical vert-colored-div"
						id="homepage_interception_need_follow_up">
						<button style="background: #f9ae04;" type="button"
							class="btn btn-default">
							<img alt=""
								src="${pageContext.servletContext.contextPath}/static/pulse/img/interception/RD_NEEDFOLLOWUP_WHITE_2.0x.png"
								height="35"> <span class="number-desc-left">Need<br>Follow-up
							</span>
						</button>
						<button style="background: #f9ae04;" type="button"
							class="btn btn-default">
							<span class="big-number" id="need_follow_up_members_today">0</span>
							<span class="number-desc-right">Today</span>
						</button>
						<button style="background: #f9ae04;" type="button"
							class="btn btn-default">
							<span class="big-number" id="need_follow_up_members_this_month">0</span>
							<span class="number-desc-right">Month</span>
						</button>
		
					</div>
		
					<div class="col-md-4 col-sm-4  btn-group-vertical vert-colored-div"
						id="homepage_interception_need_attention">
						<button style="background: #f03224;" type="button"
							class="btn btn-default">
							<img alt=""
								src="${pageContext.servletContext.contextPath}/static/pulse/img/interception/RD_NEEDATTENTION_WHITE_2.0x.png"
								height="35"> <span class="number-desc-left">Need<br>Attention
							</span>
						</button>
						<button style="background: #f03224;" type="button"
							class="btn btn-default">
							<span class="big-number" id="need_attention_members_today">0</span>
							<span class="number-desc-right">Today</span>
						</button>
						<button style="background: #f03224;" type="button"
							class="btn btn-default">
							<span class="big-number" id="need_attention_members_this_month">0</span>
							<span class="number-desc-right">Month</span>
						</button>
		
		
					</div>
		
		
				</div>
			</div>
		</div>
	</jsp:body>
</ui:baseview>
