<%@taglib prefix="ui" tagdir="/WEB-INF/tags/beat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<ui:beatbaseview contextPath="" pageTitle="ReunifyBeat" pageName="pulsePageView">
	<jsp:attribute name="headTag">
	<!--  Add header files here  -->
	<script src="js/commentTable.js"></script>
	<script src="js/jquery/tables/Scroller/js/dataTables.scroller.min.js"></script>
	</jsp:attribute>
	<jsp:body>
	<div class="row" style="padding-right: 4%; padding-top: 10px">
		
		<!--row1 in content container - starts-->
		<table id="commentTable">
			<thead>
			<tr>
				<th>Id</th>
				<th>Author</th>
				<th>Content</th>
				<th>Location Name</th>
				<th>Status</th>
				<th>Time</th>
			</tr>
			</thead>
			<tbody>
			<c:forEach items="${commentList}" var="comment">
				<tr>
					<td>${comment.recordId}</td>
					<td>${comment.commentAuthor}</td>
					<td>${comment.commentMessage}</td>
					<td>
					<span id="status-${comment.recordId}">
						<c:choose>
							<c:when test="${comment.resolved}">
								resolved
							</c:when>
							<c:otherwise>
								unresolved
							</c:otherwise>
						</c:choose>
					</span> <button id="resolve-${comment.recordId}" type="button" onclick="resolveComment(this.id);" <c:if test="${comment.resolved}">disabled</c:if>>Resolve</button>
					</td>
					<td>${comment.locationName}</td>
					<td>${comment.messageTime}</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</div>
	</jsp:body>
</ui:beatbaseview>

