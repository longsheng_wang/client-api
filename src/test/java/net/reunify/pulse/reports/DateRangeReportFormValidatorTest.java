/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

/**
 * @author Vivek Hungund
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/spring-security.xml",
		"file:src/test/webapp/WEB-INF/mvc-dispatcher-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class DateRangeReportFormValidatorTest {
	private static final Logger logger = Logger.getLogger(DateRangeReportFormValidatorTest.class);

	/**
	 * Test for invalid start date
	 */
	@Test
	public void testInvalidStartDate() {
		DateRangeReportForm form = new DateRangeReportForm("2015-01-35", "2015-03-04");
		Errors errors = new BeanPropertyBindingResult(form, "form");

		DateRangeReportFormValidator validator = new DateRangeReportFormValidator();
		validator.validate(form, errors);

		assertTrue(errors.hasErrors());
		assertEquals("invalid_date_format", errors.getAllErrors().get(0).getCode());
	}
	

	/**
	 * Test for invalid end date
	 */
	@Test
	public void testInvalidEndDate() {
		DateRangeReportForm form = new DateRangeReportForm("2015-03-04", "2015-01-35");
		Errors errors = new BeanPropertyBindingResult(form, "form");
		
		DateRangeReportFormValidator validator = new DateRangeReportFormValidator();
		validator.validate(form, errors);
		assertTrue(errors.hasErrors());
		assertEquals("invalid_date_format", errors.getAllErrors().get(0).getCode());

	}

	/**
	 * Test when start date comes after end date
	 */
	@Test
	public void testStartDateAfterEndDate() {
		DateRangeReportForm form = new DateRangeReportForm("2015-01-25", "2015-01-04");
		Errors errors = new BeanPropertyBindingResult(form, "form");
		
		DateRangeReportFormValidator validator = new DateRangeReportFormValidator();
		validator.validate(form, errors);

		assertTrue(errors.hasErrors());
		assertEquals("invalid_date_range", errors.getAllErrors().get(0).getCode());
	}

	/**
	 * Test that no errors occur with valid input
	 */
	@Test
	public void testSuccess() {
		DateRangeReportForm form = new DateRangeReportForm("2015-01-01", "2015-01-04");
		Errors errors = new BeanPropertyBindingResult(form, "form");

		DateRangeReportFormValidator validator = new DateRangeReportFormValidator();
		validator.validate(form, errors);

		assertTrue(!errors.hasErrors());
	}
}
