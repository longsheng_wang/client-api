/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;

import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * Holds one row of data for the Daily Report
 * 
 * @author Vivek Hungund
 *
 */
public class InterceptProgressReportDataRow {
	private static final Logger logger = Logger.getLogger(InterceptProgressReportDataRow.class);

	private String locationName;

	private int weeklyInterceptGoal;

	private Map<Date, Integer> interceptCount;

	private int remainingIntercepts;

	private int weeklySurveyGoal;

	private Map<Date, Integer> surveyCount;

	private int remainingSurveys;
	
	private boolean hasKiosk;

	/**
	 * @param locationName
	 * @param weeklyInterceptGoal
	 * @param interceptCount
	 * @param remainingIntercepts
	 * @param weeklySurveyGoal
	 * @param surveyCount
	 * @param remainingSurveys
	 * @param hasKiosk
	 */
	public InterceptProgressReportDataRow(String locationName, int weeklyInterceptGoal, Map<Date, Integer> interceptCount,
								int remainingIntercepts, int weeklySurveyGoal, Map<Date, Integer> surveyCount,
								int remainingSurveys, boolean hasKiosk) {
		this.locationName = locationName;
		this.weeklyInterceptGoal = weeklyInterceptGoal;
		this.interceptCount = interceptCount;
		this.remainingIntercepts = remainingIntercepts;
		this.weeklySurveyGoal = weeklySurveyGoal;
		this.surveyCount = surveyCount;
		this.remainingSurveys = remainingSurveys;
		this.setHasKiosk(hasKiosk);
	}
	
	

	/**
	 * Default Constructor
	 */
	public InterceptProgressReportDataRow() {
		super();
	}



	/**
	 * @return the locationName
	 */
	public String getLocationName() {
		return locationName;
	}

	/**
	 * @param locationName
	 *            the locationName to set
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	/**
	 * @return the weeklyInterceptGoal
	 */
	public int getWeeklyInterceptGoal() {
		return weeklyInterceptGoal;
	}

	/**
	 * @param weeklyInterceptGoal
	 *            the weeklyInterceptGoal to set
	 */
	public void setWeeklyInterceptGoal(int weeklyInterceptGoal) {
		this.weeklyInterceptGoal = weeklyInterceptGoal;
	}

	/**
	 * @return the interceptCount
	 */
	public Map<Date, Integer> getInterceptCount() {
		return interceptCount;
	}

	/**
	 * @param interceptCount
	 *            the interceptCount to set
	 */
	public void setInterceptCount(Map<Date, Integer> interceptCount) {
		this.interceptCount = interceptCount;
	}

	/**
	 * @return the remainingIntercepts
	 */
	public int getRemainingIntercepts() {
		return remainingIntercepts;
	}

	/**
	 * @param remainingIntercepts
	 *            the remainingIntercepts to set
	 */
	public void setRemainingIntercepts(int remainingIntercepts) {
		this.remainingIntercepts = remainingIntercepts;
	}

	/**
	 * @return the weeklySurveyGoal
	 */
	public int getWeeklySurveyGoal() {
		return weeklySurveyGoal;
	}

	/**
	 * @param weeklySurveyGoal
	 *            the weeklySurveyGoal to set
	 */
	public void setWeeklySurveyGoal(int weeklySurveyGoal) {
		this.weeklySurveyGoal = weeklySurveyGoal;
	}

	/**
	 * @return the surveyCount
	 */
	public Map<Date, Integer> getSurveyCount() {
		return surveyCount;
	}

	/**
	 * @param surveyCount
	 *            the surveyCount to set
	 */
	public void setSurveyCount(Map<Date, Integer> surveyCount) {
		this.surveyCount = surveyCount;
	}

	/**
	 * @return the remainingSurveys
	 */
	public int getRemainingSurveys() {
		return remainingSurveys;
	}

	/**
	 * @param remainingSurveys
	 *            the remainingSurveys to set
	 */
	public void setRemainingSurveys(int remainingSurveys) {
		this.remainingSurveys = remainingSurveys;
	}
	
	/**
	 * @return the hasKiosk
	 */
	public boolean hasKiosk() {
		return hasKiosk;
	}

	/**
	 * @param hasKiosk the hasKiosk to set
	 */
	public void setHasKiosk(boolean hasKiosk) {
		this.hasKiosk = hasKiosk;
	}
}
