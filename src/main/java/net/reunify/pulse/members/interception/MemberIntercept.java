/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members.interception;

/**
 * Implementation of MemberInterception - Client: Default
 * 
 * @author kiana.baradaran
 * 
 */

public class MemberIntercept {
	private int interceptId;
	private String interceptStartTime;
	private String interceptEndTime;
	private double interceptDuration;
	private int interceptStatus;
	private int locationId;
	private int interceptorId;
	private int memberId;
	private String interceptNote;
	private int attritionScore;

	public MemberIntercept() {
		// TODO Auto-generated constructor stub
	}

	public MemberIntercept(int interceptId, String interceptStartTime, int interceptStatus, int locationId,
							int interceptorId, int memberId, String interceptNote, int attritionScore) {

		this.setInterceptId(interceptId);
		this.setInterceptStartTime(interceptStartTime);
		this.setInterceptStatus(interceptStatus);
		this.setLocationId(locationId);
		this.setInterceptorId(interceptorId);
		this.setMemberId(memberId);
		this.setInterceptNote(interceptNote);
		this.setAttritionScore(attritionScore);

	}

	public int getInterceptId() {
		return interceptId;
	}

	public void setInterceptId(int interceptId) {
		this.interceptId = interceptId;
	}

	public int getInterceptStatus() {
		return interceptStatus;
	}

	public void setInterceptStatus(int interceptStatus) {
		this.interceptStatus = interceptStatus;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public int getInterceptorId() {
		return interceptorId;
	}

	public void setInterceptorId(int interceptorId) {
		this.interceptorId = interceptorId;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public String getInterceptNote() {
		return interceptNote;
	}

	public void setInterceptNote(String interceptNote) {
		this.interceptNote = interceptNote;
	}

	public int getAttritionScore() {
		return attritionScore;
	}

	public void setAttritionScore(int attritionScore) {
		this.attritionScore = attritionScore;
	}

	public String getInterceptStartTime() {
		return interceptStartTime;
	}

	public void setInterceptStartTime(String interceptStartTime) {
		this.interceptStartTime = interceptStartTime;
	}

	public String getInterceptEndTime() {
		return interceptEndTime;
	}

	public void setInterceptEndTime(String interceptEndTime) {
		this.interceptEndTime = interceptEndTime;
	}

	public double getInterceptDuration() {
		return interceptDuration;
	}

	public void setInterceptDuration(double interceptDuration) {
		this.interceptDuration = interceptDuration;
	}

	@Override
	public String toString() {
		return "MemberIntercept [interceptId=" + interceptId + ", interceptStartTime=" + interceptStartTime
				+ ", interceptEndTime=" + interceptEndTime + ", interceptDuration=" + interceptDuration
				+ ", interceptStatus=" + interceptStatus + ", locationId=" + locationId + ", interceptorId="
				+ interceptorId + ", memberId=" + memberId + ", interceptNote=" + interceptNote + ", attritionScore="
				+ attritionScore + "]";
	}
	
	

}
