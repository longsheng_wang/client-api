-- table for storing users' daily performance KPIs
CREATE TABLE users_daily_stats(
user_id INT REFERENCES users(user_id) NOT NULL
,report_date DATE NOT NULL DEFAULT now()::DATE
,intercepts_done INT DEFAULT 0
,added_to_queue INT DEFAULT 0
,removed_from_queue INT DEFAULT 0
,PRIMARY KEY (user_id,report_date)
);

-- creating indices for foreign keys
CREATE INDEX IX_users_daily_stats_user_id ON users_daily_stats(user_id);

-- Granting permissions
GRANT SELECT
	,INSERT
	,UPDATE
	,DELETE
	ON users_daily_stats
	TO ltfdashboard;	