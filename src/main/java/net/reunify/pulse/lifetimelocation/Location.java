/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.lifetimelocation;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * Stores information about a location
 * 
 * @author Vivek Hungund
 *
 */
@Component
public class Location {
	private static final Logger logger = Logger.getLogger(Location.class);
	
	private int locationId;
	private String locationName;
	
	/**
	 * Empty constructor
	 */
	public Location() {
		
	}
	
	/**
	 * Overloaded Constructor
	 * @param locationId
	 * @param locationName
	 */
	public Location(int locationId, String locationName) {
		this.locationId = locationId;
		this.locationName = locationName;
	}
	
	/**
	 * Get this location's locationId
	 * @return the locationId
	 */
	public int getLocationId() {
		return locationId;
	}
	/**
	 * Set this location's locationId
	 * @param locationId the locationId to set
	 */
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
	/**
	 * Get this location's locationName
	 * @return the locationName
	 */
	public String getLocationName() {
		return locationName;
	}
	/**
	 * Set this location's locationName
	 * @param locationName the locationName to set
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	
	
}
