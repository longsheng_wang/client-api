/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.authentication;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

/**
 * Implementation of UserDetailsService that takes care of user authentication
 * 
 * @author kiana.baradaran
 * 
 */
@Service
public class JdbcUserDetailsService implements UserDetailsService {

	private static final Logger logger = LoggerFactory.getLogger(JdbcUserDetailsService.class);

	UsernameValidator validator = new UsernameValidator();

	@Inject
	@Named("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@Override
	public UserDetails loadUserByUsername(String username) {

		logger.info(username);
		if (!validator.validate(username.toLowerCase())) {
			throw new InvalidUsernameException("No such user: " + username);
		}

		String selectUser =
				"select id::int,(firstname||' '||lastname)::text as name, username::text,password, t1.clubcode, t1.location_id "
						+ " from ltm_users t1 left join ltm_clubs t2 on t1.location_id = t2.mmsclubid::int where lower(username)=lower(?);";

		User user = this.jdbcTemplate.queryForObject(selectUser, new Object[] { username }, new RowMapper<User>() {
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				List<SimpleGrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));

				User user = new User(rs.getString("username"), "ReunifyTest278", authorities);

				return user;
			}
		});

		return user;

	}

}
