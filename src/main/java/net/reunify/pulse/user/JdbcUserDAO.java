/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import net.reunify.pulse.base.InvalidInputFormat;

import org.apache.log4j.Logger;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 * JDBC implementation of UserDAO
 * 
 * @author kiana.baradaran
 * 
 */
@Component
public class JdbcUserDAO implements UserDAO {

	private static final Logger logger = Logger.getLogger(JdbcUserDAO.class);

	@Inject
	@Named("pulseJdbcTemplate")
	private JdbcTemplate pulseJdbcTemplate;

	@Override
	public int createUserAccount(UserDTO userDTO) {

		String insertUser =
				"insert into users(location_id,client_user_id,firstname,lastname,username,email_address,password,is_active,user_role) values (?,?,?,?,?,?,md5(?),true,?) returning user_id;";

		int userId = 0;
		try {
			userId =
					this.pulseJdbcTemplate.queryForObject(insertUser,
							new Object[] { userDTO.getLocationId(), userDTO.getUserId(), userDTO.getFirstName(),
									userDTO.getLastName(), userDTO.getEmailAddress(), userDTO.getEmailAddress(),
									userDTO.getPassword(), userDTO.getRole().getRoleId() }, new RowMapper<Integer>() {
								public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
									return rs.getInt(1);
								}

							});
		} catch (DuplicateKeyException e) {
			throw new UserAccountAlreadyExistsException(
														"An account associated with this email address already exists.",
														e);
		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat(
											"The account could not be added. Please check the input data and try again. StackTrace: "
													+ e.getMessage());
		}

		return userId;
	}

	@Override
	public boolean removeUserAccount(int userId, int clientId) {
		String deleteUser =
				"update users set is_active = false where user_id=? and "
						+ "EXISTS (select 1 from users join client_locations on users.location_id = client_locations.location_id where user_id <> ? and client_id =? and users.is_active = true and user_role =1);";

		int deletedRows = 0;
		boolean success = false;

		deletedRows = pulseJdbcTemplate.update(deleteUser, new Object[] { userId, userId, clientId });
		if (deletedRows != 0) {
			success = true;
			logger.info("Deleting account was successful");
		} else {
			logger.error("Deleting account failed");
		}
		return success;
	}

	@Override
	public boolean changePassword(UserDTO userDTO, int clientId) {

		String updateUser =
				"update users set password=md5(?) where user_id = ? and password = md5(?) and "
						+ "EXISTS (select 1 from users join client_locations on users.location_id = client_locations.location_id where user_id = ? and client_id =?);";
		int updatedRows = 0;
		boolean success = false;
		updatedRows =
				pulseJdbcTemplate.update(
						updateUser,
						new Object[] { userDTO.getPassword(), userDTO.getUserId(), userDTO.getOldPassword(),
								userDTO.getUserId(), clientId });

		if (updatedRows != 0) {
			success = true;
			logger.info("The password was changed successfully");
		} else {
			logger.error("The password could not be changed.");
		}
		return success;
	}

	@Override
	public boolean editUserAccount(UserDTO userDTO, int clientId) {

		String updateUser =
				"update users set location_id=?,client_user_id=?,firstname=?,lastname=?,email_address=?,username=?,user_role= ? where user_id = ? and "
						+ "EXISTS (select 1 from users join client_locations on users.location_id = client_locations.location_id where user_id = ? and client_id =?);";

		int updatedRows = 0;
		boolean success = false;
		updatedRows =
				pulseJdbcTemplate.update(updateUser,
						new Object[] { userDTO.getLocationId(), userDTO.getClientUserId(), userDTO.getFirstName(),
								userDTO.getLastName(), userDTO.getEmailAddress(), userDTO.getEmailAddress(),
								userDTO.getRole().getRoleId(), userDTO.getUserId(), userDTO.getUserId(), clientId });

		if (updatedRows != 0) {
			success = true;
			logger.info("updating account was successful");
		} else {
			logger.error("updating account failed");
		}
		return success;
	}

	@Override
	public List<UserDTO> getUserAccounts(int clientId) {
		List<UserDTO> users = new ArrayList<UserDTO>();
		String selectUsers =
				"select user_id,users.location_id,client_user_id,firstname,lastname,email_address,location_name, role_name,user_role from users join client_locations on users.location_id = client_locations.location_id join user_roles_lookup on user_roles_lookup.record_id= users.user_role where users.is_active = true and client_id = ?";

		List<Map<String, Object>> rows = this.pulseJdbcTemplate.queryForList(selectUsers, new Object[] { clientId });

		for (Map<String, Object> row : rows) {
			UserDTO user =
					new UserDTO((Integer) row.get("user_id"), (String) row.get("firstname"),
								(String) row.get("lastname"), (String) row.get("email_address"),
								(String) row.get("client_user_id"), (Integer) row.get("location_id"),
								(String) row.get("location_name"), new UserRole((Integer) row.get("user_role"),
																				(String) row.get("role_name")));

			users.add(user);
		}

		return users;
	}

}
