/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;

/**
 * enum for labeling ClubPerformanceReportGroup
 * 
 * @author Vivek Hungund
 *
 */

public enum ClubPerformanceReportGroup {
	HIGH_RISK (0),
	MED_RISK (1),
	NEW (2),
	TOTAL (3)
	;
	
	private int displayOrder;
	
	private ClubPerformanceReportGroup (int displayOrder) {
		this.displayOrder = displayOrder;
	}
	
	public int getDisplayOrder() {
		return this.displayOrder;
	}
}