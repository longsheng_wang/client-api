/* 
 * Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
//
//function check_login_status(event, data) {
//    var c1 = localStorage.getItem('club_code');
//    var c2 = sessionStorage.getItem('club_code');
//    //console.log('c1 '+c1);
//    //console.log('c2 '+c2);
//    //console.log(reset_date);
//    //console.log(typeof reset_date === "undefined");
//    //console.log(jQuery(location).attr('href'));
//    //console.log($(location).attr('hash'));
//    var hash = window.location.hash;
////    if ((c1 === null || c2 === null || typeof reset_date === "undefined")) {
//    if (c1 === null || c2 === null) {
//
//        if (hash !== '' && hash !== '#login_page') {
//            //console.log('Change Page');
//            //console.log(event); 
//            //console.log(data);
//            event.stopImmediatePropagation();
//            event.stopPropagation();
//            event.preventDefault();
//            //data.deferred.reject( data.absUrl, data.options );
//            window.location.replace('index.html');
//            //window.location.replace('index.fake.html');
//        } else {
////            console.log('No Change Page');
//        }
//    }
//}


function check_date_status() {
    var c1 = localStorage.getItem('club_code');
    var c2 = sessionStorage.getItem('club_code');
    //console.log('c1 '+c1);
    //console.log('c2 '+c2);
    if (c1 === null || c2 === null) {
        $.mobile.changePage("#login_page");
    }
}

function login() {

    $.ajax({url: 'jsp/auth/CheckUsername.jsp',
        data: {action: 'login', username: $('#username').val(), password: $('#password').val()}, // Convert a form to a JSON string representation
        type: 'POST',
        async: true,
        dataType: 'json',
        beforeSend: function() {
            // This callback function will trigger before data is sent
            $.mobile.showPageLoadingMsg(true); // This will show ajax spinner
        },
        complete: function() {
            // This callback function will trigger on data sent/received complete
            $.mobile.hidePageLoadingMsg(); // This will hide ajax spinner
        },
        success: function(data) {
            //sessionStorage.clear();
//            var obj = jQuery.parseJSON(result);
            if (jQuery.isEmptyObject(data)) {
                $("#errorpopup").popup("open");
            } else {

                var query = window.location.search.substring(1);
                var session_id = query.substring(query.indexOf("ssoSessionId=") + 13);
                localStorage.setItem('session_id', session_id);

//                console.log(window.location.search);
//                console.log(session_id);
//                var session_id = "7dea643d-5c6f-4df6-ab0f-42e94eb085d9";
                sessionStorage.setItem('name', data.name);
                sessionStorage.setItem('club_code', data.club);
                sessionStorage.setItem('location_id', data.id);
//                sessionStorage.setItem('username', obj.username);
//                sessionStorage.setItem('name', obj.name);
//                sessionStorage.setItem('datetime', data.datetime);
//                sessionStorage.setItem('date', data.date);
                sessionStorage.setItem('clubname', data.clubname);

                localStorage.setItem('club_code', data.club);
                localStorage.setItem('location_id', data.id);
                localStorage.setItem('name', data.name);
                localStorage.setItem("latest_record", "0");
                localStorage.setItem('lastValidation', new Date().getTime());

//                console.log(localStorage.getItem("latest_record"));
//                localStorage.setItem('datetime', data.datetime);
//                localStorage.setItem('date', data.date);
                localStorage.setItem('clubname', data.clubname);
                $.mobile.changePage("#homepage");
                //console.log(obj.datestr);
            }
            //resultObject.formSubmitionResult = result;
//            dateSet(obj.datestr);
            //$.mobile.changePage("#clbinfopg");
            // $.mobile.changePage("#pulsepg");

        },
        error: function(request, error) {
            // This callback function will trigger on unsuccessful action                
            alert('Network error has occurred please try again!');
        }
    });
}
//
//function debug_login(page) {
//    $.ajax({url: 'jsp/auth/CheckUsername.jsp',
//        data: {action: 'login', username: 'jafar', password: 'jafar'}, // Convert a form to a JSON string representation
//        type: 'POST',
//        async: false,
//        beforeSend: function() {
//            // This callback function will trigger before data is sent
////            $.mobile.showPageLoadingMsg(true); // This will show ajax spinner
//        },
//        complete: function() {
//            // This callback function will trigger on data sent/received complete
////            $.mobile.hidePageLoadingMsg(); 
//        },
//        success: function(result) {
//            //sessionStorage.clear();
//            var obj = jQuery.parseJSON(result);
//            if (jQuery.isEmptyObject(obj)) {
//                $("#errorpopup").popup("open");
//            } else {
//                sessionStorage.setItem('gmname', obj.name);
//                sessionStorage.setItem('club_code', obj.club);
//                sessionStorage.setItem('username', obj.username);
//                sessionStorage.setItem('name', obj.name);
//                sessionStorage.setItem('datetime', obj.datetime);
//                sessionStorage.setItem('date', obj.date);
//                sessionStorage.setItem('clubname', obj.clubname);
//                localStorage.setItem('club_code', obj.club);
//                localStorage.setItem('datetime', obj.datetime);
//                localStorage.setItem('date', obj.date);
//                localStorage.setItem('clubname', obj.clubname);
//                //console.log(obj.datestr);
//                dateSet(obj.datestr);
//                $.mobile.changePage(page);
//            }
//            //resultObject.formSubmitionResult = result;
//            //$.mobile.changePage("#second");
//        },
//        error: function(request, error) {
//            // This callback function will trigger on unsuccessful action                
//            alert('Network error has occurred please try again!');
//        }
//    });
//}