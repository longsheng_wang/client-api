/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.inject.Inject;
import javax.inject.Named;

import net.reunify.pulse.base.InvalidInputFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 * 
 * @author kiana.baradaran
 * 
 */
public class JdbcMemberCheckInDAO implements MemberCheckInDAO {

	private static final Logger logger = LoggerFactory.getLogger(JdbcMemberCheckInDAO.class);

	@Inject
	@Named("pulseJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@Override
	public void insertMemberCheckIn(MemberCheckInDTO memberCheckInDTO) {
		int locationId = 0;
		String insertMemberCheckIn =
				"INSERT INTO member_check_in (client_member_id,location_id,check_in_time) values (?,?,?::timestamp);";

		String selectLocationId =
				"Select location_id from client_locations where client_location_id = ? and client_id = ?";

		String selectLocationIdForClient = "Select location_id from client_locations where client_id = ?";

		try {

			if (memberCheckInDTO.getClientLocationId() == null
					|| memberCheckInDTO.getClientLocationId().trim().equals("")) {
				locationId =
						this.jdbcTemplate.queryForObject(selectLocationIdForClient,
								new Object[] { memberCheckInDTO.getClientId() }, new RowMapper<Integer>() {
									public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
										return rs.getInt(1);
									}
								});
			} else {
				locationId =
						this.jdbcTemplate.queryForObject(
								selectLocationId,
								new Object[] { memberCheckInDTO.getClientLocationId(), memberCheckInDTO.getClientId() },
								new RowMapper<Integer>() {
									public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
										return rs.getInt(1);
									}
								});
			}
			memberCheckInDTO.setLocationId(locationId);

			this.jdbcTemplate.update(insertMemberCheckIn, new Object[] { memberCheckInDTO.getClientMemberId(),
					memberCheckInDTO.getLocationId(), memberCheckInDTO.getCheckInTime() });

		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat("Check-in entry " + memberCheckInDTO
					+ " is not valid. Please check the input data and try again. StackTrace: " + e.getMessage());
		} catch (EmptyResultDataAccessException e) {
			throw new InvalidInputFormat("Client location ID is missing from check-in entry: " + memberCheckInDTO);
		}
	}

}
