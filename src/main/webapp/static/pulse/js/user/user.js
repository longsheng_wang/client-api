$(document).on(
		'click',
		".user-delete",
		function() {
			window.parent.location = ctx + "/user/removeaccount/"
					+ $(this).attr("data");
		});

$(document).on('click', ".user-edit", function() {

	window.parent.location = ctx + "/user/edit?userId=" + $(this).attr("data");
});

$(document).on('click', ".user-add", function() {
	window.parent.location = ctx + "/user/register";
});

$(document).on(
		'click',
		".change-password",
		function() {
			window.parent.location = ctx + "/user/changepassword?userId="
					+ $(this).attr("data");
		});