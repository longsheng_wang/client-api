/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.authentication;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This module validates username against the specified criteria using regular
 * expression
 * 
 * @author kiana.baradaran
 * 
 */
public class UsernameValidator {

	private Pattern pattern;
	private Matcher matcher;

	private static final String USERNAME_PATTERN = "^[a-z0-9_-]{3,15}$";

	public UsernameValidator() {
		pattern = Pattern.compile(USERNAME_PATTERN);
	}

	/**
	 * Validate username with regular expression
	 * 
	 * @param username
	 *            username for validation
	 * @return true valid username, false invalid username
	 */
	public boolean validate(final String username) {

		matcher = pattern.matcher(username);
		return matcher.matches();

	}
}