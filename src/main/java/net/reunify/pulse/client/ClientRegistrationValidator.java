/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.client;

import net.reunify.pulse.user.UserDTO;
import net.reunify.pulse.user.UserRegistrationValidator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class ClientRegistrationValidator implements Validator {

	@Override
	public boolean supports(Class<?> target) {
		return ClientDTO.class.isAssignableFrom(target);

	}

	@Override
	public void validate(Object target, Errors errors) {

		ClientDTO client = (ClientDTO) target;
		LocationRegistrationValidator locationValidator = new LocationRegistrationValidator();
		if (client.getLocations() != null) {
			for (int i = 0; i < client.getLocations().size(); i++) {
				locationValidator.validateName(client.getLocations().get(i), errors, "locations[" + i + "].name");
				locationValidator.validateTimeZone(client.getLocations().get(i), errors, "locations[" + i
						+ "].timeZone");
				locationValidator.validateClientLocationId(client.getLocations().get(i), errors, "locations[" + i
						+ "].clientLocationId");
			}
		}
		validateIndustryName(client, errors, "industryName");
		validateCompanyName(client, errors, "companyName");
		if (client.getAccountAdmin() != null) {
			UserRegistrationValidator userValidator = new UserRegistrationValidator();
			UserDTO user = client.getAccountAdmin();
			userValidator.validateFirstName(user, errors, "accountAdmin.firstName");
			userValidator.validateLastName(user, errors, "accountAdmin.lastName");
			userValidator.validateEmailAddress(user, errors, "accountAdmin.emailAddress");
			userValidator.validatePassword(user, errors, "accountAdmin.password", "accountAdmin.matchingPassword");
			userValidator.validateClientUserId(user, errors, "accountAdmin.clientUserId");
		}
	}

	void validateIndustryName(ClientDTO client, Errors errors, String industryNameFieldPath) {
		if (("NONE").equals(client.getIndustryName())) {
			errors.rejectValue(industryNameFieldPath, "industryName_required", "Please select an industry name.");
		}
	}

	void validateCompanyName(ClientDTO client, Errors errors, String companyNameFieldPath) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, companyNameFieldPath, "companyName_required",
				"Company Name is required.");
		if (client.getCompanyName() != null) {
			if (client.getCompanyName().length() > 100) {
				errors.rejectValue(companyNameFieldPath, "incorrect_companyName",
						"Company name should not be longer than 100 characters.");
			}
		}
	}

}
