<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<ui:loginview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Pulse - Reunify Login">
	<jsp:attribute name="headTag">
	<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/login/reunify-login.css"
			rel="stylesheet">
	</jsp:attribute>
	<jsp:body>
	
<div class="wrapper">
    <form class="form-signin"
				ACTION="${pageContext.servletContext.contextPath}/j_spring_security_check"
				METHOD=POST>       
      <input type="text" class="form-control" name="username"
					placeholder="Username" required="" autofocus="" />
      <input type="password" class="form-control" name="password"
					placeholder="Password" required="" />
					<a href="${pageContext.servletContext.contextPath}/client/registration">Don't have an account? Register here!</a>      
      <button class="btn btn-lg btn-primary btn-block submit-btn"
					type="submit">Login</button>   
    </form>
  </div>
	 
	</jsp:body>
</ui:loginview>