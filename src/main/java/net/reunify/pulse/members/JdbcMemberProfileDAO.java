/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import net.reunify.pulse.base.InvalidInputFormat;
import net.reunify.pulse.base.RecordNotFoundException;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 * JDBC Implementation of MemberProfileDAO
 * 
 * @author kiana.baradaran
 * 
 */
@Component
public class JdbcMemberProfileDAO implements MemberProfileDAO {

	@Inject
	@Named("pulseJdbcTemplate")
	private JdbcTemplate pulseJdbcTemplate;

	@Override
	public MemberProfile getMemberProfile(int memberId, int clientId) {
		String clientMemberId = null;
		String selectmemberId =
				"SELECT client_member_id FROM members join client_locations on members.location_id = client_locations.location_id WHERE member_id = ? and client_id = ?; ";
		try {
			clientMemberId =
					this.pulseJdbcTemplate.queryForObject(selectmemberId, new Object[] { memberId, clientId },
							new RowMapper<String>() {
								public String mapRow(ResultSet rs, int rowNum) throws SQLException {
									return rs.getString("client_member_id");
								}
							});
		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat("The input format is not valid. memberId: " + memberId + " . StackTrace: "
					+ e.getMessage());

		} catch (EmptyResultDataAccessException e) {
			throw new RecordNotFoundException("No member with ID " + memberId + " for client " + clientId
					+ " was found. StackTrace: " + e.getMessage());
		}

		DefaultMemberProfile profile = new DefaultMemberProfile();
		MemberPersonalInfo info = getMemberPersonalInfo(memberId);
		MemberAttributes attributes = getMemberAttributes(memberId);
		profile.setId(clientMemberId);
		profile.setMemberPersonalInfo(info);
		profile.setMemberAttributes(attributes);
		return profile;
	}

	private MemberAttributes getMemberAttributes(int memberId) {
		// TODO Implement different attributes based on the industry
		String selectAttributes =
				"SELECT membership_status,membership_junior_members,membership_type_description,member_join_date,member_type,member_status FROM members_fitness WHERE member_id = ?";

		List<MemberAttribute<?>> attributes = new ArrayList<MemberAttribute<?>>();

		try {
			List<Map<String, Object>> rows =
					this.pulseJdbcTemplate.queryForList(selectAttributes, new Object[] { memberId });

			for (Map<String, Object> row : rows) {
				attributes.add(new MemberAttribute<String>("membershipStatus", (String) row.get("membership_status")));
				attributes.add(new MemberAttribute<Integer>("membershipJuniorMembers",
															(Integer) row.get("membership_junior_members")));
				attributes.add(new MemberAttribute<String>("membershipTypeDescription",
															(String) row.get("membership_type_description")));
				attributes.add(new MemberAttribute<Date>("joinDate", (Date) row.get("member_join_date")));
				attributes.add(new MemberAttribute<String>("memberType", (String) row.get("member_type")));
				attributes.add(new MemberAttribute<String>("memberStatus", (String) row.get("member_status")));
			}
		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat("No member with the ID " + memberId + " was found. StackTrace: "
					+ e.getMessage());
		}
		MemberAttributes memberAttributes = new MemberAttributes(attributes);
		return memberAttributes;
	}

	private MemberPersonalInfo getMemberPersonalInfo(int memberId) {
		MemberPersonalInfo memberPersonalInfo = null;
		String selectPersonalInfo =
				"SELECT firstname,lastname,birthdate,gender,address,city,zipcode,client_location_id from members join client_locations on members.location_id = client_locations.location_id where member_id = ?;";

		try {

			memberPersonalInfo =
					this.pulseJdbcTemplate.queryForObject(selectPersonalInfo, new Object[] { memberId },
							new RowMapper<MemberPersonalInfo>() {
								public MemberPersonalInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
									MemberPersonalInfo memberPersonalInfo =
											new MemberPersonalInfo(rs.getString("firstname"), rs.getString("lastname"),
																	rs.getDate("birthdate"), rs.getString("gender"),
																	rs.getString("address"), rs.getString("city"),
																	rs.getString("zipcode"),
																	rs.getString("client_location_id"));

									return memberPersonalInfo;
								}
							});

			return memberPersonalInfo;

		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat(" Member ID: " + memberId + " is not valid. StackTrace: " + e.getMessage());
		} catch (EmptyResultDataAccessException e) {
			throw new RecordNotFoundException("No member with ID " + memberId + " was found. StackTrace: "
					+ e.getMessage());
		}

	}

	@Override
	public void updateMemberProfile(MemberProfile memberProfile, String memberId, int clientId) {
		String updatePersonalData =
				"UPDATE members SET firstname = ? ,lastname = ?,birthdate = ?,gender = ?,address = ?,city = ?,zipcode = ? WHERE member_id = ?::int;";
		// TODO Implement different attributes based on the industry
		String updateAttributes =
				"UPDATE members_fitness SET membership_status=?,membership_junior_members=?,membership_type_description=?,member_join_date=?,member_type=?,member_status=? WHERE member_id = ?::int;";

		try {
			MemberPersonalInfo personalInfo = memberProfile.getMemberPersonalInfo();

			this.pulseJdbcTemplate.update(updatePersonalData,
					new Object[] { personalInfo.getFirstName(), personalInfo.getLastName(),
							personalInfo.getBirthdate(), personalInfo.getGender(), personalInfo.getAddress(),
							personalInfo.getCity(), personalInfo.getZipeCode(), memberId });

			MemberAttributes memberAttributes = memberProfile.getMemberAttributes();
			List<MemberAttribute<?>> attributes = memberAttributes.getAttributes();
			Map<String, Object> attribureMap = new HashMap<String, Object>();
			for (MemberAttribute<?> attribute : attributes) {
				attribureMap.put(attribute.getAttributeName(), attribute.getAttributeValue());
			}

			this.pulseJdbcTemplate.update(
					updateAttributes,
					new Object[] {
							(String) attribureMap.get("membershipStatus"),
							(Double) attribureMap.get("membershipJuniorMembers"),
							(String) attribureMap.get("membershipTypeDescription"),
							new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.US).parse((String) attribureMap.get("joinDate")),
							(String) attribureMap.get("memberType"), (String) attribureMap.get("memberStatus"),
							memberId });

		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat(" Member Profile: " + memberProfile + " is not valid. StackTrace: "
					+ e.getMessage());
		} catch (DataAccessException e) {
			throw new InvalidInputFormat(" Member Profile: " + memberProfile + " is not valid. StackTrace: "
					+ e.getMessage());
		} catch (ParseException e) {
			throw new InvalidInputFormat(" Member Profile: " + memberProfile + " is not valid. StackTrace: "
					+ e.getMessage());
		}
	}

	@Override
	public void insertMemberProfile(MemberProfile memberProfile, int clientId) {
		DefaultMemberProfile defaultMemberProfile = (DefaultMemberProfile) memberProfile;
		String insertPersonalData =
				"INSERT INTO members(client_member_id,location_id,firstname,lastname,birthdate,gender,address,city,zipcode) values (?,(select location_id from client_locations where client_location_id = ? and client_id = ?),?,?,?,?,?,?,?) returning member_id;";
		// TODO Implement different attributes based on the industry
		String insertAttributes =
				"INSERT INTO members_fitness (member_id,membership_id,membership_status,membership_junior_members,membership_type_description,member_join_date,member_type,member_status) values (?,?,?,?::int,?,?,?,?);";

		try {
			MemberPersonalInfo personalInfo = defaultMemberProfile.getMemberPersonalInfo();
			int memberId =
					this.pulseJdbcTemplate.queryForObject(
							insertPersonalData,
							new Object[] { defaultMemberProfile.getId(), personalInfo.getLocationId(), clientId,
									personalInfo.getFirstName(), personalInfo.getLastName(),
									personalInfo.getBirthdate(), personalInfo.getGender(), personalInfo.getAddress(),
									personalInfo.getCity(), personalInfo.getZipeCode() }, new RowMapper<Integer>() {
								public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
									return rs.getInt(1);
								}

							});

			MemberAttributes memberAttributes = defaultMemberProfile.getMemberAttributes();
			List<MemberAttribute<?>> attributes = memberAttributes.getAttributes();
			Map<String, Object> attribureMap = new HashMap<String, Object>();
			for (MemberAttribute<?> attribute : attributes) {
				attribureMap.put(attribute.getAttributeName(), attribute.getAttributeValue());
			}

			this.pulseJdbcTemplate.update(
					insertAttributes,
					new Object[] {
							memberId,
							(String) attribureMap.get("membershipId"),
							(String) attribureMap.get("membershipStatus"),
							(Double) attribureMap.get("membershipJuniorMembers"),
							(String) attribureMap.get("membershipTypeDescription"),
							new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.US).parse((String) attribureMap.get("joinDate")),
							(String) attribureMap.get("memberType"), (String) attribureMap.get("memberStatus") });

		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat(" Member Profile: " + memberProfile + " is not valid. StackTrace: "
					+ e.getMessage());
		} catch (DataAccessException e) {
			throw new InvalidInputFormat(" Member Profile: " + memberProfile + " is not valid. StackTrace: "
					+ e.getMessage());
		} catch (ParseException e) {
			throw new InvalidInputFormat(" Member Profile: " + memberProfile + " is not valid. StackTrace: "
					+ e.getMessage());
		}
	}

	@Override
	public boolean validateClientMemberId(String clientMemberId, int clientId) {
		boolean valid = false;
		String select =
				"SELECT client_member_id from members join client_locations on members.location_id = client_locations.location_id WHERE client_member_id = ? and client_id = ?";
		try {

			int out =
					this.pulseJdbcTemplate.queryForObject(select, new Object[] { clientMemberId, clientId },
							new RowMapper<Integer>() {
								public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
									return rs.getInt(1);
								}

							});

			if (out > 0) {
				valid = true;
			}

		} catch (EmptyResultDataAccessException e) {
			throw new InvalidInputFormat("Client memebr ID " + clientMemberId + " for client " + clientId
					+ " was not found.");
		}
		return valid;
	}

	@Override
	public boolean validateClientLocationId(String clientLocationId, int clientId) {
		boolean valid = false;
		String select =
				"SELECT client_location_id from client_locations WHERE client_location_id = ? and client_id = ?";
		try {
			int out =
					this.pulseJdbcTemplate.queryForObject(select, new Object[] { clientLocationId, clientId },
							new RowMapper<Integer>() {
								public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
									return rs.getInt(1);
								}
							});

			if (out > 0) {
				valid = true;
			}

		} catch (EmptyResultDataAccessException e) {
			throw new InvalidInputFormat("Client location ID " + clientLocationId + " for client " + clientId
					+ " was not found.");
		}
		return valid;
	}

}
