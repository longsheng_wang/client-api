<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<ui:baseview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Trace - Club Traffic">
	<jsp:attribute name="headTag">
		<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/clubtraffic/clubtraffic.css"
			rel="stylesheet">
		<script
			src="${pageContext.servletContext.contextPath}/static/pulse/js/clubtraffic/clubtraffic.js"
			type="text/javascript"></script>
	</jsp:attribute>
	<jsp:attribute name="modalTag">	
			<div id="floor_page_no_count_modal" class="modal bs-modal-sm"
			tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
			aria-hidden="true" style="margin-top: 12%; position: absolute;">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class=" close" data-dismiss="modal"
							aria-hidden="true">×</button>
						<h4 class="modal-title">Alert</h4>

					</div>
					<div class="modal-body">Camera data is unavailable!</div>

				</div>
			</div>
		</div>
	</jsp:attribute>
	<jsp:body>
<div id="trafficpage_content" class="col-md-12 col-sm-12"
			style="height: 100%; padding: 0;">
	<div class="floorpage_filter_bar">

		<table class="floorpage_filter_bar_table" class=" col-md-10 col-sm-10">

			<tr>
				<td><span><img
								src="${pageContext.servletContext.contextPath}/static/pulse/img/clubtraffic/clock.png"
								width="35"></span></td>
				<td><span id="floorpage_filter_bar_timestamp"></span></td>
				<td><span id="floorpage_filter_bar_total_swipes"></span></td>
			</tr>
		</table>

	</div>
	<div id="floorpage_count" class=" col-md-10 col-sm-10">
		<div class="floorpage_count">
			<table id="floorpage_count_table" class="scrollable table">
				<thead>

					<tr>
						<th id="floorpage_count_table_col_1" width="20%">Zone</th>
						<th id="floorpage_count_table_col_2" width="15%"></th>
						<th id="floorpage_count_table_col_3" width="20%">Floor</th>
						<th id="floorpage_count_table_col_4" width="20%">Count (Avg)</th>
						<th id="floorpage_count_table_col_5" width="25%">Traffic
							(%Today)</th>
					</tr>
				</thead>
				<tbody>

					<tr>
						<td id="floorpage_count_table_col_1_row_1"></td>
						<td id="floorpage_count_table_col_2_row_1"></td>
						<td id="floorpage_count_table_col_3_row_1"></td>
						<td id="floorpage_count_table_col_4_row_1"></td>
						<td id="floorpage_count_table_col_5_row_1"></td>
					</tr>
					<tr>
						<td id="floorpage_count_table_col_1_row_2"></td>
						<td id="floorpage_count_table_col_2_row_2"></td>
						<td id="floorpage_count_table_col_3_row_2"></td>
						<td id="floorpage_count_table_col_4_row_2"></td>
						<td id="floorpage_count_table_col_5_row_2"></td>
					</tr>
					<tr>
						<td id="floorpage_count_table_col_1_row_3"></td>
						<td id="floorpage_count_table_col_2_row_3"></td>
						<td id="floorpage_count_table_col_3_row_3"></td>
						<td id="floorpage_count_table_col_4_row_3"></td>
						<td id="floorpage_count_table_col_5_row_3"></td>
					</tr>
					<tr>
						<td id="floorpage_count_table_col_1_row_4"></td>
						<td id="floorpage_count_table_col_2_row_4"></td>
						<td id="floorpage_count_table_col_3_row_4"></td>
						<td id="floorpage_count_table_col_4_row_4"></td>
						<td id="floorpage_count_table_col_5_row_4"></td>

					</tr>
					<tr>
						<td id="floorpage_count_table_col_1_row_5"></td>
						<td id="floorpage_count_table_col_2_row_5"></td>
						<td id="floorpage_count_table_col_3_row_5"></td>
						<td id="floorpage_count_table_col_4_row_5"></td>
						<td id="floorpage_count_table_col_5_row_5"></td>

					</tr>
					<tr>
						<td id="floorpage_count_table_col_1_row_6"></td>
						<td id="floorpage_count_table_col_2_row_6"></td>
						<td id="floorpage_count_table_col_3_row_6"></td>
						<td id="floorpage_count_table_col_4_row_6"></td>
						<td id="floorpage_count_table_col_5_row_6"></td>
					</tr>

					<tr>
						<td id="floorpage_count_table_col_1_row_7"></td>
						<td id="floorpage_count_table_col_2_row_7"></td>
						<td id="floorpage_count_table_col_3_row_7"></td>
						<td id="floorpage_count_table_col_4_row_7"></td>
						<td id="floorpage_count_table_col_5_row_7"></td>

					</tr>
					<tr>
						<td id="floorpage_count_table_col_1_row_8"></td>
						<td id="floorpage_count_table_col_2_row_8"></td>
						<td id="floorpage_count_table_col_3_row_8"></td>
						<td id="floorpage_count_table_col_4_row_8"></td>
						<td id="floorpage_count_table_col_5_row_8"></td>


					</tr>
					<tr>
						<td id="floorpage_count_table_col_1_row_9"></td>
						<td id="floorpage_count_table_col_2_row_9"></td>
						<td id="floorpage_count_table_col_3_row_9"></td>
						<td id="floorpage_count_table_col_4_row_9"></td>
						<td id="floorpage_count_table_col_5_row_9"></td>
					</tr>
					<tr>
						<td id="floorpage_count_table_col_1_row_10"></td>
						<td id="floorpage_count_table_col_2_row_10"></td>
						<td id="floorpage_count_table_col_3_row_10"></td>
						<td id="floorpage_count_table_col_4_row_10"></td>
						<td id="floorpage_count_table_col_5_row_10"></td>
					</tr>
					<tr>
						<td id="floorpage_count_table_col_1_row_11"></td>
						<td id="floorpage_count_table_col_2_row_11"></td>
						<td id="floorpage_count_table_col_3_row_11"></td>
						<td id="floorpage_count_table_col_4_row_11"></td>
						<td id="floorpage_count_table_col_5_row_11"></td>
					</tr>


				</tbody>

				<tfoot>
				</tfoot>
			</table>
		</div>
	</div>
</div>
	</jsp:body>

</ui:baseview>