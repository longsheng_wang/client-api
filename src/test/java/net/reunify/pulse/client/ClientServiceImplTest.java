/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.client;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import net.reunify.pulse.test.util.MockDatabaseRecordCreator;
import net.reunify.pulse.user.UserDTO;
import net.reunify.pulse.user.UserRole;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author kiana.baradaran
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/spring-security.xml",
		"file:src/test/webapp/WEB-INF/mvc-dispatcher-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@TransactionConfiguration(defaultRollback = true)
@Transactional(value = "pulseTxManager")
public class ClientServiceImplTest {

	private static final Logger logger = Logger.getLogger(ClientServiceImplTest.class);

	@Inject
	private ClientService clientServiceHandler;

	@Inject
	MockDatabaseRecordCreator mockDatabaseRecordCreator;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
		mockDatabaseRecordCreator.cleanUp();
	}

	@Test
	public void testAddClientValid() {
		ClientDTO client = new ClientDTO();
		client.setCompanyName("TestCompanyFromReunify");
		client.setIndustryName("Test");
		LocationDTO location = new LocationDTO();
		location.setName("TestLocationFromReunify");
		location.setTimeZone("US/PACIFIC");
		List<LocationDTO> locations = new ArrayList<LocationDTO>();
		locations.add(location);
		UserDTO user = new UserDTO();
		user.setFirstName("firstNameForTest");
		user.setLastName("lastNameForTest");
		user.setEmailAddress("a@test.com");
		user.setPassword("Testing1");
		user.setMatchingPassword("Testing1");
		user.setRole(new UserRole(1, "Admin"));
		client.setAccountAdmin(user);
		client.setLocations(locations);
		assertTrue(clientServiceHandler.createClientAccount(client) > 0);
	}

	@Test
	public void testUpdateClientValid() {
		int clientId = mockDatabaseRecordCreator.createTestClientRecord();
		ClientDTO client = new ClientDTO();
		client.setCompanyName("TestCompanyFromReunify");
		client.setIndustryName("Test");
		client.setClientId(clientId);
		assertTrue(clientServiceHandler.updateClientAccount(client));
	}

	@Test
	public void testRemoveClientAccountInvalidId() {
		assertFalse(clientServiceHandler.removeClientAccount(-1));
	}

	@Test
	public void testRemoveClientAccountValid() {
		int testClientId = mockDatabaseRecordCreator.createTestClientRecord();
		assertTrue(clientServiceHandler.removeClientAccount(testClientId));
	}

	@Test
	public void testAddLocationValid() {
		int clientId = mockDatabaseRecordCreator.createTestClientRecord();
		LocationDTO locationDTO = new LocationDTO();
		locationDTO.setClientId(clientId);
		locationDTO.setName("TestLocationFromReunify");
		locationDTO.setTimeZone("US/PACIFIC");
		int testLocationId = clientServiceHandler.addLocation(locationDTO);
		assertTrue(testLocationId > 0);
	}

	@Test
	public void testUpdateLocation() {
		int clientId = mockDatabaseRecordCreator.createTestClientRecord();
		int testLocationId = mockDatabaseRecordCreator.createTestLocationRecord(clientId);
		LocationDTO locationDTO = new LocationDTO();
		locationDTO.setName("TestLocationFromReunify");
		locationDTO.setTimeZone("US/PACIFIC");
		locationDTO.setLocationId(testLocationId);
		locationDTO.setClientId(clientId);
		assertTrue(clientServiceHandler.updateLocation(locationDTO));

	}

	@Test
	public void testRemoveLocationValid() {
		int clientId = mockDatabaseRecordCreator.createTestClientRecord();
		int testLocationId = mockDatabaseRecordCreator.createTestLocationRecord(clientId);
		LocationDTO locationDTO = new LocationDTO();
		locationDTO.setClientId(clientId);
		locationDTO.setName("TestLocationFromReunify");
		locationDTO.setTimeZone("US/PACIFIC");
		clientServiceHandler.addLocation(locationDTO);
		assertTrue(clientServiceHandler.removeLocation(testLocationId, clientId));
	}

	@Test
	public void testRemoveLocationInvalidLocationId() {
		assertFalse(clientServiceHandler.removeLocation(0, 1));
	}

}
