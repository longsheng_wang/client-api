/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;
import org.apache.log4j.Logger;

/**
 * @author Vivek Hungund
 *
 */
public class ClubPerformanceReportDataRow {

	private static final Logger logger = Logger.getLogger(ClubPerformanceReportDataRow.class);
	
	private int distinctMemberVisits, totalVisits, distinctMemberIntercepts, totalIntercepts, termination,
		interceptOnTermination, terminationAfterInterceptThreeMonths, interceptAndTerminationCurrentMonth, terminationWithoutVisit;
	
	/**
	 * 
	 */
	public ClubPerformanceReportDataRow() {
		
	}
	
	/**
	 * @param distinctMemberVisits
	 * @param totalVisits
	 * @param distinctMemberIntercepts
	 * @param totalIntercepts
	 * @param termination
	 * @param interceptOnTermination
	 * @param terminationAfterInterceptThreeMonths
	 * @param interceptAndTerminationCurrentMonth
	 * @param terminationWithoutVisit
	 */
	public ClubPerformanceReportDataRow(int distinctMemberVisits, int totalVisits, int distinctMemberIntercepts,
										int totalIntercepts, int termination, int interceptOnTermination,
										int terminationAfterInterceptThreeMonths,
										int interceptAndTerminationCurrentMonth, int terminationWithoutVisit) {
		
		this.distinctMemberVisits = distinctMemberVisits;
		this.totalVisits = totalVisits;
		this.distinctMemberIntercepts = distinctMemberIntercepts;
		this.totalIntercepts = totalIntercepts;
		this.termination = termination;
		this.interceptOnTermination = interceptOnTermination;
		this.terminationAfterInterceptThreeMonths = terminationAfterInterceptThreeMonths;
		this.interceptAndTerminationCurrentMonth = interceptAndTerminationCurrentMonth;
		this.terminationWithoutVisit = terminationWithoutVisit;
	}

	/**
	 * @return the distinctMemberVisits
	 */
	public int getDistinctMemberVisits() {
		return distinctMemberVisits;
	}

	/**
	 * @param distinctMemberVisits the distinctMemberVisits to set
	 */
	public void setDistinctMemberVisits(int distinctMemberVisits) {
		this.distinctMemberVisits = distinctMemberVisits;
	}

	/**
	 * @return the totalVisits
	 */
	public int getTotalVisits() {
		return totalVisits;
	}

	/**
	 * @param totalVisits the totalVisits to set
	 */
	public void setTotalVisits(int totalVisits) {
		this.totalVisits = totalVisits;
	}

	/**
	 * @return the totalIntercepts
	 */
	public int getTotalIntercepts() {
		return totalIntercepts;
	}

	/**
	 * @param totalIntercepts the totalIntercepts to set
	 */
	public void setTotalIntercepts(int totalIntercepts) {
		this.totalIntercepts = totalIntercepts;
	}

	/**
	 * @return the termination
	 */
	public int getTermination() {
		return termination;
	}

	/**
	 * @param termination the termination to set
	 */
	public void setTermination(int termination) {
		this.termination = termination;
	}

	/**
	 * @return the interceptOnTermination
	 */
	public int getInterceptOnTermination() {
		return interceptOnTermination;
	}

	/**
	 * @param interceptOnTermination the interceptOnTermination to set
	 */
	public void setInterceptOnTermination(int interceptOnTermination) {
		this.interceptOnTermination = interceptOnTermination;
	}

	/**
	 * @return the terminationAfterInterceptThreeMonths
	 */
	public int getTerminationAfterInterceptThreeMonths() {
		return terminationAfterInterceptThreeMonths;
	}

	/**
	 * @param terminationAfterInterceptThreeMonths the terminationAfterInterceptThreeMonths to set
	 */
	public void setTerminationAfterInterceptThreeMonths(int terminationAfterInterceptThreeMonths) {
		this.terminationAfterInterceptThreeMonths = terminationAfterInterceptThreeMonths;
	}

	/**
	 * @return the interceptAndTerminationCurrentMonth
	 */
	public int getInterceptAndTerminationCurrentMonth() {
		return interceptAndTerminationCurrentMonth;
	}

	/**
	 * @param interceptAndTerminationCurrentMonth the interceptAndTerminationCurrentMonth to set
	 */
	public void setInterceptAndTerminationCurrentMonth(int interceptAndTerminationCurrentMonth) {
		this.interceptAndTerminationCurrentMonth = interceptAndTerminationCurrentMonth;
	}

	/**
	 * @return the terminationWithoutVisit
	 */
	public int getTerminationWithoutVisit() {
		return terminationWithoutVisit;
	}

	/**
	 * @param terminationWithoutVisit the terminationWithoutVisit to set
	 */
	public void setTerminationWithoutVisit(int terminationWithoutVisit) {
		this.terminationWithoutVisit = terminationWithoutVisit;
	}

	/**
	 * @return the distinctMemberIntercepts
	 */
	public int getDistinctMemberIntercepts() {
		return distinctMemberIntercepts;
	}

	/**
	 * @param distinctMemberIntercepts the distinctMemberIntercepts to set
	 */
	public void setDistinctMemberIntercepts(int distinctMemberIntercepts) {
		this.distinctMemberIntercepts = distinctMemberIntercepts;
	}
}
