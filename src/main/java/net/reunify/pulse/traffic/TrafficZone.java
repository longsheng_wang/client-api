/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.traffic;

/**
 * A certain area that is surveilled by counting cameras. The zone boundaries
 * are defined by the client.
 * 
 * @author kiana.baradaran
 * 
 */
public class TrafficZone {

	private int zoneId;
	private String zoneName;
	private int peoplePresentInZone;
	private int floorNumber;
	/*
	 * The traffic (people who entered the zone comparing to the same number for
	 * all zones in a day)
	 */
	private int trafficPercentage;

	public TrafficZone() {
		// TODO Auto-generated constructor stub
	}

	public TrafficZone(int zoneId, String zoneName, int peoplePresentInZone, int floorNumber, int trafficPercentage) {
		// TODO Auto-generated constructor stub
		this.zoneId = zoneId;
		this.zoneName = zoneName;
		this.peoplePresentInZone = peoplePresentInZone;
		this.floorNumber = floorNumber;
		this.trafficPercentage = trafficPercentage;

	}

	public int getTrafficPercentage() {
		return trafficPercentage;
	}

	public void setTrafficPercentage(int trafficPercentage) {
		this.trafficPercentage = trafficPercentage;
	}

	public int getFloorNumber() {
		return floorNumber;
	}

	public void setFloorNumber(int floorNumber) {
		this.floorNumber = floorNumber;
	}

	public int getPeoplePresentInZone() {
		return peoplePresentInZone;
	}

	public void setPeoplePresentInZone(int peoplePresentInZone) {
		this.peoplePresentInZone = peoplePresentInZone;
	}

	public int getZoneId() {
		return zoneId;
	}

	public void setZoneId(int zoneId) {
		this.zoneId = zoneId;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

}
