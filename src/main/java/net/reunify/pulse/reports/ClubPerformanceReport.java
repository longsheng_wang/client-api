/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;

import java.io.IOException;
import java.util.Properties;
import java.util.TreeMap;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * Club Performance Report class
 * 
 * @author Vivek Hungund
 *
 */
@Component
public class ClubPerformanceReport implements Report {
	private static final Logger logger = Logger.getLogger(ClubPerformanceReport.class);

	@Inject
	@Named("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	/**
	 * The actual report data
	 */
	private TreeMap<ClubPerformanceReportGroup, ClubPerformanceReportData> reportData;
	

	/**
	 * Create a Club Performance Report currently made up of statistics on High Risk Members, Med Risk Members, New Members and Total Members groups 
	 * 
	 * @param startDate
	 * @param endDate
	 * @param locationId
	 */
	public void createReport(String startDate, String endDate, String locationId) {

		logger.debug("Creating Club Performance Report for club: " + locationId + " between " + startDate + " and " + endDate);
		TreeMap<ClubPerformanceReportGroup, ClubPerformanceReportData> fullReportData = new TreeMap<ClubPerformanceReportGroup, ClubPerformanceReportData>();

		Properties props = new Properties();
		try {
			props.load(ClubPerformanceReport.class.getResourceAsStream("reportqueries.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("Properties file with queries not found");
			e.printStackTrace();
		}

		ClubPerformanceReportData newMemberReportData = getNewMemberReportData(startDate, endDate, locationId, props);
		fullReportData.put(ClubPerformanceReportGroup.NEW, newMemberReportData);

		ClubPerformanceReportData highRiskMemberReportData = getHighRiskMemberReportData(startDate, endDate,
				locationId, props);
		fullReportData.put(ClubPerformanceReportGroup.HIGH_RISK, highRiskMemberReportData);

		ClubPerformanceReportData medRiskMemberReportData = getMedRiskMemberReportData(startDate, endDate, locationId,
				props);
		fullReportData.put(ClubPerformanceReportGroup.MED_RISK, medRiskMemberReportData);

		ClubPerformanceReportData totalMemberReportData = getTotalMemberReportData(startDate, endDate, locationId,
				props);
		fullReportData.put(ClubPerformanceReportGroup.TOTAL, totalMemberReportData);

		this.setReportData(fullReportData);
	}

	/**
	 * Get the Club Report data for the High Risk Members group
	 * 
	 * @param startDate
	 * @param endDate
	 * @param locationId
	 * @return
	 */
	private ClubPerformanceReportData getHighRiskMemberReportData(String startDate, String endDate, String locationId,
			Properties props) {

		ClubPerformanceReportData reportData = new ClubPerformanceReportData(ClubPerformanceReportGroup.HIGH_RISK);

		int highRiskDistinctMemberVisits = getMetric(props.getProperty("sql.select.getHighRiskDistinctMemberVisits"),
				new Object[] { startDate, endDate, startDate, locationId });
		int highRiskTotalVisits = getMetric(props.getProperty("sql.select.getHighRiskTotalVisits"), new Object[] {});
		int highRiskDistinctMemberIntercepts = getMetric(
				props.getProperty("sql.select.getHighRiskDistinctMemberIntercepts"), new Object[] { startDate,
						startDate, endDate, locationId });
		int highRiskTotalIntercepts = getMetric(props.getProperty("sql.select.getHighRiskTotalIntercepts"),
				new Object[] {});
		int highRiskTermination = getMetric(props.getProperty("sql.select.getHighRiskTermination"), new Object[] {
				startDate, endDate, startDate, locationId });
		int highRiskInterceptOnTermination = getMetric(
				props.getProperty("sql.select.getHighRiskInterceptOnTermination"), new Object[] { startDate, startDate,
						endDate, locationId });
		int highRiskTerminationAfterInterceptThreeMonths = getMetric(
				props.getProperty("sql.select.getHighRiskTerminationAfterInterceptThreeMonths"), new Object[] {
						startDate, startDate, endDate, locationId });
		int highRiskInterceptAndTerminationCurrentMonth = getMetric(
				props.getProperty("sql.select.getHighRiskInterceptAndTerminationCurrentMonth"), new Object[] {
						startDate, startDate, endDate, startDate, locationId });
		int highRiskTerminationWithoutVisit = getMetric(
				props.getProperty("sql.select.getHighRiskTerminationWithoutVisit"), new Object[] { startDate,
						startDate, endDate, locationId, startDate, startDate, endDate, locationId });

		reportData.addRow(new ClubPerformanceReportDataRow(highRiskDistinctMemberVisits, highRiskTotalVisits,
															highRiskDistinctMemberIntercepts, highRiskTotalIntercepts,
															highRiskTermination, highRiskInterceptOnTermination,
															highRiskTerminationAfterInterceptThreeMonths,
															highRiskInterceptAndTerminationCurrentMonth,
															highRiskTerminationWithoutVisit));
		return reportData;
	}

	/**
	 * Get the Club Report data for the Med Risk Members group
	 * 
	 * @param startDate
	 * @param endDate
	 * @param locationId
	 * @return
	 */
	private ClubPerformanceReportData getMedRiskMemberReportData(String startDate, String endDate, String locationId,
			Properties props) {
		// TODO get real values for medRiskTotalVisits and
		// medRiskTotalIntercepts

		ClubPerformanceReportData reportData = new ClubPerformanceReportData(ClubPerformanceReportGroup.MED_RISK);

		int medRiskDistinctMemberVisits = getMetric(props.getProperty("sql.select.getMedRiskDistinctMemberVisits"),
				new Object[] { startDate, startDate, endDate, locationId });
		int medRiskTotalVisits = getMetric(props.getProperty("sql.select.getMedRiskTotalVisits"), new Object[] {});
		int medRiskDistinctMemberIntercepts = getMetric(
				props.getProperty("sql.select.getMedRiskDistinctMemberIntercepts"), new Object[] { startDate,
						startDate, endDate, locationId });
		int medRiskTotalIntercepts = getMetric(props.getProperty("sql.select.getMedRiskTotalIntercepts"),
				new Object[] {});
		int medRiskTermination = getMetric(props.getProperty("sql.select.getMedRiskTermination"), new Object[] {
				startDate, endDate, startDate, locationId });
		int medRiskInterceptOnTermination = getMetric(props.getProperty("sql.select.getMedRiskInterceptOnTermination"),
				new Object[] { startDate, startDate, endDate, locationId });
		int medRiskTerminationAfterInterceptThreeMonths = getMetric(
				props.getProperty("sql.select.getMedRiskTerminationAfterInterceptThreeMonths"), new Object[] {
						startDate, startDate, endDate, locationId });
		int medRiskInterceptAndTerminationCurrentMonth = getMetric(
				props.getProperty("sql.select.getMedRiskInterceptAndTerminationCurrentMonth"), new Object[] {
						startDate, startDate, endDate, startDate, locationId });
		int medRiskTerminationWithoutVisit = getMetric(
				props.getProperty("sql.select.getMedRiskTerminationWithoutVisit"), new Object[] { startDate, startDate,
						endDate, locationId, startDate, startDate, endDate, locationId });

		reportData.addRow(new ClubPerformanceReportDataRow(medRiskDistinctMemberVisits, medRiskTotalVisits,
															medRiskDistinctMemberIntercepts, medRiskTotalIntercepts,
															medRiskTermination, medRiskInterceptOnTermination,
															medRiskTerminationAfterInterceptThreeMonths,
															medRiskInterceptAndTerminationCurrentMonth,
															medRiskTerminationWithoutVisit));
		return reportData;
	}

	/**
	 * Get the Club Report data for the Total Members group
	 * 
	 * @param startDate
	 * @param endDate
	 * @param locationId
	 * @param props
	 * @return
	 */
	private ClubPerformanceReportData getTotalMemberReportData(String startDate, String endDate, String locationId,
			Properties props) {
		ClubPerformanceReportData reportData = new ClubPerformanceReportData(ClubPerformanceReportGroup.TOTAL);

		int totalDistinctMemberVisits = getMetric(props.getProperty("sql.select.getTotalDistinctMemberVisits"),
				new Object[] { startDate, endDate, locationId });
		int totalTotalVisits = getMetric(props.getProperty("sql.select.getTotalTotalVisits"), new Object[] { startDate,
				endDate, locationId });
		int totalDistinctMemberIntercepts = getMetric(props.getProperty("sql.select.getTotalDistinctMemberIntercepts"),
				new Object[] { startDate, endDate, locationId });
		int totalTotalIntercepts = getMetric(props.getProperty("sql.select.getTotalTotalIntercepts"), new Object[] {
				startDate, endDate, locationId });
		int totalTermination = getMetric(props.getProperty("sql.select.getTotalTermination"), new Object[] { startDate,
				endDate, locationId });
		int totalInterceptOnTermination = getMetric(props.getProperty("sql.select.getTotalInterceptOnTermination"),
				new Object[] { startDate, endDate, locationId });
		int totalTerminationAfterInterceptThreeMonths = getMetric(
				props.getProperty("sql.select.getTotalTerminationAfterInterceptThreeMonths"), new Object[] { startDate,
						endDate, locationId });
		int totalInterceptAndTerminationCurrentMonth = getMetric(
				props.getProperty("sql.select.getTotalInterceptAndTerminationCurrentMonth"), new Object[] { startDate,
						startDate, endDate, locationId });
		int totalTerminationWithoutVisit = getMetric(props.getProperty("sql.select.getTotalTerminationWithoutVisit"),
				new Object[] { startDate, endDate, locationId, startDate, endDate, locationId });

		reportData.addRow(new ClubPerformanceReportDataRow(totalDistinctMemberVisits, totalTotalVisits,
															totalDistinctMemberIntercepts, totalTotalIntercepts,
															totalTermination, totalInterceptOnTermination,
															totalTerminationAfterInterceptThreeMonths,
															totalInterceptAndTerminationCurrentMonth,
															totalTerminationWithoutVisit));
		return reportData;
	}

	/**
	 * Get the Club Report data for the New Members group 
	 * 
	 * @param startDate
	 * @param endDate
	 * @param locationId
	 * @return
	 */
	private ClubPerformanceReportData getNewMemberReportData(String startDate, String endDate, String locationId,
			Properties props) {

		ClubPerformanceReportData reportData = new ClubPerformanceReportData(ClubPerformanceReportGroup.NEW);

		int newDistinctMemberVisits = getMetric(props.getProperty("sql.select.getNewDistinctMemberVisits"),
				new Object[] { startDate, endDate, locationId });
		int newTotalVisits = getMetric(props.getProperty("sql.select.getNewTotalVisits"), new Object[] { startDate, endDate, locationId });
		int newDistinctMemberIntercepts = getMetric(
				props.getProperty("sql.select.getNewDistinctMemberIntercepts"), new Object[] { locationId, startDate, endDate });
		int newTotalIntercepts = getMetric(props.getProperty("sql.select.getNewTotalIntercepts"),
				new Object[] { locationId, startDate, endDate });
		int newTermination = getMetric(props.getProperty("sql.select.getNewTermination"), new Object[] { startDate, endDate, locationId });
		int newInterceptOnTermination = getMetric(props.getProperty("sql.select.getNewInterceptOnTermination"),
				new Object[] { locationId, startDate, endDate });
		int newTerminationAfterInterceptThreeMonths = getMetric(
				props.getProperty("sql.select.getNewTerminationAfterInterceptThreeMonths"), new Object[] { locationId, startDate, endDate });
		int newInterceptAndTerminationCurrentMonth = getMetric(
				props.getProperty("sql.select.getNewInterceptAndTerminationCurrentMonth"), new Object[] { locationId, startDate, startDate, endDate });
		int newTerminationWithoutVisit = getMetric(
				props.getProperty("sql.select.getNewTerminationWithoutVisit"), new Object[] { startDate, endDate, locationId, startDate, endDate, locationId });

		reportData.addRow(new ClubPerformanceReportDataRow(newDistinctMemberVisits, newTotalVisits,
															newDistinctMemberIntercepts, newTotalIntercepts,
															newTermination, newInterceptOnTermination,
															newTerminationAfterInterceptThreeMonths,
															newInterceptAndTerminationCurrentMonth,
															newTerminationWithoutVisit));
		return reportData;
	}

	/**
	 * return the full report data
	 * 
	 * @return
	 */
	public TreeMap<ClubPerformanceReportGroup, ClubPerformanceReportData> getReportData() {

		return this.reportData;
	}

	/**
	 * Generic method that takes a prepared statement and the argument types for
	 * the inputs to the prepared statement
	 * 
	 * @param query
	 * @param argTypes
	 * @return
	 */
	private int getMetric(String query, Object[] argTypes) {
		try {
			return this.jdbcTemplate.queryForObject(query, Integer.class, argTypes);
		} catch (EmptyResultDataAccessException e) {
			return 0;
		}
	}

	/**
	 * Set the Club Performance report data
	 * 
	 * @param reportData
	 */
	private void setReportData(TreeMap<ClubPerformanceReportGroup, ClubPerformanceReportData> reportData) {

		this.reportData = reportData;

	}

}
