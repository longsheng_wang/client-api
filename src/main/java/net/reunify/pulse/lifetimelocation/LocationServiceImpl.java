/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.lifetimelocation;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service around club locations
 * 
 * @author Vivek Hungund
 *
 */

@Service("LocationService")
public class LocationServiceImpl implements LocationService {
	private static final Logger logger = Logger.getLogger(LocationServiceImpl.class);
	
	@Autowired
	private LocationDAO locationDAO;
	
	/**
	 * Simply returns all the pilot clubs
	 * @return
	 */
	public Map<String,Location> getLocations() {
		return locationDAO.getPilotClubLocations();
	}
}
