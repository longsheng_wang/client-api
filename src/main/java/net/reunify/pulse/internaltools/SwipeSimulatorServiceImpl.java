/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.internaltools;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * Implementation of SwipeSimulatorService. Provides service to SwipeSimulatorController
 * @author Vivek Hungund
 *
 */
@Service("SwipeSimulatorService")
public class SwipeSimulatorServiceImpl implements SwipeSimulatorService {
	private static final Logger logger = Logger.getLogger(SwipeSimulatorServiceImpl.class);

	@Inject
	@Named("pulseJdbcTemplate")
	private JdbcTemplate jdbcTemplate;
	
	@Inject
	private SwipeSimulatorDAO simulatorDAO; 
	
	/**
	 * Call to start the swipe simulator
	 * @param clientName
	 * @param location
	 * @param numSwipes
	 */
	@Override
	public void startSwipeSimulator(int clientId, int locationId, int numSwipes) {
		logger.info("Starting swipe simulator for client: " + clientId + " at location " + locationId);
		SwipeSimulatorTask task = new SwipeSimulatorTask(clientId, locationId, numSwipes);

		//TODO Figure out how to inject pulseJdbcTemplate correctly in Spring
		
		task.setJdbcTemplate(jdbcTemplate);
		Thread t = new Thread(task);
		t.setName("SwipeSimulatorTask-" + locationId);
		t.setDaemon(true);
		t.start();
		
		// Sleep for 1s so that the view will display this simulator
		try {
			Thread.sleep(1000l);
		} catch (InterruptedException e) {
			logger.error("Exception", e);
		}
	}
	
	/**
	 * Return a map of running swipe simulators, ordered by start time
	 * @return
	 */
	@Override	
	public List<SwipeSimulator> getRunningSwipeSimulators() {
		return simulatorDAO.getRunningSwipeSimulators();
		
	}

}
