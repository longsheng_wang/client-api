/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.beat.base;

import net.reunify.pulse.base.APIErrorResource;
import net.reunify.pulse.base.InvalidInputFormat;
import net.reunify.pulse.base.RecordNotFoundException;

import org.apache.log4j.Logger;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception handling for beat controllers
 * 
 * @author longsheng.wang
 */
public class BeatBaseController {

	private static final Logger logger = Logger.getLogger(BeatBaseController.class);

	/**
	 * Exception handler for incorrect request param types provided by client
	 * 
	 * @param exception
	 * @return
	 */
	@ExceptionHandler({ IllegalArgumentException.class, TypeMismatchException.class})
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody
	APIErrorResource handleException(Exception exception) {
		return new APIErrorResource(exception);
	}

	/**
	 * Exception handler for unsupported param provided by client
	 * 
	 * @param exception
	 * @return
	 */

	@ExceptionHandler({ RecordNotFoundException.class, InvalidInputFormat.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public @ResponseBody
	APIErrorResource handleInputException(Exception exception) {
		logger.warn(exception.getMessage());
		return new APIErrorResource(exception);
	}

	/**
	 * Catch all error handler to ensure that unhandled errors are reported back
	 * in a consistent format
	 * 
	 * @param exception
	 * @return
	 */
	@ExceptionHandler(Throwable.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody
	APIErrorResource handleException(Throwable exception) {
		logger.error(null, exception);
		return new APIErrorResource(exception);
	}

}
