/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.internaltools;
import java.util.List;

/**
 * Provides services to SwipeSimulatorController
 * 
 * @author Vivek Hungund
 *
 */
public interface SwipeSimulatorService {

	/**
	 * Start a thread simulating a series of swipes
	 * 
	 * @param clientId
	 * @param locationId
	 * @param numSwipes
	 * @param loop
	 */
	public void startSwipeSimulator(int clientId, int locationId, int numSwipes);

	/**
	 * Get a list of running simulators, ordered by date
	 * @return
	 */
	public List<SwipeSimulator> getRunningSwipeSimulators();
	
}
