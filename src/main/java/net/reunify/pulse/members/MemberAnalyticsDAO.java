package net.reunify.pulse.members;

/**
 * 
 * @author kiana.baradaran
 * 
 */
public interface MemberAnalyticsDAO {

	/**
	 * Get member analytics info
	 * 
	 * @param memberId
	 * @param clientId
	 * @return
	 */
	MemberAnalytics getMemberAnalytics(int memberId, int clientId);

}
