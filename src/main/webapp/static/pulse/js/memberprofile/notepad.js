/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).on('keyup', "#notepad-views-search-field", function() {
	if (event.keyCode == 13) {
		$("#notepad-views-search-field").blur();
	}

});

$(document).on(
		'keyup',
		"#notepad_post_content",
		function() {
			var text_length = $(this).val().replace(/\r(?!\n)|\n(?!\r)/g,
					"\r\n").length;
			var left = 1000 - text_length;
			if (left < 0) {
				left = 0;
			}
			$('#notepad-post-chars-left').text(left + ' character(s) left.');

		});

$(document).on("click tap", ".notesDiv", function(event) {

	var index = $(this).index();
	var position = $(this).position();
	var positionNumber = parseInt(index) * 60 - 2;

	// Height of each collapsed note is 60
	if ($("#note_" + index).hasClass("searched")) {
		expandAndCollapse(this.id);
	} else {

		$('#notepad-insert-and-view-div').animate({
			scrollTop : positionNumber
		}, 700);
		expandAndCollapse(this.id);
	}

});

$(document).on("click tap", "#clear_search_note", function(event) {
	$("#notepad-views-search-field").val("");
	$(".notesDiv").css("display", "block");
	$(".highlight").removeClass("highlight");
	$(".searched").removeClass("searched");
	showShortMessages();
	$("#clear_search_note").css("display", "none");
});

var selected_note_id;
var oldNotesJSON;
var totalNumberOfNotes = 0;

function viewCurrentListHandler() {
	$("#notepad_post_content").prop('disabled', false);
	$("#notepad_post_button").prop('disabled', false);
	$("#notepad-views-search-field").prop('disabled', false);
	$(".list-group-item").prop('disabled', false);
	var member_id = getURLParameter("member_id");

	$.ajax({
		data : {
			"memberId" : member_id
		},
		type : 'POST',
		async : false,
		cache : false,
		url : ctx+'/memberinterceptionsearch',
		dataType : 'json',
		success : function(data) {
			if (jQuery.isEmptyObject(data)) {
			} else {
				oldNotesJSON = data.notes;
			}
		}
	});
	var dataDiv = $("#notepad-insert-and-view-div");
	dataDiv.empty();

	totalNumberOfNotes = oldNotesJSON.length;
	for ( var i = 0; i < oldNotesJSON.length; i++) {
		var messageContent = oldNotesJSON[i].msg;
		var timeStamp = oldNotesJSON[i].timestamp;
		var author = oldNotesJSON[i].author;
		var note_id = oldNotesJSON[i].id;
		var notificationTag = oldNotesJSON[i].tag;
	
	
//	show notes
	newDiv = document.createElement('div');
	newDiv.setAttribute("id", "note_"+i);
	$(newDiv).addClass("notesDiv");
 	$(newDiv).appendTo($('#notepad-insert-and-view-div'));
 	newDiv_timeStamp_autour = document.createElement('div');
 	newDiv_timeStamp_autour.setAttribute("id", "time_stamp_author_"+i);
 	newDiv_timeStamp_autour.style.display = "inline-block";
	var statusIcon = document.createElement("img");
	statusIcon.style.height = "15px";
	statusIcon.style.rightMargin = "5px;"
	if (notificationTag == "0010") {
		statusIcon.src = ctx
				+ "/static/img/interception/interception_need_follow_up.png";
	} else if (notificationTag == "0011") {
		statusIcon.src = ctx
				+ "/static/img/interception/interception_need_attention.png";
	} else if (notificationTag == "0001") {
		statusIcon.src = ctx
				+ "/static/img/interception/interception_good_to_go.png";
	}
	$(newDiv_timeStamp_autour).prepend(statusIcon);
	newDiv_timeStamp_autour_text = document.createElement('div');
 	newDiv_timeStamp_autour_text.setAttribute("id", "time_stamp_author_text_"+i);
 	newDiv_timeStamp_autour_text.style.display = "inline-block";
	newDiv_timeStamp_autour_text.textContent =timeStamp + " - " + author;
	$(newDiv_timeStamp_autour).append(newDiv_timeStamp_autour_text);
	newDiv_timeStamp_autour_text.style.fontWeight = "bold";
	newDiv_timeStamp_autour_text.id = "time_stamp_" + i;
	$(newDiv).append(newDiv_timeStamp_autour);
	newDiv_message = document.createElement('div');
	$(newDiv_message).addClass("divFullMessage");
	newDiv_message.setAttribute("id", "content_message_"+i);
	$(newDiv_message).append(messageContent);
	$(newDiv).append(newDiv_message);
	newDiv_message.style.display="none";
	newDiv_message_trimmed = document.createElement('div');
	$(newDiv_message_trimmed).addClass("divShortMessage");
	newDiv_message_trimmed.setAttribute("id", "content_message_trimmed_"+i);
	$(newDiv).append(newDiv_message_trimmed);
	

}
	showShortMessages();
};



function showShortMessages(){

	var trimmed_text;
	for(var i=0 ; i<=totalNumberOfNotes ; i++){
			var checkLengthId = "#content_message_"+i;

			if($(checkLengthId).text().length < 50){

				trimmed_text = $(checkLengthId).text();
				
		}else{
			
			 var notesTextMessage = $("#content_message_"+i).text();
				var trimmed = notesTextMessage.substring(50,0);
			 var positionOfLastWord = trimmed.lastIndexOf(" ");
			 var makeShortMessageWhitCompleteWord = trimmed.substring(0,positionOfLastWord);
			 trimmed_text = makeShortMessageWhitCompleteWord + " ...";
			}
		var trimmedMessageId = "#content_message_trimmed_"+i;
		$(trimmedMessageId).html( trimmed_text);
//		Always shows first message expanded
		
		expandAndCollapse("note_0");

	}

};

function expandAndCollapse(idOfClickedNote) {
	$(".divFullMessage").css("display", "none");
	$(".divShortMessage").css("display", "block");
	var thisDiv = document.getElementById(idOfClickedNote);
	var idOfThisDiv = $(thisDiv).attr('id');
	var n = idOfThisDiv.lastIndexOf('_');
	var result = idOfThisDiv.substring(n + 1);
	$("#content_message_trimmed_" + result).css("display", "none");
	$("#content_message_" + result).css("display", "block");
};




function searchNotes(searchValue){
	
	$(".notesDiv").css("display","none");
	$(".highlight").removeClass("highlight");
	$(".searched").removeClass("searched");

	var searcheReq = new RegExp(searchValue,"ig");
	
//	search titles
	for(var i = 0 ; i<totalNumberOfNotes ; i++){
		


		var searchTimeStampId = "#time_stamp_author_"+i;
		var timeStampText = $(searchTimeStampId).text();
	    var indexOfSearchedString = timeStampText.toLowerCase().indexOf(searchValue.toLowerCase());
	    var searchContentId = "#content_message_"+i;
		var searchContentText = $(searchContentId).text();
		var indexOfSearchedStringContent = searchContentText.toLowerCase().indexOf(searchValue.toLowerCase()); 
		
		if(indexOfSearchedString !== -1 ||indexOfSearchedStringContent !== -1 ){
			$("#note_"+i).css("display","block");
    		$("#note_"+i).addClass("searched");
    		
			
		}
	  
		if (indexOfSearchedString !== -1){

			var titileToShow = timeStampText.replace(searcheReq, function(searcheReq){
													var highlightTitle = timeStampText.match(searcheReq);
													return '<span class="highlight">' +highlightTitle + '</span>';
			});
			
				$("#time_stamp_"+i).html(titileToShow);

	    		
	    	};
	    	var TrimmedTextToShow;
		 if (indexOfSearchedStringContent !== -1){
			 		
			 
			
			 		if(indexOfSearchedStringContent+ searchValue.length <= 50 ){
			 			
			 			if(searchContentText.length <50){
			 			
			 			   TrimmedTextToShow = searchContentText;
			 			
			 			
			 			
			 			}else{
			 				
			 				var TrimmedTextToShowTemp = searchContentText.substring(50,0);
			 				TrimmedTextToShow = TrimmedTextToShowTemp +" ...";
			 				
			 			}
			 			
			 			
			 			
			 		}else{
			 			
			 			
			 			var end =indexOfSearchedStringContent+ searchValue.length - 50;
			 			var TrimmedTextToShowTemp = searchContentText.substring(end,end+50);
			 			
			 			
			 				if(indexOfSearchedStringContent+ searchValue.length < searchContentText.length){
			 					
			 					TrimmedTextToShow = "... "+TrimmedTextToShowTemp +" ...";
			 					
			 				}else{
			 					
			 					TrimmedTextToShow = "... "+TrimmedTextToShowTemp;
			 					
			 				}
			 		}	
			 
			 		var highlightTrimTextToShow = TrimmedTextToShow.replace(searcheReq, function(searcheReq){
						var highlightText = TrimmedTextToShow.match(searcheReq);
						return '<span class="highlight">' +highlightText + '</span>';
			    			});

		    	var textToShow = searchContentText.replace(searcheReq, function(searcheReq){
					var highlightText = searchContentText.match(searcheReq);
					return '<span class="highlight">' +highlightText + '</span>';
		    			});

		    	
		    	$("#content_message_"+i).html(textToShow);
		    	$("#content_message_"+i).css("display","none");
		    	
		    	$("#content_message_trimmed_"+i).html(highlightTrimTextToShow);
				$("#content_message_trimmed_"+i).css("display","block");
				
			

				
 	};

		
	}
	// show the first result expanded and others collapsed
	for (k = 0; k <= totalNumberOfNotes; k++) {
		if ($("#note_" + k).hasClass("searched")) {
			expandAndCollapse("note_" + k);
			break;

		}
	}

};


