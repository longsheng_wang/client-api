<%@ tag language="java" pageEncoding="UTF-8"%>

<%@attribute name="contextPath" required="true"%>
<%@attribute name="pageTitle" required="true"%>
<%@attribute name="headTag" fragment="true"%>
<%@attribute name="modalTag" fragment="true"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style"
	content="black-translucent" />
<meta charset="utf-8">
<!-- end: Meta -->

<!-- start: Mobile Specific -->
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- end: Mobile Specific -->


<!-- start: CSS -->
<link
	href="${contextPath}/static/pulse/css/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link
	href="${contextPath}/static/pulse/css/bootstrap/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link href="${contextPath}/static/pulse/css/bootstrap/css/style.min.css"
	rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css'>
<link
	href="${contextPath}/static/pulse/css/global/resetCSSglobal-v2.css"
	rel="stylesheet" type="text/css">
<link href="${contextPath}/static/pulse/css/global/global-v2.css"
	rel="stylesheet" type="text/css">
<link
	href="${contextPath}/static/pulse/css/memberpulse/memberPulse-v2.css"
	rel="stylesheet" type="text/css">

<!-- start: Scripts -->
<script src="${contextPath}/static/pulse/js/jquery/jquery-1.11.0.min.js"
	type="text/javascript"></script>
<script src="${contextPath}/static/pulse/js/bootstrap/bootstrap.min.js"></script>


<script>
	var ctx = "${contextPath}";
</script>

<jsp:invoke fragment="headTag" />
<!-- end: Scripts -->

<title>newPulse</title>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top " role="navigation">
		<div class="container-fluid navigation">
			<div class="navbar-header">
				<img id="logo" src="${contextPath}/static/pulse/img/img-v2/Logo.png" />
				<div id="location">LAGUNA NIGUEL ,CA</div>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right right">

					<li><img id="search-icon"
						src="${contextPath}/static/pulse/img/icons/icon-v2/search-icon.png" />
					</li>
					<li id="search">SEARCH</li>
					<li class="active-page" id="page">MEMBER PULSE</li>
					<li id="notification">NOTIFICATIONS</li>
					<li><img id="notification-icon"
						src="${contextPath}/static/pulse/img/icons/icon-v2/notification-pink.png" />
					</li>
					<li id="logged-in-name">JENNIFER</li>
					<li><img id="logged-in-picture"
						src="${contextPath}/static/pulse/img/img-v2/loggedInMember.png" />
					</li>

				</ul>

			</div>
		</div>
	</nav>

	<div class="container-fluid" id="main-content">
		<jsp:doBody />
	</div>

</body>

</html>