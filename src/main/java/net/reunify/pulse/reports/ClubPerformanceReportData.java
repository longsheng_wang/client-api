/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;


/**
 * @author Vivek Hungund
 *
 */
public class ClubPerformanceReportData {
	private static final Logger logger = Logger.getLogger(ClubPerformanceReportData.class);

	/**
	 * The actual report data
	 */
	private List<ClubPerformanceReportDataRow> reportData;
	
	/**
	 * Does this report apply to High Risk, Med. Risk, New Members, or Total
	 */
	private ClubPerformanceReportGroup group;
	
	/**
	 * Constructor, initialize the report data
	 * 
	 * @param
	 */
	public ClubPerformanceReportData() {
		this.reportData = new ArrayList<ClubPerformanceReportDataRow>();
	}
	
	/**
	 * Constructor, Initialize the list and set the group
	 * 
	 * @param group
	 */
	public ClubPerformanceReportData(ClubPerformanceReportGroup group) {
		this.reportData = new ArrayList<ClubPerformanceReportDataRow>();
		this.group = group;
	}
	
	/**
	 * Constructor, set the report data
	 * 
	 * @param reportData
	 */
	public ClubPerformanceReportData(List<ClubPerformanceReportDataRow> reportData) {
		this.reportData = reportData;
	}
	
	/**
	 * 
	 * 
	 * @return the group
	 */
	public ClubPerformanceReportGroup getReportGroup() {
		return group;
	}

	/**
	 * @param group the group to set
	 */
	public void setType(ClubPerformanceReportGroup group) {
		this.group = group;
	}



	/**
	 * Retrieve the report data
	 * 
	 * @return
	 */
	public List<ClubPerformanceReportDataRow> getReportData() {

		return this.reportData;
	}
	
	/**
	 * set reportData to some input
	 * 
	 * @param reportData
	 */
	public void setReportData(List<ClubPerformanceReportDataRow> reportData) {
		this.reportData = reportData;
	}
	
	/**
	 * Add a row of data to the report data
	 * 
	 * @param dataRow
	 */
	public void addRow(ClubPerformanceReportDataRow dataRow) {
		this.reportData.add(dataRow);
	}
}
