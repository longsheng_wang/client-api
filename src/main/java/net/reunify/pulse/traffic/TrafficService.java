/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.traffic;

import java.util.List;

/**
 * Provides service to traffic controller
 * 
 * @author kiana.baradaran
 * 
 */
public interface TrafficService {
	/**
	 * Get traffic information of all zones in the specified location
	 * 
	 * @param locatioId
	 * @return List<TrafficZone>
	 */
	public List<TrafficZone> getLocationTraffic(String locatioId);

}
