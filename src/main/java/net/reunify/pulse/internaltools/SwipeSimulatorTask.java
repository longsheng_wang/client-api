/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.internaltools;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * Inserts a swipes at random intervals into the db
 * 
 * @author Vivek Hungund
 *
 */
@Component("SwipeSimulatorTask")
@Scope(value = "prototype")
public class SwipeSimulatorTask implements Runnable {
	private static final Logger logger = Logger.getLogger(SwipeSimulatorTask.class);

	private JdbcTemplate jdbcTemplate;

	private int clientId;
	private int locationId;
	private int numSwipes;
	private int numSwipesSoFar;
	private boolean success;
	private int recordId;
	
	/**
	 * @return the clientId
	 */
	public int getClientId() {
		return clientId;
	}

	/**
	 * @param clientId
	 *            the clientId to set
	 */
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	/**
	 * @return the locationId
	 */
	public int getLocationId() {
		return locationId;
	}

	/**
	 * @param locationId
	 *            the locationId to set
	 */
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	/**
	 * @return the numSwipes
	 */
	public int getNumSwipes() {
		return numSwipes;
	}

	/**
	 * @param numSwipes
	 *            the numSwipes to set
	 */
	public void setNumSwipes(int numSwipes) {
		this.numSwipes = numSwipes;
	}

	/**
	 * @return the numSwipesSoFar
	 */
	public int getNumSwipesSoFar() {
		return numSwipesSoFar;
	}

	/**
	 * @param numSwipesSoFar
	 *            the numSwipesSoFar to set
	 */
	public void setNumSwipesSoFar(int numSwipesSoFar) {
		this.numSwipesSoFar = numSwipesSoFar;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @param clientId
	 * @param locationId
	 * @param numSwipes
	 */
	public SwipeSimulatorTask(int clientId, int locationId, int numSwipes) {
		this.clientId = clientId;
		this.locationId = locationId;
		this.numSwipes = numSwipes;
		this.success = true;
		this.numSwipesSoFar = 0;
	}

	public SwipeSimulatorTask() {

	}

	/**
	 * @param jdbcTemplate
	 *            the jdbcTemplate to set
	 */
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		// TODO Need to revise code to have Spring inject the jdbcTemplate field
		// automatically. For now, this is field must be set by the calling
		// code.

		this.jdbcTemplate = jdbcTemplate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		logger.info("Starting a swipe simulator thread for location_id " + locationId);
		try {
			beforeTask();
			runTask();
		} catch (Exception e) {
			logger.error("Swipe simulator exited before completion ", e);

		} finally {
			endTask();
		}
	}

	/**
	 * log the start of the task
	 * 
	 */
	private void beforeTask() {
		final String logStartTaskQuery = "INSERT INTO swipe_simulator_status(is_running,started_at, client_id, location_id, num_swipes_requested) values (true,now(), ?, ?, ?) returning record_id";
		recordId = jdbcTemplate.queryForObject(logStartTaskQuery, new Object[] {clientId, locationId, numSwipes}, new int[] {Types.INTEGER, Types.INTEGER, Types.INTEGER} , Integer.class);
		}

	/**
	 * Execute the simulator
	 */
	private void runTask() {
		String getMembersQuery = "SELECT member_id, location_id FROM members WHERE location_id = ? limit ?";
		List<Map<String, Object>> memberList = this.jdbcTemplate.queryForList(getMembersQuery, new Object[] {
				locationId, numSwipes });

		if (memberList.size() < numSwipes)
			memberList = augmentMemberList(memberList);

		for (Map<String, Object> member : memberList) {

			int memberId = (Integer) member.get("member_id");
			int locationId = (Integer) member.get("location_id");

			String insertSwipeQuery = "INSERT INTO member_check_in (client_member_id, location_id, check_in_time) VALUES(?,?,now())";
			int row = jdbcTemplate.update(insertSwipeQuery, new Object[] { memberId, locationId }, new int[] {
					Types.INTEGER, Types.INTEGER });
			numSwipesSoFar++;
			updateProgress();
			
			// sleep for a random interval between 0-10 seconds
			try {
				Random r = new Random();
				Thread.sleep((long) r.nextInt(10) * 1000);
			} catch (InterruptedException e) {
				logger.error("InterruptedException: ", e);
			}
		}
	}


	/**
	 * Log status of the simulator when it's done  
	 * 
	 */
	private void endTask() {
		String logEndTaskQuery = "UPDATE swipe_simulator_status SET num_swipes_inserted = ?, successful_run = ?, is_running = false where record_id = ?";
			
		int row = jdbcTemplate.update(logEndTaskQuery, new Object[] { numSwipesSoFar, success, recordId  }, new int[] {
				Types.INTEGER, Types.BOOLEAN, Types.INTEGER });
	}
	

	/**
	 * After every swipe, update num_swipes_inserted in the db 
	 */
	private void updateProgress() {
		String updateQuery = "UPDATE swipe_simulator_status SET num_swipes_inserted = ? where record_id = ?";
		int row = jdbcTemplate.update(updateQuery, new Object[] {numSwipesSoFar, recordId}, new int[] {Types.INTEGER, Types.INTEGER});
	}

	/**
	 * In case the size of memberList < the number of swipes requested, loop
	 * over the results until the numSwipes is reached
	 * 
	 * @param memberList
	 * @return
	 */
	private List<Map<String, Object>> augmentMemberList(List<Map<String, Object>> memberList) {

		List<Map<String, Object>> newMemberList = new ArrayList<Map<String, Object>>();

		do {
			for (Map<String, Object> member : memberList) {
				newMemberList.add(member);
				if (newMemberList.size() >= numSwipes)
					break;
			}
		} while (newMemberList.size() < numSwipes);

		return newMemberList;
	}
}
