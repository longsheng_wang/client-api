/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.client;

import java.util.List;

import org.springframework.stereotype.Component;

/**
 * DAO for client registration. Implemented separately for each data source
 * 
 * @author kiana.baradaran
 * 
 */
@Component
public interface ClientDAO {
	/**
	 * Get a list of active clients
	 * 
	 * @return
	 */
	public List<ClientDTO> getActiveClients();

	/**
	 * Gets all the assigned locations for the given client
	 * 
	 * @param clientId
	 * @return
	 */
	public List<LocationDTO> getClientLocations(int clientId);

	/**
	 * Create an account for the specified client
	 * 
	 * @param client
	 * @return
	 */
	public int createClientAccount(ClientDTO client);

	/**
	 * Add a location
	 * 
	 * @param locationDTO
	 * @return
	 */
	public int addLocation(LocationDTO locationDTO);

	/**
	 * Removes (deactivates) the location that matches the given information
	 * 
	 * @param locationId
	 * @param clientId
	 * @return
	 */
	public boolean removeLocation(int locationId, int clientId);

	/**
	 * update location information
	 * 
	 * @param location
	 * @return
	 */
	public boolean updateLocation(LocationDTO location);

	/**
	 * update client information
	 * 
	 * @param client
	 * @return
	 */
	public boolean updateClientAccount(ClientDTO client);

	/**
	 * Removes (deactivates) the client that matches the given information
	 * 
	 * @param clientId
	 * @return
	 */
	public boolean removeClientAccount(int clientId);

}
