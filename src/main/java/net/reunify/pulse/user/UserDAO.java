package net.reunify.pulse.user;

import java.util.List;

import org.springframework.stereotype.Component;

/**
 * DAO for registering users. The class is implemented separately for different
 * data sources
 * 
 * @author kiana.baradaran
 * 
 */

@Component
public interface UserDAO {
	/**
	 * Creates a new user account
	 * 
	 * @param userDTO
	 * @return id of the inserted row
	 */
	int createUserAccount(UserDTO userDTO);

	/**
	 * Removes(deactivates) an account
	 * 
	 * @param userId
	 * @return
	 */
	boolean removeUserAccount(int userId, int clientId);

	/**
	 * Edits an already existing account
	 * 
	 * @param user
	 * @return
	 */
	boolean editUserAccount(UserDTO user, int clientId);

	/**
	 * Gets all the user account for the specified client
	 * 
	 * @param clientId
	 * @return
	 */
	List<UserDTO> getUserAccounts(int clientId);

	/**
	 * Changes the password for an already existing account
	 * 
	 * @param userDTO
	 * @return
	 */
	boolean changePassword(UserDTO userDTO, int clientId);

}
