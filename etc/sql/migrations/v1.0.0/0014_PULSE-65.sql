-- making the client_location_id unique within each business
CREATE UNIQUE INDEX ON client_locations(client_id,client_location_id);

-- making the client_member_id unique within a location
CREATE UNIQUE INDEX ON members(location_id,client_member_id);

ALTER TABLE client_locations ALTER COLUMN client_location_id SET NOT NULL;