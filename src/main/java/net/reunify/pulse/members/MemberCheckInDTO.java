/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members;


/**
 * 
 * @author kiana.baradaran
 * 
 */
public class MemberCheckInDTO {

	private int clientId;
	private int locationId;
	private String clientLocationId;
	private String clientMemberId;
	private String checkInTime;// check-in time in UTC

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public String getClientLocationId() {
		return clientLocationId;
	}

	public void setClientLocationId(String clientLocationId) {
		this.clientLocationId = clientLocationId;
	}

	public String getCheckInTime() {
		return checkInTime;
	}

	public void setCheckInTime(String checkInTime) {
		this.checkInTime = checkInTime;
	}

	public String getClientMemberId() {
		return clientMemberId;
	}

	public void setClientMemberId(String clientMemberId) {
		this.clientMemberId = clientMemberId;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	@Override
	public String toString() {
		return "{\"clientId\":" + clientId + "\"checkInTime\":" + checkInTime + " , \"clientLocationId\":"
				+ clientLocationId + ",\"clientMemberId\":" + clientMemberId + "}";
	}

}
