/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.traffic;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import net.reunify.pulse.base.BaseController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Handles requests for traffic information.
 * 
 * @author kiana.baradaran
 * 
 */

@Controller
public class TrafficController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(TrafficController.class);

	@Inject
	@Named("TrafficService")
	private TrafficService trafficServiceHandler;

	/**
	 * Request for traffic information of the specified location
	 * 
	 * @param locatioId
	 * @return TrafficResponse
	 */
	@RequestMapping(value = "/traffic", method = RequestMethod.GET)
	@ResponseBody
	public TrafficResponse getLocationTraffic(HttpServletRequest request) {
		String locationId = (String) request.getSession().getAttribute("locationId");
		if (logger.isDebugEnabled()) {
			logger.debug(locationId + "");
		}
		Long start = System.currentTimeMillis();
		Assert.notNull(locationId, "Location ID cannot be null");
		List<TrafficZone> trafficInfo = trafficServiceHandler.getLocationTraffic(locationId);
		logger.debug("Time: " + (System.currentTimeMillis() - start));
		return new TrafficResponse(trafficInfo);
	}

}
