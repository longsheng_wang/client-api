<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Swipe Search Results</title>
</head>
<body>
 
	<%
	String locationId = request.getParameter("locationId");
	String searchDate = request.getParameter("searchDate");
	String memberId = request.getParameter("memberId");
	String firstName = request.getParameter("firstName");
	String lastName = request.getParameter("lastName");
	
	if(request.getParameter("locationId") == null) {locationId = "";}
	if(request.getParameter("searchDate") == null) {searchDate = "";}
	if(request.getParameter("memberId") == null) {memberId = "";}
	if(request.getParameter("firstName") == null) {firstName = "";}
	if(request.getParameter("lastName") == null) {lastName = "";}
	
	pageContext.setAttribute("locationId", locationId,PageContext.SESSION_SCOPE);
	pageContext.setAttribute("searchDate", searchDate,PageContext.SESSION_SCOPE);
	pageContext.setAttribute("memberId", memberId,PageContext.SESSION_SCOPE);
	pageContext.setAttribute("firstName", firstName,PageContext.SESSION_SCOPE);
	pageContext.setAttribute("lastName", lastName,PageContext.SESSION_SCOPE);
	%>

	<form method="post" action="memberSwipeSearch-internal">
 		Club ID (e.g. 152): <input type="text" name="locationId" value="<%= locationId %>" /><br />
		Date: <input type="text" name="searchDate" value="<%= searchDate %>" /><br />
		Member ID: <input type="text" name="memberId" value="<%= memberId %>" /><br />
		Member First Name: <input type="text" name="firstName" value="<%= firstName %>" /><br />
		Member Last Name: <input type="text" name="lastName" value="<%= lastName %>" /><br />
		<b>Searches may consist of one of the following</b>
		<br />
		<ul>
			<li>Club ID + Date</li>
			<li>Member ID</li>
			<li>Member First Name + Member Last Name + Club ID</li>
		</ul>
		<br />
		<input type="submit" value="Search">
	</form>
	
	<c:if test="${pageContext.request.method=='POST'}">
		<c:set var="locationId" value="${locationId}" />
		<c:set var="searchDate" value="${searchDate}" />
		<c:set var="firstName" value="${firstName}" />
		<c:set var="lastName" value="${lastName}" />
		<c:set var="memberId" value="${memberId}" />
		
		<c:if test="${not empty locationId && not empty searchDate}">
			<table class="data" border="1">
				<tr>
					<th>Member ID</th>
					<th>Member First Name</th>
					<th>Member Last Name</th>
					<th>Location ID</th>
					<th>Swipe Time</th>
				</tr>
						
				<c:forEach items="${memberSwipeObject}" var="swipe">
					<tr>
						<td>${swipe.getMemberId()}</td>
						<td>${swipe.getFirstName()}</td>
						<td>${swipe.getLastName()}</td>
						<td>${swipe.getSwipeLocation()}</td>
						<td>${swipe.getSwipeTime()}</td>
					</tr>
				</c:forEach>
			</table>
		</c:if>
		
		<c:if test="${not empty memberId}">
			<table class="data" border="1">
				<tr>
					<th>Member ID</th>
					<th>Member First Name</th>
					<th>Member Last Name</th>
					<th>Newcomer</th>
					<th>Attrition</th>
					<th>Interception Flag</th>
					<th>Interception Date</th>
					<th>Swipe Time</th>
				</tr>
						
				<c:forEach items="${memberSwipeObject}" var="swipe">
					<tr>
						<td>${swipe.getMemberId()}</td>
						<td>${swipe.getFirstName()}</td>
						<td>${swipe.getLastName()}</td>
						<td>${swipe.isNew()}</td>
						<td>${swipe.getAttrition()}</td>
						<td>${swipe.getInterceptionFlag()}</td>
						<td>${swipe.getLastIntercept()}</td>
						<td>${swipe.getSwipeTime()}</td>
					</tr>
				</c:forEach>
			</table>
		</c:if>
		
		<c:if test="${not empty firstName && not empty lastName && not empty locationId}">
			<table class="data" border="1">
				<tr>
					<th>Member ID</th>
					<th>Member First Name</th>
					<th>Member Last Name</th>
					<th>Newcomer</th>
					<th>Attrition</th>
					<th>Interception Flag</th>
					<th>Interception Date</th>
					<th>Swipe Time</th>
				</tr>
						
				<c:forEach items="${memberSwipeObject}" var="swipe">
					<tr>
						<td>${swipe.getMemberId()}</td>
						<td>${swipe.getFirstName()}</td>
						<td>${swipe.getLastName()}</td>
						<td>${swipe.isNew()}</td>
						<td>${swipe.getAttrition()}</td>
						<td>${swipe.getInterceptionFlag()}</td>
						<td>${swipe.getLastIntercept()}</td>
						<td>${swipe.getSwipeTime()}</td>
					</tr>
				</c:forEach>
			</table>
		</c:if>
	</c:if>
</body>
</html>