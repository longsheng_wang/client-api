/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.analytics.metrics;

import java.util.Date;

import net.reunify.pulse.members.MemberMetric;

import org.apache.log4j.Logger;

public class ProbabilisticMetric implements TimestampMetric<Double> {

	private static final Logger logger = Logger.getLogger(ProbabilisticMetric.class);

	private MemberMetric metricName;
	private double value;
	private Date date;

	public ProbabilisticMetric() {
		// TODO Auto-generated constructor stub
	}

	public ProbabilisticMetric(MemberMetric metricName, double value, Date date) {
		this.metricName = metricName;
		this.value = value;
		this.date = date;
	}

	public void setMetricName(MemberMetric metricName) {
		this.metricName = metricName;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public MemberMetric getMetricName() {
		return metricName;
	}

	@Override
	public Double getValue() {
		return value;
	}

	@Override
	public Date getDate() {
		return date;
	}
}
