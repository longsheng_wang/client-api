-- create role 
CREATE ROLE webapps WITH ENCRYPTED PASSWORD 'I26u-V2OeQ643|3BWd3SI0q6980Y*QAfP45tMp57U.%5H5g4B3As1As*y03jI+4v' LOGIN;

-- grant for tables
GRANT SELECT,INSERT,UPDATE ON ALL TABLES IN SCHEMA public TO webapps;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT,INSERT,UPDATE ON TABLES TO webapps;

-- grant for sequences
GRANT ALL PRIVILEGES ON ALL sequences IN SCHEMA PUBLIC TO webapps;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL ON SEQUENCES TO webapps;


-- for the "ltf" db
create role ltfwebapps with encrypted password 'LiwZ27.%Ap85HsHJu1*_591=8109n142Y6+%282R+Vt23eL79L66:uf6Ta|=24vE' login;

alter table lifetime_daily_statistics owner to ltfadmin;
alter table lifetime_pilot_clubs owner to ltfadmin;

-- grant for tables
GRANT SELECT,INSERT,UPDATE ON ALL TABLES IN SCHEMA public TO ltfwebapps;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT,INSERT,UPDATE ON TABLES TO ltfwebapps;

-- grant for sequences
GRANT ALL PRIVILEGES ON ALL sequences IN SCHEMA PUBLIC TO ltfwebapps;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL ON SEQUENCES TO ltfwebapps;

-- special privileges inherited from ltfsurveyios
GRANT REFERENCES,TRIGGER on feedback_accesscode,feedback_app_settings,feedback_assistance,feedback_client,feedback_client_employee,feedback_client_location,feedback_comment,feedback_counter,feedback_device,feedback_device_status,feedback_questionnaire,feedback_questionnaire_questions,feedback_questionnaire_questions_v2,feedback_questions,feedback_sequence_generation_table,feedback_survey_buttondata,feedback_survey_buttondata_view,feedback_survey_main to ltfwebapps;

-- special privileges inherited from ltfdashboard
GRANT ALL PRIVILEGES ON lifetime_daily_statistics,lifetime_pilot_clubs TO ltfwebapps;

alter table lifetime_daily_statistics owner to ltfdashboard;
alter table lifetime_pilot_clubs owner to ltfdashboard;
