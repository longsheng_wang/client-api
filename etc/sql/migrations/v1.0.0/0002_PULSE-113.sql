
-- Create the partitioning function for member_check_in insert
CREATE OR REPLACE FUNCTION 
member_check_in_partition_function()
RETURNS TRIGGER AS 
$BODY$
DECLARE
tablename text;
startdate text;
enddate text;
BEGIN
	startdate := to_char(NEW.check_in_time , 'YYYY-MM-DD');
	tablename := 'member_check_in_'|| to_char(NEW.check_in_time , 'YYYY_MM_DD');
	--check if the partition exists
	PERFORM 1
	FROM   pg_catalog.pg_class c
	JOIN   pg_catalog.pg_namespace n ON n.oid = c.relnamespace 
	WHERE  c.relkind = 'r'
	AND    c.relname = tablename;
	-- If the partition needed does not yet exist, then we create it:
	IF NOT FOUND THEN
	enddate:=startdate::timestamp + INTERVAL '1 day';
	EXECUTE 'CREATE TABLE '|| quote_ident(tablename) ||' (check(check_in_time >= TIMESTAMPTZ ''' || startdate || ''' and check_in_time < TIMESTAMPTZ ''' || enddate || ''')) INHERITS (member_check_in)'; 
	
	EXECUTE 'ALTER TABLE ' || quote_ident(tablename) || ' OWNER TO ltfadmin';
	EXECUTE 'GRANT SELECT,INSERT,UPDATE ON TABLE ' || quote_ident(tablename) || ' TO ltfdashboard';
	
	EXECUTE 'CREATE INDEX ' || quote_ident(tablename||'_idx1') || ' ON ' || quote_ident(tablename) || ' (client_member_id, location_id, check_in_time)';
	EXECUTE 'CREATE INDEX ' || quote_ident(tablename||'_idx2') || ' ON ' || quote_ident(tablename) || ' (record_id)';
	
	END IF;
	
	-- Insert the current record into the correct partition.
	EXECUTE 'INSERT INTO ' || quote_ident(tablename) || ' VALUES ($1.*)' USING NEW;	
	RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql;


-- Create trigger for swipe insert
CREATE TRIGGER member_check_in_insert_trigger
BEFORE INSERT ON member_check_in
FOR EACH ROW EXECUTE PROCEDURE member_check_in_partition_function();