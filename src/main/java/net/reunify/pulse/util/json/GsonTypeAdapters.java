package net.reunify.pulse.util.json;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import net.reunify.pulse.members.DefaultMemberProfile;
import net.reunify.pulse.members.MemberAttribute;
import net.reunify.pulse.members.MemberAttributes;
import net.reunify.pulse.members.MemberPersonalInfo;
import net.reunify.pulse.members.MemberProfile;

import com.google.gson.InstanceCreator;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class GsonTypeAdapters {

	private static final String[] DATE_FORMATS = new String[] { "MMM dd, yyyy HH:mm:ss", "MMM dd, yyyy",
			"yyyy-MM-dd HH:mm:ss.SSS", "yyyy-MM-dd" };

	private static class DateSerializer implements JsonDeserializer<Date> {

		@Override
		public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
			for (String format : DATE_FORMATS) {
				try {
					return new SimpleDateFormat(format, Locale.US).parse(json.getAsString());
				} catch (ParseException e) {
				}
			}
			throw new JsonParseException("Unparseable date: \"" + json.getAsString() + "\". Supported formats: "
					+ Arrays.toString(DATE_FORMATS));

		}
	}

	private static class MemberProfileAdapter implements JsonDeserializer<MemberProfile>,
			InstanceCreator<MemberProfile> {

		@Override
		public MemberProfile createInstance(Type type) {
			return new DefaultMemberProfile();
		}

		@Override
		public MemberProfile deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {

			String id = json.getAsJsonObject().get("id").getAsString();
			JsonObject personalInfo = json.getAsJsonObject().get("memberPersonalInfo").getAsJsonObject();
			JsonObject attributes = json.getAsJsonObject().get("memberAttributes").getAsJsonObject();

			// TODO Add support for different industries
			return new DefaultMemberProfile(id, (MemberPersonalInfo) context.deserialize(personalInfo,
					MemberPersonalInfo.class), (MemberAttributes) context.deserialize(attributes,
					MemberAttributes.class));
		}

	}

	private static class MemberAttributeSerializer implements JsonSerializer<MemberAttribute<?>> {

		@Override
		public JsonElement serialize(MemberAttribute<?> src, Type typeOfSrc, JsonSerializationContext context) {
			JsonObject json = new JsonObject();
			json.addProperty("attributeName", src.getAttributeName());
			if (src.getAttributeValue() instanceof String) {
				json.addProperty("attributeValue", (String) src.getAttributeValue());
			} else if (src.getAttributeValue() instanceof Date) {
				json.addProperty("attributeValue", (String) src.getAttributeValue().toString());
			} else if (src.getAttributeValue() instanceof Integer) {
				json.addProperty("attributeValue", (Integer) src.getAttributeValue());
			}
			return json;

		}

	}

	public static final DateSerializer dateDeserializer = new DateSerializer();
	public static final MemberAttributeSerializer memberAttributeSerializer = new MemberAttributeSerializer();
	public static final MemberProfileAdapter memberProfileAdapter = new MemberProfileAdapter();
}
