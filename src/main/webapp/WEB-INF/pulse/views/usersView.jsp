<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<ui:baseview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Pulse - Users">
	<jsp:attribute name="headTag">
<script
			src="${pageContext.servletContext.contextPath}/static/pulse/js/user/user.js"
			type="text/javascript"></script>
	</jsp:attribute>
	<jsp:body>
		<div id="users_content" class="col-md-12 col-sm-12"
			style="height: 100%; padding: 0;">
			<div>
				<a class="user-add">Add User</a>
			</div>		

		<table class="table-bordered" id="users-table">
		<thead>
		<tr>
		<th>Email Address</th>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Location</th>
		<th>Role</th>
		<th>Action</th>
		</tr>
		</thead>
		
	<c:if test="${not empty usersList}">

        <c:forEach var="user" items="${usersList}">
            <tr>
             <td>${user.emailAddress}</td>
                <td>${user.firstName}</td>
                <td>${user.lastName}</td>
                <td>${user.locationName}</td> 
                <td>${user.roleName}</td>   
                <td><span><a class="user-edit"
									data="${user.userId}">edit</a>/<a class="user-delete"
									data="${user.userId}">delete</a>/<a class="change-password"
									data="${user.userId}">change password</a></span></td>
            </tr>
        </c:forEach>

</c:if>
		</table>
		</div>
	</jsp:body>
</ui:baseview>
