/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members.interception;

/**
 * 
 * @author kiana.baradaran
 * 
 */
public class MemberInterceptionSearchCriteria {
	private String memberId;
	private String locationId;
	private String employeeId;
	private String from;
	private String to;

	public MemberInterceptionSearchCriteria() {
		// TODO Auto-generated constructor stub
	}

	public MemberInterceptionSearchCriteria(String employeeId, String locationId, String from, String to) {
		this.employeeId = employeeId;
		this.from = from;
		this.to = to;

	}

	public MemberInterceptionSearchCriteria(String locationId, String from, String to) {
		this.locationId = locationId;
		this.from = from;
		this.to = to;

	}

	public MemberInterceptionSearchCriteria(String memberId) {
		this.memberId = memberId;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}
	/**
	 * will implement later
	 * @return
	 */

	public boolean isValid() {
		// TODO Auto-generated method stub
		return true;
	}

}
