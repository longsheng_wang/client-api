
ALTER TABLE member_intercepts ADD COLUMN intercept_end_time TIMESTAMP WITH TIME ZONE;

ALTER TABLE member_intercepts ADD COLUMN intercept_duration INT;

