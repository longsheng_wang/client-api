/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.beat.model;

/**
 * @author longsheng.wang
 *
 */
public class FeedbackCommentIssueSum {
	
	private String item;
	private long count;

	/**
	 * @param item
	 * @param count
	 */
	public FeedbackCommentIssueSum(String item, long count) {
		super();
		this.item = item;
		this.count = count;
	}

	/**
	 * 
	 */
	public FeedbackCommentIssueSum() {
	}

	/**
	 * @return the item
	 */
	public String getItem() {
		return item;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(String item) {
		this.item = item;
	}

	/**
	 * @return the count
	 */
	public long getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(long count) {
		this.count = count;
	}

}
