package net.reunify.pulse.user;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of UserService
 * 
 * @author kiana.baradaran
 * 
 */
@Service("UserService")
@Transactional(value = "pulseTxManager")
public class UserServiceImpl implements UserService {

	@Inject
	private UserRoleDAO userRoleDAO;

	@Inject
	private UserDAO userDAO;

	@Override
	public int createUserAccount(UserDTO userDTO) {
		return userDAO.createUserAccount(userDTO);
	}

	@Override
	public boolean removeUserAccount(int userId, int clientId) {
		return userDAO.removeUserAccount(userId, clientId);
	}

	@Override
	public boolean editUserAccount(UserDTO user, int clientId) {
		return userDAO.editUserAccount(user, clientId);
	}

	@Override
	public List<UserDTO> getUserAccounts(int clientId) {
		return userDAO.getUserAccounts(clientId);
	}

	@Override
	public boolean changePassword(UserDTO userDTO, int clientId) {
		return userDAO.changePassword(userDTO, clientId);
	}

	@Override
	public List<UserRole> getUserRoles() {
		return userRoleDAO.getAllRoles();
	}

}
