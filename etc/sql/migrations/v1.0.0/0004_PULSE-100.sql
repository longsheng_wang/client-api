-- Adds the extension for furthur use
CREATE EXTENSION dblink;
-- Opens a connection using the connection string
SELECT dblink_connect('connection','dbname=ltf user=systemadmin password=93-g5r9=6;U@');
-- Adding lifetime as a client
insert into clients(industry_name,client_name,is_active) values ('health & fitness','lifetime',true);
-- Adding a test user
insert into users(location_id,client_user_id,firstname,lastname,email_address,username,password,is_active) values (124,-1,'USER','TEST','test@reunify.net','testuser',md5('ReunifyTest278'),true);
-- Load data to client_locations table
INSERT INTO client_locations (
	client_id
	,client_location_id
	,location_name
	,location_timezone
	,is_active
	)
SELECT *,true
FROM dblink('connection', 'select 1,location_id::text, location_name,timezone from location_to_timezone') AS t(client_id INT,client_location_id TEXT, location_name TEXT, location_timezone TEXT);
-- Insert UNKNOWN locations to client_locations table
INSERT INTO client_locations (
	client_id
	,client_location_id
	,location_name
	,location_timezone
	)
SELECT 1,client_location_id,'UNKNOWN','US/CENTRAL'
FROM dblink('connection', 'select distinct club_id from members except select location_id from location_to_timezone') AS t(client_location_id TEXT);
-- Load data to member_check_in
INSERT INTO member_check_in (
	client_member_id
	,location_id
	,check_in_time
	,insert_time
	)
SELECT client_member_id
	,location_id
	,check_in_time
	,insert_time
FROM dblink('connection', 'select distinct member_id::text,location_id::text ,timestamp, insert_time from swipe_table where timestamp >= ''2014-01-01'' and member_or_guest::int = 0') AS t(client_member_id TEXT, client_location_id TEXT, check_in_time timestamptz, insert_time timestamptz)
INNER JOIN client_locations cl ON t.client_location_id = cl.client_location_id; 
-- Load data to members table 
INSERT INTO members (
	location_id
	,client_member_id
	,firstname
	,lastname
	,birthdate
	,gender
	,email_address
	,address
	,city
	,zipcode
	,attrition_score
	,value_score
	,last_score_date
	,last_check_in_id
	)
SELECT cl.location_id
	,t.client_member_id
	,firstname
	,lastname
	,birthdate
	,gender
	,email_address
	,address
	,city
	,zipcode
	,attrition_score
	,value_score
	,last_score_date
	,mci.record_id
FROM dblink('connection','SELECT club_id::TEXT, m.member_id::TEXT, member_first_name, member_last_name, member_birthdate, member_gender,email_address, replace((COALESCE(address_1, '''') || '' '' || COALESCE(address_2, '''')),'' null'','''') AS address, city, zipcode, attrition, value_score, score_date,timestamp FROM members m LEFT JOIN (
		SELECT member_id
			,value_score
			,attrition
			,score_date
			,max(score_date) OVER (PARTITION BY member_id)
		FROM members_analytic_score
		) AS t ON m.member_id = t.member_id left join (select  max(timestamp) as timestamp, member_id from swipe_table where timestamp >= now()-''3months''::interval group by member_id) as s on m.member_id = s.member_id WHERE score_date = max or max isnull') AS t(client_location_id TEXT, client_member_id TEXT, firstname TEXT, lastname TEXT, birthdate DATE, gender TEXT,email_address TEXT, address TEXT, city TEXT, zipcode TEXT, attrition_score INT, value_score FLOAT, last_score_date DATE, last_check_in timestamptz)
		JOIN client_locations cl ON t.client_location_id = cl.client_location_id
		LEFT JOIN member_check_in mci ON t.client_member_id = mci.client_member_id and t.last_check_in = mci.check_in_time;
-- Load data to members_score_history
INSERT INTO members_score_history (
	member_id
	,attrition_score
	,value_score
	,score_date
	,insert_time
	)
SELECT *
FROM dblink(SELECT member_id::TEXT, attrition, value_score, score_date, updatedate FROM members_analytic_score) AS t(client_member_id TEXT, attrition_score INT, value_score FLOAT, score_date DATE, insert_time timestamptz)
INNER JOIN members m ON t.client_member_id = m.client_member_id;
-- Load data to member_intercepts
INSERT INTO member_intercepts (
	member_id
	,interceptor_id
	,location_id
	,intercept_note
	,intercept_tag
	,intercept_time
	,member_attrition_score_at_time
	)
SELECT member_id
	,user_id
	,cl.location_id
	,intercept_note
	,intercept_tag
	,intercept_time
	,member_attrition_score_at_time
FROM dblink('connection', 'SELECT member_id
	,employee_id
	,location_id
	,content
	,notification_tag
	,created_at
	,attrition
FROM (
	SELECT a.member_id::TEXT
		,employee_id::TEXT
		,location_id::TEXT
		,content
		,notification_tag::INT
		,created_at
		,attrition
		,score_date
		,max(score_date) OVER (PARTITION BY a.member_id)
	FROM dashboard_notepad d
	LEFT JOIN members_analytic_score a ON d.member_id = a.member_id
		AND score_date <= created_at
	WHERE notification_tag::INT != 0
	) AS t
WHERE max = score_date
	OR max isnull;') AS t(client_member_id TEXT, client_user_id TEXT, client_location_id TEXT, intercept_note TEXT, intercept_tag INT, intercept_time timestamptz, member_attrition_score_at_time INT)
INNER JOIN client_locations cl ON t.client_location_id = cl.client_location_id
INNER JOIN members m ON t.client_member_id = m.client_member_id
INNER JOIN users u ON t.client_user_id = u.client_user_id;
-- Load persona data to member_personal_interests
INSERT INTO member_personal_interests (
	member_id
	,type
	,score_name
	,score_value
	,update_time
	)
SELECT 	member_id
	,'PERSONA'
	,score_name
	,score_value
	,update_time
FROM dblink('connection', 'SELECT member_id
	,''STUDENT''
	,student
	,update_time
FROM members_persona

UNION ALL

SELECT member_id
	,''BUSINESS PERSON''
	,business_person
	,update_time
FROM members_persona

UNION ALL

SELECT member_id
	,''FAMILY PERSON''
	,family_person
	,update_time
FROM members_persona

UNION ALL

SELECT member_id
	,''SPORTS FAN''
	,sports_fan
	,update_time
FROM members_persona

UNION ALL

SELECT member_id
	,''ATHELETIC''
	,athletic
	,update_time
FROM members_persona

UNION ALL

SELECT member_id
	,''HEALTH CONSCIOUS''
	,health_conscious
	,update_time
FROM members_persona

UNION ALL

SELECT member_id
	,''SOCIAL INFLUENCER''
	,social_influencer
	,update_time
FROM members_persona

UNION ALL

SELECT member_id
	,''SPENDER''
	,spender
	,update_time
FROM members_persona

UNION ALL

SELECT member_id
	,''FASHIONABLE''
	,fashionable
	,update_time
FROM members_persona

UNION ALL

SELECT member_id
	,''BEAUTY''
	,beauty
	,update_time
FROM members_persona

UNION ALL

SELECT member_id
	,''MOVIE BUFF''
	,movie_buff
	,update_time
FROM members_persona

UNION ALL

SELECT member_id
	,''TECHIE''
	,techie
	,update_time
FROM members_persona') AS t(client_member_id TEXT, score_name TEXT, score_value FLOAT, update_time timestamptz)
INNER JOIN members m ON t.client_member_id = m.client_member_id;
-- Load interest data to member_personal_interests
INSERT INTO member_personal_interests (
	member_id
	,type
	,score_name
	,score_value
	)
SELECT 	member_id
	,'INTEREST'
	,score_name
	,score_value
FROM dblink('connection', 'SELECT member_id
	,''AQUATIC''
	,100
FROM members_interest
WHERE aquatic = true

UNION ALL

SELECT member_id
	,''TENNIS''
	,100
FROM members_interest
WHERE tennis = true

UNION ALL

SELECT member_id
	,''BEAUTY & SPA''
	,100
FROM members_interest
WHERE beauty_spa = true

UNION ALL

SELECT member_id
	,''PERSONAL TRAINING''
	,100
FROM members_interest
WHERE personal_training = true

UNION ALL

SELECT member_id
	,''WEIGHT LOSS''
	,100
FROM members_interest
WHERE weight_loss = true

UNION ALL

SELECT member_id
	,''YOGA & PILATES''
	,100
FROM members_interest
WHERE yoga_pilates = true

UNION ALL

SELECT member_id
	,''SPORTS''
	,100
FROM members_interest
WHERE sports = true

UNION ALL

SELECT member_id
	,''FITNESS & CARDIO''
	,100
FROM members_interest
WHERE fitness_cardio = true') AS t(client_member_id TEXT, score_name TEXT, score_value FLOAT)
INNER JOIN members m ON t.client_member_id = m.client_member_id;
-- Load data to intercept_status_guideline
INSERT INTO intercept_status_guideline(record_id,status) values(1,'GOOD TO GO');
INSERT INTO intercept_status_guideline(record_id,status) values(2,'NEED FOLLOW-UP');
INSERT INTO intercept_status_guideline(record_id,status) values(3,'NEED ATTENTION');
-- Load data to member_gym_buddy
INSERT INTO member_gym_buddy(
	location_id
	,member_id
	,client_membership_id
 	,gymbuddy_member_id
 	,gymbuddy_membership_id
 	,score
 	,score_date
	,insert_time
	)
SELECT 	cl.location_id
	,m1.member_id
	,client_membership_id
 	,m2.member_id
 	,gymbuddy_membership_id
 	,score
 	,score_date
	,insert_time
FROM dblink('connection', 'SELECT location_id::text
	,member_id::text
	,membership_id 
	,gymbuddy_member_id::text
	,gymbuddy_membership_id
	,score
	,score_date
	,insert_timestamp
	from members_gymbuddy') AS t(
	client_location_id TEXT
	,client_member_id TEXT
	,client_membership_id INT
 	,gymbuddy_member_id TEXT
 	,gymbuddy_membership_id INT
 	,score DOUBLE PRECISION
 	,score_date DATE
	,insert_time timestamptz
)
INNER JOIN client_locations cl ON t.client_location_id = cl.client_location_id
INNER JOIN members m1 ON t.client_member_id = m1.client_member_id
INNER JOIN members m2 ON t.gymbuddy_member_id = m2.client_member_id;

-- Load data to member_swipes_pattern
INSERT INTO member_swipes_pattern(
	location_id
	,member_id
	,recency
	,frequency
	,sun
	,mon
	,tue
	,wed
	,thu
	,fri
	,sat
	,dawn
	,morning
	,afternoon
	,evening
	,score_date
	,insert_time
)
SELECT cl.location_id
	,member_id
	,recency
	,frequency
	,sun
	,mon
	,tue
	,wed
	,thu
	,fri
	,sat
	,dawn
	,morning
	,afternoon
	,evening
	,score_date
	,insert_time
FROM dblink('connection', 'select club_id::text
	,member_id::text
	,recency
	,frequency
	,sun
	,mon
	,tue
	,wed
	,thu
	,fri
	,sat
	,dawn
	,morning
	,afternoon
	,evening
	,score_date
	,insert_timestamp
	from members_swipes_patterns') AS t(
	client_location_id TEXT 
	,client_member_id TEXT
	,recency INT
	,frequency INT
	,sun  DOUBLE PRECISION
	,mon  DOUBLE PRECISION
	,tue  DOUBLE PRECISION
	,wed  DOUBLE PRECISION
	,thu  DOUBLE PRECISION
	,fri  DOUBLE PRECISION
	,sat  DOUBLE PRECISION
	,dawn  DOUBLE PRECISION
	,morning  DOUBLE PRECISION
	,afternoon  DOUBLE PRECISION
	,evening  DOUBLE PRECISION
	,score_date DATE
	,insert_time timestamptz) 
INNER JOIN client_locations cl ON t.client_location_id = cl.client_location_id
INNER JOIN members m ON t.client_member_id = m.client_member_id;
-- Load data to member_intercept_impact_on_usage
INSERT INTO member_intercept_impact_on_usage(
	location_id
	,score_date
	,time_window_in_days
	,intercepted_members
	,checked_in_members
	,intercepted_members_usage_increase_rate
	,checked_in_members_usage_increase_rate
	,insert_time
)
SELECT 	location_id
	,score_date
	,time_window_in_days
	,intercepted_members
	,checked_in_members
	,intercepted_members_usage_increase_rate
	,checked_in_members_usage_increase_rate
	,insert_time
FROM dblink('connection', 'SELECT location_id::text
	,score_date
	,time_window_length_in_day
	,num_intercepted
	,num_checked_in
	,usage_increase_rate_intercepted_members
	,usage_increase_rate_checked_in_members
	,insert_timestamp from interception_impact_on_usage')AS t(
	client_location_id TEXT
	,score_date DATE
	,time_window_in_days INT
	,intercepted_members INT
	,checked_in_members INT
	,intercepted_members_usage_increase_rate DOUBLE PRECISION
	,checked_in_members_usage_increase_rate DOUBLE PRECISION
	,insert_time timestamptz
	)
INNER JOIN client_locations cl ON t.client_location_id = cl.client_location_id;
-- Load data to member_intercept_impact_on_attrition
INSERT INTO member_intercept_impact_on_attrition(
	location_id
	,score_date
	,attrition_group
	,time_window_in_months
	,distinct_checked_in_members
	,distinct_checked_in_members_cancellation_requested
	,distinct_intercepted_members
	,distinct_intercepted_members_cancellation_requested
	,checked_in_members_attrition_rate
	,intercepted_members_attrition_rate
	,intercepted_members_expected_cancellation
	,intercepted_members_saved
	,insert_time
)
SELECT 
	location_id
	,score_date
	,attrition_group
	,time_window_in_months
	,distinct_checked_in_members
	,distinct_checked_in_members_cancellation_requested
	,distinct_intercepted_members
	,distinct_intercepted_members_cancellation_requested
	,checked_in_members_attrition_rate
	,intercepted_members_attrition_rate
	,intercepted_members_expected_cancellation
	,intercepted_members_saved
	,insert_time
FROM dblink('connection', 'SELECT location_id::text
	,score_date
	,risk_score
	,time_window_length_in_month
	,distinct_checked_in_members
	,distinct_checked_in_members_cancel_requested
	,distinct_intercepted_members
	,distinct_intercepted_members_cancel_requested
	,attrition_rate_checked_in
	,attrition_rate_intercepted
	,among_intercepted_expected_cancelation
	,among_intercepted_save
	,insert_timestamp
	from interception_impact_on_attrition') AS t(
	client_location_id TEXT
	,score_date DATE
	,attrition_group INT
	,time_window_in_months INT
	,distinct_checked_in_members INT
	,distinct_checked_in_members_cancellation_requested INT
	,distinct_intercepted_members INT
	,distinct_intercepted_members_cancellation_requested INT
	,checked_in_members_attrition_rate DOUBLE PRECISION
	,intercepted_members_attrition_rate DOUBLE PRECISION
	,intercepted_members_expected_cancellation INT
	,intercepted_members_saved INT
	,insert_time timestamptz )
INNER JOIN client_locations cl ON t.client_location_id = cl.client_location_id;
-- Load data in members_fitness
INSERT INTO members_fitness(
	member_id
	,membership_id
	,membership_status
	,membership_non_junior_members
	,membership_junior_members
	,membership_type
	,membership_type_description
	,membership_creation_date
	,membership_activation_date
	,membership_cancellation_request_date
	,membership_termination_date
	,cancellation_reason
	,member_join_date
	,monthly_dues
	,member_type
	,member_status
)
SELECT member_id
	,membership_id
	,membership_status
	,membership_non_junior_members
	,membership_junior_members
	,membership_type
	,membership_type_description
	,membership_creation_date
	,membership_activation_date
	,membership_cancellation_request_date
	,membership_termination_date
	,cancellation_reason
	,member_join_date
	,monthly_dues
	,member_type
	,member_status
FROM dblink('connection','SELECT member_id::text
	,membership_id
	,membership_status
	,total_non_junior_members
	,total_junior_members
	,membership_type_family_status
	,membership_type_description
	,membership_created_date
	,membership_activation_date
	,membership_cancellation_request_date
	,membership_termination_date
	,cancellation_reason
	,member_join_date
	,monthly_dues_price
	,member_type
	,member_status	
	from members') AS t(
	client_member_id TEXT
	,membership_id TEXT
	,membership_status TEXT
	,membership_non_junior_members INT
	,membership_junior_members INT
	,membership_type TEXT
	,membership_type_description TEXT
	,membership_creation_date DATE
	,membership_activation_date DATE
	,membership_cancellation_request_date DATE
	,membership_termination_date DATE
	,cancellation_reason TEXT
	,member_join_date DATE
	,monthly_dues DOUBLE PRECISION
	,member_type TEXT
	,member_status TEXT
)
INNER JOIN members m ON t.client_member_id = m.client_member_id;

-- disconnect 
SELECT dblink_disconnect('connection'); 
-- Remove the extension
DROP EXTENSION dblink;