/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.beat.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import net.reunify.beat.dao.CommentDao;
import net.reunify.beat.model.FeedbackComment;
import net.reunify.beat.model.FeedbackCommentIssueSum;
import net.reunify.beat.model.PersonMention;
import net.reunify.beat.model.SentimentCount;
import net.reunify.beat.model.SurveyAnswerCount;
import net.reunify.pulse.base.InvalidInputFormat;
import net.reunify.pulse.base.RecordNotFoundException;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author longsheng.wang
 *
 */
@Service("commentService")
public class CommentServiceImpl implements CommentService{
	
	@Inject
	@Named("commentDao")
	private CommentDao commentDao;
	
	private final int DEFAULT_NUMBER_OF_COMMENTS = 10;
	
	/**
	 * 
	 */
	public CommentServiceImpl() {
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getComments(int, java.sql.Timestamp, java.sql.Timestamp)
	 */
	@Override
	public List<FeedbackComment> getComments(int maxNum, Timestamp from, Timestamp to) {
		if (maxNum <= 0){
			throw new InvalidInputFormat("Maximum number of comments must be larger than 0.");
		}
		if (to.before(from)){
			throw new InvalidInputFormat("Time window set incorrectly!");
		}
		return commentDao.getComments(maxNum, from, to);
	}
	
	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getMostRecentComments(int)
	 */
	@Override
	public List<FeedbackComment> getAllComments(int maxNum) {
		if (maxNum <= 0){
			throw new InvalidInputFormat("Maximum number of comments must be larger than 0.");
		}
		return getComments(maxNum, DatePeriod.getVeryBeginningDate(), DatePeriod.getNow());
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getMostRecentComments()
	 */
	@Override
	public List<FeedbackComment> getAllComments() {
		return getAllComments(DEFAULT_NUMBER_OF_COMMENTS);
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getLatestComment()
	 */
	@Override
	public FeedbackComment getLatestComment() {
		return getAllComments(1).get(0);
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getCommentIssueCount(int, java.sql.Timestamp)
	 */
	@Override
	public List<FeedbackCommentIssueSum> getCommentIssueCount(int maxNum, Timestamp from, Timestamp to) {
		if (maxNum <= 0){
			throw new InvalidInputFormat("Maximum number of comments must be larger than 0.");
		}
		return commentDao.getIssueSum(maxNum, from, to);
	}
	
	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getCommentIssueCount(int, java.sql.Timestamp)
	 */
	@Override
	public List<FeedbackCommentIssueSum> getCommentIssueCount(int maxNum, Timestamp from) {
		return getCommentIssueCount(maxNum, from, convertDateToTimestamp(DatePeriod.getNow()));
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getSentimentCount(java.sql.Timestamp)
	 */
	@Override
	public SentimentCount getSentimentCount(Timestamp from, Timestamp to) {
		return commentDao.getSentimentCount(from, to);
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getSentimentCount(java.sql.Timestamp)
	 */
	@Override
	public SentimentCount getSentimentCount(Timestamp from) {
		return getSentimentCount(from, convertDateToTimestamp(DatePeriod.getNow()));
	}
	
	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getMostMentionedPeople(int, java.sql.Timestamp)
	 */
	@Override
	public List<PersonMention> getMostMentionedPeople(int maxNum, Timestamp from, Timestamp to) {
		if (maxNum <= 0){
			throw new InvalidInputFormat("Maximum number of comments must be larger than 0.");
		}
		return commentDao.getMostMentionedPeople(maxNum, from, to);
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getMostMentionedPeople(int, java.sql.Timestamp)
	 */
	@Override
	public List<PersonMention> getMostMentionedPeople(int maxNum, Timestamp from) {
		return getMostMentionedPeople(maxNum, from, convertDateToTimestamp(DatePeriod.getNow()));
	}
	
	private Timestamp convertDateToTimestamp(Date date){
		return new Timestamp(date.getTime());
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getComments(int, java.util.Date, java.util.Date)
	 */
	@Override
	public List<FeedbackComment> getComments(int maxNum, Date from, Date to) {
		return getComments(maxNum, convertDateToTimestamp(from), convertDateToTimestamp(to));
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getCommentIssueCount(int, java.util.Date)
	 */
	@Override
	public List<FeedbackCommentIssueSum> getCommentIssueCount(int maxNum, Date from) {
		return getCommentIssueCount(maxNum, convertDateToTimestamp(from));
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getCommentIssueCount(int, java.util.Date, java.util.Date)
	 */
	@Override
	public List<FeedbackCommentIssueSum> getCommentIssueCount(int maxNum, Date from, Date to) {
		return getCommentIssueCount(maxNum, convertDateToTimestamp(from), convertDateToTimestamp(to));
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getSentimentCount(java.util.Date)
	 */
	@Override
	public SentimentCount getSentimentCount(Date from) {
		return getSentimentCount(convertDateToTimestamp(from));
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getSentimentCount(java.util.Date, java.util.Date)
	 */
	@Override
	public SentimentCount getSentimentCount(Date from, Date to) {
		return getSentimentCount(convertDateToTimestamp(from), convertDateToTimestamp(to));
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getMostMentionedPeople(int, java.util.Date)
	 */
	@Override
	public List<PersonMention> getMostMentionedPeople(int maxNum, Date from) {
		return getMostMentionedPeople(maxNum, convertDateToTimestamp(from));
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getMostMentionedPeople(int, java.util.Date, java.util.Date)
	 */
	@Override
	public List<PersonMention> getMostMentionedPeople(int maxNum, Date from, Date to) {
		return getMostMentionedPeople(maxNum, convertDateToTimestamp(from), convertDateToTimestamp(to));
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getAllSurveyAnswerCounts(java.util.Date, java.util.Date)
	 */
	@Override
	public List<SurveyAnswerCount> getAllSurveyAnswerCounts(Date from, Date to) {
		return commentDao.getAllSurveyQuestionAnswerCount(convertDateToTimestamp(from), convertDateToTimestamp(to));
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#getAllSurveyAnswerCountsByLocation(java.util.Date, java.util.Date)
	 */
	@Override
	public List<SurveyAnswerCount> getAllSurveyAnswerCountsByLocation(Date from, Date to) {
		return commentDao.getAllSurveyAnswerCountByLocation(convertDateToTimestamp(from), convertDateToTimestamp(to));
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#resolveComment(long)
	 */
	@Override
	@Transactional
	public int resolveOneComment(long id) {
		try{
			if (commentDao.checkCommentResolved(id)){
				return 0;
			}
		}
		catch (EmptyResultDataAccessException exception){
			throw new RecordNotFoundException("No comment id: " + id);
		}
		return commentDao.updateCommentResolvedAndResolvedTime(id, convertDateToTimestamp(new Date()));
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.service.CommentService#checkCommentResolvedTime(long)
	 */
	@Override
	public Date checkCommentResolvedTime(long id) {
		return commentDao.checkCommentResolvedTimeStamp(id);
	}
}
