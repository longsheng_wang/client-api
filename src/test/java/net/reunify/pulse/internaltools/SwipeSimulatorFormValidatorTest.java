/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.internaltools;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

/**
 * @author Vivek Hungund
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/spring-security.xml",
		"file:src/test/webapp/WEB-INF/mvc-dispatcher-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class SwipeSimulatorFormValidatorTest {
	private static final Logger logger = Logger.getLogger(SwipeSimulatorFormValidatorTest.class);

	@Test
	public void testSuccess() {
		SwipeSimulatorForm form = new SwipeSimulatorForm(1,137,10);
		Errors errors = new BeanPropertyBindingResult(form, "form");
		
		SwipeSimulatorFormValidator validator = new SwipeSimulatorFormValidator();
		validator.validate(form, errors);
		
		assertTrue(!errors.hasErrors());
	}
	
	@Test
	public void testInvalidClient() {
		SwipeSimulatorForm form = new SwipeSimulatorForm(0,137,10);
		Errors errors = new BeanPropertyBindingResult(form, "form");
		
		SwipeSimulatorFormValidator validator = new SwipeSimulatorFormValidator();
		validator.validate(form, errors);
		assertTrue(errors.hasErrors());
		assertEquals("invalid_client", errors.getAllErrors().get(0).getCode());
	}
	
	@Test
	public void testInvalidLocation() {
		SwipeSimulatorForm form = new SwipeSimulatorForm(1,0,10);
		Errors errors = new BeanPropertyBindingResult(form, "form");
		
		SwipeSimulatorFormValidator validator = new SwipeSimulatorFormValidator();
		validator.validate(form, errors);
		assertTrue(errors.hasErrors());
		assertEquals("invalid_location", errors.getAllErrors().get(0).getCode());
	}
	
	@Test
	public void testNumSwipes() {
		SwipeSimulatorForm form = new SwipeSimulatorForm(1,137,5001);
		Errors errors = new BeanPropertyBindingResult(form, "form");
		
		SwipeSimulatorFormValidator validator = new SwipeSimulatorFormValidator();
		validator.validate(form, errors);
		assertTrue(errors.hasErrors());
		assertEquals("numSwipes_out_of_range", errors.getAllErrors().get(0).getCode());
	}
	
}
