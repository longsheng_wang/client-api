/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.internaltools;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import net.reunify.pulse.client.ClientDTO;
import net.reunify.pulse.client.ClientService;
import net.reunify.pulse.client.LocationDTO;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * SwipeSimulatorController handles all requests for simulation tools
 *
 * @author Vivek Hungund
 *
 */
@Controller
public class SwipeSimulatorController {
	private static final Logger logger = Logger.getLogger(SwipeSimulatorController.class);

	@Inject
	@Named("SwipeSimulatorService")
	private SwipeSimulatorService simulatorServiceHandler;
	
	@Inject
	@Named("ClientService")
	private ClientService clientServiceHandler;

	/**
	 * Retrieve form to start swipe simulator
	 *
	 * @param request
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@RequestMapping(value = "/simulators/swipesimulator", produces = "text/html", method = { RequestMethod.GET,
			RequestMethod.POST })
	@ResponseBody
	public ModelAndView swipeSimulator(HttpServletRequest request,
			@RequestParam(required = false, defaultValue = "0") String clientId,
			@RequestParam(required = false, defaultValue = "0") String locationId,
			@RequestParam(required = false) Integer numSwipes,
			@ModelAttribute("SwipeSimulatorForm") SwipeSimulatorForm form,
			BindingResult result) {
		
		int cId = Integer.parseInt(clientId);
		int lId = Integer.parseInt(locationId); 
		
		ModelAndView mv = new ModelAndView("swipesimulator");
		List<ClientDTO> activeClients = clientServiceHandler.getActiveClients();
		
		
		List<LocationDTO> locations = new ArrayList<LocationDTO>();
		if (cId > 0) {
			locations = clientServiceHandler.getClientLocations(cId);
		}

		mv.addObject("clients", activeClients);
		mv.addObject("locations", locations);
		

		if (request.getMethod().equals("POST")) {
			
			SwipeSimulatorFormValidator validator = new SwipeSimulatorFormValidator();
			validator.validate(form, result);
			
			if (!result.hasErrors()) {
				simulatorServiceHandler.startSwipeSimulator(cId, lId,numSwipes);
				
				ArrayList<SwipeSimulator> runningSimulators = (ArrayList<SwipeSimulator>) simulatorServiceHandler.getRunningSwipeSimulators();
				mv.addObject("latestSimulator", runningSimulators.get(runningSimulators.size() - 1));
			}
		}
		
		List<SwipeSimulator> runningSimulators = simulatorServiceHandler.getRunningSwipeSimulators();
		mv.addObject("runningSimulators", runningSimulators);
		
		mv.addObject(form);

		return mv;
	}

}
