/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var JsonObj = null;

function load_survey_result_details(survey_type, device_location_id, mode) {

    var location_id = localStorage.getItem("location_id");
    $.ajax({
        data: {
            location_id: location_id,
            survey_type: survey_type,
            device_location_id: device_location_id
        },
        type: 'POST',
        async: true,
        url: 'jsp/survey/GetSurveyIndividualProgressBars.jsp',
        dataType: 'json',
        success: function(data) {
            if (jQuery.isEmptyObject(data)) {

                $('.pool_member_satisfaction_week').animate({width: 0 + "%"}, 700);
                $('.pool_staff_attitude_week').animate({width: 0 + "%"}, 700);
                $('.pool_club_cleanliness_week').animate({width: 0 + "%"}, 700);
                $('.pool_member_satisfaction_month').animate({width: 0 + "%"}, 700);
                $('.pool_staff_attitude_month').animate({width: 0 + "%"}, 700);
                $('.pool_club_cleanliness_month').animate({width: 0 + "%"}, 700);
                $('.group_fitness_member_satisfaction_month').animate({width: 0 + "%"}, 700);
                $('.group_fitness_staff_attitude_month').animate({width: 0 + "%"}, 700);
                $('.group_fitness_club_cleanliness_month').animate({width: 0 + "%"}, 700);
                $('.group_fitness_member_satisfaction_week').animate({width: 0 + "%"}, 700);
                $('.group_fitness_staff_attitude_week').animate({width: 0 + "%"}, 700);
                $('.group_fitness_club_cleanliness_week').animate({width: 0 + "%"}, 700);
                $('.workout_floor_member_satisfaction_month').animate({width: 0 + "%"}, 700);
                $('.workout_floor_staff_attitude_month').animate({width: 0 + "%"}, 700);
                $('.workout_floor_club_cleanliness_month').animate({width: 0 + "%"}, 700);
                $('.workout_floor_member_satisfaction_week').animate({width: 0 + "%"}, 700);
                $('.workout_floor_staff_attitude_week').animate({width: 0 + "%"}, 700);
                $('.workout_floor_club_cleanliness_week').animate({width: 0 + "%"}, 700);
                $('.men_locker_room_member_satisfaction_month').animate({width: 0 + "%"}, 700);
                $('.women_locker_room_staff_attitude_month').animate({width: 0 + "%"}, 700);
                $('.family_locker_room_club_cleanliness_month').animate({width: 0 + "%"}, 700);
                $('.men_locker_room_member_satisfaction_week').animate({width: 0 + "%"}, 700);
                $('.women_locker_room_staff_attitude_week').animate({width: 0 + "%"}, 700);
                $('.family_locker_room_club_cleanliness_week').animate({width: 0 + "%"}, 700);
                $('.entrance_member_satisfaction_month').animate({width: 0 + "%"}, 700);
                $('.entrance_staff_attitude_month').animate({width: 0 + "%"}, 700);
                $('.entrance_club_cleanliness_month').animate({width: 0 + "%"}, 700);
                $('.entrance_member_satisfaction_week').animate({width: 0 + "%"}, 700);
                $('.entrance_staff_attitude_week').animate({width: 0 + "%"}, 700);
                $('.entrance_club_cleanliness_week').animate({width: 0 + "%"}, 700);
                $('.unknown_locker_room_club_cleanliness_month').animate({width: 0 + "%"}, 700);
                $('.unknown_locker_room_club_cleanliness_week').animate({width: 0 + "%"}, 700);

            } else {


                if (mode === 1) {

                    $('#pool-today-rate-percentage').text(data.daily.rate + "%");
                    $('#pool-today-number-helped').text(data.daily.help);
                    $('#pool-7days-rate-percentage').text(data.weekly.rate + "%");
                    $('#pool-7days-number-helped').text(data.weekly.help);
                    $('#pool-month-rate-percentage').text(data.monthly.rate + "%");
                    $('#pool-month-number-helped').text(data.monthly.help);
                    $('.pool_member_satisfaction_month').animate({width: data.aggregate.month.id00 + "%"}, 500);
                    $('.pool_member_satisfaction_week').animate({width: data.aggregate.week.id00 + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('.pool_member_satisfaction_week', data.aggregate.week.id00)
                    });
//                    $('#member-satisfaction-pool-speech-bubble').text(data.aggregate.week.id00 + '%');
//                    $('#member-satisfaction-pool-main-bubble-div').css('margin-left', data.aggregate.week.id00 - 47 + '%');

                    $('.pool_staff_attitude_month').animate({width: data.aggregate.month.id01 + "%"}, 500);
                    $('.pool_staff_attitude_week').animate({width: data.aggregate.week.id01 + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('.pool_staff_attitude_week', data.aggregate.week.id01)
                    });
//                    $('#staff-attitude-pool-speech-bubble').text(data.aggregate.week.id01 + '%');
//                    $('#staff-attitude-pool-main-bubble-div').css('margin-left', (data.aggregate.week.id01 - 47) + '%');

                    $('.pool_club_cleanliness_month').animate({width: data.aggregate.month.id10 + "%"}, 500);
                    $('.pool_club_cleanliness_week').animate({width: data.aggregate.week.id10 + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('.pool_club_cleanliness_week', data.aggregate.week.id10)
                    });
//                    $('#club-cleanliness-pool-speech-bubble').text(data.aggregate.week.id10 + '%');
//                    $('#club-cleanliness-pool-main-bubble-div').css('margin-left', data.aggregate.week.id10 - 47 + '%');



                }
                else if (mode === 2) {
                    $('#group_fitness-today-rate-percentage').text(data.daily.rate + "%");
                    $('#group_fitness-today-number-helped').text(data.daily.help);
                    $('#group_fitness-7days-rate-percentage').text(data.weekly.rate + "%");
                    $('#group_fitness-7days-number-helped').text(data.weekly.help);
                    $('#group_fitness-month-rate-percentage').text(data.monthly.rate + "%");
                    $('#group_fitness-month-number-helped').text(data.monthly.help);
                    $('.group_fitness_member_satisfaction_month').animate({width: data.aggregate.month.id00 + "%"}, 500);
                    $('.group_fitness_member_satisfaction_week').animate({width: data.aggregate.week.id00 + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('.group_fitness_member_satisfaction_week', data.aggregate.week.id00)
                    });
//                    $('#member-satisfaction-group-fitness-speech-bubble').text(data.aggregate.week.id00 + '%');
//                    $('#member-satisfaction-group-fitness-main-bubble-div').css('margin-left', data.aggregate.week.id00 - 47 + '%');

                    $('.group_fitness_staff_attitude_month').animate({width: data.aggregate.month.id01 + "%"}, 500);
                    $('.group_fitness_staff_attitude_week').animate({width: data.aggregate.week.id01 + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('.group_fitness_staff_attitude_week', data.aggregate.week.id01)
                    });
//                    $('#staff-attitude-group-fitness-speech-bubble').text(data.aggregate.week.id01 + '%');
//                    $('#staff-attitude-group-fitness-main-bubble-div').css('margin-left', data.aggregate.week.id01 - 47 + '%');

                    $('.group_fitness_club_cleanliness_month').animate({width: data.aggregate.month.id10 + "%"}, 500);
                    $('.group_fitness_club_cleanliness_week').animate({width: data.aggregate.week.id10 + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('.group_fitness_club_cleanliness_week', data.aggregate.week.id10)
                    });
//                    $('#club-cleanliness-group-fitness-speech-bubble').text(data.aggregate.week.id10 + '%');
//                    $('#club-cleanliness-group-fitness-main-bubble-div').css('margin-left', data.aggregate.week.id10 - 47 + '%');

                } else if (mode === 3) {
                    $('#workout_floor-today-rate-percentage').text(data.daily.rate + "%");
                    $('#workout_floor-today-number-helped').text(data.daily.help);
                    $('#workout_floor-7days-rate-percentage').text(data.weekly.rate + "%");
                    $('#workout_floor-7days-number-helped').text(data.weekly.help);
                    $('#workout_floor-month-rate-percentage').text(data.monthly.rate + "%");
                    $('#workout_floor-month-number-helped').text(data.monthly.help);
                    $('.workout_floor_member_satisfaction_month').animate({width: data.aggregate.month.id00 + "%"}, 500);
                    $('.workout_floor_member_satisfaction_week').animate({width: data.aggregate.week.id00 + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('.workout_floor_member_satisfaction_week', data.aggregate.week.id00)
                    });
//                    $('#member-satisfaction-workout-floor-speech-bubble').text(data.aggregate.week.id00 + '%');
//                    $('#member-satisfaction-workout-floor-main-bubble-div').css('margin-left', data.aggregate.week.id00 - 47 + '%');

                    $('.workout_floor_staff_attitude_month').animate({width: data.aggregate.month.id01 + "%"}, 500);
                    $('.workout_floor_staff_attitude_week').animate({width: data.aggregate.week.id01 + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('.workout_floor_staff_attitude_week', data.aggregate.week.id01)
                    });
//                    $('#staff-attitude-workout-floor-speech-bubble').text(data.aggregate.week.id01 + '%');
//                    $('#staff-attitude-workout-floor-main-bubble-div').css('margin-left', data.aggregate.week.id01 - 47 + '%');

                    $('.workout_floor_club_cleanliness_month').animate({width: data.aggregate.month.id10 + "%"}, 500);
                    $('.workout_floor_club_cleanliness_week').animate({width: data.aggregate.week.id10 + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('.workout_floor_club_cleanliness_week', data.aggregate.week.id10)
                    });
//                    $('#club-cleanliness-workout-floor-speech-bubble').text(data.aggregate.week.id10 + '%');
//                    $('#club-cleanliness-workout-floor-main-bubble-div').css('margin-left', data.aggregate.week.id10 - 47 + '%');

                } else if (mode === 4) {
                    $('#men_locker_room-today-rate-percentage').text(data.daily.rate + "%");
                    $('#men_locker_room-today-number-helped').text(data.daily.help);
                    $('#men_locker_room-7days-rate-percentage').text(data.weekly.rate + "%");
                    $('#men_locker_room-7days-number-helped').text(data.weekly.help);
                    $('#men_locker_room-month-rate-percentage').text(data.monthly.rate + "%");
                    $('#men_locker_room-month-number-helped').text(data.monthly.help);
                    $('.men_locker_room_club_cleanliness_month').animate({width: data.aggregate.month.id10 + "%"}, 500);
                    $('.men_locker_room_club_cleanliness_week').animate({width: data.aggregate.week.id10 + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('.men_locker_room_club_cleanliness_week', data.aggregate.week.id10)
                    });
//                    $('#club-cleanliness-men-locker-room-speech-bubble').text(data.aggregate.week.id10 + '%');
//                    $('#club-cleanliness-men-locker-room-main-bubble-div').css('margin-left', data.aggregate.week.id10 - 47 + '%');
                } else if (mode === 5) {
                    $('#women_locker_room-today-rate-percentage').text(data.daily.rate + "%");
                    $('#women_locker_room-today-number-helped').text(data.daily.help);
                    $('#women_locker_room-7days-rate-percentage').text(data.weekly.rate + "%");
                    $('#women_locker_room-7days-number-helped').text(data.weekly.help);
                    $('#women_locker_room-month-rate-percentage').text(data.monthly.rate + "%");
                    $('#women_locker_room-month-number-helped').text(data.monthly.help);
                    $('.women_locker_room_club_cleanliness_month').animate({width: data.aggregate.month.id10 + "%"}, 500);
                    $('.women_locker_room_club_cleanliness_week').animate({width: data.aggregate.week.id10 + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('.women_locker_room_club_cleanliness_week', data.aggregate.week.id10)
                    });
//                    $('#club-cleanliness-women-locker-room-speech-bubble').text(data.aggregate.week.id10 + '%');
//                    $('#club-cleanliness-women-locker-room-main-bubble-div').css('margin-left', data.aggregate.week.id10 - 47 + '%');
                } else if (mode === 6) {
                    $('#family_locker_room-today-rate-percentage').text(data.daily.rate + "%");
                    $('#family_locker_room-today-number-helped').text(data.daily.help);
                    $('#family_locker_room-7days-rate-percentage').text(data.weekly.rate + "%");
                    $('#family_locker_room-7days-number-helped').text(data.weekly.help);
                    $('#family_locker_room-month-rate-percentage').text(data.monthly.rate + "%");
                    $('#family_locker_room-month-number-helped').text(data.monthly.help);
                    $('.family_locker_room_club_cleanliness_month').animate({width: data.aggregate.month.id10 + "%"}, 500);
                    $('.family_locker_room_club_cleanliness_week').animate({width: data.aggregate.week.id10 + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('.family_locker_room_club_cleanliness_week', data.aggregate.week.id10)
                    });
//                    $('#club-cleanliness-family-locker-room-speech-bubble').text(data.aggregate.week.id10 + '%');
//                    $('#club-cleanliness-family-locker-room-main-bubble-div').css('margin-left', data.aggregate.week.id10 - 47 + '%');
                } else if (mode === 8) {
                    $('#entrance-today-rate-percentage').text(data.daily.rate + "%");
                    $('#entrance-today-number-helped').text(data.daily.help);
                    $('#entrance-7days-rate-percentage').text(data.weekly.rate + "%");
                    $('#entrance-7days-number-helped').text(data.weekly.help);
                    $('#entrance-month-rate-percentage').text(data.monthly.rate + "%");
                    $('#entrance-month-number-helped').text(data.monthly.help);
                    $('.entrance_member_satisfaction_month').animate({width: data.aggregate.month.id00 + "%"}, 500);
                    $('.entrance_member_satisfaction_week').animate({width: data.aggregate.week.id00 + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('.entrance_member_satisfaction_week', data.aggregate.week.id00)
                    });
//                    $('#member-satisfaction-workout-floor-speech-bubble').text(data.aggregate.week.id00 + '%');
//                    $('#member-satisfaction-workout-floor-main-bubble-div').css('margin-left', data.aggregate.week.id00 - 47 + '%');

                    $('.entrance_staff_attitude_month').animate({width: data.aggregate.month.id01 + "%"}, 500);
                    $('.entrance_staff_attitude_week').animate({width: data.aggregate.week.id01 + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('.entrance_staff_attitude_week', data.aggregate.week.id01)
                    });
//                    $('#staff-attitude-workout-floor-speech-bubble').text(data.aggregate.week.id01 + '%');
//                    $('#staff-attitude-workout-floor-main-bubble-div').css('margin-left', data.aggregate.week.id01 - 47 + '%');

                    $('.entrance_club_cleanliness_month').animate({width: data.aggregate.month.id10 + "%"}, 500);
                    $('.entrance_club_cleanliness_week').animate({width: data.aggregate.week.id10 + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('.entrance_club_cleanliness_week', data.aggregate.week.id10)
                    });
                } else if (mode === 7) {
                    $('#unknown_locker_room-today-rate-percentage').text(data.daily.rate + "%");
                    $('#unknown_locker_room-today-number-helped').text(data.daily.help);
                    $('#unknown_locker_room-7days-rate-percentage').text(data.weekly.rate + "%");
                    $('#unknown_locker_room-7days-number-helped').text(data.weekly.help);
                    $('#unknown_locker_room-month-rate-percentage').text(data.monthly.rate + "%");
                    $('#unknown_locker_room-month-number-helped').text(data.monthly.help);
                    $('.unknown_locker_room_club_cleanliness_month').animate({width: data.aggregate.month.id10 + "%"}, 500);
                    $('.unknown_locker_room_club_cleanliness_week').animate({width: data.aggregate.week.id10 + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('.unknown_locker_room_club_cleanliness_week', data.aggregate.week.id10)
                    });
//                    $('#club-cleanliness-family-locker-room-speech-bubble').text(data.aggregate.week.id10 + '%');
//                    $('#club-cleanliness-family-locker-room-main-bubble-div').css('margin-left', data.aggregate.week.id10 - 47 + '%');
                }

            }
        }
    });
//    var interval = setTimeout(function() {
//               load_survey_result_details(survey_type, device_location_id, mode);
//       }, 30000);
//       member_feedback_individual_interval.push(interval);
       //console.log(member_feedback_individual_interval);
    

}



function load_member_feedback_progress_bars() {
    var location_id = localStorage.getItem("location_id");
    $.ajax({
        data: {
            location_id: location_id
        },
        type: 'POST',
        async: true,
        url: 'jsp/survey/GetSurveyProgressBars.jsp',
        dataType: 'json',
        success: function(data) {
            if (jQuery.isEmptyObject(data)) {
                $('.member_satisfaction_week').css('width', "0%");
                $('.staff_attitude_week').css('width', "0%");
                $('.club_cleanliness_week').css('width', "0%");
                $('.member_satisfaction_month').css('width', "0%");
                $('.staff_attitude_month').css('width', "0%");
                $('.club_cleanliness_month').css('width', "0%");
            } else {
                $('#members_taken_survey_today').text(data.total1);
                $('#members_taken_survey_this_month').text(data.total2);
                for (var key in data.week) {
                    if (key === "00") {
                        $('.member_satisfaction_week').animate({width: data.week[key] + "%"}, {
                            duration: 600,
                            start: set_progress_bars_colors('.member_satisfaction_week', data.week[key])
                        });
                    } else if (key === "01") {
                        $('.staff_attitude_week').animate({width: data.week[key] + "%"}, {
                            duration: 600,
                            start: set_progress_bars_colors('.staff_attitude_week', data.week[key])
                        });
                    } else {
                        $('.club_cleanliness_week').animate({width: data.week[key] + "%"}, {
                            duration: 600,
                            start: set_progress_bars_colors('.club_cleanliness_week', data.week[key])
                        });
                    }
                }
                for (var key in data.month) {
                    if (key === "00") {
                        $('.member_satisfaction_month').animate({width: data.month[key] + "%"}, 600);

                    } else if (key === "01") {
                        $('.staff_attitude_month').animate({width: data.month[key] + "%"}, 600);
                    } else {
                        $('.club_cleanliness_month').animate({width: data.month[key] + "%"}, 600);
                    }
                }
            }
        }
    });
}


function get_member_feedback_aggregation_data() {

//    var JsonObj_monthly;
//    var JsonObj_hourly;

    var location_id = localStorage.getItem("location_id");
    $.ajax({
        data: {
            location_id: location_id,
            interval: "11 months",
            step: "1 month",
        },
        type: 'POST',
        async: false,
        url: 'jsp/survey/GetSurveyAggregation.jsp',
        dataType: 'json',
        success: function(data) {
            if (jQuery.isEmptyObject(data)) {

            } else {

                JsonObj = data;
//                console.log(JsonObj);
            }
        }

    });
    $.ajax({
        data: {
            location_id: location_id,
            interval: "11 hours",
            step: "1 hour",
        },
        type: 'POST',
        async: false,
        url: 'jsp/survey/GetSurveyAggregation.jsp',
        dataType: 'json',
        success: function(data) {
            if (jQuery.isEmptyObject(data)) {

            } else {

                $.extend(JsonObj, data);
//                console.log(JsonObj_hourly);
            }
        }

    });
//    var Json = "{" + JsonObj_hourly +"," + JsonObj_monthly +"}";
//    var JsonObj = eval('(' + Json + ')');

}

function load_member_feedback_aggregation() {
    
    if(JsonObj!== null && JsonObj !== ""){

    var location = $("#member-feedback-aggregation-location").text();
    var interval_text = $("#member-feedback-aggregation-interval").text();
    var interval;
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    if (interval_text === "Hourly") {
//        var this_hour = new Date().getHours();
        var this_hour = localStorage.getItem("hour");
        console.log(this_hour);
        for (var i = 0; i <12; i++) {

            $("#member-feedback-aggregation-col-" + (11-i)).text((this_hour - i  + 24) % 24 + ":00\n" + (this_hour - i + 25) % 24 + ":00");
        }
        interval = 'hourly';
//        JsonObj = JsonObj_hourly;

    } else {



        var this_mon = new Date().getMonth();
//                    console.log(13 % 12);
        for (var i = 0; i < 12 ; i++) {
            $("#member-feedback-aggregation-col-" + (11-i)).text(months[(this_mon - i + 12) % 12]);
        }
        interval = 'monthly';
//        JsonObj = JsonObj_monthly;
    }
    var j = 1;
//    console.log(JsonObj);

    for (var key in (JsonObj[interval])[location]) {
        var question = (JsonObj[interval])[location][key];
        var i = 1;
        $("#member-feedback-aggregation-row-" + j + "-col-0").text(key);
//                    $("#member-feedback-aggregation-row-" + j + "-col-0").text(key);
//                    var newRow = document.createElement('tr');
//                    var currentQuestion = document.createTextNode(key);
//                    var newCol = document.createElement('td');
//                    newCol.appendChild(currentQuestion);
//                    newRow.appendChild(newCol);
        for (var item in question) {

            var value = question[item];
//                        var yearCol = document.createElement('td');
//                        yearCol.id = "month-number" + i;

            if (value >= 0 && value <= 25) {
//                            yearCol.style.backgroundColor = "#f03224";
                $("#member-feedback-aggregation-row-" + j + "-col-" + i).css("background-color", "#f03224");
            }

            else if (value > 25 && value < 75) {
//                            yearCol.style.backgroundColor = "#f9ae04";
                $("#member-feedback-aggregation-row-" + j + "-col-" + i).css("background-color", "#f9ae04");
            }

            else if (value >= 75 && value <= 100) {
//                            yearCol.style.backgroundColor = "#27b24a";
                $("#member-feedback-aggregation-row-" + j + "-col-" + i).css("background-color", "#27b24a");
            }
//                        else if (value === 'undefined') {
//                            yearCol.style.backgroundColor = "#b3b3b3";
//                        }

//                        newRow.appendChild(yearCol);
            i++;
        }
//                    $('#summary-year-questions-table-body').append(newRow);
        j++;
    }

    }
}
