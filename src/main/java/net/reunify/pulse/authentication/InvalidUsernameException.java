/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.authentication;

/**
 * This exception is thrown when the username does not match the specified
 * criteria
 * 
 * @author kiana.baradaran
 * 
 */
public class InvalidUsernameException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidUsernameException() {
		// TODO Auto-generated constructor stub
	}

	public InvalidUsernameException(String message) {
		super(message);
	}

}
