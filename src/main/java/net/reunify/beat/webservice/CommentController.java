/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.beat.webservice;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;
import javax.inject.Named;

import net.reunify.beat.base.BeatBaseController;
import net.reunify.beat.model.FeedbackComment;
import net.reunify.beat.model.FeedbackCommentIssueSum;
import net.reunify.beat.model.PersonMention;
import net.reunify.beat.model.SentimentCount;
import net.reunify.beat.model.SurveyAnswerCount;
import net.reunify.beat.service.CommentService;
import net.reunify.beat.service.DatePeriod;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author longsheng.wang
 *
 */
@Controller
public class CommentController extends BeatBaseController{
	
	private static final Logger logger = Logger.getLogger(CommentController.class);

	@Inject
	@Named("commentService")
	private CommentService commentService;
	
	@RequestMapping(value = "/pulsePageView", method = RequestMethod.GET)
	public @ResponseBody ModelAndView getMostRecentComments(String time){
		Date[] timeWindow = getTimeWindow(time);
		Date from = timeWindow[0];
		Date to = timeWindow[1];
		List<FeedbackComment> comments = commentService.getComments(100, from, to);
		ModelAndView modelAndView = new ModelAndView("pulse");
		modelAndView.addObject("commentList", comments);
		return modelAndView;
	}
	
	@RequestMapping(value = "/whatPageView", method = RequestMethod.GET)
	public @ResponseBody ModelAndView getIssuePeopleDepartmentSentiment(String time){
		Date[] timeWindow = getTimeWindow(time);
		Date from = timeWindow[0];
		Date to = timeWindow[1];
		List<FeedbackCommentIssueSum> issueList = commentService.getCommentIssueCount(5, from, to);
		ModelAndView modelAndView = new ModelAndView("what");
		modelAndView.addObject("issueList", issueList);
		SentimentCount sentimentCount = commentService.getSentimentCount(from, to);
		modelAndView.addObject("sentimentCount", sentimentCount);
		List<PersonMention> personList = commentService.getMostMentionedPeople(6, from, to);
		modelAndView.addObject("personList", personList);
		return modelAndView;
	}
	
	@RequestMapping(value = "/whichPageView", method = RequestMethod.GET)
	public @ResponseBody ModelAndView getSurveyAnswerAnalytics(String time){
		Date[] timeWindow = getTimeWindow(time);
		Date from = timeWindow[0];
		Date to = timeWindow[1];
		List<SurveyAnswerCount> surveyAnswerCount = commentService.getAllSurveyAnswerCounts(from, to);
		ModelAndView modelAndView = new ModelAndView("which");
		modelAndView.addObject("surveyAnswerCount", surveyAnswerCount);
		return modelAndView;
	}
	
	@RequestMapping(value = "/wherePageView", method = RequestMethod.GET)
	public @ResponseBody ModelAndView getSurveyAnswerAnalyticsByLocation(String time){
		Date[] timeWindow = getTimeWindow(time);
		Date from = timeWindow[0];
		Date to = timeWindow[1];
		List<SurveyAnswerCount> surveyAnswerCountByLocation = commentService.getAllSurveyAnswerCountsByLocation(from, to);
		ModelAndView modelAndView = new ModelAndView("where");
		modelAndView.addObject("surveyAnswerCountByLocation", surveyAnswerCountByLocation);
		return modelAndView;
	}
	
	@RequestMapping(value = "/comments/{commentId}", method = RequestMethod.PUT)
	public ResponseEntity<String> updatedCommentResolveStatus(@PathVariable Long commentId, String status){
		if (logger.isDebugEnabled()){
			logger.debug("Resolve comment call: " + "commentId=" + commentId + ", status=" + status + ".");
		}
		if (commentId != null && status != null && status.toLowerCase().equals("resolved")){
			commentService.resolveOneComment(commentId);
			return new ResponseEntity<String>(HttpStatus.OK);
		}
		throw new IllegalArgumentException("Comment status is: " + status);
	}
	
	protected Date[] getTimeWindow(String string){
		Date[] result = new Date[2];
		if (string == null){
			string = "today";
		}
		switch (string) {
		case "today":
			result[0] = DatePeriod.TODAY.getStartDate(TimeZone.getDefault());
			break;
		case "yesterday":
			result[0] = DatePeriod.YESTERDAY.getStartDate(TimeZone.getDefault());
			break;
		case "week":
			result[0] = DatePeriod.SAMEDAYLASTWEEK.getStartDate(TimeZone.getDefault());
			break;
		case "month":
			result[0] = DatePeriod.SAMEDATELASTMONTH.getStartDate(TimeZone.getDefault());
			break;
		case "year":
			result[0] = DatePeriod.SAMEDATELASTYEAR.getStartDate(TimeZone.getDefault());
			break;
		default:
			result[0] = DatePeriod.TODAY.getStartDate(TimeZone.getDefault());
			break;
		}
		result[1] = DatePeriod.getNow();
		return result;
	}
}
