/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {

	stop_page_specific_timeouts();
	make_count_table();

});

function make_count_table() {

	$
			.ajax({

				type : 'GET',
				async : true,
				url : ctx + '/traffic',
				dataType : 'json',
				cache : false,
				success : function(data) {
					if (jQuery.isEmptyObject(data)) {

					} else {
						if (!jQuery.isEmptyObject(data.totalSwipes)) {
							$("#floorpage_filter_bar_total_swipes").text(
									"Swipes Today: " + data.totalSwipes);
						} else {
							$("#floorpage_filter_bar_total_swipes").text(
									"Swipes Today: 0");
						}
						$("#floorpage_filter_bar_timestamp").text(
								data.timeWindow);
						var i = 1;

						for ( var key in data) {
							if (key !== "totalSwipes" && key !== "timeWindow") {
								$("#floorpage_count_table_col_1_row_" + i)
										.html(
												'<div class=\"btn-floor-div\"><div class=\"btn-floor\"><img src=\"'
														+ ctx
														+ data[key][0]
														+ '\" width=\"50\" height=\"50\" /></div></div>');
								$("#floorpage_count_table_col_2_row_" + i)
										.html(data[key][1]);
								$("#floorpage_count_table_col_4_row_" + i)
										.html(data[key][2]);
								$("#floorpage_count_table_col_3_row_" + i)
										.html(data[key][3]);
								if (data[key][4] !== '0'
										&& data[key][4] !== null
										&& data[key][4] !== ''
										&& !jQuery.isEmptyObject(data[key][4])) {
									$("#floorpage_count_table_col_5_row_" + i)
											.html(data[key][4]);
								} else {
									$("#floorpage_count_table_col_5_row_" + i)
											.html("0");
								}
								i++;
							}
						}

					}

					$('#floorpage_count_table').dataTable({
						"aaSorting" : [ [ 1, "asc" ] ],
						"aoColumns" : [ {
							"sClass" : "center",
							"bSortable" : false,"width":"20%",
						}, {
							"sClass" : "left",
							"bSortable" : true,
							"sSortDataType" : "dom-text",
							"width":"15%",
						}, {
							"sClass" : "center",
							"bSortable" : true,
							"sSortDataType" : "dom-text",
							"sType" : "numeric",
							"width":"20%",
						}, {
							"sClass" : "center",
							"bSortable" : true,
							"sSortDataType" : "dom-text",
							"sType" : "numeric",
							"width":"20%",
						}, {
							"sClass" : "center",
							"bSortable" : true,
							"sSortDataType" : "dom-text",
							"sType" : "numeric","width":"25%",
						} ],
						"bFilter" : false,
						"bInfo" : false,
						"bPaginate" : false,
						"sScrollY" : "470px",
						"bSort" : true,
						"bRetrieve" : true,
						"bDestroy" : true,
					});
				}
			});

	floorpage_count_interval = setTimeout(function() {
		make_count_table();
	}, 30000);

}
