/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



function login() {

    $.ajax({
        url: 'jsp/auth/CheckUsername.jsp',
        data: {action: 'login', username: $('#username').val(), password: $('#password').val()}, // Convert a form to a JSON string representation
        type: 'POST',
        async: true,
        dataType: 'json',
        beforeSend: function () {
            // This callback function will trigger before data is sent
            $.mobile.showPageLoadingMsg(true); // This will show ajax spinner
        },
        complete: function () {
            // This callback function will trigger on data sent/received complete
            $.mobile.hidePageLoadingMsg(); // This will hide ajax spinner
        },
        success: function (data) {

            if (jQuery.isEmptyObject(data)) {
                $("#errorpopup").popup("open");
            } else {

                sessionStorage.setItem('name', data.name);
                sessionStorage.setItem('club_code', data.club);
                sessionStorage.setItem('location_id', data.id);
                sessionStorage.setItem('clubname', data.clubname);
                localStorage.setItem('club_code', data.club);
                localStorage.setItem('location_id', data.id);
                localStorage.setItem('name', data.name);
                localStorage.setItem('clubname', data.clubname);

                $.mobile.changePage("#homepage");

            }

        },
        error: function (request, error) {
            // This callback function will trigger on unsuccessful action                
            alert('Network error has occurred please try again!');
        }
    });
}


function check_status() {
    var c1 = localStorage.getItem('club_code');
    var c2 = sessionStorage.getItem('club_code');
    //console.log('c1 '+c1);
    //console.log('c2 '+c2);
    if (c1 === null || c2 === null) {
        $.mobile.changePage("#login_page");
    }
}


function getDailyReports() {
    var location_id = localStorage.getItem("location_id");

    $.ajax({
        data: {
            location_id: location_id
        },
        type: 'POST',
        async: true,
        url: 'jsp/weekly_report/GetLtfDailyReport.jsp',
        dataType: 'json',
        beforeSend: function () {

        },
        complete: function () {

        },
        success: function (data) {

            if (jQuery.isEmptyObject(data)) {
            } else {

                var d = new Date();
                var month = d.getMonth() + 1;
                console.log(month);
                var year = d.getFullYear();
                var daysInThisMonth = Math.round(((new Date(year, month)) - (new Date(year, month - 1))) / 86400000);
                var daysInLastMonth = Math.round(((new Date(year, month - 1)) - (new Date(year, month - 2))) / 86400000);
//                console.log(daysInLastMonth);
//                console.log(daysInThisMonth);

                if (!jQuery.isEmptyObject(data.TimeIntervals)) {
                    $("#retention_overview_termination_table th:nth-child(2)").html(data.TimeIntervals.LastDay);
                    $("#retention_overview_termination_table th:nth-child(3)").html("Week of " + data.TimeIntervals.LastWeek);
                    $("#retention_overview_termination_table th:nth-child(4)").html(data.TimeIntervals.ThisMonth);
                    $("#retention_overview_termination_table th:nth-child(5)").html(data.TimeIntervals.LastMonth);

                }



                if (!jQuery.isEmptyObject(data.LastDayInterception)) {
                    $("#retention_overview_action_table tr:nth-child(1) td:nth-child(3)").html(data.LastDayInterception.talkedTo);
                    $("#retention_overview_action_table tr:nth-child(2) td:nth-child(3)").html(data.LastDayInterception.goodToGo);
                    $("#retention_overview_action_table tr:nth-child(3) td:nth-child(3)").html(data.LastDayInterception.followUp);
                    $("#retention_overview_action_table tr:nth-child(4) td:nth-child(3)").html(data.LastDayInterception.needAttention);
                    $("#retention_overview_action_table tr:nth-child(5) td:nth-child(3)").html(data.LastDayInterception.highRisk);
                    $("#retention_overview_action_table tr:nth-child(6) td:nth-child(3)").html(data.LastDayInterception.medRisk);
                    $("#retention_overview_action_table tr:nth-child(7) td:nth-child(3)").html(data.LastDayInterception.lowRisk);
                    $("#retention_overview_action_table tr:nth-child(8) td:nth-child(3)").html(data.LastDayInterception.new);
                    $("#retention_overview_action_table tr:nth-child(9) td:nth-child(3)").html(data.LastDayInterception.notes);

                }
                if (!jQuery.isEmptyObject(data.LastWeekInterception)) {
                    $("#retention_overview_action_table tr:nth-child(1) td:nth-child(4)").html(data.LastWeekInterception.talkedTo);
                    $("#retention_overview_action_table tr:nth-child(2) td:nth-child(4)").html(data.LastWeekInterception.goodToGo);
                    $("#retention_overview_action_table tr:nth-child(3) td:nth-child(4)").html(data.LastWeekInterception.followUp);
                    $("#retention_overview_action_table tr:nth-child(4) td:nth-child(4)").html(data.LastWeekInterception.needAttention);
                    $("#retention_overview_action_table tr:nth-child(5) td:nth-child(4)").html(data.LastWeekInterception.highRisk);
                    $("#retention_overview_action_table tr:nth-child(6) td:nth-child(4)").html(data.LastWeekInterception.medRisk);
                    $("#retention_overview_action_table tr:nth-child(7) td:nth-child(4)").html(data.LastWeekInterception.lowRisk);
                    $("#retention_overview_action_table tr:nth-child(8) td:nth-child(4)").html(data.LastWeekInterception.new);
                    $("#retention_overview_action_table tr:nth-child(9) td:nth-child(4)").html(data.LastWeekInterception.notes);
                }
                if (!jQuery.isEmptyObject(data.ThisMonthInterception)) {
                    $("#retention_overview_action_table tr:nth-child(1) td:nth-child(5)").html(data.ThisMonthInterception.talkedTo);
                    $("#retention_overview_action_table tr:nth-child(2) td:nth-child(5)").html(data.ThisMonthInterception.goodToGo);
                    $("#retention_overview_action_table tr:nth-child(3) td:nth-child(5)").html(data.ThisMonthInterception.followUp);
                    $("#retention_overview_action_table tr:nth-child(4) td:nth-child(5)").html(data.ThisMonthInterception.needAttention);
                    $("#retention_overview_action_table tr:nth-child(5) td:nth-child(5)").html(data.ThisMonthInterception.highRisk);
                    $("#retention_overview_action_table tr:nth-child(6) td:nth-child(5)").html(data.ThisMonthInterception.medRisk);
                    $("#retention_overview_action_table tr:nth-child(7) td:nth-child(5)").html(data.ThisMonthInterception.lowRisk);
                    $("#retention_overview_action_table tr:nth-child(8) td:nth-child(5)").html(data.ThisMonthInterception.new);
                    $("#retention_overview_action_table tr:nth-child(9) td:nth-child(5)").html(data.ThisMonthInterception.notes);
                }
                if (!jQuery.isEmptyObject(data.LastMonthInterception)) {
                    $("#retention_overview_action_table tr:nth-child(1) td:nth-child(6)").html(data.LastMonthInterception.talkedTo);
                    $("#retention_overview_action_table tr:nth-child(2) td:nth-child(6)").html(data.LastMonthInterception.goodToGo);
                    $("#retention_overview_action_table tr:nth-child(3) td:nth-child(6)").html(data.LastMonthInterception.followUp);
                    $("#retention_overview_action_table tr:nth-child(4) td:nth-child(6)").html(data.LastMonthInterception.needAttention);
                    $("#retention_overview_action_table tr:nth-child(5) td:nth-child(6)").html(data.LastMonthInterception.highRisk);
                    $("#retention_overview_action_table tr:nth-child(6) td:nth-child(6)").html(data.LastMonthInterception.medRisk);
                    $("#retention_overview_action_table tr:nth-child(7) td:nth-child(6)").html(data.LastMonthInterception.lowRisk);
                    $("#retention_overview_action_table tr:nth-child(8) td:nth-child(6)").html(data.LastMonthInterception.new);
                    $("#retention_overview_action_table tr:nth-child(9) td:nth-child(6)").html(data.LastMonthInterception.notes);
                }

                if (!jQuery.isEmptyObject(data.LastWeekTermination)) {
                    $("#retention_overview_termination_table tr:nth-child(1) td:nth-child(3)").html(data.LastWeekTermination.total);

                }
                if (!jQuery.isEmptyObject(data.LastMonthTermination)) {
                    $("#retention_overview_termination_table tr:nth-child(1) td:nth-child(5)").html(data.LastMonthTermination.total);

                }
                if (!jQuery.isEmptyObject(data.LastDayTermination)) {
                    $("#retention_overview_termination_table tr:nth-child(1) td:nth-child(2)").html(data.LastDayTermination.total);

                }
                if (!jQuery.isEmptyObject(data.ThisMonthTermination)) {
                    $("#retention_overview_termination_table tr:nth-child(1) td:nth-child(4)").html(data.ThisMonthTermination.total);

                }





                if (!jQuery.isEmptyObject(data.LastMonthTraffic)) {
                    $("#retention_overview_traffic_table tr:nth-child(1) td:nth-child(6)").html(data.LastMonthTraffic.members);
                    $("#retention_overview_traffic_table tr:nth-child(2) td:nth-child(6)").html(data.LastMonthTraffic.highRisk);
                    $("#retention_overview_traffic_table tr:nth-child(3) td:nth-child(6)").html(data.LastMonthTraffic.medRisk);
                    $("#retention_overview_traffic_table tr:nth-child(4) td:nth-child(6)").html(data.LastMonthTraffic.lowRisk);
                    $("#retention_overview_traffic_table tr:nth-child(5) td:nth-child(6)").html(data.LastMonthTraffic.newMember);
                    $("#retention_overview_traffic_table tr:nth-child(6) td:nth-child(6)").html(data.LastMonthTraffic.birthdayWeek);


                }
                if (!jQuery.isEmptyObject(data.ThisMonthTraffic)) {
                    $("#retention_overview_traffic_table tr:nth-child(1) td:nth-child(5)").html(data.ThisMonthTraffic.members);
                    $("#retention_overview_traffic_table tr:nth-child(2) td:nth-child(5)").html(data.ThisMonthTraffic.highRisk);
                    $("#retention_overview_traffic_table tr:nth-child(3) td:nth-child(5)").html(data.ThisMonthTraffic.medRisk);
                    $("#retention_overview_traffic_table tr:nth-child(4) td:nth-child(5)").html(data.ThisMonthTraffic.lowRisk);
                    $("#retention_overview_traffic_table tr:nth-child(5) td:nth-child(5)").html(data.ThisMonthTraffic.newMember);
                    $("#retention_overview_traffic_table tr:nth-child(6) td:nth-child(5)").html(data.ThisMonthTraffic.birthdayWeek);



                }
                if (!jQuery.isEmptyObject(data.LastWeekTraffic)) {
                    $("#retention_overview_traffic_table tr:nth-child(1) td:nth-child(4)").html(data.LastWeekTraffic.members);
                    $("#retention_overview_traffic_table tr:nth-child(2) td:nth-child(4)").html(data.LastWeekTraffic.highRisk);
                    $("#retention_overview_traffic_table tr:nth-child(3) td:nth-child(4)").html(data.LastWeekTraffic.medRisk);
                    $("#retention_overview_traffic_table tr:nth-child(4) td:nth-child(4)").html(data.LastWeekTraffic.lowRisk);
                    $("#retention_overview_traffic_table tr:nth-child(5) td:nth-child(4)").html(data.LastWeekTraffic.newMember);
                    $("#retention_overview_traffic_table tr:nth-child(6) td:nth-child(4)").html(data.LastWeekTraffic.birthdayWeek);

                }
                if (!jQuery.isEmptyObject(data.LastDayTraffic)) {
                    $("#retention_overview_traffic_table tr:nth-child(1) td:nth-child(3)").html(data.LastDayTraffic.members);
                    $("#retention_overview_traffic_table tr:nth-child(2) td:nth-child(3)").html(data.LastDayTraffic.highRisk);
                    $("#retention_overview_traffic_table tr:nth-child(3) td:nth-child(3)").html(data.LastDayTraffic.medRisk);
                    $("#retention_overview_traffic_table tr:nth-child(4) td:nth-child(3)").html(data.LastDayTraffic.lowRisk);
                    $("#retention_overview_traffic_table tr:nth-child(5) td:nth-child(3)").html(data.LastDayTraffic.newMember);
                    $("#retention_overview_traffic_table tr:nth-child(6) td:nth-child(3)").html(data.LastDayTraffic.birthdayWeek);
                }
                if (!jQuery.isEmptyObject(data.LastDayAssistance)) {

                    $("#survey_overview_total_assistance_table tr:nth-child(1) td:nth-child(2)").html(data.LastDayAssistance.assistanceRequests);
                    $("#survey_overview_total_assistance_table tr:nth-child(2) td:nth-child(2)").html(data.LastDayAssistance.requestsCanceled + " (" + data.LastDayAssistance.percentage + "%)");
                    $("#survey_overview_total_assistance_table tr:nth-child(3) td:nth-child(2)").html("_");
                }

                if (!jQuery.isEmptyObject(data.LastWeekAssistance)) {

                    $("#survey_overview_total_assistance_table tr:nth-child(1) td:nth-child(3)").html(data.LastWeekAssistance.assistanceRequests);
                    $("#survey_overview_total_assistance_table tr:nth-child(2) td:nth-child(3)").html(data.LastWeekAssistance.requestsCanceled + " (" + data.LastWeekAssistance.percentage + "%)");
                    $("#survey_overview_total_assistance_table tr:nth-child(3) td:nth-child(3)").html((data.LastWeekAssistance.assistanceRequests / 7).toFixed(2));
                }
                if (!jQuery.isEmptyObject(data.ThisMonthAssistance)) {

                    $("#survey_overview_total_assistance_table tr:nth-child(1) td:nth-child(4)").html(data.ThisMonthAssistance.assistanceRequests);
                    $("#survey_overview_total_assistance_table tr:nth-child(2) td:nth-child(4)").html(data.ThisMonthAssistance.requestsCanceled + " (" + data.ThisMonthAssistance.percentage + "%)");
                    $("#survey_overview_total_assistance_table tr:nth-child(3) td:nth-child(4)").html((data.ThisMonthAssistance.assistanceRequests / daysInThisMonth).toFixed(2));
                }
                if (!jQuery.isEmptyObject(data.LastMonthAssistance)) {

                    $("#survey_overview_total_assistance_table tr:nth-child(1) td:nth-child(5)").html(data.LastMonthAssistance.assistanceRequests);
                    $("#survey_overview_total_assistance_table tr:nth-child(2) td:nth-child(5)").html(data.LastMonthAssistance.requestsCanceled + " (" + data.LastMonthAssistance.percentage + "%)");
                    $("#survey_overview_total_assistance_table tr:nth-child(3) td:nth-child(5)").html((data.LastMonthAssistance.assistanceRequests / daysInLastMonth).toFixed(2));
                }




                if (!jQuery.isEmptyObject(data.LastDaySurvey)) {

                    $("#survey_overview_total_survey_table tr:nth-child(1) td:nth-child(2)").html(data.LastDaySurvey.surveysTaken);
                    $("#survey_overview_total_survey_table tr:nth-child(2) td:nth-child(2)").html(data.LastDaySurvey.withName + " (" + data.LastDaySurvey.percentage + "%)");
                    $("#survey_overview_total_survey_table tr:nth-child(3) td:nth-child(2)").html("_");
                }
                if (!jQuery.isEmptyObject(data.LastWeekSurvey)) {

                    $("#survey_overview_total_survey_table tr:nth-child(1) td:nth-child(3)").html(data.LastWeekSurvey.surveysTaken);
                    $("#survey_overview_total_survey_table tr:nth-child(2) td:nth-child(3)").html(data.LastWeekSurvey.withName + " (" + data.LastWeekSurvey.percentage + "%)");
                    $("#survey_overview_total_survey_table tr:nth-child(3) td:nth-child(3)").html((data.LastWeekSurvey.surveysTaken / 7).toFixed(2));
                }
                if (!jQuery.isEmptyObject(data.ThisMonthSurvey)) {

                    $("#survey_overview_total_survey_table tr:nth-child(1) td:nth-child(4)").html(data.ThisMonthSurvey.surveysTaken);
                    $("#survey_overview_total_survey_table tr:nth-child(2) td:nth-child(4)").html(data.ThisMonthSurvey.withName + " (" + data.ThisMonthSurvey.percentage + "%)");
                    $("#survey_overview_total_survey_table tr:nth-child(3) td:nth-child(4)").html((data.ThisMonthSurvey.surveysTaken / daysInThisMonth).toFixed(2));
                }
                if (!jQuery.isEmptyObject(data.LastMonthSurvey)) {

                    $("#survey_overview_total_survey_table tr:nth-child(1) td:nth-child(5)").html(data.LastMonthSurvey.surveysTaken);
                    $("#survey_overview_total_survey_table tr:nth-child(2) td:nth-child(5)").html(data.LastMonthSurvey.withName + " (" + data.LastMonthSurvey.percentage + "%)");
                    $("#survey_overview_total_survey_table tr:nth-child(3) td:nth-child(5)").html((data.LastMonthSurvey.surveysTaken / daysInLastMonth).toFixed(2));
                }




                if (!jQuery.isEmptyObject(data.LastDayComments)) {

                    $("#survey_overview_total_comments_table tr:nth-child(1) td:nth-child(2)").html(data.LastDayComments.totalComments);
                    $("#survey_overview_total_comments_table tr:nth-child(2) td:nth-child(2)").html(data.LastDayComments.withName + " (" + data.LastDayComments.percentage + "%)");
                    $("#survey_overview_total_comments_table tr:nth-child(3) td:nth-child(2)").html("_");
                }
                if (!jQuery.isEmptyObject(data.LastWeekComments)) {

                    $("#survey_overview_total_comments_table tr:nth-child(1) td:nth-child(3)").html(data.LastWeekComments.totalComments);
                    $("#survey_overview_total_comments_table tr:nth-child(2) td:nth-child(3)").html(data.LastWeekComments.withName + " (" + data.LastWeekComments.percentage + "%)");
                    $("#survey_overview_total_comments_table tr:nth-child(3) td:nth-child(3)").html((data.LastWeekComments.totalComments / 7).toFixed(2));
                }
                if (!jQuery.isEmptyObject(data.ThisMonthComments)) {

                    $("#survey_overview_total_comments_table tr:nth-child(1) td:nth-child(4)").html(data.ThisMonthComments.totalComments);
                    $("#survey_overview_total_comments_table tr:nth-child(2) td:nth-child(4)").html(data.ThisMonthComments.withName + " (" + data.ThisMonthComments.percentage + "%)");
                    $("#survey_overview_total_comments_table tr:nth-child(3) td:nth-child(4)").html((data.ThisMonthComments.totalComments / daysInThisMonth).toFixed(2));
                }
                if (!jQuery.isEmptyObject(data.LastMonthComments)) {

                    $("#survey_overview_total_comments_table tr:nth-child(1) td:nth-child(5)").html(data.LastMonthComments.totalComments);
                    $("#survey_overview_total_comments_table tr:nth-child(2) td:nth-child(5)").html(data.LastMonthComments.withName + " (" + data.LastMonthComments.percentage + "%)");
                    $("#survey_overview_total_comments_table tr:nth-child(3) td:nth-child(5)").html((data.LastMonthComments.totalComments / daysInLastMonth).toFixed(2));
                }




                if (!jQuery.isEmptyObject(data.LastDaySatisfaction)) {
                    if (!jQuery.isEmptyObject(data.LastDaySatisfaction.surveyType3)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(1) td:nth-child(3)").html(data.LastDaySatisfaction.surveyType3 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(1) td:nth-child(3)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastDaySatisfaction.surveyType1)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(2) td:nth-child(3)").html(data.LastDaySatisfaction.surveyType1 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(2) td:nth-child(3)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastDaySatisfaction.surveyType2)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(3) td:nth-child(3)").html(data.LastDaySatisfaction.surveyType2 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(3) td:nth-child(3)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastDaySatisfaction.surveyType5)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(4) td:nth-child(3)").html(data.LastDaySatisfaction.surveyType5 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(4) td:nth-child(3)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastDaySatisfaction.surveyType4)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(5) td:nth-child(3)").html(data.LastDaySatisfaction.surveyType4 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(5) td:nth-child(3)").html("-");

                    }
                }

                if (!jQuery.isEmptyObject(data.LastWeekSatisfaction)) {

                    if (!jQuery.isEmptyObject(data.LastWeekSatisfaction.surveyType3)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(1) td:nth-child(4)").html(data.LastWeekSatisfaction.surveyType3 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(1) td:nth-child(4)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastWeekSatisfaction.surveyType1)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(2) td:nth-child(4)").html(data.LastWeekSatisfaction.surveyType1 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(2) td:nth-child(4)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastWeekSatisfaction.surveyType2)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(3) td:nth-child(4)").html(data.LastWeekSatisfaction.surveyType2 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(3) td:nth-child(4)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastWeekSatisfaction.surveyType5)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(4) td:nth-child(4)").html(data.LastWeekSatisfaction.surveyType5 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(4) td:nth-child(4)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastWeekSatisfaction.surveyType4)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(5) td:nth-child(4)").html(data.LastWeekSatisfaction.surveyType4 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(5) td:nth-child(4)").html("-");

                    }

                }
                if (!jQuery.isEmptyObject(data.ThisMonthSatisfaction)) {
                    if (!jQuery.isEmptyObject(data.ThisMonthSatisfaction.surveyType3)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(1) td:nth-child(5)").html(data.ThisMonthSatisfaction.surveyType3 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(1) td:nth-child(5)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.ThisMonthSatisfaction.surveyType1)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(2) td:nth-child(5)").html(data.ThisMonthSatisfaction.surveyType1 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(2) td:nth-child(5)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.ThisMonthSatisfaction.surveyType2)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(3) td:nth-child(5)").html(data.ThisMonthSatisfaction.surveyType2 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(3) td:nth-child(5)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.ThisMonthSatisfaction.surveyType5)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(4) td:nth-child(5)").html(data.ThisMonthSatisfaction.surveyType5 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(4) td:nth-child(5)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.ThisMonthSatisfaction.surveyType4)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(5) td:nth-child(5)").html(data.ThisMonthSatisfaction.surveyType4 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(5) td:nth-child(5)").html("-");

                    }

                }
                if (!jQuery.isEmptyObject(data.LastMonthSatisfaction)) {
                    if (!jQuery.isEmptyObject(data.LastMonthSatisfaction.surveyType3)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(1) td:nth-child(6)").html(data.LastMonthSatisfaction.surveyType3 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(1) td:nth-child(6)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastMonthSatisfaction.surveyType1)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(2) td:nth-child(6)").html(data.LastMonthSatisfaction.surveyType1 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(2) td:nth-child(6)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastMonthSatisfaction.surveyType2)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(3) td:nth-child(6)").html(data.LastMonthSatisfaction.surveyType2 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(3) td:nth-child(6)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastMonthSatisfaction.surveyType5)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(4) td:nth-child(6)").html(data.LastMonthSatisfaction.surveyType5 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(4) td:nth-child(6)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastMonthSatisfaction.surveyType4)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(5) td:nth-child(6)").html(data.LastMonthSatisfaction.surveyType4 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(5) td:nth-child(6)").html("-");

                    }
                }




                if (!jQuery.isEmptyObject(data.LastDayAssistancePerLocation)) {
                    $("#survey_overview_assistance_table tr:nth-child(1) td:nth-child(3)").html(data.LastDayAssistancePerLocation.location3);
                    $("#survey_overview_assistance_table tr:nth-child(2) td:nth-child(3)").html(data.LastDayAssistancePerLocation.location1);
                    $("#survey_overview_assistance_table tr:nth-child(3) td:nth-child(3)").html(data.LastDayAssistancePerLocation.location2);
                    $("#survey_overview_assistance_table tr:nth-child(4) td:nth-child(3)").html(data.LastDayAssistancePerLocation.location5);
                    $("#survey_overview_assistance_table tr:nth-child(5) td:nth-child(3)").html(data.LastDayAssistancePerLocation.location6);
                    $("#survey_overview_assistance_table tr:nth-child(6) td:nth-child(3)").html(data.LastDayAssistancePerLocation.location7);
                }

                if (!jQuery.isEmptyObject(data.LastWeekAssistancePerLocation)) {
                    $("#survey_overview_assistance_table tr:nth-child(1) td:nth-child(4)").html(data.LastWeekAssistancePerLocation.location3);
                    $("#survey_overview_assistance_table tr:nth-child(2) td:nth-child(4)").html(data.LastWeekAssistancePerLocation.location1);
                    $("#survey_overview_assistance_table tr:nth-child(3) td:nth-child(4)").html(data.LastWeekAssistancePerLocation.location2);
                    $("#survey_overview_assistance_table tr:nth-child(4) td:nth-child(4)").html(data.LastWeekAssistancePerLocation.location5);
                    $("#survey_overview_assistance_table tr:nth-child(5) td:nth-child(4)").html(data.LastWeekAssistancePerLocation.location6);
                    $("#survey_overview_assistance_table tr:nth-child(6) td:nth-child(4)").html(data.LastWeekAssistancePerLocation.location7);
                }
                if (!jQuery.isEmptyObject(data.ThisMonthAssistancePerLocation)) {
                    $("#survey_overview_assistance_table tr:nth-child(1) td:nth-child(5)").html(data.ThisMonthAssistancePerLocation.location3);
                    $("#survey_overview_assistance_table tr:nth-child(2) td:nth-child(5)").html(data.ThisMonthAssistancePerLocation.location1);
                    $("#survey_overview_assistance_table tr:nth-child(3) td:nth-child(5)").html(data.ThisMonthAssistancePerLocation.location2);
                    $("#survey_overview_assistance_table tr:nth-child(4) td:nth-child(5)").html(data.ThisMonthAssistancePerLocation.location5);
                    $("#survey_overview_assistance_table tr:nth-child(5) td:nth-child(5)").html(data.ThisMonthAssistancePerLocation.location6);
                    $("#survey_overview_assistance_table tr:nth-child(6) td:nth-child(5)").html(data.ThisMonthAssistancePerLocation.location7);
                }
                if (!jQuery.isEmptyObject(data.LastMonthAssistancePerLocation)) {
                    $("#survey_overview_assistance_table tr:nth-child(1) td:nth-child(6)").html(data.LastMonthAssistancePerLocation.location3);
                    $("#survey_overview_assistance_table tr:nth-child(2) td:nth-child(6)").html(data.LastMonthAssistancePerLocation.location1);
                    $("#survey_overview_assistance_table tr:nth-child(3) td:nth-child(6)").html(data.LastMonthAssistancePerLocation.location2);
                    $("#survey_overview_assistance_table tr:nth-child(4) td:nth-child(6)").html(data.LastMonthAssistancePerLocation.location5);
                    $("#survey_overview_assistance_table tr:nth-child(5) td:nth-child(6)").html(data.LastMonthAssistancePerLocation.location6);
                    $("#survey_overview_assistance_table tr:nth-child(6) td:nth-child(6)").html(data.LastMonthAssistancePerLocation.location7);
                }

                if (!jQuery.isEmptyObject(data.Last30DaysComments)) {

                    if (!jQuery.isEmptyObject(data.Last30DaysComments.location3)) {

                        if (!jQuery.isEmptyObject(data.Last30DaysComments.location3[0])) {

                            for (var key in data.Last30DaysComments.location3) {

                                $("#survey_overview_front_desk_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location3[key].survey) + "</td><td>" + data.Last30DaysComments.location3[key].msg + "</td><td>" + data.Last30DaysComments.location3[key].author + "</td><td>" + data.Last30DaysComments.location3[key].time + "</td></tr>");
                            }
                        } else {
                            $("#survey_overview_front_desk_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location3.survey) + "</td><td>" + data.Last30DaysComments.location3.msg + "</td><td>" + data.Last30DaysComments.location3.author + "</td><td>" + data.Last30DaysComments.location3.time + "</td></tr>");

                        }
                    }

                    if (!jQuery.isEmptyObject(data.Last30DaysComments.location6)) {
                        if (!jQuery.isEmptyObject(data.Last30DaysComments.location6[0])) {
                            for (var key in data.Last30DaysComments.location6) {

                                $("#survey_overview_womens_locker_room_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location6[key].survey) + "</td><td>" + data.Last30DaysComments.location6[key].msg + "</td><td>" + data.Last30DaysComments.location6[key].author + "</td><td>" + data.Last30DaysComments.location6[key].time + "</td></tr>");
                            }
                        } else {
                            $("#survey_overview_womens_locker_room_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location6.survey) + "</td><td>" + data.Last30DaysComments.location6.msg + "</td><td>" + data.Last30DaysComments.location6.author + "</td><td>" + data.Last30DaysComments.location6.time + "</td></tr>");

                        }
                    }

                    if (!jQuery.isEmptyObject(data.Last30DaysComments.location7)) {
                        if (!jQuery.isEmptyObject(data.Last30DaysComments.location7[0])) {

                            for (var key in data.Last30DaysComments.location7) {

                                $("#survey_overview_mens_locker_room_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location7[key].survey) + "</td><td>" + data.Last30DaysComments.location7[key].msg + "</td><td>" + data.Last30DaysComments.location7[key].author + "</td><td>" + data.Last30DaysComments.location7[key].time + "</td></tr>");
                            }

                        } else {
                            $("#survey_overview_mens_locker_room_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location7.survey) + "</td><td>" + data.Last30DaysComments.location7.msg + "</td><td>" + data.Last30DaysComments.location7.author + "</td><td>" + data.Last30DaysComments.location7.time + "</td></tr>");

                        }
                    }

                    if (!jQuery.isEmptyObject(data.Last30DaysComments.location5)) {
                        if (!jQuery.isEmptyObject(data.Last30DaysComments.location5[0])) {


                            for (var key in data.Last30DaysComments.location5) {
                                $("#survey_overview_family_locker_room_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location5[key].survey) + "</td><td>" + data.Last30DaysComments.location5[key].msg + "</td><td>" + data.Last30DaysComments.location5[key].author + "</td><td>" + data.Last30DaysComments.location5[key].time + "</td></tr>");
                            }
                        } else {
                            $("#survey_overview_family_locker_room_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location5.survey) + "</td><td>" + data.Last30DaysComments.location5.msg + "</td><td>" + data.Last30DaysComments.location5.author + "</td><td>" + data.Last30DaysComments.location5.time + "</td></tr>");

                        }
                    }

                    if (!jQuery.isEmptyObject(data.Last30DaysComments.location2)) {
                        if (!jQuery.isEmptyObject(data.Last30DaysComments.location2[0])) {

                            for (var key in data.Last30DaysComments.location2) {

                                $("#survey_overview_group_fitness_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location2[key].survey) + "</td><td>" + data.Last30DaysComments.location2[key].msg + "</td><td>" + data.Last30DaysComments.location2[key].author + "</td><td>" + data.Last30DaysComments.location2[key].time + "</td></tr>");
                            }
                        } else {
                            $("#survey_overview_group_fitness_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location2.survey) + "</td><td>" + data.Last30DaysComments.location2.msg + "</td><td>" + data.Last30DaysComments.location2.author + "</td><td>" + data.Last30DaysComments.location2.time + "</td></tr>");

                        }
                    }
                    if (!jQuery.isEmptyObject(data.Last30DaysComments.location1)) {
                        if (!jQuery.isEmptyObject(data.Last30DaysComments.location1[0])) {

                            for (var key in data.Last30DaysComments.location1) {

                                $("#survey_overview_fitness_floor_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location1[key].survey) + "</td><td>" + data.Last30DaysComments.location1[key].msg + "</td><td>" + data.Last30DaysComments.location1[key].author + "</td><td>" + data.Last30DaysComments.location1[key].time + "</td></tr>");
                            }
                        } else {
                            $("#survey_overview_fitness_floor_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location1.survey) + "</td><td>" + data.Last30DaysComments.location1.msg + "</td><td>" + data.Last30DaysComments.location1.author + "</td><td>" + data.Last30DaysComments.location1.time + "</td></tr>");

                        }
                    }

                }

            }
        }
    }
    );
}



function setValues() {

    $("#retention_overview_club_name").html(localStorage.getItem('clubname'));
}

function getSurveyType(group_type) {
    var survey;
    if (group_type === '5') {
        survey = "Family Locker Room";
    } else if (group_type === '3') {
        survey = "Main Entrance";
    } else if (group_type === '2') {
        survey = "Group Fitness";
    } else if (group_type === '4') {
        survey = "Pool";
    } else if (group_type === '1') {
        survey = "Fitness Floor";
    } else if (group_type === '6') {
        survey = "Women's Locker Room";
    } else if (group_type === '7') {
        survey = " Men's Locker Room";
    } else if (group_type === '8') {
        survey = "Locker Room";
    } else {
        survey = "Comment Page";
    }
    return survey;
}
