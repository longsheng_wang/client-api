/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members;


/**
 * Enumeration of metrics that apply to members
 * 
 * @author Shakti Shrivastava
 * 
 */
public enum MemberMetric {

	ATTRITION("Attrition", "Attrition"), VALUE("Value", "Value"), RECENCY("Recency", "Recency"), FREQUENCY("Frequency",
			"Frequency"), INTERESTS("Interests", "Interests"), PERSONAS("Personas", "Personas"), LAST_INTERCEPT(
			"Last Intercept", "Last Intercept"), LAST_VITSIT("Last Visit", "Last Visit");

	private final String name;
	private final String description;

	private MemberMetric(String name, String description) {
		this.name = name;
		this.description = description;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
}
