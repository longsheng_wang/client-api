-- Adding column for goals and number of users to location table
ALTER TABLE client_locations ADD COLUMN performance_goal INT DEFAULT 0;
ALTER TABLE client_locations ADD COLUMN active_users INT DEFAULT 0;