/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members;

import java.util.List;
import java.util.Map;

import net.reunify.pulse.analytics.metrics.DateMetric;
import net.reunify.pulse.analytics.metrics.MapMetric;
import net.reunify.pulse.analytics.metrics.ProbabilisticMetric;

import org.apache.log4j.Logger;

/**
 * Example member analytics class
 * 
 * @author Shakti Shrivastava
 * 
 */
public class MemberAnalytics {
	private static final Logger logger = Logger.getLogger(MemberAnalytics.class);

	private List<ProbabilisticMetric> scores;
	private MapMetric<Map<String, Double>> personas;
	private MapMetric<Map<String, Double>> interests;
	private List<DateMetric> dateMetrics;

	public MemberAnalytics(List<ProbabilisticMetric> scores, MapMetric<Map<String, Double>> personas,
							MapMetric<Map<String, Double>> interests, List<DateMetric> dateMetrics) {
		this.dateMetrics = dateMetrics;
		this.interests = interests;
		this.personas = personas;
		this.scores = scores;
	}

	public MemberAnalytics() {
		// TODO Auto-generated constructor stub
	}

	public List<ProbabilisticMetric> getScores() {
		return scores;
	}

	public void setScores(List<ProbabilisticMetric> scores) {
		this.scores = scores;
	}

	public MapMetric<Map<String, Double>> getPersonas() {
		return personas;
	}

	public void setPersonas(MapMetric<Map<String, Double>> personas) {
		this.personas = personas;
	}

	public MapMetric<Map<String, Double>> getInterests() {
		return interests;
	}

	public void setInterests(MapMetric<Map<String, Double>> interests) {
		this.interests = interests;
	}

	public List<DateMetric> getDateMetrics() {
		return dateMetrics;
	}

	public void setDateMetrics(List<DateMetric> dateMetrics) {
		this.dateMetrics = dateMetrics;
	}

}
