/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.internaltools;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Validator for Swipe Simulator form
 * 
 * @author Vivek Hungund
 *
 */
public class SwipeSimulatorFormValidator implements Validator {
	private static final Logger logger = Logger.getLogger(SwipeSimulatorFormValidator.class);

	/* (non-Javadoc)
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> clazz) {
		return SwipeSimulatorForm.class.equals(clazz);
	}

	/* (non-Javadoc)
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	@Override
	public void validate(Object target, Errors errors) {
		SwipeSimulatorForm form = (SwipeSimulatorForm) target;
		
		validateClient(form,errors,"clientId");
		validateLocation(form,errors,"locationId");
		validateNumSwipes(form,errors,"numSwipes");
		
	}

	/**
	 * Reject invalid client choice
	 * 
	 * @param form
	 * @param errors
	 * @param clientFieldPath
	 */
	private void validateClient(SwipeSimulatorForm form, Errors errors, String clientFieldPath) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, clientFieldPath, "client_required", "Client required");
		
		if ((Integer) form.getClientId() != null)
			if (form.getClientId() == 0) 
				errors.rejectValue(clientFieldPath, "invalid_client", "Please choose a valid client");
	}

	/**
	 * Reject invalid location choice
	 * 
	 * @param form
	 * @param errors
	 * @param locationFieldPath
	 */
	private void validateLocation(SwipeSimulatorForm form, Errors errors, String locationFieldPath) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, locationFieldPath, "location_required", "Location required");
		
		if ((Integer) form.getLocationId() != null)
			if (form.getLocationId() == 0) 
				errors.rejectValue(locationFieldPath, "invalid_location", "Please choose a valid location");
		
	}

	/**
	 * Reject invalid number of swipes
	 * 
	 * @param form
	 * @param errors
	 * @param numSwipesFieldPath
	 */
	private void validateNumSwipes(SwipeSimulatorForm form, Errors errors, String numSwipesFieldPath) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, numSwipesFieldPath, "numSwipes_required", "Number of Swipes required");
		
		if ((Integer) form.getNumSwipes() != null)
			if (form.getNumSwipes() <= 0 || form.getNumSwipes() > 5000) 
				errors.rejectValue(numSwipesFieldPath,"numSwipes_out_of_range", "Number of Swipes must be between 1-5000");
		
	}
	
	
}
