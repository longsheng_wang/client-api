/* 
 * Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */

var HOMEPAGE_TABLE_TIMEOUT = 15000;
var FLOORPAGE_TABLE_TIMEOUT = 15000;
var MEMBERPULSE_TABLE_TIMEOUT = 5000;

var member_table_aasorting = [ [ 7, "asc", 0 ] ];
var member_table_displaystart = 0;
var member_table_displaylength = 10;

var search_table_aasorting = [ [ 3, "asc", 0 ] ];
var search_table_displaystart = 0;
var search_table_displaylength = 10;
var ajax_loading_timer;

var clock_interval;
var member_pulse_interval;
var floorpage_count_interval;
var analytics_table_interval;
var homepage_pulse_interval;
var homepage_interception_info_interval;
var homepage_survey_info_interval;
var notification_pulse_interval;

var currentLocation = window.location;
var is_five_star;

var risky_notification_timeout;
var birthday_notification_timeout;
var new_notification_timeout;
var attention_notification_timeout;