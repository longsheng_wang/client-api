/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Container for Daily Report data
 * 
 * @author Vivek Hungund
 *
 */

public class InterceptProgressReportData {
	private static final Logger logger = Logger.getLogger(InterceptProgressReportData.class);
	
	private List<InterceptProgressReportDataRow> reportData;

	/**
	 * @param reportData
	 */
	public InterceptProgressReportData(List<InterceptProgressReportDataRow> reportData) {
		this.reportData = reportData;
	}
	
	public InterceptProgressReportData() {
		this.reportData = new ArrayList<InterceptProgressReportDataRow> ();
	}

	/**
	 * @return the reportData
	 */
	public List<InterceptProgressReportDataRow> getReportData() {
		return reportData;
	}

	/**
	 * @param reportData the reportData to set
	 */
	public void setReportData(List<InterceptProgressReportDataRow> reportData) {
		this.reportData = reportData;
	}
	
	/**
	 * @param row to add to the reportData
	 */
	public void addDataRow(InterceptProgressReportDataRow row) {
		this.reportData.add(row);
	}
}
