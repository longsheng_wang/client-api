/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

/**
 * 
 * @author kiana.baradaran
 * 
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/spring-security.xml",
		"file:src/test/webapp/WEB-INF/mvc-dispatcher-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class LocationRegistrationValidatorTest {

	@Test
	public void testLocationRegisterationValidatorNoNameProvided() {
		LocationDTO location = new LocationDTO();
		location.setName(" ");
		Errors errors = new BeanPropertyBindingResult(location, "location");
		LocationRegistrationValidator validator = new LocationRegistrationValidator();
		validator.validateName(location, errors, "name");
		assertTrue(errors.hasErrors());
		assertEquals("name_required", errors.getAllErrors().get(0).getCode());
	}

	@Test
	public void testLocationRegisterationValidatorNameTooLong() {
		LocationDTO location = new LocationDTO();
		location.setName("test test test test test test test test test test test test test test test test test test test test test");
		Errors errors = new BeanPropertyBindingResult(location, "location");
		LocationRegistrationValidator validator = new LocationRegistrationValidator();
		validator.validateName(location, errors, "name");
		assertTrue(errors.hasErrors());
		assertEquals("incorrect_name", errors.getAllErrors().get(0).getCode());
	}

	@Test
	public void testClientRegisterationValidatorForNoTimeZoneProvided() {
		LocationDTO location = new LocationDTO();
		location.setTimeZone("NONE");
		Errors errors = new BeanPropertyBindingResult(location, "location");
		LocationRegistrationValidator validator = new LocationRegistrationValidator();
		validator.validateTimeZone(location, errors, "timeZone");

		assertTrue(errors.hasErrors());
		assertEquals("timeZone_required", errors.getAllErrors().get(0).getCode());
	}

	@Test
	public void testClientRegisterationValidatorForClientLocationIDToolLong() {
		LocationDTO location = new LocationDTO();
		location.setClientLocationId("123547592735474587293652634912534925349125");
		Errors errors = new BeanPropertyBindingResult(location, "location");
		LocationRegistrationValidator validator = new LocationRegistrationValidator();
		validator.validateClientLocationId(location, errors, "clientLocationId");

		assertTrue(errors.hasErrors());
		assertEquals("incorrect_clientLocationId", errors.getAllErrors().get(0).getCode());
	}

}
