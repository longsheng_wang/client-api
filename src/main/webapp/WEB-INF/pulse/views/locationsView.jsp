<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<ui:baseview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Pulse - Users">
	<jsp:attribute name="headTag">
<script
			src="${pageContext.servletContext.contextPath}/static/pulse/js/location/location.js"
			type="text/javascript"></script>
	</jsp:attribute>
	<jsp:body>
		<div id="users_content" class="col-md-12 col-sm-12"
			style="height: 100%; padding: 0;">
			<div>
				<a class="location-add">Add Location</a>
			</div>		

		<table class="table-bordered" id="location-table">
		<thead>
		<tr>

		<th>Location Name</th>
		<th>Time Zone</th>
		<th>Active Users</th>
		<th>Performance Goal</th>
		<th>Action</th>
		</tr>
		</thead>
		
	<c:if test="${not empty locationsList}">

        <c:forEach var="location" items="${locationsList}">
            <tr>
             <td>${location.name}</td>
                <td>${location.timeZone}</td>
                <td>${location.activeUsers}</td>
                <td>${location.performanceGoal}</td>
                <td><span><a class="location-edit"
									data="${location.locationId}">edit</a>/<a
									class="location-delete" data="${location.locationId}">delete</a></span></td>
            </tr>
        </c:forEach>

</c:if>
		</table>
		</div>
	</jsp:body>
</ui:baseview>
