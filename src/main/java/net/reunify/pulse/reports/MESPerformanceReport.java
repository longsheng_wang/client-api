/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * MES Performance Report class.
 * 
 * @author Vivek Hungund
 *
 */
@Component
public class MESPerformanceReport implements Report {
	private static final Logger logger = Logger.getLogger(MESPerformanceReport.class);

	@Inject
	@Named("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	/**
	 * The actual report data
	 */
	private MESPerformanceReportData reportData;

	/**
	 * Initialize the report data
	 */
	public MESPerformanceReport() {
		reportData = new MESPerformanceReportData();
	}

	/**
	 * Save report data
	 * 
	 * @param reportData
	 */
	private void setReportData(MESPerformanceReportData reportData) {
		this.reportData = reportData;
	}

	/**
	 * Return the report data
	 * 
	 * @return reportData
	 */
	public MESPerformanceReportData getReportData() {
		return reportData;
	}

	/**
	 * 
	 * Create an MES Performance Report for a date range
	 * 
	 * @param startDate
	 * @param endDate
	 */
	public void createReport(String startDate, String endDate) {

		// 2015-01-15 - Don't need to calculate separate counts for intercepts
		// and notes, since both are now compulsory

		// startDate and endDate are String and not Date because they are
		// manipulated by the sql in order to get the correct time zone
		String query = "select employee_id::int,n.location_id::int, created_by,count(*)::int from dashboard_notepad n left join location_to_timezone l on n.location_id ="
				+ "l.location_id where created_at >= (?::timestamp) at time zone timezone and created_at < (?::timestamp) at time zone timezone "
				+ "and automated = 0 and is_deleted::int = 0 and is_edited::int = 0 group by created_by, n.location_id,employee_id order by location_id,created_by";
		List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(query, new Object[] { startDate, endDate });

		MESPerformanceReportData reportData = new MESPerformanceReportData();
		for (Map<String, Object> row : rows) {
			MESPerformanceReportDataRow resultRow = new MESPerformanceReportDataRow((Integer) row.get("employee_id"),
																					(Integer) row.get("location_id"),
																					row.get("created_by").toString(),
																					(Integer) row.get("count"));
			reportData.addRow(resultRow);
		}

		this.setReportData(reportData);

	}

}
