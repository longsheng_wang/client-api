/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.beat.model;

import java.sql.Timestamp;

/**
 * @author longsheng.wang
 *
 */
public class FeedbackComment{
	 private long recordId;
	 private boolean anonymousAuthor;
	 private String commentAuthor;
	 private String commentMessage;
	 private int groupType;
	 private Timestamp insertionTime;
	 private Timestamp messageTime;
	 private int surveyLocationId;
	 private int surveyType;
	 private int clientId;
	 private long deviceRecordId;
	 private int locationId;
	 private String locationName;
	 private Timestamp resolvedTime;
	 private boolean resolved;

	/**
	 * 
	 */
	public FeedbackComment() {
	}

	/**
	 * @param recordId
	 * @param anonymousAuthor
	 * @param commentAuthor
	 * @param commentMessage
	 * @param groupType
	 * @param insertionTime
	 * @param messageTime
	 * @param surveyLocationId
	 * @param surveyType
	 * @param clientId
	 * @param deviceRecordId
	 * @param locationId
	 * @param locationName
	 * @param resolvedTime
	 * @param resolved
	 */
	public FeedbackComment(long recordId, boolean anonymousAuthor, String commentAuthor, String commentMessage,
							int groupType, Timestamp insertionTime, Timestamp messageTime, int surveyLocationId,
							int surveyType, int clientId, long deviceRecordId, int locationId, String locationName,
							Timestamp resolvedTime, boolean resolved) {
		super();
		this.recordId = recordId;
		this.anonymousAuthor = anonymousAuthor;
		this.commentAuthor = commentAuthor;
		this.commentMessage = commentMessage;
		this.groupType = groupType;
		this.insertionTime = insertionTime;
		this.messageTime = messageTime;
		this.surveyLocationId = surveyLocationId;
		this.surveyType = surveyType;
		this.clientId = clientId;
		this.deviceRecordId = deviceRecordId;
		this.locationId = locationId;
		this.locationName = locationName;
		this.resolvedTime = resolvedTime;
		this.resolved = resolved;
	}

	/**
	 * @return the recordId
	 */
	public long getRecordId() {
		return recordId;
	}

	/**
	 * @param recordId the recordId to set
	 */
	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	/**
	 * @return the anonymousAuthor
	 */
	public boolean isAnonymousAuthor() {
		return anonymousAuthor;
	}

	/**
	 * @param anonymous_author the anonymous_author to set
	 */
	public void setAnonymousAuthor(boolean anonymousAuthor) {
		this.anonymousAuthor = anonymousAuthor;
	}

	/**
	 * @return the comment_author
	 */
	public String getCommentAuthor() {
		return commentAuthor;
	}

	/**
	 * @param commentAuthor the commentAuthor to set
	 */
	public void setCommentAuthor(String commentAuthor) {
		this.commentAuthor = commentAuthor;
	}

	/**
	 * @return the commentMessage
	 */
	public String getCommentMessage() {
		return commentMessage;
	}

	/**
	 * @param commentMessage the commentMessage to set
	 */
	public void setCommentMessage(String commentMessage) {
		this.commentMessage = commentMessage;
	}

	/**
	 * @return the groupType
	 */
	public int getGroupType() {
		return groupType;
	}

	/**
	 * @param groupType the groupType to set
	 */
	public void setGroupType(int groupType) {
		this.groupType = groupType;
	}

	/**
	 * @return the insertionTime
	 */
	public Timestamp getInsertionTime() {
		return insertionTime;
	}

	/**
	 * @param insertionTime the insertion_time to set
	 */
	public void setInsertionTime(Timestamp insertionTime) {
		this.insertionTime = insertionTime;
	}

	/**
	 * @return the messageTime
	 */
	public Timestamp getMessageTime() {
		return messageTime;
	}

	/**
	 * @param messageTime the messageTime to set
	 */
	public void setMessageTime(Timestamp messageTime) {
		this.messageTime = messageTime;
	}

	/**
	 * @return the surveyLocationId
	 */
	public int getSurveyLocationId() {
		return surveyLocationId;
	}

	/**
	 * @param surveyLocationId the surveyLocationId to set
	 */
	public void setSurveyLocationId(int surveyLocationId) {
		this.surveyLocationId = surveyLocationId;
	}

	/**
	 * @return the surveyType
	 */
	public int getSurveyType() {
		return surveyType;
	}

	/**
	 * 
	 * @param surveyType
	 */
	public void setSurveyType(int surveyType) {
		this.surveyType = surveyType;
	}

	/**
	 * 
	 * @return
	 */
	public int getClientId() {
		return clientId;
	}

	/**
	 * 
	 * @param clientId
	 */
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	/**
	 * 
	 * @return
	 */
	public long getDeviceRecordId() {
		return deviceRecordId;
	}

	/**
	 * @param deviceRecordId the device_record_id to set
	 */
	public void setDeviceRecordId(long deviceRecordId) {
		this.deviceRecordId = deviceRecordId;
	}

	/**
	 * 
	 * @return
	 */
	public int getLocationId() {
		return locationId;
	}

	/**
	 * @param locationId the location_id to set
	 */
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	/**
	 * @return the locationName
	 */
	public String getLocationName() {
		return locationName;
	}

	/**
	 * @param locationName the locationName to set
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	/**
	 * @return the resolvedTime
	 */
	public Timestamp getResolvedTime() {
		return resolvedTime;
	}

	/**
	 * @param resolvedTime the resolvedTime to set
	 */
	public void setResolvedTime(Timestamp resolvedTime) {
		this.resolvedTime = resolvedTime;
	}

	/**
	 * @return the resolved
	 */
	public boolean isResolved() {
		return resolved;
	}

	/**
	 * @param resolved the resolved to set
	 */
	public void setResolved(boolean resolved) {
		this.resolved = resolved;
	}

}
