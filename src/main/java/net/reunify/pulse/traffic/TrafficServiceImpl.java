/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.traffic;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of TrafficService
 * 
 * @author kiana.baradaran
 * 
 */

@Service("TrafficService")
@Transactional(value = "pulseTxManager")
public class TrafficServiceImpl implements TrafficService {

	@Inject
	private TrafficDAO trafficDAO;

	@Override
	public List<TrafficZone> getLocationTraffic(String locatioId) {
		// TODO Auto-generated method stub
		return trafficDAO.getLocationTraffic(locatioId);
	}

}
