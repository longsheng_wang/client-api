/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;

/**
 * Interface for any report that may be requested
 * 
 * @author Vivek Hungund
 *
 */
public interface Report {

}
