/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.analytics.metrics;

import java.util.Date;

import net.reunify.pulse.members.MemberMetric;

import org.apache.log4j.Logger;

/**
 * A date valued metric
 * 
 * @author Shakti Shrivastava
 * 
 */
public class DateMetric implements Metric<Date> {

	private static final Logger logger = Logger.getLogger(DateMetric.class);

	private MemberMetric metricName;
	private Date value;

	public DateMetric() {
		// TODO Auto-generated constructor stub
	}

	public DateMetric(MemberMetric metricName, Date value) {
		this.metricName = metricName;
		this.value = value;
	}

	@Override
	public MemberMetric getMetricName() {
		return metricName;
	}

	@Override
	public Date getValue() {
		return value;
	}

	public void setMetricName(MemberMetric metricName) {
		this.metricName = metricName;
	}

	public void setValue(Date value) {
		this.value = value;
	}
}
