package net.reunify.pulse.members.interception;

import java.util.List;

/**
 * Response object for getMemberInterceptQueue service
 * 
 * @author kiana.baradaran
 * 
 */
public class InterceptQueueResponse {
	private List<InterceptQueueEntry> entries;
	private int queueSize;

	public InterceptQueueResponse() {
		// TODO Auto-generated constructor stub
	}

	public InterceptQueueResponse(List<InterceptQueueEntry> entries) {
		this.entries = entries;
		this.queueSize = entries.size();
	}

	public List<InterceptQueueEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<InterceptQueueEntry> entries) {
		this.entries = entries;
	}

	public int getQueueSize() {
		return queueSize;
	}

	public void setQueueSize(int queueSize) {
		this.queueSize = queueSize;
	}
}
