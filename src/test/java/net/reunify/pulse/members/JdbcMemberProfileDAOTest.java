package net.reunify.pulse.members;

import javax.inject.Inject;

import net.reunify.pulse.base.InvalidInputFormat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/spring-security.xml",
		"file:src/test/webapp/WEB-INF/mvc-dispatcher-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class JdbcMemberProfileDAOTest {

	@Inject
	private MemberProfileDAO memberProfileDAO;

	@Test(expected = InvalidInputFormat.class)
	public void testValidateClientMemberId() {
		memberProfileDAO.validateClientMemberId("163", 1);
	}

}
