/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.user;

import java.util.List;

/**
 * DAO for UserRole object
 * 
 * @author kiana.baradaran
 * 
 */
public interface UserRoleDAO {
	/**
	 * Get a list of valid roles
	 * 
	 * @return
	 */
	List<UserRole> getAllRoles();
}
