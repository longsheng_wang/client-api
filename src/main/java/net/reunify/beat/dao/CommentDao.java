/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.beat.dao;

import java.sql.Timestamp;
import java.util.List;

import net.reunify.beat.model.FeedbackComment;
import net.reunify.beat.model.FeedbackCommentIssueSum;
import net.reunify.beat.model.PersonMention;
import net.reunify.beat.model.SentimentCount;
import net.reunify.beat.model.SurveyAnswerCount;

/**
 * @author longsheng.wang
 *
 */
public interface CommentDao {
	
	/**
	 * Get latest comments order by message timestamp. The number of comments will be smaller one of maxNum and number of records in the database.
	 * Time window is [from, to) which means the Timestamp from is included but to is not.
	 * 
	 * @param maxNum
	 * @return
	 */
	List<FeedbackComment> getComments(int maxNum, Timestamp from, Timestamp to);

	/**
	 * Get top-count issues.
	 * 
	 * @param maxNum
	 * @param timeFrom
	 * @return
	 */
	List<FeedbackCommentIssueSum> getIssueSum(int maxNum, Timestamp from, Timestamp to);
	
	/**
	 * Get sentiment count.
	 * @return
	 */
	SentimentCount getSentimentCount(Timestamp from, Timestamp to);
	
	/**
	 * Get most mentioned people.
	 * 
	 * @param maxNum
	 * @param timestamp
	 * @return
	 */
	List<PersonMention> getMostMentionedPeople(int maxNum, Timestamp from, Timestamp to);
	
	/**
	 * Get count of all survey count.
	 * Currently (Jan, 2015) ordered by question content. 
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	List<SurveyAnswerCount> getAllSurveyQuestionAnswerCount(Timestamp from, Timestamp to);

	/**
	 * Get count of all survey group by survey_title which reflects location.
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	List<SurveyAnswerCount> getAllSurveyAnswerCountByLocation(Timestamp from, Timestamp to);
	
	/**
	 * Update a comment, set to resolved.
	 * @param id
	 * @param resolvedTime
	 * @return
	 */
	int updateCommentResolvedAndResolvedTime(long id, Timestamp resolvedTime);
	
	/**
	 * Check if a comment is resolved.
	 * @param id
	 * @return
	 */
	boolean checkCommentResolved(long id);
	
	/**
	 * Check if a comment is resolved.
	 * @param id
	 * @return null if id doesn't exist or comment status is unresolved.
	 */
	Timestamp checkCommentResolvedTimeStamp(long id);
	
	/**
	 * Unresolve a comment, set to unresolved, delete resolved time.
	 * Shouldn't be used except for testing.
	 * 
	 * @param id
	 * @param resolvedTime
	 * @return
	 */
	int unresolveCommentAndDeleteResolvedTime(long id);
	
}
