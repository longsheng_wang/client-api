/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.client;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import net.reunify.pulse.base.InvalidInputFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 * JDBC implementation of ClientDAO
 * 
 * @author kiana.baradaran
 * 
 */
public class JdbcClientDAO implements ClientDAO {
	private static final Logger logger = LoggerFactory.getLogger(JdbcClientDAO.class);

	@Inject
	@Named("pulseJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.reunify.pulse.client.ClientDAO#getActiveClients()
	 */
	@Override
	public List<ClientDTO> getActiveClients() {
		List<ClientDTO> activeClients = new ArrayList<ClientDTO>();

		String selectActiveClients =
				"SELECT client_name,client_id FROM clients WHERE is_active=true order by client_name";

		try {
			List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(selectActiveClients);

			for (Map<String, Object> row : rows) {
				ClientDTO client = new ClientDTO((Integer) row.get("client_id"), (String) row.get("client_name"));
				activeClients.add(client);
			}
		} catch (DataIntegrityViolationException e) {
			logger.error("Exception: ", e);
		}

		return activeClients;

	}

	public List<LocationDTO> getClientLocations(int clientId) {
		List<LocationDTO> locations = new ArrayList<LocationDTO>();

		String selectLocations =
				"select location_name,location_id,client_id,location_timezone,performance_goal,active_users from client_locations where client_id = ? and is_active = true order by location_name";
		try {
			List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(selectLocations, new Object[] { clientId });

			for (Map<String, Object> row : rows) {
				LocationDTO location =
						new LocationDTO((String) row.get("location_name"), (Integer) row.get("location_id"),
										(String) row.get("location_timezone"), (Integer) row.get("performance_goal"),
										(Integer) row.get("active_users"), clientId);
				locations.add(location);
			}
		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat("No locations are registered for client with id " + clientId
					+ ". Please check the input data and try again. StackTrace: " + e.getMessage());
		}
		return locations;
	}

	@Override
	public int createClientAccount(ClientDTO client) {

		String insertClient =
				"insert into clients(industry_name,client_name,is_active) values (?,?,true) returning client_id;";

		int clientId = 0;
		try {
			clientId =
					this.jdbcTemplate.queryForObject(insertClient,
							new Object[] { client.getIndustryName(), client.getCompanyName() },
							new RowMapper<Integer>() {
								public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
									return rs.getInt(1);
								}

							});
		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat(
											"The client could not be added. Please check the input data and try again. StackTrace: "
													+ e.getMessage());
		}

		return clientId;
	}

	@Override
	public int addLocation(LocationDTO locationDTO) {

		String insertClientLocation =
				"INSERT INTO client_locations (client_id,client_location_id,location_name,location_timezone,performance_goal,active_users,is_active) values (?,?,?,?,?,?,true) returning location_id;";

		String updateClientLocationId =
				"UPDATE client_locations SET client_location_id = location_id WHERE location_id = ?;";

		int locationId = 0;
		
		try {
			if (locationDTO.getClientLocationId() != null && !locationDTO.getClientLocationId().trim().equals("")) {
				locationId =
						this.jdbcTemplate.queryForObject(
								insertClientLocation,
								new Object[] { locationDTO.getClientId(), locationDTO.getClientLocationId(),
										locationDTO.getName(), locationDTO.getTimeZone(),
										locationDTO.getPerformanceGoal(), locationDTO.getActiveUsers() },
								new RowMapper<Integer>() {
									public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
										return rs.getInt(1);
									}

								});
			} else {
				locationId =
						this.jdbcTemplate.queryForObject(insertClientLocation, new Object[] {
								locationDTO.getClientId(), -1, locationDTO.getName(), locationDTO.getTimeZone(),
								locationDTO.getPerformanceGoal(), locationDTO.getActiveUsers() },
								new RowMapper<Integer>() {
									public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
										return rs.getInt(1);
									}

								});
				this.jdbcTemplate.update(updateClientLocationId, new Object[] { locationId });
			}
		} catch (DataIntegrityViolationException e) {
			logger.error("Adding the location failed.", e);
		}

		return locationId;
	}

	@Override
	public boolean removeLocation(int locationId, int clientId) {
		String deleteLocation =
				"update client_locations set is_active = false where location_id=? and client_id = ? and "
						+ "EXISTS (select 1 from client_locations where location_id <> ? and client_id = ? and is_active = true);";

		int deletedRows = 0;
		boolean success = false;
		try {
			deletedRows =
					jdbcTemplate.update(deleteLocation, new Object[] { locationId, clientId, locationId, clientId });
			if (deletedRows != 0) {
				success = true;
				logger.info("Deleting the location was successful");
			} else {
				logger.error("Deleting the location failed");
			}

		} catch (DataIntegrityViolationException e) {
			logger.error("Removing location failed.", e);
		}
		return success;
	}

	@Override
	public boolean updateLocation(LocationDTO location) {

		String updateLocation =
				"update client_locations set location_name=? , location_timezone=?,performance_goal=?, active_users= ? where location_id=? and client_id = ?;";
		int updatedRows = 0;
		boolean success = false;

		if (location.getClientLocationId() != null) {
			updateLocation =
					"update client_locations set location_name=? , location_timezone=?,client_location_id=?,performance_goal=?, active_users= ?  where location_id=? and client_id = ?;";
			updatedRows =
					jdbcTemplate.update(updateLocation, new Object[] { location.getName(), location.getTimeZone(),
							location.getClientLocationId(), location.getPerformanceGoal(), location.getActiveUsers(),
							location.getLocationId(), location.getClientId() });

		} else {
			updatedRows =
					jdbcTemplate.update(updateLocation, new Object[] { location.getName(), location.getTimeZone(),
							location.getPerformanceGoal(), location.getActiveUsers(), location.getLocationId(),
							location.getClientId() });

		}

		if (updatedRows != 0) {
			success = true;
			logger.info("Updating the location was successful");
		} else {
			logger.error("Updating the location failed");
		}
		return success;
	}

	@Override
	public boolean updateClientAccount(ClientDTO client) {
		String updateClient = "update clients set client_name=? , industry_name=? where client_id=?;";
		int updatedRows = 0;
		boolean success = false;

		updatedRows =
				jdbcTemplate.update(updateClient, new Object[] { client.getCompanyName(), client.getIndustryName(),
						client.getClientId() });
		if (updatedRows != 0) {
			success = true;
			logger.info("Updating the client account was successful");
		} else {
			logger.error("Updating the  client account failed");
		}
		return success;
	}

	@Override
	public boolean removeClientAccount(int clientId) {
		String deleteLocation = "update clients set is_active = false where client_id=?;";
		int deletedRows = 0;
		boolean success = false;
		try {
			deletedRows = jdbcTemplate.update(deleteLocation, new Object[] { clientId });
			if (deletedRows != 0) {
				success = true;
				logger.info("Deleting the account was successful");
			} else {
				logger.error("Deleting the account failed");
			}

		} catch (DataIntegrityViolationException e) {
			logger.error("Removing client account failed.", e);
		}
		return success;
	}
}
