-- These roles are not being used by anyone or anything
REVOKE ALL ON social_updates FROM ltfsocial;
REVOKE ALL ON social_profiles FROM ltfsocial;
DROP ROLE ltffind,ltflighthaus,ltfpersona,ltfrecordlinkage,ltfsocial;
