/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.beat.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.inject.Inject;
import javax.inject.Named;

import net.reunify.beat.model.FeedbackComment;
import net.reunify.beat.model.FeedbackCommentIssueSum;
import net.reunify.beat.model.PersonMention;
import net.reunify.beat.model.SentimentCount;
import net.reunify.beat.model.SurveyAnswerCount;
import net.reunify.pulse.base.InvalidInputFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * @author longsheng.wang
 *
 */
@Component("commentDao")
public class FeedbackCommentDao implements CommentDao {

	private static final Logger logger = LoggerFactory.getLogger(FeedbackCommentDao.class);

	@Inject
	@Named("feedbackJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.reunify.beat.dao.CommentDao#getLatestComments(int)
	 */
	@Override
	public List<FeedbackComment> getComments(int maxNum, Timestamp from, Timestamp to) {
		List<FeedbackComment> res = new ArrayList<FeedbackComment>();
		String query = "SELECT fc.*, fcl.name FROM feedback_comment AS fc, feedback_client_location AS fcl "
				+ "WHERE fc.location_id = fcl.location_id AND fc.message_time >= ? AND fc.message_time < ? "
				+ "ORDER BY fc.message_time DESC LIMIT ?";

		try {
			List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(query, new Object[] { from, to, maxNum });

			for (Map<String, Object> row : rows) {

				FeedbackComment comment = new FeedbackComment((long) row.get("record_id"),
																(boolean) row.get("anonymous_author"),
																(String) row.get("comment_author"),
																(String) row.get("comment_message"),
																(int) row.get("group_type"),
																(Timestamp) row.get("insertion_time"),
																(Timestamp) row.get("message_time"),
																(int) row.get("survey_location_id"),
																(int) row.get("survey_type"),
																(int) row.get("client_id"),
																(long) row.get("device_record_id"),
																(int) row.get("location_id"), (String) row.get("name"),
																(Timestamp) row.get("resolved_time"),
																(boolean) row.get("resolved"));
				res.add(comment);
			}
		} catch (DataIntegrityViolationException e) {
			logger.error("DAO error: Class: " + getClass().getName() + ". Message: " + e.getMessage());
			throw new InvalidInputFormat("StackTrace: " + e.getMessage());
		}
		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.reunify.beat.dao.CommentDao#getIssueSum(int, java.sql.Timestamp)
	 */
	@Override
	public List<FeedbackCommentIssueSum> getIssueSum(int maxNum, Timestamp from, Timestamp to) {
		List<FeedbackCommentIssueSum> result = new ArrayList<FeedbackCommentIssueSum>();
		String query = "SELECT count(comment_id),item " + "FROM feedback_comment_features "
				+ "WHERE feature_type ='ISSUE' AND message_time >= ? AND message_time < ? "
				+ "GROUP BY item ORDER BY count DESC LIMIT ?";
		try {
			List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(query, new Object[] { from, to, maxNum });

			for (Map<String, Object> row : rows) {

				FeedbackCommentIssueSum issueSum = new FeedbackCommentIssueSum((String) row.get("item"),
																				(long) row.get("count"));
				result.add(issueSum);
			}
		} catch (DataIntegrityViolationException e) {
			logger.error("DAO error: Class: " + getClass().getName() + ". Message: " + e.getMessage());
			throw new InvalidInputFormat("StackTrace: " + e.getMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.reunify.beat.dao.CommentDao#getSentimentCount()
	 */
	@Override
	public SentimentCount getSentimentCount(Timestamp from, Timestamp to) {
		SentimentCount sentimentCount = new SentimentCount();
		String query = "SELECT COUNT(comment_id),item " + "FROM feedback_comment_features "
				+ "WHERE feature_type ='SENTIMENT' AND message_time >= ? AND message_time<? "
				+ "GROUP BY item ORDER BY count DESC";
		try {
			List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(query, new Object[] { from, to });
			for (Map<String, Object> row : rows) {
				String item = (String) row.get("item");
				long count = (long) row.get("count");
				sentimentCount.setByName(item, count);
			}
		} catch (DataIntegrityViolationException e) {
			logger.error("DAO error: Class: " + getClass().getName() + ". Message: " + e.getMessage());
			throw new InvalidInputFormat("StackTrace: " + e.getMessage());
		}
		return sentimentCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.reunify.beat.dao.CommentDao#getMostMentionedPeople(int,
	 * java.sql.Timestamp)
	 */
	@Override
	public List<PersonMention> getMostMentionedPeople(int maxNum, Timestamp from, Timestamp to) {
		List<PersonMention> result = new ArrayList<PersonMention>();
		String query = "SELECT count(*),item " + "FROM feedback_comment_features "
				+ "WHERE feature_type ='PERSON' AND message_time >= ? AND message_time < ? "
				+ "GROUP BY item ORDER BY count DESC LIMIT ?";
		try {
			List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(query, new Object[] { from, to, maxNum });

			for (Map<String, Object> row : rows) {

				PersonMention personMention = new PersonMention((String) row.get("item"), (long) row.get("count"));
				result.add(personMention);
			}
		} catch (DataIntegrityViolationException e) {
			logger.error("DAO error: Class: " + getClass().getName() + ". Message: " + e.getMessage());
			throw new InvalidInputFormat("StackTrace: " + e.getMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.reunify.beat.dao.CommentDao#getAllSurveyQuestionAnswerCount(java.
	 * sql.Timestamp, java.sql.Timestamp)
	 */
	@Override
	public List<SurveyAnswerCount> getAllSurveyQuestionAnswerCount(Timestamp from, Timestamp to) {
		Map<String, SurveyAnswerCount> questionMap = new TreeMap<String, SurveyAnswerCount>();
		String query = "select qa.questions, qa.survey_button_value, count(*)"
				+ " from (with questions as ("
				+ " select t.*, rank() OVER (PARTITION BY questions_record_id order by rownum) as position"
				+ " from (select *, row_number() over (order by questions_record_id) rownum"
				+ "	from feedback_questionnaire_questions) t)"
				+ " select fqq.questions_record_id, fqq.questions, a.survey_survey_id, a.survey_button_id, a.survey_button_value"
				+ " from feedback_survey_buttondata a join feedback_survey_main fsm on a.survey_survey_id = fsm.survey_id"
				+ " join feedback_questionnaire fq on fsm.survey_location_id = fq.survey_location_id and fsm.location_id = fq.location_id"
				+ " join questions fqq on fq.record_id = fqq.questions_record_id  and fqq.position = a.survey_button_id::bit(3)::int+1"
				+ " where fsm.insertion_time >= ? and fsm.insertion_time < ? ) qa"
				+ " group by qa.questions, qa.survey_button_value order by 1";
		try {
			List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(query, new Object[] { from, to });
			for (Map<String, Object> row : rows) {
				String answer = (String) row.get("survey_button_value");
				String question = (String) row.get("questions");
				long count = (long) row.get("count");
				SurveyAnswerCount answerCount = questionMap.get(question);
				if (answerCount == null) {
					answerCount = new SurveyAnswerCount();
					answerCount.setContent(question);
					questionMap.put(question, answerCount);
				}
				switch (answer) {
				case "000":
					answerCount.setNotanswered(count);
					break;
				case "001":
					answerCount.setNegative(count);
					break;
				case "010":
					answerCount.setNeutral(count);
					break;
				case "100":
					answerCount.setPositive(count);
					break;
				}
			}
		} catch (DataIntegrityViolationException e) {
			logger.error("DAO error: Class: " + getClass().getName() + ". Message: " + e.getMessage());
			throw new InvalidInputFormat("StackTrace: " + e.getMessage());
		}
		return new ArrayList<SurveyAnswerCount>(questionMap.values());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.reunify.beat.dao.CommentDao#getAllSurveyAnswerCountByLocation(java
	 * .sql.Timestamp, java.sql.Timestamp)
	 */
	@Override
	public List<SurveyAnswerCount> getAllSurveyAnswerCountByLocation(Timestamp from, Timestamp to) {
		Map<String, SurveyAnswerCount> locationAnswerMap = new TreeMap<String, SurveyAnswerCount>();
		String query = "select qa.survey_title, qa.survey_button_value, count(*)"
				+ " from (with questions as ("
				+ " select t.*, rank() OVER (PARTITION BY questions_record_id order by rownum) as position"
				+ " from (select *, row_number() over (order by questions_record_id) rownum"
				+ "	from feedback_questionnaire_questions) t)"
				+ " select fqq.questions_record_id, fqq.questions, a.survey_survey_id, a.survey_button_id, a.survey_button_value, fq.survey_title"
				+ " from feedback_survey_buttondata a join feedback_survey_main fsm on a.survey_survey_id = fsm.survey_id"
				+ " join feedback_questionnaire fq on fsm.survey_location_id = fq.survey_location_id and fsm.location_id = fq.location_id"
				+ " join questions fqq on fq.record_id = fqq.questions_record_id  and fqq.position = a.survey_button_id::bit(3)::int+1"
				+ " where fsm.insertion_time >= ? and fsm.insertion_time < ? ) qa"
				+ " group by qa.survey_title, qa.survey_button_value order by 1";
		try {
			List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(query, new Object[] { from, to });
			for (Map<String, Object> row : rows) {
				String answer = (String) row.get("survey_button_value");
				String location = (String) row.get("survey_title");
				long count = (long) row.get("count");
				SurveyAnswerCount answerCount = locationAnswerMap.get(location);
				if (answerCount == null) {
					answerCount = new SurveyAnswerCount();
					answerCount.setContent(location);
					locationAnswerMap.put(location, answerCount);
				}
				switch (answer) {
				case "000":
					answerCount.setNotanswered(count);
					break;
				case "001":
					answerCount.setNegative(count);
					break;
				case "010":
					answerCount.setNeutral(count);
					break;
				case "100":
					answerCount.setPositive(count);
					break;
				}
			}
		} catch (DataIntegrityViolationException e) {
			logger.error("DAO error: Class: " + getClass().getName() + ". Message: " + e.getMessage());
			throw new InvalidInputFormat("StackTrace: " + e.getMessage());
		}
		return new ArrayList<SurveyAnswerCount>(locationAnswerMap.values());
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.dao.CommentDao#updateCommentResolvedAndResolvedTime(long, java.sql.Timestamp)
	 */
	@Override
	public int updateCommentResolvedAndResolvedTime(long id, Timestamp resolvedTime) {
		String query = "UPDATE feedback_comment SET resolved = 't', resolved_time = ? WHERE record_id = ?";
		return jdbcTemplate.update(query, new Object[]{resolvedTime, id});
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.dao.CommentDao#checkCommentResolved(long)
	 */
	@Override
	public boolean checkCommentResolved(long id) {
		String query = "select resolved from feedback_comment where record_id = ?";
		return jdbcTemplate.queryForObject(query, new Object[]{id}, Boolean.class);
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.dao.CommentDao#checkCommentResolvedTimeStamp(long)
	 */
	@Override
	public Timestamp checkCommentResolvedTimeStamp(long id) {
		try {
			String query = "select resolved, resolved_time from feedback_comment where record_id = ?";
			List<Map<String, Object>> rows =  jdbcTemplate.queryForList(query, new Object[]{id});
			if (rows.size() == 0) return null;
			if ((Boolean)(rows.get(0).get("resolved"))){
				return (Timestamp)rows.get(0).get("resolved_time");
			}
		}
		catch(Exception e){
			logger.warn("Check comment timestamp exception in DAO !!!", e);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see net.reunify.beat.dao.CommentDao#unresolveCommentAndDeleteResolvedTime(long)
	 */
	@Override
	public int unresolveCommentAndDeleteResolvedTime(long id) {
		String query = "UPDATE feedback_comment SET resolved = 'f', resolved_time = NULL WHERE record_id = ?";
		return jdbcTemplate.update(query, new Object[]{id});
	}

}
