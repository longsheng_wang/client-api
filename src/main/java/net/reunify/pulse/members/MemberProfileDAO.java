package net.reunify.pulse.members;

/**
 * 
 * @author kiana.baradaran
 * 
 */
public interface MemberProfileDAO {
	/**
	 * Get a member profile by id
	 * 
	 * @param memberId
	 * @param clientId
	 * @return
	 */
	MemberProfile getMemberProfile(int memberId, int clientId);

	/**
	 * Update an existing member profile
	 * 
	 * @param memberProfile
	 * @param memberId
	 * @param clientId
	 */
	void updateMemberProfile(MemberProfile memberProfile, String memberId, int clientId);

	/**
	 * Insert a new member profile
	 * 
	 * @param memberProfile
	 */
	void insertMemberProfile(MemberProfile memberProfile, int clientId);

	/**
	 * Verify is the given id exists
	 * 
	 * @param clientMemberId
	 * @param clientId
	 * @return
	 */
	boolean validateClientMemberId(String clientMemberId, int clientId);

	/**
	 * Verify is the given id exists
	 * 
	 * @param clientLocationId
	 * @param clientId
	 * @return
	 */
	boolean validateClientLocationId(String clientLocationId, int clientId);

}
