/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;
import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Validator for reports that only use a date range
 * @author Vivek Hungund
 *
 */
public class DateRangeReportFormValidator implements Validator {
	private static final Logger logger = Logger.getLogger(DateRangeReportFormValidator.class);
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> clazz) {
		return DateRangeReportForm.class.isAssignableFrom(clazz);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object,
	 * org.springframework.validation.Errors)
	 */
	@Override
	public void validate(Object target, Errors errors) {
		DateRangeReportForm form = (DateRangeReportForm) target;

		DateRangeReportFormValidationUtils.validateDate(form.getStartDate(), errors, "start");
		DateRangeReportFormValidationUtils.validateDate(form.getEndDate(), errors, "end");
		DateRangeReportFormValidationUtils.validateDateRange(form, errors);

	}
}
