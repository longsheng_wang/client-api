<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<ui:loginview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Pulse - New User Sign-up">
	<jsp:attribute name="headTag">
		<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/login/registration.css"
			rel="stylesheet">
	</jsp:attribute>
	<jsp:body>
	
<div class="wrapper">

<form:form method="POST"
				action="${pageContext.servletContext.contextPath}/client/registration"
				commandName="client">
             <table>
                <tr>
                    <td><form:label path="companyName">Company Name</form:label></td>
                    <td><form:input path="companyName" /></td>
                      <td class="error">  <form:errors
								path="companyName" element="div" /></td>
                    
                </tr>
                
                 <tr>
                    <td><form:label path="industryName">Industry Name</form:label></td>
                    <td><form:select path="industryName">
							<form:option value="NONE" label="--- Select ---" />
							<form:option value="health & fitness" label="Fitness & Health" />
   						</form:select></td>
                   <td class="error"><form:errors
								path="industryName" element="div" /></td>
                </tr>
        
          		<tr>
                    <td><form:label
								path="locations[0].clientLocationId">Location ID</form:label></td>
                    <td><form:input
								path="locations[0].clientLocationId" /></td>
                   <td class="error"><form:errors
								path="locations[0].clientLocationId" element="div" /></td>
                </tr>
       	 		<tr>
                    <td><form:label path="locations[0].name">Location Name</form:label></td>
                    <td><form:input path="locations[0].name" /></td>
                   <td class="error"><form:errors
								path="locations[0].name" element="div" /></td>
                </tr>
                     <tr>
                    <td><form:label path="locations[0].timeZone">Location Time Zone</form:label></td>
                    <td><form:select path="locations[0].timeZone">
							<form:option value="NONE" label="--- Select ---" />
							<form:option value="US/Pacific" label="US Pacific" />
							<form:option value="US/Eastern" label="US Eastern" />
							<form:option value="US/Mountain" label="US Mountain" />
							<form:option value="US/Central" label="US Central" />
   						</form:select></td>
                   <td class="error"><form:errors
								path="locations[0].timeZone" element="div" /></td>
                </tr>
                
                	<tr>
                    <td><form:label path="locations[0].activeUsers">Number of Active Users</form:label></td>
                    <td><form:input path="locations[0].activeUsers" /></td>
                   <td class="error"><form:errors
								path="locations[0].activeUsers" element="div" /></td>
                </tr>
                	<tr>
                    <td><form:label
								path="locations[0].performanceGoal">Performance Goal</form:label></td>
                    <td><form:input
								path="locations[0].performanceGoal" /></td>
                   <td class="error"><form:errors
								path="locations[0].performanceGoal" element="div" /></td>
                </tr>
                
                <tr>
                    <td><form:label path="accountAdmin.firstName">First Name</form:label></td>
                    <td><form:input path="accountAdmin.firstName" /></td>
                      <td class="error">  <form:errors
								path="accountAdmin.firstName" element="div" /></td>
                    
                </tr>
                        <tr>
                    <td><form:label path="accountAdmin.lastName">Last Name</form:label></td>
                    <td><form:input path="accountAdmin.lastName" /></td>
                    <td class="error"><form:errors
								path="accountAdmin.lastName" element="div" /></td>
                </tr>
                <tr>
                    <td><form:label
								path="accountAdmin.emailAddress">Email Address</form:label></td>
                    <td><form:input
								path="accountAdmin.emailAddress" /></td>
                        <td class="error"><form:errors
								path="accountAdmin.emailAddress" element="div" /></td>
                    
                </tr>       
                <tr>
                    <td><form:label
								path="accountAdmin.clientUserId">User ID</form:label></td>
                    <td><form:input
								path="accountAdmin.clientUserId" /></td>
                        <td class="error"><form:errors
								path="accountAdmin.clientUserId" element="div" /></td>
                </tr>
                                                
                 <tr>
                          <td><form:label
								path="accountAdmin.password">Password</form:label></td>
                    <td><form:password path="accountAdmin.password" /></td>
                         <td class="error"><form:errors
								path="accountAdmin.password" element="div" /></td>
                    
                </tr>
                 <tr>
                    <td><form:label
								path="accountAdmin.matchingPassword">Confirm Password</form:label></td>
                    <td><form:password
								path="accountAdmin.matchingPassword" /></td>
                         <td class="error"><form:errors
								path="accountAdmin.matchingPassword" element="div" /></td>
                    
                </tr>
                <tr>
                    <td><input type="submit" value="Submit" /></td>
                </tr>
            </table>
        </form:form>
  </div>
	 
	</jsp:body>
</ui:loginview>