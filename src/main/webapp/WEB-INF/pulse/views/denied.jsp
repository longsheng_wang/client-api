<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@page import="java.net.URLEncoder"%>

<%
	String queryString = "";
	if (request.getParameter("target") != null) {
		String targetURL = URLEncoder.encode(request.getParameter("target"), "UTF-8");
		queryString = "?target=" + targetURL;
	}
	session.setAttribute("queryString", queryString);
%>
<ui:loginview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Trace - Access Denied">
	<jsp:attribute name="headTag">
	<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/login/access-denied.css"
			rel="stylesheet">
	</jsp:attribute>
	<jsp:body>
<div class="wrapper">
<div class="main-div">
	Your login credentials are invalid.
	<br>
	<br>
	<A
					HREF="${pageContext.servletContext.contextPath}/login/reunify${sessionScope.queryString}">Try again.</A>
	</div>
	</div>
	</jsp:body>
</ui:loginview>

