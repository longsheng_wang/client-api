/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.client;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import net.reunify.pulse.base.BaseController;
import net.reunify.pulse.user.UserRole;
import net.reunify.pulse.user.UserService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Handles requests for creation of new clients
 * 
 * @author kiana.baradaran
 * 
 */
@Controller
public class ClientController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(ClientController.class);

	@Inject
	@Named("ClientService")
	private ClientService clientServiceHandler;

	@Inject
	@Named("UserService")
	private UserService userServiceHandler;

	/**
	 * Populate the list of locations for a client
	 * 
	 * @param clientId
	 * @return
	 */
	protected Map<Integer, String> populateLocationList(int clientId) {
		List<LocationDTO> locations = clientServiceHandler.getClientLocations(clientId);

		Map<Integer, String> locationsList = new HashMap<Integer, String>();
		for (LocationDTO location : locations) {
			locationsList.put(location.getLocationId(), location.getName());
		}
		return locationsList;
	}

	/**
	 * Provides the tabular view of locations
	 * 
	 * @return
	 */
	@RequestMapping(value = "/client/locations", method = RequestMethod.GET)
	public ModelAndView locationsView(HttpServletRequest request) {

		List<LocationDTO> locationsList =
				clientServiceHandler.getClientLocations(Integer.parseInt((String) request.getSession().getAttribute(
						"clientId")));

		ModelAndView locationsView = new ModelAndView("locationsView");
		locationsView.addObject("locationsList", locationsList);

		return locationsView;
	}

	/**
	 * Provides the view for adding locations
	 * 
	 * @return
	 */
	@RequestMapping(value = "/client/location/addview", method = RequestMethod.GET)
	public ModelAndView showAddLocationView(HttpServletRequest request) {

		ModelAndView addLocationView = new ModelAndView("locationregistration");
		addLocationView.addObject("location", new LocationDTO());

		return addLocationView;
	}

	/**
	 * Provides the view for updating locations
	 * 
	 * @return
	 */
	@RequestMapping(value = "/client/location/updateview/{locationId}", method = RequestMethod.GET)
	public ModelAndView showEditLocationView(HttpServletRequest request, @PathVariable int locationId) {

		ModelAndView addLocationView = new ModelAndView("locationregistration");
		LocationDTO location = new LocationDTO();
		location.setLocationId(locationId);
		addLocationView.addObject("location", location);
		return addLocationView;
	}

	/**
	 * Request for adding a location
	 * 
	 * @return
	 */
	@RequestMapping(value = "/client/location/addorupdate", method = RequestMethod.POST)
	public String addLocation(@ModelAttribute("location") LocationDTO location, BindingResult result, ModelMap model,
			HttpServletRequest request) {
		location.setClientId(Integer.parseInt((String) request.getSession().getAttribute("clientId")));
		LocationRegistrationValidator formValidator = new LocationRegistrationValidator();
		formValidator.validate(location, result);
		int locationId = 0;
		String view = "locationregistration";

		if (!result.hasErrors()) {
			// if it is an update request
			if (location.getLocationId() > 0) {
				boolean success = clientServiceHandler.updateLocation(location);
				if (success) {
					model.addAttribute("message",
							"The location has been successfully updated for " + location.getName() + ".");
					model.addAttribute("redirectUrl", "/client/locations");
					model.addAttribute("redirectMessage", "Go back to locations view.");

				} else {
					model.addAttribute("message", "The location could not be updated.");
					model.addAttribute("redirecturl", "/client/locations");
					model.addAttribute("redirectMessage", "Try Again!");
				}
			} else {
				// if it is an add request
				locationId = clientServiceHandler.addLocation(location);
				if (locationId > 0) {
					model.addAttribute("message", "The location has been successfully added for " + location.getName()
							+ ".");
					model.addAttribute("redirectUrl", "/client/locations");
					model.addAttribute("redirectMessage", "Go back to locations view.");

				} else {
					model.addAttribute("message", "The location could not be added.");
					model.addAttribute("redirecturl", "/client/locations");
					model.addAttribute("redirectMessage", "Try Again!");
				}
			}
			view = "actionresult";
		}

		return view;
	}

	/**
	 * Removes (deactivates) the location that matches the given information
	 * 
	 * @param locationId
	 * @return
	 */
	@RequestMapping(value = "/client/location/remove/{locationId}", method = RequestMethod.GET)
	public String removeLocation(HttpServletRequest request, @PathVariable int locationId, ModelMap model) {
		String view = "actionresult";
		int clientId = Integer.parseInt((String) request.getSession().getAttribute("clientId"));

		boolean success = clientServiceHandler.removeLocation(locationId, clientId);

		if (success) {
			model.addAttribute("message", "The location was successfully removed.");
			model.addAttribute("redirectUrl", "/client/locations");
			model.addAttribute("redirectMessage", "Go back to locations view.");
		} else {
			model.addAttribute("message", "The location could not be removed.");
			model.addAttribute("redirectUrl", "/client/locations");
			model.addAttribute("redirectMessage", "Try Again");
		}
		return view;
	}

	/**
	 * Request an account creation for the information provided
	 * 
	 * @param client
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/client/registration", method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView createAccount(@ModelAttribute("client") ClientDTO client, BindingResult result,
			HttpServletRequest request) {

		ModelAndView registrationView = new ModelAndView("clientregistration");

		if (request.getMethod().equals("POST")) {
			client.getAccountAdmin().setRole(new UserRole(1, "Admin"));
			ClientRegistrationValidator formValidator = new ClientRegistrationValidator();
			formValidator.validate(client, result);
			int userId = 0;

			if (!result.hasErrors()) {
				logger.info("creating client account...");
				userId = clientServiceHandler.createClientAccount(client);
				registrationView = new ModelAndView("actionresult");
				if (userId > 0) {
					registrationView.addObject("message",
							"The account has been successfully created for " + client.getCompanyName() + ".");
					registrationView.addObject("redirectUrl", "/login");
					registrationView.addObject("redirectMessage", "Login now!");

				} else {
					registrationView.addObject("message", "The account could not be created.");
					registrationView.addObject("redirecturl", "/client/registration");
					registrationView.addObject("redirectMessage", "Try Again!");
				}

			}
		}
		registrationView.addObject("client", client);

		return registrationView;
	}

	/**
	 * Provides the client edit view
	 * 
	 * @return
	 */
	@RequestMapping(value = "/client/edit", method = RequestMethod.GET)
	public ModelAndView editFormView(HttpServletRequest request) {

		ModelAndView clientupdate = new ModelAndView("clientupdate");
		clientupdate.addObject("client", new ClientDTO());
		clientupdate.addObject("clientId", Integer.parseInt((String) request.getSession().getAttribute("clientId")));
		return clientupdate;
	}

	/**
	 * Request an account update for the information provided
	 * 
	 * @param client
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/client/updateaccount", method = RequestMethod.POST)
	public String updateAccount(@ModelAttribute("client") ClientDTO client, BindingResult result, ModelMap model,
			HttpServletRequest request) {

		ClientRegistrationValidator formValidator = new ClientRegistrationValidator();
		formValidator.validate(client, result);
		boolean success = false;
		String view = "actionresult";

		if (!result.hasErrors()) {
			logger.info("updating client account...");
			success = clientServiceHandler.updateClientAccount(client);
			if (success) {
				model.addAttribute("message",
						"The account has been successfully updated for " + client.getCompanyName() + ".");
				model.addAttribute("redirectUrl", "/clubpulse");
				model.addAttribute("redirectMessage", "Go back to club pulse");

			} else {
				model.addAttribute("message", "The account could not be updated.");
				model.addAttribute("redirecturl", "/clubpulse");
				model.addAttribute("redirectMessage", "Try Again!");
			}
			view = "actionresult";
		}

		return view;
	}

	/**
	 * Removes (deactivates) the client that matches the given information
	 * 
	 * @param locationId
	 * @return
	 */
	@RequestMapping(value = "/client/remove", method = RequestMethod.GET)
	public String removeClientAccount(ModelMap model, HttpServletRequest request) {
		String view = "actionresult";

		boolean success =
				clientServiceHandler.removeClientAccount(Integer.parseInt((String) request.getSession().getAttribute(
						"clientId")));

		if (success) {
			model.addAttribute("message", "The account was successfully removed.");
			model.addAttribute("redirectUrl", "/login");
			model.addAttribute("redirectMessage", "Go to login page.");
		} else {
			model.addAttribute("message", "The account could not be removed.");
			model.addAttribute("redirectUrl", "/clubpulse");
			model.addAttribute("redirectMessage", "Try Again");
		}
		return view;
	}
}
