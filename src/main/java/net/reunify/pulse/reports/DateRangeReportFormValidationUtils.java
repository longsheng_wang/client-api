/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

/**
 * Validation functions for fields that are common in all the report forms
 * @author Vivek Hungund
 *
 */
public class DateRangeReportFormValidationUtils {
	private static final Logger logger = Logger.getLogger(DateRangeReportFormValidationUtils.class);
	
	final static String dateFormat = "yyyy-MM-dd";
	
	/**
	 * @param form
	 * @param errors
	 * @param dateType
	 */
	protected static void validateDate(String date, Errors errors, String dateType) {
		
		String dateFieldPath = dateType + "Date";
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, dateFieldPath, dateFieldPath + "_required", dateType + " date required");
		if (!date.equals(""))
			validateDateFormat(date, errors, dateFieldPath);

	}
	
	/**
	 * @param date
	 * @param errors
	 * @param dateFieldPath
	 */
	protected static void validateDateFormat(String date, Errors errors, String dateFieldPath) {
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		Date d;
		try {
			format.setLenient(false);
			d = format.parse(date);
		} catch (ParseException e) {
			logger.error("Could not parse date ", e);
			errors.rejectValue(dateFieldPath, "invalid_date_format",
					"Date must be in the format yyyy-MM-dd, e.g. 2015-03-01");
		}

	}
	
	/**
	 * @param form
	 * @param errors
	 */
	protected static void validateDateRange(DateRangeReportForm form, Errors errors) {

		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		Date start, end;
		try {
			format.setLenient(false);
			start = format.parse(form.getStartDate());
			end = format.parse(form.getEndDate());

			if (start.after(end))
				errors.rejectValue("startDate", "invalid_date_range", "Start date must come before end date");

			if (start.after(new Date()))
				errors.rejectValue("startDate", "invalid_future_date", "Cannot select a future date");
			else if (end.after(new Date()))
				errors.rejectValue("endDate", "invalid_future_date", "Cannot select a future date");
		} catch (ParseException e) {
			logger.error("Could not parse date ", e);
		}

	}
}
