/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import net.reunify.pulse.analytics.metrics.DateMetric;
import net.reunify.pulse.analytics.metrics.MapMetric;
import net.reunify.pulse.analytics.metrics.ProbabilisticMetric;
import net.reunify.pulse.base.InvalidInputFormat;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * JDBC Implementation of MemberProfileDAO
 * 
 * @author kiana.baradaran
 * 
 */
@Component
public class JdbcMemberAnalyticsDAO implements MemberAnalyticsDAO {

	@Inject
	@Named("pulseJdbcTemplate")
	private JdbcTemplate pulseJdbcTemplate;

	@Override
	public MemberAnalytics getMemberAnalytics(int memberId, int clientId) {

		List<ProbabilisticMetric> scores = getMemberScores(memberId);
		MapMetric<Map<String, Double>> personas = getMemberPersona(memberId);
		MapMetric<Map<String, Double>> interests = getMemberInterest(memberId);
		List<DateMetric> dateMetrics = getDateMetrics(memberId);
		MemberAnalytics analytics = new MemberAnalytics(scores, personas, interests, dateMetrics);
		return analytics;
	}

	private List<DateMetric> getDateMetrics(int memberId) {
		String selectDateMetrics =
				"WITH last_intercept AS (SELECT max(intercept_time) last_intercept FROM member_intercepts WHERE member_id = ?)"
						+ " SELECT check_in_time ,last_intercept FROM members LEFT JOIN member_check_in on last_check_in_id = member_check_in.record_id,last_intercept WHERE member_id = ?;";

		List<DateMetric> dateMetrics = new ArrayList<DateMetric>();

		try {

			List<Map<String, Object>> rows =
					this.pulseJdbcTemplate.queryForList(selectDateMetrics, new Object[] { memberId, memberId });
			for (Map<String, Object> row : rows) {
				dateMetrics.add(new DateMetric(MemberMetric.LAST_VITSIT, (Date) row.get("check_in_time")));
				dateMetrics.add(new DateMetric(MemberMetric.LAST_INTERCEPT, (Date) row.get("last_intercept")));
			}
		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat("The input format is not valid. memberId: " + memberId + " . StackTrace: "
					+ e.getMessage());
		}

		return dateMetrics;
	}

	private List<ProbabilisticMetric> getMemberScores(int memberId) {
		String selectScores = "SELECT value_score, attrition_score, last_score_date FROM members WHERE member_id = ?;";
		String selectPatterns =
				"SELECT frequency, recency,score_date FROM (SELECT frequency, recency,score_date, max(score_date) OVER (partition by member_id) FROM member_swipes_pattern  WHERE member_id = ?) AS scores WHERE scores.max = scores.score_date;";
		List<ProbabilisticMetric> probabilisticMetrics = new ArrayList<ProbabilisticMetric>();

		try {
			List<Map<String, Object>> scoresRows =
					this.pulseJdbcTemplate.queryForList(selectPatterns, new Object[] { memberId });

			for (Map<String, Object> row : scoresRows) {
				ProbabilisticMetric attrition =
						new ProbabilisticMetric(MemberMetric.FREQUENCY, (Integer) row.get("frequency"),
												(Date) row.get("score_date"));
				ProbabilisticMetric value =
						new ProbabilisticMetric(MemberMetric.RECENCY, (Integer) row.get("recency"),
												(Date) row.get("score_date"));
				probabilisticMetrics.add(attrition);
				probabilisticMetrics.add(value);
			}

			List<Map<String, Object>> patternRows =
					this.pulseJdbcTemplate.queryForList(selectScores, new Object[] { memberId });

			for (Map<String, Object> row : patternRows) {
				ProbabilisticMetric attrition =
						new ProbabilisticMetric(MemberMetric.ATTRITION, (Integer) row.get("attrition_score"),
												(Date) row.get("last_score_date"));
				ProbabilisticMetric value =
						new ProbabilisticMetric(MemberMetric.VALUE, (Integer) row.get("value_score"),
												(Date) row.get("last_score_date"));
				probabilisticMetrics.add(attrition);
				probabilisticMetrics.add(value);
			}

		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat("The input format is not valid. memberId: " + memberId + " . StackTrace: "
					+ e.getMessage());

		}

		return probabilisticMetrics;
	}

	private MapMetric<Map<String, Double>> getMemberInterest(int memberId) {
		String selectInterests =
				"SELECT score_name ,score_value::float,update_time FROM member_personal_interests WHERE member_id = ? AND type = 'INTEREST';";

		Map<String, Double> interests = new HashMap<String, Double>();
		Date scoreDate = null;
		try {
			List<Map<String, Object>> rows =
					this.pulseJdbcTemplate.queryForList(selectInterests, new Object[] { memberId });

			for (Map<String, Object> row : rows) {
				interests.put((String) row.get("score_name"), (Double) row.get("score_value"));
				scoreDate = (Date) row.get("update_time");
			}

		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat("The input format is not valid. memberId: " + memberId + " . StackTrace: "
					+ e.getMessage());

		}

		MapMetric<Map<String, Double>> mapMetric =
				new MapMetric<Map<String, Double>>(MemberMetric.INTERESTS, interests, scoreDate);
		return mapMetric;
	}

	private MapMetric<Map<String, Double>> getMemberPersona(int memberId) {
		String selectPersona =
				"SELECT score_name,score_value::float,update_time FROM member_personal_interests WHERE member_id = ? AND type = 'PERSONA';";
		Map<String, Double> interests = new HashMap<String, Double>();
		Date scoreDate = null;

		try {
			List<Map<String, Object>> rows =
					this.pulseJdbcTemplate.queryForList(selectPersona, new Object[] { memberId });

			for (Map<String, Object> row : rows) {
				interests.put((String) row.get("score_name"), (Double) row.get("score_value"));
				scoreDate = (Date) row.get("update_time");
			}

		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat("The input format is not valid. memberId: " + memberId + " . StackTrace: "
					+ e.getMessage());

		}
		MapMetric<Map<String, Double>> mapMetric =
				new MapMetric<Map<String, Double>>(MemberMetric.PERSONAS, interests, scoreDate);
		return mapMetric;
	}

}
