/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import net.reunify.pulse.lifetimelocation.Location;
import net.reunify.pulse.lifetimelocation.LocationService;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Handles requests for reporting and submits them to ReportsService.
 * 
 * @author Vivek Hungund
 *
 */

@Controller
public class ReportsController {
	private static final Logger logger = Logger.getLogger(ReportsController.class);

	@Inject
	@Named("ReportsService")
	private ReportsService reportsServiceHandler;
	
	@Inject
	@Named("LocationService")
	private LocationService locationServiceHandler;

	/**
	 * Menu for reports
	 * 
	 * @return
	 */
	@RequestMapping(value = "/reports/", produces = "text/html", method = RequestMethod.GET)
	public ModelAndView menu() {
		ModelAndView mv = new ModelAndView("reportsmenu");

		return mv;
	}

	/**
	 * Retrieves an Intercept Progress Report
	 * 
	 * @param request
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@RequestMapping(value = "/reports/interceptprogressreport", produces = "text/html", method = { RequestMethod.GET,
			RequestMethod.POST })
	@ResponseBody
	public ModelAndView getDailyReport(HttpServletRequest request,
			@ModelAttribute("DateRangeReportForm") DateRangeReportForm form, BindingResult result) {

		ModelAndView mv = new ModelAndView("interceptprogressreport");

		if (request.getMethod().equals("POST")) {

			if (request.getParameter("dailyReport") != null) {
				if (logger.isDebugEnabled())
					logger.debug("Date of report: " + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

				InterceptProgressReport report = reportsServiceHandler.getInterceptProgressReport();
				mv.addObject("report", report.getReportData());
				mv.addObject("reportType", "daily");
			} else {

				DateRangeReportFormValidator validator = new DateRangeReportFormValidator();
				validator.validate(form, result);

				if (!result.hasErrors()) {

					if (request.getParameter("dateRangeReport") != null) {
						if (logger.isDebugEnabled())
							logger.debug("Date range of report: " + form.getStartDate() + " to " + form.getEndDate());

						InterceptProgressReport report = reportsServiceHandler.getInterceptProgressReport(
								form.getStartDate(), form.getEndDate());
						mv.addObject("report", report.getReportData());
						mv.addObject("reportType", "dateRange");

					}
				}
			}
		}

		return mv;
	}

	/**
	 * 
	 * Retrieves an MES Performance Report based on a date range supplied by the
	 * user date ranges are inclusive
	 * 
	 * @param request
	 * @param startDate
	 * @param endDate
	 * @return mv
	 */
	@RequestMapping(value = "/reports/mesperformance", produces = { "text/html" }, method = { RequestMethod.GET,
			RequestMethod.POST })
	@ResponseBody
	public ModelAndView getMESPerformanceReport(HttpServletRequest request,
			@ModelAttribute("DateRangeReportForm") DateRangeReportForm form, BindingResult result) {

		if (logger.isDebugEnabled())
			logger.debug("Date Range:" + form.getStartDate() + " - " + form.getEndDate());

		ModelAndView mv = new ModelAndView("mesperformancereport");

		if (request.getMethod().equals("POST")) {
			
			DateRangeReportFormValidator validator = new DateRangeReportFormValidator();
        	validator.validate(form, result);

        	if (!result.hasErrors()) {
        		MESPerformanceReportData report = reportsServiceHandler.getMESPerformaceReport(form.getStartDate(), form.getEndDate());
        		mv.addObject("report", report.getReportData());
        	}

		}

		return mv;
	}
	
	/**
     * 
     * Retrieves an Club Performance Report based on a date range + location id
     * startDate is inclusive, endDate is exclusive
     * 
     * @param request
     * @param startDate
     * @param endDate
     * @param locationId
     * @return mv
     */
    @RequestMapping(value = "/reports/clubperformance", produces = { "text/html" }, method = { RequestMethod.GET,
            RequestMethod.POST })
    @ResponseBody
    public ModelAndView getClubPerformanceReport(HttpServletRequest request,
            @ModelAttribute("ClubPerformanceReportForm") ClubPerformanceReportForm form,
			BindingResult result) {

        if (logger.isDebugEnabled())
            logger.debug("Date Range:" + form.getStartDate() + " - " + form.getEndDate() + "\tLocation ID: " + form.getLocationId());

        ModelAndView mv = new ModelAndView("clubperformancereport");
        Map<String,Location> locations = locationServiceHandler.getLocations();
        mv.addObject("locations", locations);

        if (request.getMethod().equals("POST")) {

        	ClubPerformanceReportFormValidator validator = new ClubPerformanceReportFormValidator();
        	validator.validate(form, result);

        	if (!result.hasErrors()) {
	            ClubPerformanceReport report = reportsServiceHandler.getClubPerformanceReport(form.getStartDate(), form.getEndDate(), form.getLocationId());
	            mv.addObject("report", report.getReportData());
        	}

        }

        return mv;
    }

}
