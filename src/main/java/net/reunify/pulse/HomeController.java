/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.reunify.pulse.base.BaseController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Handles requests for the application home page.
 * 
 * @author kiana.baradaran
 * 
 */
@Controller
public class HomeController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String root(Locale locale, Model model) {

		return "redirect:login";
	}

	@RequestMapping(value = "/login/reunify", method = RequestMethod.GET)
	public String rootRedirect(Locale locale, Model model) {

		return "login";
	}

	@RequestMapping(value = "/clubpulse", method = RequestMethod.GET)
	public ModelAndView getClubPulse(HttpServletRequest request, Model model) {

		return new ModelAndView("clubpulse");
	}

	@RequestMapping(value = "/clubtraffic", method = RequestMethod.GET)
	public ModelAndView getClubTraffic(HttpServletRequest request, Model model) {

		return new ModelAndView("clubtraffic");
	}

	@RequestMapping(value = "/clubanalytics", method = RequestMethod.GET)
	public ModelAndView getClubAnalytics(HttpServletRequest request, Model model) {

		return new ModelAndView("clubanalytics");
	}

	@RequestMapping(value = "/memberpulse", method = RequestMethod.GET)
	public ModelAndView getMemberPulse(HttpServletRequest request, Model model) {

		return new ModelAndView("memberpulse");
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ModelAndView getMemberSearch(HttpServletRequest request, Model model) {

		return new ModelAndView("search");
	}

	@RequestMapping(value = "/memberprofile", method = RequestMethod.GET)
	public ModelAndView getMemberProfile(HttpServletRequest request) {
		Assert.notNull(request.getParameter("memberId"));
		return new ModelAndView("memberprofile");
	}

	@RequestMapping(value = "/newpulse", method = RequestMethod.GET)
	public ModelAndView getNewPulse(HttpServletRequest request, Model model) {

		return new ModelAndView("newpulse");
	}

	/**
	 * Used for testing (will be removed in the final draft)
	 * 
	 * @param locale
	 * @param model
	 * @param id
	 * @return
	 */

	@RequestMapping(value = "/emp/get/{id}", method = RequestMethod.GET)
	public String getEmployee(Locale locale, Model model, @PathVariable("id") int id) {
		logger.info("Welcome user! Requested Emp ID is: " + id);
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);
		model.addAttribute("id", id);
		model.addAttribute("name", "Pankaj");

		return "employee";
	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public String authenticate(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		session.setAttribute("username", "TestUser");
		session.setAttribute("name", "User Test");
		session.setAttribute("locationId", "36");
		session.setAttribute("locationCode", "EPR");
		session.setAttribute("locationName", "Eden Prairie");
		session.setAttribute("userRole", "internal");
		session.setAttribute("hasCamera", "true");
		session.setAttribute("clientId", "1");
		return "redirect:clubpulse";
	}

	@RequestMapping(value = "/login")
	public String login(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("username", "testUser");
		session.setAttribute("name", "testUser");
		session.setAttribute("locationId", "36");
		session.setAttribute("locationCode", "EPR");
		session.setAttribute("locationName", "Test");
		session.setAttribute("userRole", "internal");
		session.setAttribute("hasCamera", "true");
		session.setAttribute("clientId", "1");

		return "login";
	}

	@RequestMapping(value = "/logout")
	public String logout() {
		return "logout";
	}

	@RequestMapping(value = "/denied")
	public String denied() {
		return "denied";
	}

}
