/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.member.interception;

import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import net.reunify.pulse.members.interception.InterceptQueueEntry;
import net.reunify.pulse.members.interception.MemberInterceptionDAO;
import net.reunify.pulse.test.util.MockDatabaseRecordCreator;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author kiana.baradaran
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/spring-security.xml",
		"file:src/test/webapp/WEB-INF/mvc-dispatcher-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@TransactionConfiguration(defaultRollback = true)
@Transactional(value = "pulseTxManager")
public class MemberInterceptionDAOTest {

	@Inject
	private MemberInterceptionDAO memberInterceptionDAO;

	@Inject
	MockDatabaseRecordCreator mockDatabaseRecordCreator;

	@Inject
	@Named("pulseJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@After
	public void tearDown() throws Exception {
		mockDatabaseRecordCreator.cleanUp();
	}

	@Test
	public void testEnqueueMemberIntercept() {
		int locationId = mockDatabaseRecordCreator.createTestLocationRecord(0);
		int interceptorId = mockDatabaseRecordCreator.createTestUserRecord(locationId);
		int memberId = mockDatabaseRecordCreator.createTestMemberRecord(locationId);
		InterceptQueueEntry interceptQueueEntry =
				new InterceptQueueEntry(memberId, interceptorId, "Reunify ---- intercept later test ---- Reunify",
										"5mins", locationId);
		assertTrue(memberInterceptionDAO.enqueueMemberIntercept(interceptQueueEntry) > 0);

	}

	@Test
	public void testRemoveMemberInterceptFromQueue() {
		int recordId = mockDatabaseRecordCreator.createTestQueueEntryRecord(0, 0, 0);
		assertTrue(memberInterceptionDAO.removeMemberInterceptFromQueue(recordId));
	}

	@Test
	public void testGetMemberInterceptQueue() {
		int locationId = mockDatabaseRecordCreator.createTestLocationRecord(0);
		int userId = mockDatabaseRecordCreator.createTestUserRecord(locationId);
		int memberId = mockDatabaseRecordCreator.createTestMemberRecord(locationId);
		mockDatabaseRecordCreator.createTestQueueEntryRecord(memberId, userId, locationId);
		mockDatabaseRecordCreator.createTestQueueEntryRecord(memberId, userId, locationId);
		List<InterceptQueueEntry> entries = memberInterceptionDAO.getMemberInterceptQueue(userId);
		assertTrue(entries.size() == 2);

	}

	@Test
	public void testUpdateUserStatsWithQueueInsert() {
		int locationId = mockDatabaseRecordCreator.createTestLocationRecord(0);
		int userId = mockDatabaseRecordCreator.createTestUserRecord(locationId);
		assertTrue(memberInterceptionDAO.updateUserStatsWithQueueInsert(userId, locationId));
		cleanUpUserStatsTable(userId, 0);
	}

	@Test
	public void testUpdateUserStatsWithIntercept() {
		int locationId = mockDatabaseRecordCreator.createTestLocationRecord(0);
		int userId = mockDatabaseRecordCreator.createTestUserRecord(locationId);
		assertTrue(memberInterceptionDAO.updateUserStatsWithIntercept(userId, locationId));
		cleanUpUserStatsTable(userId, 0);
	}

	@Test
	public void testUpdateUserStatsWithQueueDelete() {
		int recordId = mockDatabaseRecordCreator.createTestQueueEntryRecord(0, 0, 0);
		assertTrue(memberInterceptionDAO.updateUserStatsWithQueueDelete(recordId));
		cleanUpUserStatsTable(0, recordId);
	}

	private void cleanUpUserStatsTable(int userId, int recordId) {
		if (userId > 0) {
			String tableCleanUp = "delete from users_daily_stats where user_id = ?;";
			jdbcTemplate.update(tableCleanUp, new Object[] { userId });
		} else if (recordId > 0) {
			String tableCleanUp =
					"delete from users_daily_stats where user_id IN (select user_id from member_intercepts_queue where record_id = ?);";
			jdbcTemplate.update(tableCleanUp, new Object[] { recordId });
		}

	}
}
