/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.analytics.metrics;

import net.reunify.pulse.members.MemberMetric;

/**
 * A metric represents a data point with a value
 *
 * @author Shakti Shrivastava
 *
 * @param <T>
 */
public interface Metric<T> {

	MemberMetric getMetricName();

	T getValue();

}
