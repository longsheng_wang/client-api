/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.authentication;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * Implementation of UserModel - Client: LifeTime
 * 
 * @author kiana.baradaran
 * 
 */
public class LifeTimeUserDetails extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;
	private int id;
	private int locationId;
	private String locationCode;

	public LifeTimeUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities,
								String name, int id, int locationId, String locationCode) {
		super(username, password, authorities);
		this.name = name;
		this.id = id;
		this.locationId = locationId;
		this.locationCode = locationCode;

	}

	public void setId(int id) {
		this.id = id;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public int getLocationId() {
		return locationId;
	}

}
