/* Copyright 2014 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import net.reunify.pulse.util.json.GsonTypeAdapters;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Functional testing for member controller
 * 
 * @author Shakti Shrivastava
 * @author kiana.baradaran
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/spring-security.xml",
		"file:src/test/webapp/WEB-INF/mvc-dispatcher-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class MemberControllerTest {
	private static final Logger logger = Logger.getLogger(MemberControllerTest.class);

	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private FilterChainProxy springSecurityFilter;
	private MockMvc mockMvc;
	private MockHttpSession session;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		mockMvc = webAppContextSetup(wac).addFilters(springSecurityFilter).alwaysDo(print()).build();
		// enable authentication for all tests
		Authentication authentication = new UsernamePasswordAuthenticationToken("reunifyuser", "user@reunify278");
		SecurityContext securityContext = SecurityContextHolder.getContext();
		securityContext.setAuthentication(authentication);
		session = new MockHttpSession();
		session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, securityContext);
		session.setAttribute("locationId", "36");
	}

	@After
	public void tearDown() throws Exception {
		mockMvc = null;
	}

	@Test
	public void testFindByIdInvalidDataType() throws Exception {
		//@formatter:off
		mockMvc.perform(get("/members/a")
				.session(session))
				.andExpect(status().isBadRequest());
		//@formatter:on
	}

	@Test
	public void testInsertMemberCheckIn() throws Exception {
		//@formatter:off
				Gson gson = new Gson();	
				MemberCheckInDTO memberCheckInDTO = new MemberCheckInDTO();		
				memberCheckInDTO.setCheckInTime("2015-05-03 12:20:03");
				memberCheckInDTO.setClientMemberId("102052649");
				memberCheckInDTO.setClientLocationId("175");
		mockMvc.perform(post("/membercheckins").contentType(MediaType.APPLICATION_JSON).content(gson.toJson(memberCheckInDTO)).session(session))
						.andExpect(status().isOk());		
		//@formatter:on	

	}

	@Test
	public void testInsertMemberCheckInNoClientLocationId() throws Exception {
		//@formatter:off
				Gson gson = new Gson();	
				MemberCheckInDTO memberCheckInDTO = new MemberCheckInDTO();		
				memberCheckInDTO.setCheckInTime("2015-05-03 12:20:03");
				memberCheckInDTO.setClientMemberId("102052649");
				memberCheckInDTO.setClientLocationId("");
		mockMvc.perform(post("/membercheckins").contentType(MediaType.APPLICATION_JSON).content(gson.toJson(memberCheckInDTO)).session(session))
						.andExpect(status().isBadRequest());		
		//@formatter:on	

	}

	@Test
	public void testInsertMemberCheckInInvalidClientMemberId() throws Exception {
		//@formatter:off
				Gson gson = new Gson();	
				MemberCheckInDTO memberCheckInDTO = new MemberCheckInDTO();		
				memberCheckInDTO.setCheckInTime("2015-05-03 12:20:03");
				memberCheckInDTO.setClientMemberId("102");
				memberCheckInDTO.setClientLocationId("175");
		mockMvc.perform(post("/membercheckins").contentType(MediaType.APPLICATION_JSON).content(gson.toJson(memberCheckInDTO)).session(session))
						.andExpect(status().isBadRequest());		
		//@formatter:on	

	}

	@Test
	public void testInsertMemberCheckInInvalidClientLocationId() throws Exception {
		//@formatter:off
				Gson gson = new Gson();	
				MemberCheckInDTO memberCheckInDTO = new MemberCheckInDTO();		
				memberCheckInDTO.setCheckInTime("2015-05-03 12:20:03");
				memberCheckInDTO.setClientMemberId("102052649");
				memberCheckInDTO.setClientLocationId("1753");
		mockMvc.perform(post("/membercheckins").contentType(MediaType.APPLICATION_JSON).content(gson.toJson(memberCheckInDTO)).session(session))
						.andExpect(status().isBadRequest());		
		//@formatter:on	
	}

	@Ignore
	@Test
	public void testInsertMemberProfile() throws Exception {
		Gson gson =
				new GsonBuilder().registerTypeAdapter(Date.class, GsonTypeAdapters.dateDeserializer)
									.registerTypeAdapter(DefaultMemberProfile.class,
											GsonTypeAdapters.memberProfileAdapter)
									.registerTypeAdapter(MemberAttribute.class,
											GsonTypeAdapters.memberAttributeSerializer).create();

		MemberPersonalInfo info =
				new MemberPersonalInfo("Laurent", "Dechery",
										new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse("1956-05-22"), "M",
										"180 Norman Ridge Dr", "West Bloomington", "55437", "175");
		List<MemberAttribute<?>> attributes = new ArrayList<MemberAttribute<?>>();

		attributes.add(new MemberAttribute<String>("membershipStatus", "Active"));
		attributes.add(new MemberAttribute<String>("membershipId", "0"));
		attributes.add(new MemberAttribute<Integer>("membershipJuniorMembers", 0));
		attributes.add(new MemberAttribute<String>("membershipTypeDescription", "Diamond Couple"));
		attributes.add(new MemberAttribute<Date>("joinDate",
													new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse("2006-08-07")));
		attributes.add(new MemberAttribute<String>("memberType", "Partner"));
		attributes.add(new MemberAttribute<String>("memberStatus", "Active"));

		MemberAttributes memberAttributes = new MemberAttributes(attributes);

		DefaultMemberProfile profile = new DefaultMemberProfile("123", info, memberAttributes);
		logger.info(profile.toString());

		mockMvc.perform(
				post("/members").contentType(MediaType.APPLICATION_JSON).content(gson.toJson(profile)).session(session))
				.andExpect(status().isOk());
	}

	@Ignore
	@Test
	public void testUpdateMemberProfile() throws Exception {
		Gson gson =
				new GsonBuilder().registerTypeAdapter(Date.class, GsonTypeAdapters.dateDeserializer)
									.registerTypeAdapter(DefaultMemberProfile.class,
											GsonTypeAdapters.memberProfileAdapter)
									.registerTypeAdapter(MemberAttribute.class,
											GsonTypeAdapters.memberAttributeSerializer).create();

		MemberPersonalInfo info =
				new MemberPersonalInfo("STACIE", "GARCIA",
										new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse("1974-10-25"), "F",
										"801 PRINCE CHARLES LANE", "SCHAUMBURG", "60195", "50");
		List<MemberAttribute<?>> attributes = new ArrayList<MemberAttribute<?>>();

		attributes.add(new MemberAttribute<String>("membershipStatus", "Terminated"));
		attributes.add(new MemberAttribute<String>("membershipId", "77"));
		attributes.add(new MemberAttribute<Integer>("membershipJuniorMembers", 2));
		attributes.add(new MemberAttribute<String>("membershipTypeDescription", "Advantage Sports (Old Fitness) Couple"));
		attributes.add(new MemberAttribute<Date>("joinDate",
													new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse("2000-03-08")));
		attributes.add(new MemberAttribute<String>("memberType", "Secondary"));
		attributes.add(new MemberAttribute<String>("memberStatus", "Inactive"));

		MemberAttributes memberAttributes = new MemberAttributes(attributes);

		DefaultMemberProfile profile = new DefaultMemberProfile("123", info, memberAttributes);
		logger.info(profile.toString());

		mockMvc.perform(
				put("/members/123").contentType(MediaType.APPLICATION_JSON).content(gson.toJson(profile))
									.session(session)).andExpect(status().isOk());
	}
}