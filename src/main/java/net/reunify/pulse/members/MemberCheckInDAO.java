package net.reunify.pulse.members;

/**
 * DAO for MemberCheckIn Object
 * 
 * @author kiana.baradaran
 * 
 */
public interface MemberCheckInDAO {
	/**
	 * Insert a member check-in entry
	 * 
	 * @param memberCheckInDTO
	 */
	void insertMemberCheckIn(MemberCheckInDTO memberCheckInDTO);

}
