/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.beat.service;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Date period util to get <code>Date</code> objects of a specific time.
 * 
 * Suggestion: For valid TimeZone, use jdk valid time zone id "US/Pacific", "US/Mountain", "US/Central", "US/Eastern", "US/Hawaii" and "US/Alaska"
 * Warning: "PDT" is not a valid TimeZone ID. Check <code>TimeZone.getAvailableIDs()</code>
 * 
 * @author longsheng.wang
 *
 */
public enum DatePeriod {
	
	TODAY("Today","Today"),
	YESTERDAY("Yesterday", "Yesterday"),
	WTD("WTD", "Week to date. Beginning of current week."),
	SAMEDAYLASTWEEK("SAMEDAYLASTWEEK", "Same day of last week. 7 days ago"),
	MTD("MTD", "Month to date. Beginning of current month."),
	SAMEDATELASTMONTH("SAMEDATELASTMONTH", "Same date of last month."),
	YTD("YTD", "Year to date. Beginning of current year"),
	SAMEDATELASTYEAR("SAMEDATELASTYEAR", "Same date of last year.");
	
	private final String name;
	private final String description;
	
	private DatePeriod(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	public Date getStartDate(TimeZone targetTimeZone) {
		return getStartDate(new Date(), targetTimeZone);
	}	
	public Date getStartDate(Date date, TimeZone targetTimeZone) {
		Calendar calendar = new GregorianCalendar(targetTimeZone);
		calendar.setFirstDayOfWeek(Calendar.SUNDAY);
		calendar.setTime(date);
		//adjust date.
		switch (this) {
		case TODAY:
			break;
		case YESTERDAY:
			calendar.add(Calendar.DATE, -1);
			break;
		case WTD:
			calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY); // Calendar.SUNDAY == 1
			break;
		case MTD:
			calendar.set(Calendar.DATE, 1);
			break;
		case YTD:
			calendar.set(Calendar.MONTH, Calendar.JANUARY);
			calendar.set(Calendar.DATE, 1);
			break;
		case SAMEDAYLASTWEEK:
			calendar.add(Calendar.WEEK_OF_YEAR, -1);
			break;
		case SAMEDATELASTMONTH:
			calendar.add(Calendar.MONTH, -1);
			break;
		case SAMEDATELASTYEAR:
			calendar.add(Calendar.YEAR, -1);
			break;
		}
		
		//adjust time.
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.AM_PM, Calendar.AM);
		calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	/**
	 * Get <code>Date</code> of January 1, 1970, 00:00:00 GMT.
	 * @return
	 */
	public static Date getVeryBeginningDate(){
		return new Date(0L);
	}
	
	/**
	 * Get <code>Date</code> of now, which is UTC based.
	 * @return
	 */
	public static Date getNow(){
		return new Date();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

}
