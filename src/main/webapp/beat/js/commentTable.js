function resolveComment(commentId){
	commentId = commentId.substring(8);
	$.ajax({
        url: 'comments/' + commentId + '?status=resolved',
        type:'PUT',
        success: function(response) {
            $("#resolve-"+commentId).prop("disabled", true);
            $("#status-"+commentId).html("resolved");
        }
    });
}

$(document).ready(function() {
    $('#commentTable').DataTable({
    	"scrollY": "500px",
        "scrollCollapse": true,
        "dom": "frtiS",
        "deferRender": true
    });
} );