/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

/**
 * 
 * @author kiana.baradaran
 * 
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/spring-security.xml",
		"file:src/test/webapp/WEB-INF/mvc-dispatcher-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class UserRegistrationValidatorTest {

	@Test
	public void testRegisterationValidatorForValidUserWithUserId() {
		UserDTO user = new UserDTO();
		user.setClientUserId("121");
		Errors errors = new BeanPropertyBindingResult(user, "user");
		UserRegistrationValidator validator = new UserRegistrationValidator();
		validator.validateClientUserId(user, errors, "clientUserId");
		assertFalse(errors.hasErrors());

	}

	@Test
	public void testRegisterationValidatorWithInvalidPassword() {
		UserDTO user = new UserDTO();
		user.setPassword("testing1");
		user.setMatchingPassword("testing1");
		Errors errors = new BeanPropertyBindingResult(user, "user");
		UserRegistrationValidator validator = new UserRegistrationValidator();
		validator.validatePassword(user, errors, "password", "matchingPassword");
		assertTrue(errors.hasErrors());
		assertEquals("incorrect_password", errors.getAllErrors().get(0).getCode());
	}

	@Test
	public void testRegisterationValidatorWithPasswordTooLong() {
		UserDTO user = new UserDTO();
		user.setPassword("Testingestingestingestingestingestingesting1");
		user.setMatchingPassword("Testingestingestingestingestingestingesting1");
		Errors errors = new BeanPropertyBindingResult(user, "user");
		UserRegistrationValidator validator = new UserRegistrationValidator();
		validator.validatePassword(user, errors, "password", "matchingPassword");
		assertTrue(errors.hasErrors());
		assertEquals("incorrect_password", errors.getAllErrors().get(0).getCode());
	}

	@Test
	public void testRegisterationValidatorWithPasswordTooShort() {
		UserDTO user = new UserDTO();
		user.setPassword("Tng1");
		user.setMatchingPassword("Tng1");
		Errors errors = new BeanPropertyBindingResult(user, "user");
		UserRegistrationValidator validator = new UserRegistrationValidator();
		validator.validatePassword(user, errors, "password", "matchingPassword");
		assertTrue(errors.hasErrors());
		assertEquals("incorrect_password", errors.getAllErrors().get(0).getCode());
	}

	@Test
	public void testRegisterationValidatorPasswordsNotMatching() {
		UserDTO user = new UserDTO();
		user.setPassword("Testing1");
		user.setMatchingPassword("testing1");
		Errors errors = new BeanPropertyBindingResult(user, "user");
		UserRegistrationValidator validator = new UserRegistrationValidator();
		validator.validatePassword(user, errors, "password", "matchingPassword");
		assertTrue(errors.hasErrors());
		assertEquals("incorrect_matchingPassword", errors.getAllErrors().get(0).getCode());

	}

	@Test
	public void testRegisterationValidatorInvalidFirstName() {
		UserDTO user = new UserDTO();
		user.setFirstName("   ");
		Errors errors = new BeanPropertyBindingResult(user, "user");
		UserRegistrationValidator validator = new UserRegistrationValidator();
		validator.validateFirstName(user, errors, "firstName");
		assertTrue(errors.hasErrors());
		assertEquals("firstName_required", errors.getAllErrors().get(0).getCode());

	}

	@Test
	public void testRegisterationValidatorInvalidLastName() {
		UserDTO user = new UserDTO();
		user.setLastName("");
		Errors errors = new BeanPropertyBindingResult(user, "user");
		UserRegistrationValidator validator = new UserRegistrationValidator();
		validator.validateLastName(user, errors, "lastName");
		assertTrue(errors.hasErrors());
		assertEquals("lastName_required", errors.getAllErrors().get(0).getCode());

	}

	@Test
	public void testRegisterationValidatorInvalidEmailNoUserProvided() {
		UserDTO user = new UserDTO();
		user.setEmailAddress("@reunify.net");
		Errors errors = new BeanPropertyBindingResult(user, "user");
		UserRegistrationValidator validator = new UserRegistrationValidator();
		validator.validateEmailAddress(user, errors, "emailAddress");
		assertTrue(errors.hasErrors());
		assertEquals("incorrect_emailAddress", errors.getAllErrors().get(0).getCode());

	}

	@Test
	public void testRegisterationValidatorInvalidEmailNoDomainProvided() {
		UserDTO user = new UserDTO();
		user.setEmailAddress("test@.net");
		Errors errors = new BeanPropertyBindingResult(user, "user");
		UserRegistrationValidator validator = new UserRegistrationValidator();
		validator.validateEmailAddress(user, errors, "emailAddress");
		assertTrue(errors.hasErrors());
		assertEquals("incorrect_emailAddress", errors.getAllErrors().get(0).getCode());

	}

	@Test
	public void testRegisterationValidatorInvalidEmailNoTopLevelDomainProvided() {
		UserDTO user = new UserDTO();
		user.setEmailAddress("test@reunify.");
		Errors errors = new BeanPropertyBindingResult(user, "user");
		UserRegistrationValidator validator = new UserRegistrationValidator();
		validator.validateEmailAddress(user, errors, "emailAddress");
		assertTrue(errors.hasErrors());
		assertEquals("incorrect_emailAddress", errors.getAllErrors().get(0).getCode());

	}

	@Test
	public void testRegisterationValidatorInvalidEmailMissingAtSign() {
		UserDTO user = new UserDTO();
		user.setEmailAddress("testgmail.net");
		Errors errors = new BeanPropertyBindingResult(user, "user");
		UserRegistrationValidator validator = new UserRegistrationValidator();
		validator.validateEmailAddress(user, errors, "emailAddress");
		assertTrue(errors.hasErrors());
		assertEquals("incorrect_emailAddress", errors.getAllErrors().get(0).getCode());

	}

}
