<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<ui:errorview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Trace - Not Found">
	<jsp:attribute name="headTag">
	<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/error/not-found.css"
			rel="stylesheet">
	</jsp:attribute>
	<jsp:body>

<div class="main-div">
<h1>404 - Page Not Found.</h1>

</div>
	</jsp:body>
</ui:errorview>

