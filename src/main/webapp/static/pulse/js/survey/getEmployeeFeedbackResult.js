/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function load_employee_feedback_progress_bars() {
    var location_id = localStorage.getItem("location_id");
    $.ajax({
        data: {
            location_id: location_id
        },
        type: 'POST',
        async: false,
        url: 'jsp/survey/GetEmployeeFeedbackResult.jsp',
        dataType: 'json',
        success: function(data) {
            if (jQuery.isEmptyObject(data)) {
                $('#employee_feedback_000_week').animate({width: 0 + "%"}, 700);
                $('#employee_feedback_010_week').animate({width: 0 + "%"}, 700);
                $('#employee_feedback_001_week').animate({width: 0 + "%"}, 700);
                $('#employee_feedback_011_week').animate({width: 0 + "%"}, 700);
                $('#employee_feedback_000_month').animate({width: 0 + "%"}, 700);
                $('#employee_feedback_010_month').animate({width: 0 + "%"}, 700);
                $('#employee_feedback_001_month').animate({width: 0 + "%"}, 700);
                $('#employee_feedback_011_month').animate({width: 0 + "%"}, 700);


            } else {

                for (var key in data.intervals.weekly) {
                    var value = data.intervals.weekly[key];
                    $('#employee_feedback_' + key + '_week').animate({width: value + "%"}, {
                        duration: 500,
                        start: set_progress_bars_colors('#employee_feedback_' + key + '_week', value)
                    });


                }


                for (var key in data.intervals.monthly) {
                    var value = data.intervals.monthly[key];
                    $('#employee_feedback_' + key + '_month').animate({width: value + "%"}, {
                        duration: 500,                     
                    });


                }

            }
        }
    });
    employee_feedback_interval = setTimeout(function() {
        load_employee_feedback_progress_bars();
    }, 30000);
}