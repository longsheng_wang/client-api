/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.beat.webservice;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author longsheng.wang
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/beat/beat-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class CommentControllerTest {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.mockMvc = null;
	}

	@Test
	public void testResolveComment() throws Exception {
		mockMvc.perform(put("/comments/1?status=resolved")).andExpect(status().isOk());
		mockMvc.perform(put("/comments/1?status=resfsafd")).andExpect(status().isBadRequest());
		mockMvc.perform(put("/comments/2839?status=resolved")).andExpect(status().isNotFound());
		mockMvc.perform(get("/comments/1?status=resolved")).andExpect(status().isMethodNotAllowed());
		mockMvc.perform(put("/comments/1")).andExpect(status().isBadRequest());
		mockMvc.perform(put("/comments/1?fsaf=resfsafd")).andExpect(status().isBadRequest());
		mockMvc.perform(put("/comments/fsaf?status=resolved")).andExpect(status().isBadRequest());
	}
}
