<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<ui:loginview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Pulse - Update User">
	<jsp:attribute name="headTag">
		<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/login/registration.css"
			rel="stylesheet">
	</jsp:attribute>
	<jsp:body>
	
<div class="wrapper">

<form:form method="POST"
				action="${pageContext.servletContext.contextPath}/user/edit"
				commandName="user">
             <table>
                <tr>
                    <td><form:label path="firstName">First Name</form:label></td>
                    <td><form:input path="firstName" /></td>
                      <td class="error">  <form:errors
								path="firstName" element="div" /></td>
                    
                </tr>
                        <tr>
                    <td><form:label path="lastName">Last Name</form:label></td>
                    <td><form:input path="lastName" /></td>
                    <td class="error"><form:errors path="lastName"
								element="div" /></td>
                </tr>
                <tr>
                    <td><form:label path="emailAddress">Email Address</form:label></td>
                    <td><form:input path="emailAddress" /></td>
                        <td class="error"><form:errors
								path="emailAddress" element="div" /></td>
                    
                </tr>       
                <tr>
                    <td><form:label path="clientUserId">User ID</form:label></td>
                    <td><form:input path="clientUserId" /></td>
                        <td class="error"><form:errors
								path="clientUserId" element="div" /></td>
                </tr>
                 <tr>
                    <td><form:label path="userRole">User Role</form:label></td>
                    <td><form:select path="userRole">
							<form:option value="0" label="--- Select ---" />
   							<form:options items="${rolesList}" />
   						</form:select></td>
                   <td class="error"><form:errors path="userRole"
								element="div" /></td>
                </tr>
                   <tr>
                    <td><form:label path="locationId">Location ID</form:label></td>
                    <td><form:select path="locationId">
							<form:option value="0" label="--- Select ---" />
   							<form:options items="${locationsList}" />
   						</form:select></td>
                   <td class="error"><form:errors path="locationId"
								element="div" /></td>
                </tr>
 				<form:hidden path="userId" value="${userId}" />
 
                <tr>
                    <td><input type="submit" value="Submit" /></td>
                </tr>
            </table>
        </form:form>
  </div>
	 
	</jsp:body>
</ui:loginview>