/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.user;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This module validates email against the specified criteria using regular
 * expression
 * 
 * @author kiana.baradaran
 * 
 */
public class UserRegistrationValidator implements Validator {

	private Matcher matcher;

	private static final String EMAIL_ADDRESS_PATTERN =
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";;
	/*
	 * 6 to 20 characters with at least one digit, one upper case letter, one
	 * lower case letter
	 */
	private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})";

	private static final Pattern passwordPattern = Pattern.compile(PASSWORD_PATTERN);
	private static final Pattern emailPattern = Pattern.compile(EMAIL_ADDRESS_PATTERN);

	@Override
	public boolean supports(Class<?> target) {
		// TODO Auto-generated method stub
		return UserDTO.class.isAssignableFrom(target);
	}

	@Override
	public void validate(Object target, Errors errors) {
		UserDTO user = (UserDTO) target;

		validateFirstName(user, errors, "firstName");
		validateLastName(user, errors, "lastName");
		validateEmailAddress(user, errors, "emailAddress");
		validatePassword(user, errors, "password", "matchingPassword");
		validateLocationId(user, errors, "locationId");
		validateClientUserId(user, errors, "clientUserId");
		validateUserRole(user, errors, "userRole");

	}

	public void validateUpdateObject(Object target, Errors errors) {
		UserDTO user = (UserDTO) target;

		validateFirstName(user, errors, "firstName");
		validateLastName(user, errors, "lastName");
		validateEmailAddress(user, errors, "emailAddress");
		validateLocationId(user, errors, "locationId");
		validateClientUserId(user, errors, "clientUserId");
		validateUserRole(user, errors, "userRole");

	}

	private void validateUserRole(UserDTO user, Errors errors, String userRoleFieldPath) {
		if (user.getRole().getRoleId() == 0) {
			errors.rejectValue(userRoleFieldPath, "userRole_required", "Please select a role.");
		}

	}

	public void validateClientUserId(UserDTO user, Errors errors, String clientUserIdFieldPath) {
		if (user.getClientUserId() != null) {
			if (user.getClientUserId().length() > 36) {
				errors.rejectValue(clientUserIdFieldPath, "incorrect_clientUserId",
						"User id cannot be longer than 36 characters.");
			}
		}
	}

	public void validatePassword(UserDTO user, Errors errors, String passwordFieldPath, String matchingPasswordFieldPath) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, passwordFieldPath, "password_required",
				"Password is required.");

		if (user.getPassword() != null) {
			matcher = passwordPattern.matcher(user.getPassword());
			if (!matcher.matches()) {
				errors.rejectValue(passwordFieldPath, "incorrect_password",
						"Password should contain 6 to 20 characters with at least one digit, one upper case letter, one lower case letter");
			}
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, matchingPasswordFieldPath, "matchingPassword_required",
					"Please confirm the password.");
			if (user.getMatchingPassword() != null) {
				if (!user.getPassword().equals(user.getMatchingPassword())) {
					errors.rejectValue(matchingPasswordFieldPath, "incorrect_matchingPassword",
							"Password does not match.");
				}
			}
		}
	}

	public void validateEmailAddress(UserDTO user, Errors errors, String emailAddressFieldPath) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, emailAddressFieldPath, "emailAddress_required",
				"Email Address is required.");
		if (user.getEmailAddress() != null) {
			matcher = emailPattern.matcher(user.getEmailAddress());
			if (!matcher.matches()) {
				errors.rejectValue(emailAddressFieldPath, "incorrect_emailAddress",
						"Please enter a valid email address no longer than 100 characters. i.e johnsmith@example.com ");
			}
		}
	}

	public void validateFirstName(UserDTO user, Errors errors, String firstNameFieldPath) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, firstNameFieldPath, "firstName_required",
				"First Name is required.");
		if (user.getFirstName() != null) {
			if (user.getFirstName().length() > 50) {
				errors.rejectValue(firstNameFieldPath, "incorrect_firstName",
						"First name should not be longer than 50 characters.");
			}
		}
	}

	public void validateLastName(UserDTO user, Errors errors, String lastNameFieldPath) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, lastNameFieldPath, "lastName_required",
				"Last Name is required.");
		if (user.getLastName() != null) {
			if (user.getLastName().length() > 50) {
				errors.rejectValue(lastNameFieldPath, "incorrect_lastName",
						"Last name should not be longer than 50 characters.");
			}
		}
	}

	public void validateLocationId(UserDTO user, Errors errors, String locationIdFieldPath) {
		if (user.getLocationId() == 0) {
			errors.rejectValue(locationIdFieldPath, "locationId_required", "Please select a location.");
		}
	}

}
