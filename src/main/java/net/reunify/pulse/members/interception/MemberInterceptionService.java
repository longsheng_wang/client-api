/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members.interception;

import java.util.List;

/**
 * Provides service to MemberInterceptionController
 * 
 * @author kiana.baradaran
 * 
 */
public interface MemberInterceptionService {
	/**
	 * Get the detailed object for this id
	 * 
	 * @param interceptId
	 * @return
	 */
	MemberIntercept getInterceptDetails(int interceptId);

	/**
	 * Insert the intercept
	 * 
	 * @param intercept
	 * @return 
	 */
	int insertMemberIntercept(MemberIntercept intercept);

	/**
	 * 
	 * Searching the intercept records for the given criteria
	 * 
	 * @param searchCriteria
	 * @return
	 */
	List<MemberIntercept> searchMemberIntercepts(MemberInterceptionSearchCriteria searchCriteria);

	/**
	 * Insert a member in the intercept queue to be intercepted later
	 * 
	 * @param interceptQueueEntry
	 * @return
	 */
	int enqueueMemberIntercept(InterceptQueueEntry interceptQueueEntry);

	/**
	 * Request for removing an intercept reminder from the queue
	 * 
	 * @param recordId
	 * @return
	 */
	boolean removeMemberInterceptFromQueue(int recordId);

	/**
	 * Request for retrieving the queue for a user
	 * 
	 * @param userId
	 * @return List<InterceptQueueEntry>
	 */
	List<InterceptQueueEntry> getMemberInterceptQueue(int userId);

	/**
	 * Request for ending an intercept
	 * 
	 * @param intercept
	 * @param clientId
	 */
	void endMemberIntercept(MemberIntercept intercept, int clientId);

}
