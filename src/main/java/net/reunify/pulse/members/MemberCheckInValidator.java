package net.reunify.pulse.members;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@Scope("prototype")
public class MemberCheckInValidator implements Validator {

	@Inject
	private MemberProfileDAO memberProfileDAO;


	@Override
	public boolean supports(Class<?> clazz) {
		return MemberCheckInDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		MemberCheckInDTO memberCheckInDTO = (MemberCheckInDTO) target;
		validateCheckInTime(memberCheckInDTO, errors);
		validateClientLocationId(memberCheckInDTO, errors);
		validateClientMemberId(memberCheckInDTO, errors);
	}

	private void validateCheckInTime(MemberCheckInDTO memberCheckInDTO, Errors errors) {
		//TODO Implement when a decision on check-in time type is finalized
	}

	private void validateClientMemberId(MemberCheckInDTO memberCheckInDTO, Errors errors) {

		boolean valid =
				memberProfileDAO.validateClientMemberId(memberCheckInDTO.getClientMemberId(),
						memberCheckInDTO.getClientId());
		if (!valid)
			errors.reject("clientMemberId.incorrect");

	}

	private void validateClientLocationId(MemberCheckInDTO memberCheckInDTO, Errors errors) {
		boolean valid =
				memberProfileDAO.validateClientLocationId(memberCheckInDTO.getClientLocationId(),
						memberCheckInDTO.getClientId());
		if (!valid)
			errors.reject("clientLocationId.incorrect");

	}
}
