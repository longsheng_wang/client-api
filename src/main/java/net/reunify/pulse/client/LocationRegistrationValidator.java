/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.client;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * 
 * @author kiana.baradaran
 * 
 */
public class LocationRegistrationValidator implements Validator {

	@Override
	public boolean supports(Class<?> target) {
		return LocationDTO.class.isAssignableFrom(target);

	}

	@Override
	public void validate(Object target, Errors errors) {
		LocationDTO location = (LocationDTO) target;

		validateName(location, errors, "name");
		validateTimeZone(location, errors, "timeZone");
		validateClientLocationId(location, errors, "clientLocationId");
		validatePerformanceGoal(location, errors, "perfomanceGoal");
		validateActiveUsers(location, errors, "activeUsers");

	}

	void validateActiveUsers(LocationDTO location, Errors errors, String activeUsersPath) {
		if (location.getActiveUsers() < 0) {
			errors.rejectValue(activeUsersPath, "incorrect_activeUsers",
					"Number of users should be a positive integer.");

		}
	}

	void validatePerformanceGoal(LocationDTO location, Errors errors, String PerformanceGoalPath) {
		if (location.getPerformanceGoal() < 1) {
			errors.rejectValue(PerformanceGoalPath, "incorrect_performanceGoal", "Goal should be a positive integer.");
		}
	}

	void validateClientLocationId(LocationDTO location, Errors errors, String clientLocationId) {
		if (location.getClientLocationId() != null) {
			if (location.getClientLocationId().length() > 36) {
				errors.rejectValue(clientLocationId, "incorrect_clientLocationId",
						"Location ID should not be longer than 36 characters.");
			}
		}
	}

	void validateName(LocationDTO location, Errors errors, String nameFieldPath) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, nameFieldPath, "name_required", "Name is required.");
		if (location.getName() != null) {
			if (location.getName().length() > 100) {
				errors.rejectValue(nameFieldPath, "incorrect_name", "Name should not be longer than 100 characters.");
			}
		}
	}

	void validateTimeZone(LocationDTO location, Errors errors, String timeZoneFieldPath) {
		if (("NONE").equals(location.getTimeZone())) {
			errors.rejectValue(timeZoneFieldPath, "timeZone_required", "Please select a time zone.");
		}
	}

}
