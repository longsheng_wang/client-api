<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Life Time Club Performance Report</title>
<link
	href="${pageContext.servletContext.contextPath}/static/pulse/css/error/error.css"
	rel="stylesheet">
</head>
<body>
<div class="wrapper">

	<c:set var="startDate" value="${param.startDate == null ? '' : param.startDate}" />
	<c:set var="endDate" value="${param.endDate == null ? '' : param.endDate}" />
	<c:set var="locationId" value="${param.locationId == null ? '' : param.locationId}" />
	
	<strong>Club Performance Report</strong>
	<form:form method="post" action="${pageContext.servletContext.contextPath}/reports/clubperformance" commandName="ClubPerformanceReportForm">
	<table>
		<tr>
			<td>
				<form:label path="startDate">Start Date (e.g. 2015-01-15)</form:label>
			</td>
			<td>
				<form:input type="text" name="startDate" value="${startDate}" path="startDate" />
			</td>
			<td class="error">
				<form:errors path="startDate" element="div" cssClass="error" />
			</td>
		</tr>
		
		<tr>
			<td>
				<form:label path="endDate">End Date (e.g. 2015-02-01)</form:label>
			</td>
			<td>
				<form:input type="text" name="endDate" value="${endDate}" path="endDate" />
			</td>
			<td class="error">
				<form:errors path="endDate" element="div" cssClass="error" />
			</td>
		</tr>
		
		<tr>
			<td>
				<form:label path="locationId">Club</form:label>
			</td>
			<td>
				<form:select name="locationId" id="locationId" path="locationId">
					<form:option value="0" label="- Choose Location -" />
					<c:forEach items="${locations}" var="location">
						<c:choose>
							<c:when test="${param.locationId eq location.value.getLocationId()}">
								<option value="${location.value.getLocationId()}" selected="true">${location.key.toString()}</option>
							</c:when>
							<c:otherwise>
								<option value="${location.value.getLocationId()}">${location.key.toString()}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</form:select>
			</td>
			<td class="error">
	        	<form:errors path="locationId" element="div" cssClass="error" />
	        </td>
		</tr>
		
		<tr>
			<td><input type="submit" value="Get Report"></td>
		</tr>
	</table>
	</form:form>
	Reports are not available for the current day. Please search only for past dates.

	<c:if test="${pageContext.request.method=='POST' && !empty report}">
		<table class="data" border="2">
			<c:forEach items="${report}" var="reportGroup">
				<tr>
					<th style="background-color:#99D6AD">${reportGroup.key.toString()}</th>
					<td>
							<table class="data" border="1">
								<c:forEach items="${reportGroup.value.getReportData()}" var="reportRow">
					 				<tr>
										<th>Distinct Member Visit</th>
										<td>${reportRow.getDistinctMemberVisits()}</td>
									</tr>
									<tr>
										<th>Total Visits</th>
										<td>${reportRow.getTotalVisits()}</td>
									</tr>
									<tr>
										<th>Distinct Member Intercepts</th>
										<td>${reportRow.getDistinctMemberIntercepts()}</td>
									</tr>
									<tr>
										<th>Total Intercepts</th>
										<td>${reportRow.getTotalIntercepts()}</td>
									</tr>
									<tr>
										<th>Termination</th>
										<td>${reportRow.getTermination()}</td>
									</tr>
									<tr>
										<th>Intercept on Termination</th>
										<td>${reportRow.getInterceptOnTermination()}</td>
									</tr>
									<tr>	
										<th>Termination After Intercept Within 3 Months</th>
										<td>${reportRow.getTerminationAfterInterceptThreeMonths()}</td>
									</tr>
									<tr>
										<th>Intercept & Termination in Current Month</th>
										<td>${reportRow.getInterceptAndTerminationCurrentMonth()}</td>
									</tr>
									<tr>
										<th>Termination Without Visit</th>
										<td>${reportRow.getTerminationWithoutVisit()}</td>
									</tr>
								</c:forEach>
							</table>
						</td>
				</tr>
			</c:forEach>
						
		</table>
	</c:if>
	</div>
</body>
</html>