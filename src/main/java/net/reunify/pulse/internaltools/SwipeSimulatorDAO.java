/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.internaltools;
import java.util.List;

/**
 * Interface for accessing data about simulators
 * @author Vivek Hungund
 *
 */
public interface SwipeSimulatorDAO {

	/**
	 * Return running swipe simulators
	 * @return
	 */
	List<SwipeSimulator> getRunningSwipeSimulators();

}
