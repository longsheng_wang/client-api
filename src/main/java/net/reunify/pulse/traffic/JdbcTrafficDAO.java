/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.traffic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import net.reunify.pulse.base.InvalidInputFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * JDBC implementation of TrafficDAO.
 * 
 * @author kiana.baradaran
 * 
 */
@Component
public class JdbcTrafficDAO implements TrafficDAO {

	private static final Logger logger = LoggerFactory.getLogger(JdbcTrafficDAO.class);

	@Inject
	@Named("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<TrafficZone> getLocationTraffic(String locationId) {
		List<TrafficZone> zones = new ArrayList<TrafficZone>();

		String selectZones =
				"select coalesce(t1.zone_id,t2.zone_id)::int zone_id,zone_name::text,coalesce(floor::int,0) floor,coalesce(zone_count::int,0) zone_count, coalesce(today_total::int,0) today_total, coalesce(count::int,0) count from (select distinct zone_id, (sum(count_in) over(w)::float) as zone_count, (sum(count_in) over()) as today_total from  lighthaus_count_zone a left join location_to_timezone b on a.location_id = b.location_id where a.location_id = ?::int and zone_id != 199 and zone_id != 106 and date_trunc('day',from_timestamp) = date_trunc('day',statement_timestamp() at time zone timezone) window w as (partition by zone_id)) as t1 full outer  join (select counts.zone_id,counts.zone_name,counts.count, counts.floor from (select zone_id,zone_name,count,floor from lighthaus_count_zone where location_id= ?::int and zone_id != 199 and zone_id != 106 order by from_timestamp desc limit 13) as counts group by counts.zone_id,counts.zone_name,counts.floor,counts.count order by counts.floor) as t2 on t1.zone_id = t2.zone_id";
		try {
			List<Map<String, Object>> rows =
					this.jdbcTemplate.queryForList(selectZones, new Object[] { locationId, locationId });

			for (Map<String, Object> row : rows) {

				if ((Integer) row.get("today_total") != 0) {
					TrafficZone zone =
							new TrafficZone(
											(Integer) row.get("zone_id"),
											(String) row.get("zone_name"),
											(Integer) row.get("count"),
											(Integer) row.get("floor"),
											(Integer) ((Integer) row.get("zone_count") / (Integer) row.get("today_total")));
					zones.add(zone);
				} else {
					TrafficZone zone =
							new TrafficZone((Integer) row.get("zone_id"), (String) row.get("zone_name"),
											(Integer) row.get("count"), (Integer) row.get("floor"), 0);
					zones.add(zone);
				}

			}
		} catch (DataIntegrityViolationException e) {
			throw new InvalidInputFormat("No location with id " + locationId
					+ " was found. Please check the input data and try again. StackTrace: " + e.getMessage());
		}

		return zones;
	}

}
