/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.client;

/**
 * 
 * @author kiana.baradaran
 * 
 */
public class LocationDTO {

	private String name;
	private int locationId;
	private String clientLocationId;
	private String timeZone;
	private int clientId;
	private int activeUsers;
	private int performanceGoal;

	public LocationDTO() {
		// TODO Auto-generated constructor stub
	}

	public LocationDTO(String name, int locationId, String timeZone, int activeUsers, int performanceGoal, int clientId) {
		this.name = name;
		this.locationId = locationId;
		this.timeZone = timeZone;
		this.clientId = clientId;
		this.activeUsers = activeUsers;
		this.performanceGoal = performanceGoal;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public String getClientLocationId() {
		return clientLocationId;
	}

	public void setClientLocationId(String clientLocationId) {
		this.clientLocationId = clientLocationId;
	}

	public int getPerformanceGoal() {
		return performanceGoal;
	}

	public void setPerformanceGoal(int performanceGoal) {
		this.performanceGoal = performanceGoal;
	}

	public int getActiveUsers() {
		return activeUsers;
	}

	public void setActiveUsers(int activeUsers) {
		this.activeUsers = activeUsers;
	}
}
