/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.base;

import javax.servlet.http.HttpServletRequest;

import net.reunify.pulse.user.UserAccountAlreadyExistsException;

import org.apache.log4j.Logger;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception handling for controllers
 * 
 * @author Shakti Shrivastava
 * @author kiana.baradaran
 * 
 */
public class BaseController {

	private static final Logger logger = Logger.getLogger(BaseController.class);

	/**
	 * Exception handler for unsupported search criteria provided by client
	 * 
	 * @param exception
	 * @return
	 */
	@ExceptionHandler({ InvalidSearchCriteriaException.class, HttpMessageNotReadableException.class })
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody
	APIErrorResource handleSearchException(Exception exception) {

		return new APIErrorResource(exception);
	}

	/**
	 * Exception handler for a request for creation of an account for a user
	 * that already exists
	 * 
	 * @param exception
	 * @return
	 */
	@ExceptionHandler({ UserAccountAlreadyExistsException.class })
	@ResponseStatus(value = HttpStatus.CONFLICT)
	public @ResponseBody
	APIErrorResource handleSearchException(UserAccountAlreadyExistsException exception) {

		return new APIErrorResource(exception);
	}

	/**
	 * Exception handler for incorrect request param types provided by client
	 * 
	 * @param exception
	 * @return
	 */
	@ExceptionHandler({ IllegalArgumentException.class, TypeMismatchException.class, InvalidInputFormat.class })
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody
	APIErrorResource handleException(Exception exception) {
		return new APIErrorResource(exception);
	}

	/**
	 * Exception handler for unsupported param provided by client
	 * 
	 * @param exception
	 * @return
	 */

	@ExceptionHandler({ RecordNotFoundException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public @ResponseBody
	APIErrorResource handleInputException(Exception exception) {
		logger.error(null, exception);
		return new APIErrorResource(exception);
	}

	/**
	 * Catch all error handler to ensure that unhandled errors are reported back
	 * in a consistent format
	 * 
	 * @param exception
	 * @return
	 */
	@ExceptionHandler(Throwable.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody
	APIErrorResource handleException(Throwable exception) {
		logger.error(null, exception);
		return new APIErrorResource(exception);
	}

	protected int getClientId(HttpServletRequest request) {
		return 1;
	}

}
