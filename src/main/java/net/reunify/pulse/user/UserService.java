/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.user;

import java.util.List;

/**
 * Handles User Controllers request
 * 
 * @author kiana.baradaran
 * 
 */
public interface UserService {
	/**
	 * Creates a new user account using the given user information
	 * 
	 * @param userDTO
	 * @return
	 */
	public int createUserAccount(UserDTO userDTO);

	/**
	 * Removes(deactivates) the account matching the given information
	 * 
	 * @param userId
	 * @param clientId
	 * @return
	 */
	public boolean removeUserAccount(int userId, int clientId);

	/**
	 * Edits an already existing account
	 * 
	 * @param userDTO
	 * @param clientId
	 * @return
	 */
	public boolean editUserAccount(UserDTO userDTO, int clientId);

	/**
	 * Gets all the user account for the specified client
	 * 
	 * @param clientId
	 * @return
	 */
	public List<UserDTO> getUserAccounts(int clientId);

	/**
	 * Changes the password for an already existing account
	 * 
	 * @param userDTO
	 * @param clientId
	 * @return
	 */
	public boolean changePassword(UserDTO userDTO, int clientId);

	/**
	 * Get a list of valid roles
	 * 
	 * @return
	 */
	public List<UserRole> getUserRoles();

}
