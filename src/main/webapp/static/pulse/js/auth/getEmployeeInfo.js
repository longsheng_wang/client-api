/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//var token_validation_interval = 1500000;

//Required for Corporate Login distiction 
//var corporateRoles = ["GENERAL MANAGER"];

function getEmployeeInfo() {
    var session_id = localStorage.getItem('session_id');

    var promise = $.ajax({
        data: {
            session_id: session_id
        },
        type: 'POST',
        async: false,
        url: 'jsp/auth/GetEmployeeInfo.jsp',
        dataType: 'json',
        beforeSend: function() {
            // This callback function will trigger before data is sent
            $.mobile.showPageLoadingMsg(true); // This will show ajax spinner
        },
        success: function(data) {
            if (jQuery.isEmptyObject(data)) {

            } else {

                if (data.data !== null) {
                    
                    sessionStorage.setItem('name', data.data.fullName);
                    sessionStorage.setItem('club_prefix', data.data.clubPrefix);
                    sessionStorage.setItem('position', data.data.positions[0]);
                    localStorage.setItem('club_prefix', data.data.clubPrefix);
                    localStorage.setItem('name', data.data.fullName);
                    localStorage.setItem('employee_id', data.data.employeeNumber);
                    localStorage.setItem('position', data.data.positions[0]);
                    localStorage.setItem('lastValidation',new Date().getTime());
                    console.log("Token validated!")

// Required for Corporate Login distiction 
//                    if ($.inArray(data.data.positions[0].toUpperCase(), corporateRoles)) {
//                        isCorporateRole = true;
//                    } else {
//                        isCorporateRole = false;
//
//                    }

                } else if (data.exception !== null) {
                    alert("No response from LTF server! You will be logged out of the system. Please log in again.");
                    logout();
                }
            }
        }
    });

    promise.done(getClubInfo());

    token_validation_timeout = setTimeout(function() {
        getEmployeeInfo();
    }, 600000);

}



function getClubInfo() {

    var club_prefix = localStorage.getItem('club_prefix');

// Required for Corporate Login distiction  
//        var club_prefix;
//    if (isCorporateRole === 'false' || localStorage.getItem('corporateLocation') === "" || localStorage.getItem('corporateLocation') === null || jQuery.isEmptyObject(localStorage.getItem('corporateLocation'))) {
//        club_prefix = localStorage.getItem('club_prefix');
//    } else {
//        club_prefix = localStorage.getItem('corporateLocation');
//    }

    $.ajax({
        data: {
            domain_name_prefix: club_prefix
        },
        type: 'POST',
        async: false,
        url: 'jsp/auth/GetClubInfo.jsp',
        dataType: 'json',
        complete: function() {
            // This callback function will trigger on data sent/received complete
            $.mobile.hidePageLoadingMsg(); // This will hide ajax spinner
        },
        success: function(data) {
            if (jQuery.isEmptyObject(data)) {

            } else {

                if (data !== null) {
                    sessionStorage.setItem('location_id', data.location_id);
                    localStorage.setItem('location_id', data.location_id);
                    sessionStorage.setItem('club_name', data.club_name);
                    localStorage.setItem('club_name', data.club_name);
                    sessionStorage.setItem('club_code', data.club_code);
                    localStorage.setItem('club_code', data.club_code);
                    $(".lifetime-loggedin-member-name").html(" " + localStorage.getItem("name") + " ");
                    $(".header_club_name").html(localStorage.getItem("club_code") + " - ");
                }
            }
        }
    });
}
