/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var member_pulse_interval;

$(document).ready(function() {
	
	if (getCookie("latestSwipe") === "") {
		setCookie("latestSwipe", "0");
	}
	
	stop_page_specific_timeouts();
	initEventHandlers();
	getDisplaySettings();
	make_pulse_table();
});

var initEventHandlers = function() {


	
	
	$(document).on(
			'click tap',
			'.memberInfoLink',
			function(e) {
				window.location.href = ctx
						+ '/members/'
						+ $(this).attr('data-id');
				return false;
			});

	$(document).on('click tap', '#tap_to_see_new_comers', function(e) {
		$(this).css("display", "none");
		clearTimeout(member_pulse_interval);
		make_pulse_table();
		return false;
	});

	$(document).on("click tap", "#memberpulse-risk-choice > button.btn",
			function() {
				if ($(this).hasClass('active')) {
					$(this).removeClass('active');
				} else {
					$(this).addClass('active');
					$(this).siblings('.active').removeClass('active');
				}
				clearTimeout(member_pulse_interval);
				make_pulse_table();
				return false;
			});

	$(document).on("click tap", "#memberpulse-new-choice > button.btn",
			function() {
				if ($(this).hasClass('active')) {
					$(this).removeClass('active');
				} else {
					$(this).addClass('active');
					$(this).siblings('.active').removeClass('active');
				}
				clearTimeout(member_pulse_interval);
				make_pulse_table();
				return false;
			});

	$(document).on("click tap", "#memberpulse-value-choice > button.btn",
			function() {
				if ($(this).hasClass('active')) {
					$(this).removeClass('active');
				} else {
					$(this).addClass('active');
					$(this).siblings('.active').removeClass('active');
				}
				clearTimeout(member_pulse_interval);
				make_pulse_table();
				return false;
			});
};

var getDisplaySettings = function() {
	var filter,  
		member_table_aasorting, 
		member_table_displaystart = 0, 
		member_table_displaylength = 10;
	filter = getURLParameter("filter");
	member_table_aasorting = [ [ 7, "asc", 0 ] ];
	if (filter !== null) {
		member_table_aasorting = [ [ 7, "asc", 0 ] ];

		if (filter === "risk") {
			$('#memberpulse-value-choice').find('.active')
					.removeClass('active');
			$('#memberpulse-risk-choice').find('.active').removeClass('active');
			$('#memberpulse_risk-choice-2').addClass('active');
			$('#memberpulse-new-choice').find('.active').removeClass('active');
		} else if (filter === "new") {
			$('#memberpulse-new-choice').find('.active').removeClass('active');
			$('#memberpulse-new-choice-2').addClass('active');
			$('#memberpulse-value-choice').find('.active')
					.removeClass('active');
			$('#memberpulse-risk-choice').find('.active').removeClass('active');

		} else if (filter === "notification") {
			member_table_aasorting = [ [ 2, "asc", 0 ] ];
		} else if (filter === "birthday") {
			member_table_aasorting = [ [ 8, "asc", 0 ] ];
		}
	}
	
	return {"member_table_aasorting": member_table_aasorting, 
		};

};


function make_pulse_table() {

	var displaySettings = getDisplaySettings();

	$('#memberpulsepage_member_table').DataTable(
					{
						"serverSide" : true,
						"lengthChange" : true,
						"dom" : "frtiS",
						"info" : true,
						"paginate" : true,
						"processing" : true,
						"deferRender" : true,
						"sort" : true,
						"destroy" : true,
						"scrollY" : "475px",
						"searching" : false,
						"scrollCollapse" : true,
						"order": displaySettings.member_table_aasorting,
						"scroller" : {
							"boundryScale" : "1",
							"displayBuffer" : "3",

						},
						"loadingIndicator" : false,
						"columns" : [ {
							"class" : "center","width":"7%"
						}, {
							"class" : "center","width":"7%"
						}, {
							"class" : "center","width":"7%"
						}, {
							"class" : "left","width":"22%"
						}, {
							"class" : "center","width":"12%"
						}, {
							"class" : "center","width":"7%"
						}, {
							"class" : "center","width":"8%"
						}, {
							"class" : "center","width":"10%"
						}, {
							"class" : "center","width":"7%"
						}, {
							"class" : "center","width":"7%"
						}, {
							"class" : "center",
							"sortable" : false,"width":"6%"
						} ],
						"language" : {
							"zeroRecords" : "No matching results found in adult active members.",
							"processing" : "<img style='background:transparent' src='"
									+ ctx + "/static/pulse/img/loader/716.GIF'>"
						},
						"rowCallback" : function(r, d) {
							dataTableRowCallBack(r,d);
						},
						"ajax": getSwipes
					});

}

var getFilters = function () {
	var reset = true;
	var risk = 0;
	var value = 0;
	var newmember = 0;

	if ($('#memberpulse-risk-choice').find('.active').length === 0
			&& $('#memberpulse-value-choice').find('.active').length === 0
			&& $('#memberpulse-new-choice').find('.active').length === 0) {
		reset = true;
		risk = 0;
		value = 0;
		newmember = 0;
	} else {
		reset = false;

		if ($('#memberpulse-risk-choice').find('.active').length !== 0) {
			risk = parseInt(($('#memberpulse-risk-choice').find('.active'))
					.val());
		}
		if ($('#memberpulse-value-choice').find('.active').length !== 0) {
			value = parseInt(($('#memberpulse-value-choice').find('.active'))
					.val());
		}
		if ($('#memberpulse-new-choice').find('.active').length !== 0) {
			newmember = parseInt(($('#memberpulse-new-choice').find('.active'))
					.val());
		}
	}
	
	return {"reset": reset, 
			"risk": risk, 
			"value": value, 
			"newmember":newmember};
};

var getSwipes = function(data, callback, settings) {
	var latest_record = getCookie("latestSwipe");
	var _data;
	var displaySettings = getDisplaySettings();

	_data = $.extend(data, getFilters(), {"latest_record" : getCookie("latestSwipe")});

	loadInitialSwipes(_data, callback);
		
}

var loadInitialSwipes = function (data, callback) {
	var idx;
	$.ajax({
		url : ctx + '/membersearch',
		type : "POST",
		cache : 'false',
		dataType : 'json',
		data : data,
		success: function (data, status, jqXhr) {
			// update cookie value for latest time stamp
			for (idx = 0; idx < data.aaData.length; idx++) {
				if ( parseFloat(data.aaData[idx][12]) >  parseFloat(getCookie("latestSwipe"))) {
					setCookie("latestSwipe", data.aaData[idx][12] + "");
				}
			}
			callback(data);
		},
		complete : function() {
			clearTimeout(member_pulse_interval);
			member_pulse_interval = setTimeout(function() {
				updateTable();
			}, MEMBERPULSE_TABLE_TIMEOUT);
		}

	});
	
}

var checkForNewSwipes = function (data, callback) {
	var _data = {};
	_data.order = new Array();
	_data.order[0] = {"column":7, "dir": "asc"};
	_data = $.extend(_data, getFilters());
	_data.length = 1;
	_data.latest_record = getCookie("latestSwipe");
	
	$.ajax({
		url : ctx + '/membersearch',
		type : "POST",
		cache : 'false',
		dataType : 'json',
		data : _data,
		success: function (data, status, jqXhr) {
			if ( parseFloat(data.aaData && data.aaData[0][12]) > parseFloat(getCookie("latestSwipe")) ) {
				$('#tap_to_see_new_comers').css("display","block");
				clearTimeout(member_pulse_interval);
				checkScrollforUpdate();
			} else {
				member_pulse_interval = setTimeout(function() {
					updateTable();
				}, MEMBERPULSE_TABLE_TIMEOUT);
			}
		}

	});
	
}

var checkScrollforUpdate = function(){
	var scrollAfter = $('.dataTables_scrollBody').scrollTop();
	if (scrollAfter === 0) {
		$('#tap_to_see_new_comers').css("display","none");
		updateTable();
	}else{
		member_pulse_interval = setTimeout(function() {
			checkScrollforUpdate();
		}, 1000);
		}
	

}

var updateTable = function() {

	var scroll = $('.dataTables_scrollBody').scrollTop();
	if (scroll === 0) {
		$('#memberpulsepage_member_table').DataTable().draw();
		clearTimeout(member_pulse_interval);
		member_pulse_interval = setTimeout(function() {
			updateTable();
		}, MEMBERPULSE_TABLE_TIMEOUT);
	} else {
		checkForNewSwipes();
		
	}

};

var dataTableRowCallBack = function(row, data) {

	var count = 0;
	var records = new Array();
	var lastSwipeTime = getCookie("latestSwipe");

	records[count] = data[12];
	count++;
	
	if (data[0] !== '_') {

		var dat = data[0].split('@*@');

		$('td:eq(3)', row).html(

				'<a style=color:#4d4d4d; data-role="none" class="memberInfoLink" data-id="'
						+ dat[0] + '">' + dat[1] + '</a>');
	} else {
		var ind = 'td:eq(0)';
		$(ind, row).html("");
	}
	var imghtml = "";
	if (data[1] !== "") {
		if (data[1] === "0010") {
			imghtml = '<img src=\"'
					+ ctx
					+ '/static/pulse/img/interception/interception_need_follow_up.png\" style=\"margin-top:5%;\"width=\"20\" height=\"20\">';
		} else if (data[1] === "0011") {
			imghtml = '<img src=\"'
					+ ctx
					+ '/static/pulse/img/interception/interception_need_attention.png\"  style=\"margin-top:5%;\" width=\"20\" height=\"20\">';
		} else if (data[1] === "0001") {
			imghtml = '<img src=\"'
					+ ctx
					+ '/static/pulse/img/interception/interception_good_to_go.png\" style=\"margin-top:5%;\" width=\"20\" height=\"20\">';

		}
	}
	$('td:eq(2)', row).html(imghtml);

	if (data[14] !== '') {
		var ind = 'td:eq(8)';
		if (data[14] === 't' || data[14] === '1') {
			$(ind, row)
					.html(
							'<img src=\"'
									+ ctx
									+ '/static/pulse/img/pulse/MEMBERPULSE_BIRTHDAY_GREEN.png\" width=\"25\" >');
		} else {
			$(ind, row).html('');
		}
	}

	$('td:eq(4)', row).html(data[2]);

	for (var i = 4; i < 7; i++) {
		if (data[i] !== null) {
			$('td:eq(' + (i + 1) + ')', row).html(data[i] + "");

		} else {
			$('td:eq(' + (i + 1) + ')', row).html("NA");
		}
	}

	if (data[5] !== null && data[5] !== "00:00:00") {
		$('td:eq(6)', row).html(data[5] + "");
	} else if (data[5] === "00:00:00") {
		$('td:eq(6)', row).html("1m");

	} else {
		$('td:eq(6)', row).html("");
	}

	if (data[5].indexOf("y") > -1 && data[5].indexOf("m") === -1) {

		$('td:eq(10)', row).html("&#x2713;");

	} else {
		$('td:eq(10)', row).html("");
	}

	if (data[7] !== '') {
		var ind = 'td:eq(0)';
		if (parseInt(data[7]) >= 70) {
			$(ind, row).html(
					"<img height='30' width='30' src='" + ctx
							+ "/static/pulse/img/pulse/at-risk.png'>");

		} else if (parseInt(data[7]) >= 30 && parseInt(data[7]) < 70) {
			$(ind, row)
					.html(
							"<img height='30' width='30' src='"
									+ ctx
									+ "/static/pulse/img/memberprofile/RD_AtRisk_YELLOW_2.0x.png'>");
		} else {
			$(ind, row).html('');
		}
	}

	if (data[8] !== null && data[8] !== '' && data[8] !== '0') {
		$('td:eq(1)', row).html(
				"<img  height='30' width='30' src='" + ctx + data[8] + "'>");
	} else {
		$('td:eq(1)', row).html("");
	}

	if (data[9] !== null && data[9] !== '') {
		if (data[9] === 't' || data[9] === '1') {
			$('td:eq(9)', row).html(
					"<img height='26' width='30' src='" + ctx
							+ "/static/pulse/img/memberprofile/RD_New_2.0x.png'>");
		} else {
			$('td:eq(9)', row).html('');
		}
	}

	if (data[13] === "t") {
		$('td', row).css("background-color", "#f5faf0")

	}

};

