/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members;

/**
 * 
 * @author Shakti Shrivastava
 * 
 */
public interface MemberProfile {

	String getId();

	MemberPersonalInfo getMemberPersonalInfo();

	MemberAttributes getMemberAttributes();

}
