alter table feedback_comment add column "resolved" boolean default false;
alter table feedback_comment add COLUMN "resolved_time" timestamp with time zone;