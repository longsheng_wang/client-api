<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<ui:baseview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Trace - Analytics">
	<jsp:attribute name="headTag">
	
	        <link rel="stylesheet" type="text/css"
			href="${pageContext.servletContext.contextPath}/static/pulse/css/jqPlot/jquery.jqplot.min.css">
			<link rel="stylesheet" type="text/css"
			href="${pageContext.servletContext.contextPath}/static/pulse/css/clubanalytics/clubanalytics.css">

		       <!-- jqPlot Scripts -->
 		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/static/pulse/js/jqPlot/jquery.jqplot.min.js"></script>
 		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/static/pulse/js/jqPlot/jqplot.barRenderer.min.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/static/pulse/js/jqPlot/jqplot.pieRenderer.min.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/static/pulse/js/jqPlot/jqplot.categoryAxisRenderer.min.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/static/pulse/js/jqPlot/jqplot.axisTickRenderer.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/static/pulse/js/jqPlot/jqplot.donutRenderer.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/static/pulse/js/jqPlot/jqplot.lineRenderer.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/static/pulse/js/jqPlot/jqplot.canvasAxisLabelRenderer.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/static/pulse/js/jqPlot/jqplot.canvasTextRenderer.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/static/pulse/js/jqPlot/jqplot.pyramidGridRenderer.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/static/pulse/js/jqPlot/jqplot.canvasOverlay.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/static/pulse/js/jqPlot/jqplot.pointLabels.min.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/static/pulse/js/jqPlot/jqplot.enhancedLegendRenderer.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/static/pulse/js/jqPlot/excanvas.js"></script>
		<!-- End jqplot scripts -->				
		<script
			src="${pageContext.servletContext.contextPath}/static/pulse/js/clubanalytics/clubanalytics.js"
			type="text/javascript"></script>
	</jsp:attribute>
	<jsp:body>
<div id="analyticspage_content" class="col-md-12 col-sm-12"
			style="height: 100%; padding: 0;">
	<div class="container-fluid">
                                 
                            <div class=" row-col-xs-12">
                            	<div class=" row-col-xs-12 2charts"
						style="margin-top: 30px">
                            		<div
							class="col-lg-6 col-md-6 col-sm-12 col-xs-12"
							style="padding-right: 5px; padding-left: 5px">
                            			<div
								class="panel panel-default sharp-edge" style="height: 350px">
	                                        <div
									class="panel-heading sharp-edge"
									style="color: #4d4d4d; text-shadow: none;">
	                                            <h3 class="panel-title">Achievement</h3>
	                                        </div>
	                                        <div
									class="panel-body sharp-edge" id="analytical_achievment_chart"
									style="height: 275px; margin-left: 10px; padding-bottom: 0">
	                                           
	                                        </div>
                                    	</div>
                                    </div>
                            		<div
							class="col-lg-6 col-md-6 col-sm-12 col-xs-12"
							style="padding-left: 5px; padding-right: 5px">
										<div class="panel panel-default sharp-edge"
								style="height: 350px">
                                        <div
									class="panel-heading sharp-edge"
									style="color: #4d4d4d; text-shadow: none;">
                                            <h3 class="panel-title">Attrition</h3>
                                        </div>
                                        <div
									class="panel-body sharp-edge" id="analytical_attrition_chart"
									style="height: 275px; margin-left: 10px; padding-bottom: 0">
                                           
                                        </div>
                                    	</div>
									</div>
                            	</div>
                            	<div
						class="row-col-lg-12 row-col-md-12 row-col-sm-12 row-col-xs-12"
						style="height: 350px">
                            		<div
							class="col-lg-3 col-md-3 col-sm-12 col-xs-12"
							style="padding-right: 5px; padding-left: 5px">
                            		
										<div class="panel panel-default sharp-edge"
								style="height: 350px;">
                                        <div
									class="panel-heading sharp-edge"
									style="color: #4d4d4d; text-shadow: none;">
                                            <h3 class="panel-title">Interception</h3>
                                        </div>
                                        <div
									class="panel-body sharp-edge">
                                        <div class=" row-xs-3 "
										style="height: 20px">
                                        <div class="col-xs-2">
                                        <img
												src="${pageContext.servletContext.contextPath}/static/pulse/img/interception/interception-flag-grey.png"
												style="margin-top: -10px; width: 30px; length: 30px">
                                        </div>
                                         <div class=" col-xs-3">
                                         <div
												class=" row-xs-12 cell-phone"
												style="margin-top: -15px; padding: 0px; margin-right: 0px; padding-left: 15px"
												id="donut-numbers">40</div>
                                          <div
												class="cell-phone row-xs-12" style="margin-top: -7px">Member</div> 
                                          <div
												class="cell-phone row-xs-12"
												style="margin-top: -6px; padding-left: 10px"
												id="donut-percentage">25%</div> 
                                         
                                         </div>
                                        <div class=" col-xs-6"
											style="margin-top: -10px; padding-right:0px; margin-left:8%">
                                        <div class="btn-group"
												role="group">
											  <button type="button" class="btn  btn-today active"
													id="today-button">Today</button>
											  <button type="button" class="btn  btn-week inactive"
													id="week-button">Week</button>
																		  
												</div>
                                        
                                        
                                        </div>
                                        </div>                                     
                            			<div class=" row-xs-9 "
										id="analytical_interception_chart" style="height: 290px">
                                         </div> 
                                        
                                        </div>
                                    	</div>
									</div>
                            		<div
							class="col-lg-9 col-md-9 col-sm-12 col-xs-12"
							style="padding: 0px; margin: 0px">	
                            			<div
								class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
								style="padding: 0px; margin: 0px"></div>
                            				<div
								class="col-lg-6 col-md-6 col-sm-12 col-xs-12"
								style="padding-right: 5px; padding-left: 5px">
												<div class="panel panel-default sharp-edge"
									style="height: 350px">
                                        		<div
										class="panel-heading sharp-edge"
										style="color: #4d4d4d; text-shadow: none;">
                                        	   		 <h3 class="panel-title">Frequency</h3>
                                        		</div>
                                        		<div
										class="panel-body sharp-edge" id="analytical_frequency_chart"
										style="height: 275px; margin-left: 10px">
                                        		   
                                        		</div>
                                    			</div>
											</div>
                            				<div
								class="col-lg-6 col-md-6 col-sm-12 col-xs-12"
								style="padding-left: 5px; padding-right: 5px">
												<div class="panel panel-default sharp-edge"
									style="height: 350px">
                                        		<div
										class="panel-heading sharp-edge"
										style="color: #4d4d4d; text-shadow: none;">
                                        	   		 <h3 class="panel-title">Avg. Spending</h3>
                                        		</div>
                                        		<div
										class="panel-body sharp-edge"
										id="analytical_avg_spending_chart"
										style="height: 275px; margin-left: 10px">
                                        		   
                                        		</div>
                                    			</div>
											</div>
											
											
										
                            			</div>
                            	</div>
                            </div>

                        </div>
                    </div>
	
	</jsp:body>
</ui:baseview>