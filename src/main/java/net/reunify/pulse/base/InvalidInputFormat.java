/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.base;

/**
 * 
 * @author kiana.baradaran
 * 
 */
public class InvalidInputFormat extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidInputFormat() {
		// TODO Auto-generated constructor stub
	}

	public InvalidInputFormat(String message) {
		super(message);
	}

}
