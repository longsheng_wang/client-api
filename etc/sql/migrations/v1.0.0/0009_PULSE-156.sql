-- intercept queue table
CREATE TABLE member_intercepts_queue (
record_id serial PRIMARY KEY
,interceptor_id INT REFERENCES users(user_id) NOT NULL
,member_id INT REFERENCES members(member_id) NOT NULL
,location_id INT REFERENCES client_locations(location_id) NOT NULL
,insert_time timestamptz NOT NULL DEFAULT now()
,reminder_time timestamptz
,comment VARCHAR(1000)
,is_deleted BOOLEAN NOT NULL DEFAULT FALSE 
);

-- required index for this table
CREATE INDEX IX_member_intercepts_queue_location_id ON member_intercepts_queue(location_id) ;	

-- Granting permissions
GRANT SELECT
	,INSERT
	,UPDATE
	,DELETE
	ON member_intercepts_queue
	TO ltfdashboard;	
	
GRANT ALL PRIVILEGES 
	ON ALL sequences IN SCHEMA PUBLIC
	TO ltfdashboard;
	
