package net.reunify.pulse.members;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import net.reunify.pulse.base.BaseController;
import net.reunify.pulse.base.InvalidInputFormat;
import net.reunify.pulse.util.json.GsonTypeAdapters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
public class MemberController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(MemberController.class);
	private static Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, GsonTypeAdapters.dateDeserializer)
												.registerTypeAdapter(MemberAttribute.class,
														GsonTypeAdapters.memberAttributeSerializer)
												.registerTypeAdapter(DefaultMemberProfile.class,
														GsonTypeAdapters.memberProfileAdapter).create();

	@Inject
	@Named("MemberService")
	private MemberService memberServiceHandler;

	@Inject
	private MemberCheckInValidator memberCheckInValidator;

	/**
	 * Request for retrieving a single member profile
	 * 
	 * @param memberId
	 * @return
	 */
	@RequestMapping(value = "/members/{memberId}", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	@ResponseBody
	public MemberProfile getMemberProfile(HttpServletRequest request, @PathVariable int memberId) {
		if (logger.isDebugEnabled()) {
			logger.info("Requested Member ID is: " + memberId);
		}
		Assert.notNull(memberId, "ID cannot be null");
		int clientId = super.getClientId(request);
		MemberProfile profile = memberServiceHandler.getMemberProfile(memberId, clientId);
		return profile;

	}

	/**
	 * Request for updating a single member profile
	 * 
	 * @param memberProfile
	 * @param memberId
	 */
	@RequestMapping(value = "/members/{memberId}", consumes = { "application/json;charset=UTF-8" }, method = RequestMethod.PUT)
	@ResponseBody
	public void updateMemberProfile(HttpServletRequest request, @RequestBody MemberProfile memberProfile,
			@PathVariable String memberId) {
		Assert.notNull(memberId, "ID cannot be null");
		Assert.notNull(memberProfile, "Profile cannot be null");
		int clientId = super.getClientId(request);
		memberServiceHandler.updateMemberProfile(memberProfile, memberId, clientId);
	}

	/**
	 * Request for inserting a single member profile
	 * 
	 * @param memberProfile
	 */
	@RequestMapping(value = "/members", consumes = { "application/json;charset=UTF-8" }, method = RequestMethod.POST)
	@ResponseBody
	public void insertMemberProfile(HttpServletRequest request, @RequestBody MemberProfile memberProfile) {
		Assert.notNull(memberProfile, "Profile cannot be null");
		int clientId = super.getClientId(request);

		memberServiceHandler.insertMemberProfile(memberProfile, clientId);
	}

	/**
	 * Request for inserting new member check-in
	 * 
	 * @param request
	 * @param memberCheckInDTO
	 */
	@RequestMapping(value = "/membercheckins", consumes = { "application/json;charset=UTF-8" }, method = RequestMethod.POST)
	@ResponseBody
	public void insertSwipe(HttpServletRequest request, @RequestBody MemberCheckInDTO memberCheckInDTO,
			BindingResult result) {
		int clientId = super.getClientId(request);
		memberCheckInDTO.setClientId(clientId);
		logger.info("The check-in entry is: " + memberCheckInDTO);

		memberCheckInValidator.validate(memberCheckInDTO, result);

		if (!result.hasErrors()) {
			memberServiceHandler.insertMemberCheckIn(memberCheckInDTO);
		} else {
			throw new InvalidInputFormat("The check-in entry " + memberCheckInDTO + " is not valid");
		}

	}

	/**
	 * Request for retrieving a single member analytics
	 * 
	 * @param memberId
	 * @return
	 */
	@RequestMapping(value = "/memberanalytics/{memberId}", produces = "application/json", method = RequestMethod.GET)
	@ResponseBody
	public MemberAnalytics getMemberAnalytics(HttpServletRequest request, @PathVariable int memberId) {
		if (logger.isDebugEnabled()) {
			logger.info("Requested Member ID is: " + memberId);
		}
		Assert.notNull(memberId, "ID cannot be null");
		int clientId = super.getClientId(request);
		MemberAnalytics analytics = memberServiceHandler.getMemberAnalytics(memberId, clientId);

		return analytics;

	}

}
