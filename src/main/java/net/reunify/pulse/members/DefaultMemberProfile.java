/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members;

/**
 * Default implementation of MemberProfile
 * 
 * @author kiana.baradaran
 * 
 */

public class DefaultMemberProfile implements MemberProfile {

	private String id;
	private MemberPersonalInfo memberPersonalInfo;
	private MemberAttributes memberAttributes;

	public DefaultMemberProfile() {

	}

	public DefaultMemberProfile(String id, MemberPersonalInfo memberPersonalInfo, MemberAttributes attributes) {
		setId(id);
		setMemberAttributes(attributes);
		setMemberPersonalInfo(memberPersonalInfo);
	}

	@Override
	public String toString() {
		return "DefaultMemberProfile [id=" + id + ", memberPersonalInfo=" + memberPersonalInfo + ", memberAttributes="
				+ memberAttributes + "]";
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public MemberPersonalInfo getMemberPersonalInfo() {
		return memberPersonalInfo;
	}

	@Override
	public MemberAttributes getMemberAttributes() {
		return memberAttributes;
	}

	public void setMemberPersonalInfo(MemberPersonalInfo memberPersonalInfo) {
		this.memberPersonalInfo = memberPersonalInfo;
	}

	public void setMemberAttributes(MemberAttributes memberAttributes) {
		this.memberAttributes = memberAttributes;
	}

	public void setId(String id) {
		this.id = id;
	}

}
