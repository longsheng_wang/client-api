/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * 
 * Daily Report
 * 
 * @author Vivek Hungund
 *
 */
@Component
public class InterceptProgressReport implements Report {
	private static final Logger logger = Logger.getLogger(InterceptProgressReport.class);

	@Inject
	@Named("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	/**
	 * reportData field
	 */
	private InterceptProgressReportData reportData;

	/**
	 * Create the report data for the daily report
	 * 
	 * @param date
	 */
	public void createReport() {
		InterceptProgressReportData reportData = new InterceptProgressReportData();

		String query = "SELECT a.location_name ,a.days ,lpc.weekly_interception_goal ,a.interceptions::INT,a.survey_goal,a.surveys"
				+ ",a.has_kiosk ,coalesce(b.intercepts::INT, - 1) AS intercepts FROM lifetime_pilot_clubs lpc ,( SELECT to_char(i, 'Mon dd yyyy') days"
				+ ",location_name ,- 1 a ,coalesce(interceptions, - 1) AS interceptions ,coalesce(round(0.01 * swipes_distinct, 0), - 1) AS survey_goal"
				+ ",coalesce(surveys, - 1) AS surveys ,coalesce(survey_pilot, 0::BIT (1))::INT has_kiosk ,i ,record_id "
				+ "FROM ( SELECT i::DATE ,location_name ,survey_pilot ,record_id ,location_id "
				+ "FROM generate_series(date_trunc('week', statement_timestamp() - '1 day'::interval), date_trunc('day', statement_timestamp() - '1 day'::interval), '1 day'::interval) i ,lifetime_pilot_clubs ) AS t1 "
				+ "LEFT JOIN lifetime_daily_statistics t2 ON t1.i = t2.DATE AND t1.location_id = t2.location_id ORDER BY t1.record_id ,i ) AS a "
				+ "LEFT JOIN ( ( SELECT to_char(i, 'Mon dd yyyy') days ,i::DATE ,location_name ,survey_pilot ,record_id ,location_id "
				+ "FROM generate_series(date_trunc('week', statement_timestamp() - '1 day'::interval), date_trunc('day', statement_timestamp() - '1 day'::interval), '1 day'::interval) i ,lifetime_pilot_clubs ) AS t1 "
				+ "LEFT JOIN ( SELECT i ,t.location_id ,count(DISTINCT member_id) intercepts "
				+ "FROM generate_series(date_trunc('week', statement_timestamp() - '1 day'::interval), date_trunc('day', statement_timestamp() - '1 day'::interval), '1 day'::interval) i ,interception_tags_table t "
				+ "LEFT JOIN location_to_timezone l ON t.location_id = l.location_id WHERE date_trunc('day', interception_date at TIME zone timezone) = i "
				+ "GROUP BY t.location_id ,i ) AS t ON t1.i = t.i AND t1.location_id = t.location_id ) AS b ON a.days = b.days AND a.location_name = b.location_name "
				+ "WHERE a.location_name = lpc.location_name ORDER BY a.record_id ,a.i";
		List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(query);

		String surveyGoalQuery = "SELECT to_char(date_trunc('week', i), 'Mon dd') AS week ,( SELECT clubname FROM ltm_clubs WHERE mmsclubid = location_id::TEXT ),"
				+ "round(sum(0.01 * swipes_distinct)::INT, 0) survey_goal FROM ( SELECT i::DATE FROM generate_series(date_trunc('week', statement_timestamp() - '1day'::interval) - '1week'::interval,"
				+ "date_trunc('week', statement_timestamp() - '1day'::interval) - '1week'::interval, '1week'::interval) AS i ) AS t1 LEFT JOIN lifetime_daily_statistics t2 ON "
				+ "t1.i = date_trunc('week', t2.DATE) GROUP BY clubname ,date_trunc('week', i) ORDER BY clubname ,date_trunc('week', i) DESC";
		List<Map<String, Object>> surveyGoalRows = this.jdbcTemplate.queryForList(surveyGoalQuery);
		
		reportData = populateReportData(rows, surveyGoalRows);
		this.setReportData(reportData);
	}

	/**
	 * @param startDate
	 * @param endDate
	 */
	public void createReport(String startDate, String endDate) {
		InterceptProgressReportData reportData;

		String query = "SELECT a.location_name ,a.days ,a.interceptions::INT ,a.surveys ,a.has_kiosk ,"
				+ "coalesce(b.intercepts::INT, - 1) AS intercepts FROM lifetime_pilot_clubs lpc ,( SELECT to_char(i, 'Mon dd yyyy') days ,"
				+ "location_name ,- 1 a ,coalesce(interceptions, - 1) AS interceptions ,coalesce(round(0.01 * swipes_distinct, 0), - 1) AS survey_goal ,"
				+ "coalesce(surveys, - 1) AS surveys ,coalesce(survey_pilot, 0::BIT (1))::INT has_kiosk ,i ,record_id "
				+ "FROM ( SELECT i::DATE ,location_name ,survey_pilot ,record_id ,location_id FROM generate_series(?::DATE, ?::DATE, '1 day'::interval) i ,lifetime_pilot_clubs ) AS t1 "
				+ "LEFT JOIN lifetime_daily_statistics t2 ON t1.i = t2.DATE AND t1.location_id = t2.location_id ORDER BY t1.record_id ,i ) AS a "
				+ "LEFT JOIN ( ( SELECT to_char(i, 'Mon dd yyyy') days ,i::DATE ,location_name ,survey_pilot ,record_id ,location_id "
				+ "FROM generate_series(?::DATE, ?::DATE, '1 day'::interval) i ,lifetime_pilot_clubs ) AS t1 "
				+ "LEFT JOIN ( SELECT i ,t.location_id ,count(DISTINCT member_id) intercepts "
				+ "FROM generate_series(?::DATE, ?::DATE, '1 day'::interval) i ,interception_tags_table t LEFT JOIN location_to_timezone l ON t.location_id = l.location_id "
				+ "WHERE date_trunc('day', interception_date at TIME zone timezone) = i GROUP BY t.location_id ,i ) AS t ON t1.i = t.i AND t1.location_id = t.location_id ) AS b "
				+ "ON a.days = b.days AND a.location_name = b.location_name WHERE a.location_name = lpc.location_name "
				+ "ORDER BY a.record_id ,a.i";
		List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(query, new Object[] { startDate, endDate,
				startDate, endDate, startDate, endDate });
		
		reportData = populateReportData(rows,null);
		this.setReportData(reportData);
	}

	/**
	 * @param rows
	 * @param surveyGoalRows
	 * @return
	 */
	private InterceptProgressReportData populateReportData(List<Map<String, Object>> rows, List<Map<String, Object>> surveyGoalRows) {
		InterceptProgressReportData iprd = new InterceptProgressReportData();
		InterceptProgressReportDataRow resultRow = new InterceptProgressReportDataRow();

		Map<Date, Integer> day_numIntercepts = null;
		Map<Date, Integer> day_numSurveys = null;
		int remainingIntercepts = 0;
		int remainingSurveys = 0;

		String currentLocation = null;
		for (Map<String, Object> row : rows) {
			try {
				if (!row.get("location_name").equals(currentLocation)) {
	
					// The first time through, we don't want to save anything
					if (currentLocation != null) {
						resultRow.setInterceptCount(day_numIntercepts);
						resultRow.setSurveyCount(day_numSurveys);
						if (surveyGoalRows != null){
							resultRow.setRemainingIntercepts(remainingIntercepts);
							resultRow.setRemainingSurveys(remainingSurveys);
						}
	
						iprd.addDataRow(resultRow);
						resultRow = new InterceptProgressReportDataRow();
					}
	
					currentLocation = (String) row.get("location_name");
					resultRow.setLocationName(currentLocation);
					
					if (surveyGoalRows != null) {
						int interceptGoal = (Integer) row.get("weekly_interception_goal");
						int surveyGoal = getSurveyGoal(surveyGoalRows, currentLocation);
						remainingIntercepts = interceptGoal;
						remainingSurveys = surveyGoal;
						
						resultRow.setWeeklyInterceptGoal(interceptGoal);
						resultRow.setWeeklySurveyGoal(surveyGoal);
					}
					
					boolean hasKiosk = ((Integer) row.get("has_kiosk") != 0);
					resultRow.setHasKiosk(hasKiosk);
	
					day_numIntercepts = new TreeMap<Date, Integer>();
					day_numSurveys = new TreeMap<Date, Integer>();
	
					SimpleDateFormat formatter = new SimpleDateFormat("MMM dd yyyy");
					try {
						Date d = formatter.parse((String) row.get("days"));
						int i = (Integer) row.get("intercepts") == -1 ? 0 : (Integer) row.get("intercepts");
						day_numIntercepts.put(d, i);
					
	
						i = (Integer) row.get("surveys") == -1 ? 0 : (Integer) row.get("surveys");
						day_numSurveys.put(d, i);
						
						if (surveyGoalRows != null) {
							remainingIntercepts -= day_numIntercepts.get(d);
							remainingSurveys -= day_numSurveys.get(d);
						}
	
					} catch (ParseException e) {
						logger.error("Date cannot be parsed", e);
					}
	
				} else {
	
					SimpleDateFormat formatter = new SimpleDateFormat("MMM dd yyyy");
					try {
						Date d = formatter.parse((String) row.get("days"));
						int i = (Integer) row.get("intercepts") == -1 ? 0 : (Integer) row.get("intercepts");
						day_numIntercepts.put(d, i);
	
						i = (Integer) row.get("surveys") == -1 ? 0 : (Integer) row.get("surveys");
						day_numSurveys.put(d, i);
						
						if (surveyGoalRows != null) {
							remainingIntercepts -= day_numIntercepts.get(d);
							remainingSurveys -= day_numSurveys.get(d);
						}
	
					} catch (ParseException e) {
						logger.error("Date cannot be parsed", e);
					}
				}
			} catch (NullPointerException e) {
				logger.error("location_name cannot be null", e);
			}
		}

		resultRow.setInterceptCount(day_numIntercepts);
		resultRow.setSurveyCount(day_numSurveys);
		if (surveyGoalRows != null) {
			resultRow.setRemainingIntercepts(remainingIntercepts);
			resultRow.setRemainingSurveys(remainingSurveys);
		}

		iprd.addDataRow(resultRow);
		
		return iprd;
	}

	/**
	 * Retrieve the survey goal for this club
	 * 
	 * @param rows
	 * @param currentLocation
	 * @return
	 */
	private int getSurveyGoal(List<Map<String, Object>> rows, String currentLocation) {
		for (Map<String, Object> row : rows) {
			String clubName = (String) row.get("clubname");

			try {

				if (clubName.equals(currentLocation)) {
					BigDecimal tmp = (BigDecimal) row.get("survey_goal");
					return tmp.intValue();
				}
			} catch (NullPointerException e) {
				logger.error("clubName cannot be null!", e);
			}
		}
		return 0;
	}

	/**
	 * @return reportData
	 **/
	public InterceptProgressReportData getReportData() {
		return reportData;
	}

	/**
	 * @param reportData
	 *            the reportData to set
	 */
	public void setReportData(InterceptProgressReportData reportData) {
		this.reportData = reportData;
	}
}
