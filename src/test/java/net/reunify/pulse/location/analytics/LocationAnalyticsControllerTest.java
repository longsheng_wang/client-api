/* Copyright 2014 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.location.analytics;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Functional testing for LocationAnalyticsController
 * 
 * @author kiana.baradaran
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/spring-security.xml",
		"file:src/test/webapp/WEB-INF/mvc-dispatcher-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class LocationAnalyticsControllerTest {
	private static final Logger logger = Logger.getLogger(LocationAnalyticsControllerTest.class);

	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private FilterChainProxy springSecurityFilter;
	private MockMvc mockMvc;
	private MockHttpSession session;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		mockMvc = webAppContextSetup(wac).addFilters(springSecurityFilter).alwaysDo(print()).build();
		// enable authentication for all tests
		Authentication authentication = new UsernamePasswordAuthenticationToken("reunifyuser", "user@reunify278");
		SecurityContext securityContext = SecurityContextHolder.getContext();
		securityContext.setAuthentication(authentication);
		session = new MockHttpSession();
		session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, securityContext);
		session.setAttribute("locationId", "36");
	}

	@After
	public void tearDown() throws Exception {
		mockMvc = null;
	}

	@Test
	public void getLocationAnalyticsByIdValidId() throws Exception {
		//@formatter:off
		MockHttpServletResponse response = mockMvc.perform(get("/location-analytics")
				.session(session))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8")).andReturn().getResponse();		
		 
		JsonParser parser = new JsonParser();
		JsonObject o = (JsonObject)parser.parse(response.getContentAsString());
		 assertEquals("\"36\"", o.get("locationId").toString());		
		 //@formatter:on
	}

	@Test
	public void getLocationAnalyticsDetailsByValidId() throws Exception {
		//@formatter:off
		MockHttpServletResponse response = mockMvc.perform(get("/location-analytics/details")
				.session(session))
			.andExpect(status().isOk())
							.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8")).andReturn().getResponse();		
		JsonParser parser = new JsonParser();
		JsonObject o = (JsonObject)parser.parse(response.getContentAsString());
		assertEquals("\"36\"", o.get("locationId").toString());
		// @formatter:on
	}
}
