/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.members.interception;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * Provides service to MemberInterceptionCotroller
 * 
 * @author kiana.baradaran
 * 
 */

@Service("InterceptionService")
@Transactional(value = "pulseTxManager")
public class MemberInterceptionServiceImpl implements MemberInterceptionService {

	@Autowired
	private MemberInterceptionDAO interceptionDAO;

	@Override
	public List<MemberIntercept> searchMemberIntercepts(MemberInterceptionSearchCriteria searchCriteria) {
		// TODO Auto-generated method stub
		List<MemberIntercept> result = null;

		if (searchCriteria.getEmployeeId() != null && searchCriteria.getTo() != null
				&& searchCriteria.getFrom() != null) {
			Timestamp fromTimestamp = Timestamp.valueOf(searchCriteria.getFrom());
			Timestamp toTimestamp = Timestamp.valueOf(searchCriteria.getTo());
			result =
					interceptionDAO.getMemberInterceptsByEmployeeId(searchCriteria.getEmployeeId(), fromTimestamp,
							toTimestamp);
		} else if (searchCriteria.getLocationId() != null && searchCriteria.getTo() != null
				&& searchCriteria.getFrom() != null) {
			Timestamp fromTimestamp = Timestamp.valueOf(searchCriteria.getFrom());
			Timestamp toTimestamp = Timestamp.valueOf(searchCriteria.getTo());
			result =
					interceptionDAO.getMemberInterceptsByLocationId(searchCriteria.getLocationId(), fromTimestamp,
							toTimestamp);
		} else if (searchCriteria.getMemberId() != null) {
			result = interceptionDAO.getLatestMemberIntercept(searchCriteria.getMemberId());

		}

		return result;
	}

	@Override
	public MemberIntercept getInterceptDetails(int interceptId) {
		return interceptionDAO.getInterceptDetails(interceptId);
	}

	@Override
	public int insertMemberIntercept(MemberIntercept intercept) {
		return interceptionDAO.insertMemberIntercept(intercept);
	}

	@Override
	public int enqueueMemberIntercept(InterceptQueueEntry interceptQueueEntry) {
		int recordId = interceptionDAO.enqueueMemberIntercept(interceptQueueEntry);
		if (recordId > 0) {
			interceptionDAO.updateUserStatsWithQueueInsert(interceptQueueEntry.getInterceptorId(),
					interceptQueueEntry.getLocationId());
		}
		return recordId;

	}

	@Override
	public boolean removeMemberInterceptFromQueue(int recordId) {
		boolean success = interceptionDAO.removeMemberInterceptFromQueue(recordId);
		if (success) {
			interceptionDAO.updateUserStatsWithQueueDelete(recordId);
		}
		return success;
	}

	@Override
	public List<InterceptQueueEntry> getMemberInterceptQueue(int userId) {
		return interceptionDAO.getMemberInterceptQueue(userId);
	}

	@Override
	public void endMemberIntercept(MemberIntercept intercept, int clientId) {
		interceptionDAO.endMemberIntercept(intercept, clientId);	
	}

}
