/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Provides service to ReportsController.
 * 
 * @author Vivek Hungund - vivek.hungund@reunify.com
 * 
 *
 */

@Service("ReportsService")
public class ReportsService {
	private static final Logger logger = Logger.getLogger(ReportsService.class);
	
	
	/**
	 * Retrieves a Club Performance Report for a given date interval
	 * 
	 * @param startDate
	 * @param endDate
	 * @param locationId
	 * @return 
	 */
	@Autowired
	private ClubPerformanceReport clubReport;
	public ClubPerformanceReport getClubPerformanceReport(String startDate, String endDate, String locationId) {
		logger.info("Getting Club Performance Report for location " + locationId + " from the date " + startDate + " to " + endDate);
		
		clubReport.createReport(startDate, endDate, locationId);
		return clubReport;
	}
	
	/**
     * Retrieves an MES Performance Report
     * 
     * @param startDate
     * @param endDate
     * @return 
     */
    @Autowired
    private MESPerformanceReport mesReport;
    public MESPerformanceReportData getMESPerformaceReport(String startDate, String endDate) {
        logger.info("Getting MES Performance Report for the dates " + startDate + " to " + endDate);

        mesReport.createReport(startDate, endDate);
        return mesReport.getReportData();
    }
	/**
	 * Retrieves a daily intercept report
	 * 
	 * @return
	 */
    @Autowired
    private InterceptProgressReport interceptProgressReport;
	public InterceptProgressReport getInterceptProgressReport() {
		logger.info("Getting Daily Report");
		
		interceptProgressReport.createReport();
		return interceptProgressReport;
	}
	
	/**
	 * Retrieves an intercept report based on a date range provided by the user
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public InterceptProgressReport getInterceptProgressReport(String startDate, String endDate) {
		logger.info("Getting Intercept Progress Report for the date range " + startDate + " to " + endDate);
		
		interceptProgressReport.createReport(startDate,endDate);
		return interceptProgressReport;
	}

	
}
