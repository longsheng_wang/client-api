/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
	stop_page_specific_timeouts();
	var status = getURLParameter("status");
	if (status !== null) {
	
		search_table_displaystart = 0;
		search_table_displaylength = 10;
		if (status === 'attention') {
			searchMembersByTag('0011');
		} else if (status === 'follow-up') {
			searchMembersByTag('0010');
		} else if (status === 'good-to-go') {
			searchMembersByTag('0001');
		} else {
			// redirect to Not-Found page
		}

	}

	if (!$("#searchpulse-location-choice > button.btn").hasClass('active')) {
		$("#searchpulse-location-choice-1").addClass('active');
	}

});



function clear_search_fields() {
	$("#search_page_input_field_1").val("");
	$("#search_page_input_field_2").val("");
	$("#search_page_input_field_3").val("");
	$("#searchpulse-location-choice-1").addClass('active');
	$("#searchpulse-location-choice-2").removeClass('active');
}

$(document).on('click', '#search_page_go_button', function() {


	if ($("#search_page_input_field_3").val() !== "") {
		searchMemberById();
	} else {

		searchMembersInClub();
	}

});

$(document).on('click tap', '.memberInfoLink', function(e) {
	document.location.href = ctx + '/members/'+$(this).attr('data-id');
	return false;
	});	

$(document).on("click", "#searchpulse-location-choice > button.btn",
		function() {
			if ($(this).hasClass('active')) {
				$(this).removeClass('active');
				$(this).siblings('.btn').addClass('active');
			} else {
				$(this).addClass('active');
				$(this).siblings('.active').removeClass('active');
			}
		});

$(document).on('click', '#search_page_clear_button', function() {
	clear_search_fields();

});


function searchMemberById() {

	var id = $("#search_page_input_field_3").val().trim();

	var json = {
			 "memberId" : id
	         	};
	
	if (id.length < 10 && id.length > 3 && !isNaN(id)) {

		$('#searchpage_member_table')
				.DataTable(
						{
							"serverSide" : true,
							"lengthChange" : true,
							"dom": "frtiS",
							"info" : true,
							"paginate" : true,
							"processing" : true,
							"deferRender" : true,
							"sort" : true,
							"destroy" : true,
							"scrollY":"475px",
							"searching":false,
						    "scrollCollapse": true,
						    "order":[[3,"asc"]],
							"scroller":{
							"boundryScale":"1",
							"displayBuffer":"3",
							},
							"loadingIndicator":false,

							"columns" : [ {
								"class" : "center", "width":"5%"
							}, {
								"class" : "center", "width":"5%"
							}, {
								"class" : "left", "width":"10%"
							}, {
								"class" : "left", "width":"15%"
							}, {
								"class" : "left", "width":"15%"
							}, {
								"class" : "center", "width":"10%"
							}, {
								"class" : "center", "width":"12%"
							}, {
								"class" : "center", "width":"5%"
							}, {
								"class" : "center", "width":"8%"
							}, {
								"class" : "center", "width":"5%"
							}, {
								"class" : "center", "width":"5%"
							}, {
								"class" : "center", "width":"5%"
							} ],
							"language" : {
								"zeroRecords" : "No matching results found in adult active members.",
								"processing" : "<img style='background:transparent' src='"
										+ ctx + "/static/pulse/img/loader/716.GIF'>"
							},
							
							"ajax" : {
								url :ctx
								+ '/membersearch',
								type:"POST",
								dataType : 'json',
								cache : 'false',
								data:JSON.stringify(json),
							 },
							 "rowCallback":function (row,data){

								if (data[0] !== '-') {
								

									
									$('td:eq(2)', row)
											.html(
													'<a style=color:#4d4d4d; data-role="none" class="memberInfoLink" data-id="' + data[2] + '">'
															+ data[2] + '</a>'
													);

									$('td:eq(3)', row)
											.html(
													'<a style=color:#4d4d4d; data-role="none" class="memberInfoLink" data-id="' + data[2] + '">'
													+ data[3] + '</a>');
									$('td:eq(4)', row)
											.html(
													'<a style=color:#4d4d4d; data-role="none" class="memberInfoLink" data-id="' + data[2] + '">'
													+ data[4] + '</a>');

									var imghtml = "";
									if (data[1] !== "") {
										if (data[1] === "0010") {
											imghtml = '<img src=\"'
													+ ctx
													+ '/static/pulse/img/interception/interception_need_follow_up.png\" style=\"margin-top:5%;\" width=\"20\" height=\"20\">';
										} else if (data[1] === "0011") {
											imghtml = '<img src=\"'
													+ ctx
													+ '/static/pulse/img/interception/interception_need_attention.png\" style=\"margin-top:5%;\" width=\"20\" height=\"20\">';
										} else if (data[1] === "0001") {
											imghtml = '<img src=\"'
													+ ctx
													+ '/static/pulse/img/interception/interception_good_to_go.png\" style=\"margin-top:5%;\" width=\"20\" height=\"20\">';

										}
									}
									$('td:eq(1)', row).html(imghtml);

									if (data[0] !== '') {
										var ind = 'td:eq(0)';
										if (data[0] === 't'
												|| data[0] === '1') {
											$(ind, row)
													.html(
															'<img src=\"'
																	+ ctx
																	+ '/static/pulse/img/pulse/MEMBERPULSE_BIRTHDAY_GREEN.png\" width=\"25\" >');
										} else {
											$(ind, row).html('');
										}
									}

									for ( var i = 5; i < 7; i++) {
										if (data[i] !== null) {
											$('td:eq(' + (i + 1) + ')', row)
													.html(data[i] + "");
										} else {
											$('td:eq(' + (i + 1) + ')', row)
													.html("");

										}
									}

									if (data[7] !== null
											&& data[7] !== "00:00:00") {
										$('td:eq(8)', row).html(data[7] + "");
									} else if (data[7] === "00:00:00") {
										$('td:eq(8)', row).html("1m");
									} else {
										$('td:eq(8)', row).html("");

									}

									if (data[8] !== '' && data[8] !== null) {
										if (data[13] !== '' && data[13] !== null) {
											$('td:eq(5)', row)
													.html(
															data[8] + ", "
																	+ data[13]);
										} else {
											$('td:eq(5)', row).html(
													data[8] + ", N/A");

										}

									}else{
										$('td:eq(5)', row)
										.html(
												"N/A , N/A"
													);
											}

									

									if (data[9] !== '') {
										var ind = 'td:eq(9)';
										if (parseInt(data[9]) >= 70) {
											$(ind, row)
													.html(
															"<img height='30' width='30' src='"
																	+ ctx
																	+ "/static/pulse/img/pulse/at-risk.png'>");

										} else if (parseInt(data[9]) >= 30
												&& parseInt(data[9]) < 70) {
											$(ind, row)
													.html(
															"<img height='30' width='30' src='"
																	+ ctx
																	+ "/static/pulse/img/memberprofile/RD_AtRisk_YELLOW_2.0x.png'>");
										} else {
											$(ind, row).html('');
										}
									}

									if (data[10] !== null && data[10] !== ''
											&& data[10] !== '0') {
										$('td:eq(10)', row).html(
												"<img  height='30' width='30' src='"
														+ ctx + "/"

														+ data[10] + "'>");
									} else {
										$('td:eq(10)', row).html("");
									}

									if (data[11] !== null && data[11] !== '') {
										if (data[11] === 't'
												|| data[11] === '1') {
											$('td:eq(11)', row)
													.html(
															"<img height='26' width='30' src='"
																	+ ctx
																	+ "/static/img/memberprofile/RD_New_2.0x.png'>");
										} else {
											$('td:eq(11)', row).html('');
										}
									}

								}

							},


						});

	} else {

		alert("Please input a valid member id!");
	}

}

function searchMembersInClub() {

	var input1 = $("#search_page_input_field_1").val().trim();
	var input2 = $("#search_page_input_field_2").val().trim();




		var members_num = 0;


		var json_2 = {
				
				 "firstname" : input1,
				 "lastname" : input2,

			};
		$('#searchpage_member_table')
				.DataTable(
						{
							"serverSide" : true,

							"lengthChange" : true,
							"dom": "frtiS",
							"info" : true,
							"paginate" : true,
							"processing" : true,
							"deferRender" : true,
							"sort" : true,
							"destroy" : true,
							"scrollY":"475px",
							"searching":false,
						    "scrollCollapse": true,
						    "order":[[3,"asc"]],
							"scroller":{
							"boundryScale":"1",
							"displayBuffer":"3",
							},
							"loadingIndicator":false,

							"columns" : [ {
								"class" : "center", "width":"5%"
							}, {
								"class" : "center", "width":"5%"
							}, {
								"class" : "left", "width":"10%"
							}, {
								"class" : "left", "width":"15%"
							}, {
								"class" : "left", "width":"15%"
							}, {
								"class" : "center", "width":"10%"
							}, {
								"class" : "center", "width":"12%"
							}, {
								"class" : "center", "width":"5%"
							}, {
								"class" : "center", "width":"8%"
							}, {
								"class" : "center", "width":"5%"
							}, {
								"class" : "center", "width":"5%"
							}, {
								"class" : "center", "width":"5%"
							} ],
							"language" : {
								"zeroRecords" : "No matching results found in adult active members.",
								"processing" : "<img style='background:transparent' src='"
										+ ctx + "/static/pulse/img/loader/716.GIF'>"
							},
							"ajax" : {
								url :ctx
								+ '/membersearch',
								type:"POST",
								cache : 'false',
								data: json_2,
							dataType : 'json'
								
							 },

							 "rowCallback":function (row,data){

									if (data[0] !== '-') {
										

										
										$('td:eq(2)', row)
												.html(
														'<a style=color:#4d4d4d; data-role="none" class="memberInfoLink" data-id="' + data[2] + '">'
																+ data[2] + '</a>'
														);

										$('td:eq(3)', row)
												.html(
														'<a style=color:#4d4d4d; data-role="none" class="memberInfoLink" data-id="' + data[2] + '">'
														+ data[3] + '</a>');
										$('td:eq(4)', row)
												.html(
														'<a style=color:#4d4d4d; data-role="none" class="memberInfoLink" data-id="' + data[2] + '">'
														+ data[4] + '</a>');

										var imghtml = "";
										if (data[1] !== "") {
											if (data[1] === "0010") {
												imghtml = '<img src=\"'
														+ ctx
														+ '/static/pulse/img/interception/interception_need_follow_up.png\" style=\"margin-top:5%;\" width=\"20\" height=\"20\">';
											} else if (data[1] === "0011") {
												imghtml = '<img src=\"'
														+ ctx
														+ '/static/pulse/img/interception/interception_need_attention.png\" style=\"margin-top:5%;\" width=\"20\" height=\"20\">';
											} else if (data[1] === "0001") {
												imghtml = '<img src=\"'
														+ ctx
														+ '/static/pulse/img/interception/interception_good_to_go.png\" style=\"margin-top:5%;\" width=\"20\" height=\"20\">';

											}
										}
										$('td:eq(1)', row).html(imghtml);

										if (data[0] !== '') {
											var ind = 'td:eq(0)';
											if (data[0] === 't'
													|| data[0] === '1') {
												$(ind, row)
														.html(
																'<img src=\"'
																		+ ctx
																		+ '/static/pulse/img/pulse/MEMBERPULSE_BIRTHDAY_GREEN.png\" width=\"25\" >');
											} else {
												$(ind, row).html('');
											}
										}

										for ( var i = 5; i < 7; i++) {
											if (data[i] !== null) {
												$('td:eq(' + (i + 1) + ')', row)
														.html(data[i] + "");
											} else {
												$('td:eq(' + (i + 1) + ')', row)
														.html("");

											}
										}

										if (data[7] !== null
												&& data[7] !== "00:00:00") {
											$('td:eq(8)', row).html(data[7] + "");
										} else if (data[7] === "00:00:00") {
											$('td:eq(8)', row).html("1m");
										} else {
											$('td:eq(8)', row).html("");

										}

										if (data[8] !== '' && data[8] !== null) {
											if (data[13] !== '' && data[13] !== null) {
												$('td:eq(5)', row)
														.html(
																data[8] + ", "
																		+ data[13]);
											} else {
												$('td:eq(5)', row).html(
														data[8] + ", N/A");

											}

										}else{
											$('td:eq(5)', row)
											.html(
													"N/A , N/A"
														);
												}

										

										if (data[9] !== '') {
											var ind = 'td:eq(9)';
											if (parseInt(data[9]) >= 70) {
												$(ind, row)
														.html(
																"<img height='30' width='30' src='"
																		+ ctx
																		+ "/static/pulse/img/pulse/at-risk.png'>");

											} else if (parseInt(data[9]) >= 30
													&& parseInt(data[9]) < 70) {
												$(ind, row)
														.html(
																"<img height='30' width='30' src='"
																		+ ctx
																		+ "/static/pulse/img/memberprofile/RD_AtRisk_YELLOW_2.0x.png'>");
											} else {
												$(ind, row).html('');
											}
										}

										if (data[10] !== null && data[10] !== ''
												&& data[10] !== '0') {
											$('td:eq(10)', row).html(
													"<img  height='30' width='30' src='"
															+ ctx + "/"

															+ data[10] + "'>");
										} else {
											$('td:eq(10)', row).html("");
										}

										if (data[11] !== null && data[11] !== '') {
											if (data[11] === 't'
													|| data[11] === '1') {
												$('td:eq(11)', row)
														.html(
																"<img height='26' width='30' src='"
																		+ ctx
																		+ "/static/pulse/img/memberprofile/RD_New_2.0x.png'>");
											} else {
												$('td:eq(11)', row).html('');
											}
										}

									}

								},

						});



}

function searchMembersByTag(tag) {
	var json_3 = {
						 "interceptionTag" : tag
				};
	$('#searchpage_member_table')
			.DataTable(
					{
						"serverSide" : true,

						"lengthChange" : true,
						"dom": "frtiS",
						"info" : true,
						"paginate" : true,
						"processing" : true,
						"deferRender" : true,
						"sort" : true,
						"destroy" : true,
						"scrollY":"475px",
						"searching":false,
					    "scrollCollapse": true,
					    "order":[[2,"asc"]],
						"scroller":{
						"boundryScale":"1",
						"displayBuffer":"3",
						},
						"loadingIndicator":false,

						"columns" : [ {
							"class" : "center", "width":"5%"
						}, {
							"class" : "center", "width":"5%"
						}, {
							"class" : "left", "width":"10%"
						}, {
							"class" : "left", "width":"15%"
						}, {
							"class" : "left", "width":"15%"
						}, {
							"class" : "center", "width":"10%"
						}, {
							"class" : "center", "width":"12%"
						}, {
							"class" : "center", "width":"5%"
						}, {
							"class" : "center", "width":"8%"
						}, {
							"class" : "center", "width":"5%"
						}, {
							"class" : "center", "width":"5%"
						}, {
							"class" : "center", "width":"5%"
						} ],
						"language" : {
							"zeroRecords" : "No matching results found in adult active members.",
							"processing" : "<img style='background:transparent' src='"
									+ ctx + "/static/pulse/img/loader/716.GIF'>"
						},
						"ajax" : {
							url :ctx
							+ '/membersearch',
							type:"POST",
							cache : 'false',
							data : json_3,
							dataType : 'json',
							
						 },

						 "rowCallback":function (row,data){

	if (data[0] !== '-') {
								

									
									$('td:eq(2)', row)
											.html(
													'<a style=color:#4d4d4d; data-role="none" class="memberInfoLink" data-id="' + data[2] + '">'
															+ data[2] + '</a>'
													);

									$('td:eq(3)', row)
											.html(
													'<a style=color:#4d4d4d; data-role="none" class="memberInfoLink" data-id="' + data[2] + '">'
													+ data[3] + '</a>');
									$('td:eq(4)', row)
											.html(
													'<a style=color:#4d4d4d; data-role="none" class="memberInfoLink" data-id="' + data[2] + '">'
													+ data[4] + '</a>');

									var imghtml = "";
									if (data[1] !== "") {
										if (data[1] === "0010") {
											imghtml = '<img src=\"'
													+ ctx
													+ '/static/pulse/img/interception/interception_need_follow_up.png\" style=\"margin-top:5%;\" width=\"20\" height=\"20\">';
										} else if (data[1] === "0011") {
											imghtml = '<img src=\"'
													+ ctx
													+ '/static/pulse/img/interception/interception_need_attention.png\" style=\"margin-top:5%;\" width=\"20\" height=\"20\">';
										} else if (data[1] === "0001") {
											imghtml = '<img src=\"'
													+ ctx
													+ '/static/pulse/img/interception/interception_good_to_go.png\" style=\"margin-top:5%;\" width=\"20\" height=\"20\">';

										}
									}
									$('td:eq(1)', row).html(imghtml);

									if (data[0] !== '') {
										var ind = 'td:eq(0)';
										if (data[0] === 't'
												|| data[0] === '1') {
											$(ind, row)
													.html(
															'<img src=\"'
																	+ ctx
																	+ '/static/pulse/img/pulse/MEMBERPULSE_BIRTHDAY_GREEN.png\" width=\"25\" >');
										} else {
											$(ind, row).html('');
										}
									}

									for ( var i = 5; i < 7; i++) {
										if (data[i] !== null) {
											$('td:eq(' + (i + 1) + ')', row)
													.html(data[i] + "");
										} else {
											$('td:eq(' + (i + 1) + ')', row)
													.html("");

										}
									}

									if (data[7] !== null
											&& data[7] !== "00:00:00") {
										$('td:eq(8)', row).html(data[7] + "");
									} else if (data[7] === "00:00:00") {
										$('td:eq(8)', row).html("1m");
									} else {
										$('td:eq(8)', row).html("");

									}

									if (data[8] !== '' && data[8] !== null) {
										if (data[13] !== '' && data[13] !== null) {
											$('td:eq(5)', row)
													.html(
															data[8] + ", "
																	+ data[13]);
										} else {
											$('td:eq(5)', row).html(
													data[8] + ", N/A");

										}

									}else{
										$('td:eq(5)', row)
										.html(
												"N/A , N/A"
													);
											}

									

									if (data[9] !== '') {
										var ind = 'td:eq(9)';
										if (parseInt(data[9]) >= 70) {
											$(ind, row)
													.html(
															"<img height='30' width='30' src='"
																	+ ctx
																	+ "/static/pulse/img/pulse/at-risk.png'>");

										} else if (parseInt(data[9]) >= 30
												&& parseInt(data[9]) < 70) {
											$(ind, row)
													.html(
															"<img height='30' width='30' src='"
																	+ ctx
																	+ "/static/pulse/img/memberprofile/RD_AtRisk_YELLOW_2.0x.png'>");
										} else {
											$(ind, row).html('');
										}
									}

									if (data[10] !== null && data[10] !== ''
											&& data[10] !== '0') {
										$('td:eq(10)', row).html(
												"<img  height='30' width='30' src='"
														+ ctx + "/"

														+ data[10] + "'>");
									} else {
										$('td:eq(10)', row).html("");
									}

									if (data[11] !== null && data[11] !== '') {
										if (data[11] === 't'
												|| data[11] === '1') {
											$('td:eq(11)', row)
													.html(
															"<img height='26' width='30' src='"
																	+ ctx
																	+ "/static/pulse/img/memberprofile/RD_New_2.0x.png'>");
										} else {
											$('td:eq(11)', row).html('');
										}
									}

								}

							},


					});

}