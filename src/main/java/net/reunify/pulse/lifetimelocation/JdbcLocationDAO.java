/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.lifetimelocation;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * DAO for Lifetime locations
 * @author Vivek Hungund
 *
 */
@Component
public class JdbcLocationDAO implements LocationDAO {
	private static final Logger logger = Logger.getLogger(JdbcLocationDAO.class);
	
	@Inject
	@Named("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;
	
	/**
	 * Get Pilot Clubs
	 * @return
	 */
	public Map<String, Location> getPilotClubLocations() {
		Map<String, Location> locations = new TreeMap<String,Location>();
		
		String query = "select location_name,location_id::int from lifetime_pilot_clubs";
		List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(query);
		
		for (Map<String,Object> row : rows) {
			Location location = new Location((Integer)row.get("location_id"),row.get("location_name").toString());
			locations.put((String)row.get("location_name"), location);
		}
		
		return locations;
		
	}
}
