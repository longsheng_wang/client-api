/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.client;

import java.util.List;

/**
 * Handles Client Controller's requests
 * 
 * @author kiana.baradaran
 * 
 */
public interface ClientService {
	/**
	 * Gets all the locations for a client
	 * 
	 * @param clientId
	 * @return
	 */
	public List<LocationDTO> getClientLocations(int clientId);

	/**
	 * Create an account for the given client
	 * 
	 * @param client
	 * @return
	 */
	public int createClientAccount(ClientDTO client);

	/**
	 * Add a location
	 * 
	 * @param location
	 * @return
	 */
	public int addLocation(LocationDTO location);

	/**
	 * Removes (deactivates) the location that matches the given information
	 * 
	 * @param locationId
	 * @param clientId
	 * @return
	 */
	public boolean removeLocation(int locationId, int clientId);

	/**
	 * update location information
	 * 
	 * @param location
	 * @return
	 */
	public boolean updateLocation(LocationDTO location);

	/**
	 * update client information
	 * 
	 * @param client
	 * @return
	 */
	public boolean updateClientAccount(ClientDTO client);

	/**
	 * Removes (deactivates) the client that matches the given information
	 * 
	 * @param parseInt
	 * @return
	 */
	public boolean removeClientAccount(int clientId);

	/**
	 * Get a list of active clients
	 * 
	 * @return
	 */
	List<ClientDTO> getActiveClients();
}
