/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.internaltools;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * Container for a SwipeSimulator
 * @author Vivek Hungund
 *
 */

public class SwipeSimulator {
	private static final Logger logger = Logger.getLogger(SwipeSimulator.class);
		
	private Date startedAt;
	private boolean isRunning;
	private int clientId;
	private String clientName;
	private int locationId;
	private String locationName;
	private boolean success;
	private int numSwipesInserted;
	
	private int numSwipesRequested;
	
	/**
	 * Default constructor
	 */
	public SwipeSimulator() {
	}

	/**
	 * Constructor with basic information
	 * @param startedAt
	 * @param clientId
	 * @param locationId
	 * @param numSwipesRequested
	 * @param numSwipesInserted
	 * @param clientName
	 * @param locationName
	 */
	public SwipeSimulator(Date startedAt, int clientId, int locationId, int numSwipesRequested, int numSwipesInserted, String clientName, String locationName) {
		this.startedAt = startedAt;
		this.clientId = clientId;
		this.locationId = locationId;
		this.numSwipesRequested = numSwipesRequested;
		this.numSwipesInserted = numSwipesInserted;
		this.clientName = clientName;
		this.locationName = locationName;
	}
	
	/**
	 * @return the startedAt
	 */
	public Date getStartedAt() {
		return startedAt;
	}
	/**
	 * @param startedAt the startedAt to set
	 */
	public void setStartedAt(Date startedAt) {
		this.startedAt = startedAt;
	}
	/**
	 * @return the isRunning
	 */
	public boolean isRunning() {
		return isRunning;
	}
	/**
	 * @param isRunning the isRunning to set
	 */
	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}
	/**
	 * @return the clientId
	 */
	public int getClientId() {
		return clientId;
	}
	/**
	 * @param clientId the clientId to set
	 */
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	/**
	 * @return the locationId
	 */
	public int getLocationId() {
		return locationId;
	}
	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}
	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}
	/**
	 * @return the numSwipesInserted
	 */
	public int getNumSwipesInserted() {
		return numSwipesInserted;
	}
	/**
	 * @param numSwipesInserted the numSwipesInserted to set
	 */
	public void setNumSwipesInserted(int numSwipesInserted) {
		this.numSwipesInserted = numSwipesInserted;
	}

	/**
	 * @return the numSwipesRequested
	 */
	public int getNumSwipesRequested() {
		return numSwipesRequested;
	}

	/**
	 * @param numSwipesRequested the numSwipesRequested to set
	 */
	public void setNumSwipesRequested(int numSwipesRequested) {
		this.numSwipesRequested = numSwipesRequested;
	}

	/**
	 * @return the clientName
	 */
	public String getClientName() {
		return clientName;
	}

	/**
	 * @param clientName the clientName to set
	 */
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	/**
	 * @return the locationName
	 */
	public String getLocationName() {
		return locationName;
	}

	/**
	 * @param locationName the locationName to set
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
}
