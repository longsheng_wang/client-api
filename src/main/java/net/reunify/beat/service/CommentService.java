/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.beat.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import net.reunify.beat.model.FeedbackComment;
import net.reunify.beat.model.FeedbackCommentIssueSum;
import net.reunify.beat.model.PersonMention;
import net.reunify.beat.model.SentimentCount;
import net.reunify.beat.model.SurveyAnswerCount;

/**
 * @author longsheng.wang
 *
 */
public interface CommentService {
	
	/**
	 * Get comments in a time window. Order by timestamp, most recent first.
	 * Time window is [from, to).
	 * 
	 * @param num Maximun number of comments shows.
	 * @param from
	 * @param to
	 * @return
	 */
	List<FeedbackComment> getComments(int num, Timestamp from, Timestamp to);
	
	/**
	 * Get comments in a time window. Order by timestamp, most recent first.
	 * Time window is [from, to).
	 * 
	 * @param maxNum Maximun number of comments shows.
	 * @param from
	 * @param to
	 * @return
	 */
	List<FeedbackComment> getComments(int maxNum, Date from, Date to);
	
	/**
	 * Get All Comments.
	 * 
	 * @param maxNum Maximun number of comments shows.
	 * @return
	 */
	List<FeedbackComment> getAllComments(int maxNum);

	/**
	 * Get all comments. Number of comments is default as DEFAULT_NUMBER_OF_COMMENTS.
	 * 
	 * @return
	 */
	List<FeedbackComment> getAllComments();
	
	/**
	 * Get latest comment.
	 * 
	 * @return get only 1 comment.
	 */
	FeedbackComment getLatestComment();
	
	/**
	 * Get feedback comment counts where feature_type = 'ISSUE'.
	 * 
	 * @param maxNum Max number of return items.
	 * @param from Starting timestamp.
	 * @return
	 */
	List<FeedbackCommentIssueSum> getCommentIssueCount(int maxNum, Timestamp from);
	
	/**
	 * Get feedback comment counts where feature_type = 'ISSUE'.
	 * 
	 * @param maxNum Max number of return items.
	 * @param from Starting timestamp.
	 * @return
	 */
	List<FeedbackCommentIssueSum> getCommentIssueCount(int maxNum, Date from);
	
	/**
	 * Get comment issue counts in a time window.
	 * 
	 * @param maxNum
	 * @param from
	 * @param to
	 * @return
	 */
	List<FeedbackCommentIssueSum> getCommentIssueCount(int maxNum, Timestamp from, Timestamp to);
	
	/**
	 * Get comment issue counts in a time window.
	 * 
	 * @param maxNum
	 * @param from
	 * @param to
	 * @return
	 */
	List<FeedbackCommentIssueSum> getCommentIssueCount(int maxNum, Date from, Date to);
	
	/**
	 * Get sentiment count.
	 * 
	 * @param from Starting time.
	 * @return
	 */
	SentimentCount getSentimentCount(Timestamp from);
	
	/**
	 * Get sentiment count.
	 * 
	 * @param from Starting time.
	 * @return
	 */
	SentimentCount getSentimentCount(Date from);
	
	/**
	 * Get sentiment counts in a time window.
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	SentimentCount getSentimentCount(Timestamp from, Timestamp to);
	
	/**
	 * Get sentiment counts in a time window.
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	SentimentCount getSentimentCount(Date from, Date to);
	
	/**
	 * Get feedback comment counts where feature_type = 'PERSON'.
	 * 
	 * @param maxNum
	 * @param from
	 * @return
	 */
	List<PersonMention> getMostMentionedPeople(int maxNum, Timestamp from);
	
	/**
	 * Get feedback comment counts where feature_type = 'PERSON'.
	 * 
	 * @param maxNum
	 * @param from
	 * @return
	 */
	List<PersonMention> getMostMentionedPeople(int maxNum, Date from);
	
	/**
	 * Get person mentioned in a time window
	 * 
	 * @param maxNum
	 * @param from
	 * @return
	 */
	List<PersonMention> getMostMentionedPeople(int maxNum, Timestamp from, Timestamp to);
	
	/**
	 * Get person mentioned in a time window
	 * 
	 * @param maxNum
	 * @param from
	 * @return
	 */
	List<PersonMention> getMostMentionedPeople(int maxNum, Date from, Date to);
	
	/**
	 * Get survey answer counts.
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	List<SurveyAnswerCount> getAllSurveyAnswerCounts(Date from, Date to);
	
	/**
	 * Get survey answer counts. group by survey_title which reflects location.
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	List<SurveyAnswerCount> getAllSurveyAnswerCountsByLocation(Date from, Date to);
	
	/**
	 * Resolve a comment.
	 * 
	 * @param id
	 * @return
	 */
	int resolveOneComment(long id);
	
	/**
	 * Check comment resolved time.
	 * @param id
	 * @return null if id doesn't exist or comment is unresolved.
	 */
	Date checkCommentResolvedTime(long id);
}
