/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
	stop_page_specific_timeouts();
	getClubPulse();
});

var talked_to = 0;

$(document).on("click", "#homepage_inclub_members_button", function() {
	window.parent.location = ctx + "/memberpulse";
});
$(document).on("click", "#homepage_inclub_atRisk_button", function() {
	window.parent.location = ctx + "/memberpulse?filter=risk";
});

$(document).on("click", "#homepage_new_members_button", function() {
	window.parent.location = ctx + "/memberpulse?filter=new";
});

$(document).on("click", "#homepage_interception_need_attention", function() {
	window.parent.location = ctx + "/search?status=attention";
});

$(document).on("click", "#homepage_interception_need_follow_up", function() {
	window.parent.location = ctx + "/search?status=follow-up";
});

$(document).on("click", "#homepage_interception_good_to_go", function() {
	window.parent.location = ctx + "/search?status=good-to-go";
});

function getClubPulse() {
	$.ajax({

		type : 'GET',
		async : true,
		url : ctx + '/location-analytics/',

		dataType : 'json',
		success : function(data) {

			if (jQuery.isEmptyObject(data)) {

			} else {
				var member_num = data.checkedInMembers;
				var risk_num = data.checkedInHighRisk;
				var newmember_num = data.checkedInNewComers;

				var totalDailySwipes = data.checkedInMembersToday;
				var totalDailyAtRiskSwipes = data.checkedInHighRiskToday;
				$('#total_members_today').text(totalDailySwipes);
				$('#total_at_risk_members_today').text(totalDailyAtRiskSwipes);

				var totalMonthlySwipes = data.checkedInMembersThisMonth;
				var totalMonthlyAtRiskSwipes = data.checkedInHighRiskThisMonth;
				var total_members_temp = totalDailySwipes + totalMonthlySwipes;
				$('#total_members_this_month').text(total_members_temp);

				var total_atrisk_members_temp = totalDailyAtRiskSwipes
						+ totalMonthlyAtRiskSwipes;
				$('#total_at_risk_members_this_month').text(
						total_atrisk_members_temp);

				$('#homepage_inclub_members').text(member_num);
				$('#homepage_inclub_atRisk').text(risk_num);

				$('#homepage_new_members_past10days').text(
						data.checkedInNewComer);
				$('#homepage_ltb_left').text("200");

				$('#talked_to_members_today').text(data.interceptedToday);
				$('#talked_to_members_this_month').text(
						data.interceptedThisMonth);
				$('#need_attention_members_today').text(
						data.interceptedAsNeedAttentionToday);
				$('#need_attention_members_this_month').text(
						data.interceptedAsNeedAttentionThisMonth);
				$('#need_follow_up_members_today').text(
						data.interceptedAsNeedFollowUpToday);
				$('#need_follow_up_members_this_month').text(
						data.interceptedAsNeedFollowUpThisMonth);
				$('#good_to_go_members_today').text(
						data.interceptedAsGoodToGoToday);
				$('#good_to_go_members_this_month').text(
						data.interceptedAsGoodToGoThisMonth);
			}
		}

	});
	homepage_pulse_interval = setTimeout(function() {
		getClubPulse();
	}, 5000);
}
