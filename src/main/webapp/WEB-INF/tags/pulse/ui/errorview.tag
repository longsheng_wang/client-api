<%@ tag language="java" pageEncoding="UTF-8"%>

<%@attribute name="contextPath" required="true"%>
<%@attribute name="pageTitle" required="true"%>
<%@attribute name="headTag" fragment="true"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style"
	content="black-translucent" />
<!-- start: Meta -->
<meta charset="utf-8">
<!-- end: Meta -->

<!-- start: Mobile Specific -->
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<meta name="format-detection" content="telephone=no">
<!-- end: Mobile Specific -->

<!-- start: Widget Icons -->
<link rel="apple-touch-icon"
	href="${contextPath}/static/pulse/img/icons/apple-touch-icon-iphone.png" />
<link rel="apple-touch-icon" sizes="72x72"
	href="${contextPath}/static/pulse/img/icons/apple-touch-icon-ipad.png" />
<link rel="apple-touch-icon" sizes="114x114"
	href="${contextPath}/static/pulse/img/icons/apple-touch-icon-iphone4.png" />
<link rel="apple-touch-icon" sizes="144x144"
	href="${contextPath}/static/pulse/img/icons/apple-touch-icon-ipad3.png" />
<!-- end: Widget Icons -->

<!-- start: CSS -->
<link type="text/css"
	href="${contextPath}/static/pulse/css/jquery/jquery.mobile-1.3.2.min.css"
	rel="stylesheet" />

<link href="${contextPath}/static/pulse/css/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${contextPath}/static/pulse/css/bootstrap/css/font-awesome.min.css"
	rel="stylesheet">

<link href="${contextPath}/static/pulse/css/error/global-error.css"
	rel="stylesheet">
<!-- end CSS -->

<!-- start: Scripts -->

<script>
	var ctx = "${contextPath}";
</script>


<script src="${contextPath}/static/pulse/js/jquery/jquery-1.11.0.min.js"
	type="text/javascript"></script>

<script src="${contextPath}/static/pulse/js/bootstrap/bootstrap.min.js"></script>

<jsp:invoke fragment="headTag" />
<!-- end: Scripts -->

<title>${pageTitle}</title>
</head>
<body>
	<div class="container-fluid col-md-12 col-sm-12">

		<div class="wrapper">
			<div id="main_content">
				<jsp:doBody />
				<br> <span
					onclick="javascript:window.parent.location ='${contextPath}/clubpulse'">Homepage</span>
			</div>
		</div>
	</div>

</body>
</html>