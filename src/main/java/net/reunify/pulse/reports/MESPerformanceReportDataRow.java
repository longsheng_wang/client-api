/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;
import org.apache.log4j.Logger;

/**
 * Container class for a row in an MES Performance Report
 * 
 * @author Vivek Hungund
 *
 */


public class MESPerformanceReportDataRow {
	private static final Logger logger = Logger.getLogger(MESPerformanceReportDataRow.class);
	
	private int employeeId;
	private int locationId;
	private String createdBy;
	private int count;
	
	public MESPerformanceReportDataRow() {
		
	}
	
	/**
	 * @param employeeId
	 * @param locationId
	 * @param createdBy
	 * @param count
	 */
	public MESPerformanceReportDataRow(int employeeId, int locationId, String createdBy, int count) {
		this.employeeId = employeeId;
		this.locationId = locationId;
		this.createdBy = createdBy;
		this.count = count;
	}
	
	
	/**
	 * @return the employeeId
	 */
	public int getEmployeeId() {
		return employeeId;
	}
	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	/**
	 * @return the locationId
	 */
	public int getLocationId() {
		return locationId;
	}
	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}
	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MESPerformanceReportDataRow [employeeID=" + employeeId + ", locationId=" + locationId + ", createdBy="
				+ createdBy + ", count=" + count + "]";
	}
}
