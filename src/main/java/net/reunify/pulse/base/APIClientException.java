/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.base;

/**
 * 
 * An API exception caused by invalid or missing client input. A client should
 * be able to recover from this error if the request is retried with valid
 * input.
 * 
 * @author Shakti Shrivastava
 * 
 */
public class APIClientException extends RuntimeException {

	/**
     *
     */
	private static final long serialVersionUID = 1L;

	/**
     *
     */
	public APIClientException() {

	}

	/**
	 * @param message
	 */
	public APIClientException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public APIClientException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public APIClientException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public APIClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause);
	}

}
