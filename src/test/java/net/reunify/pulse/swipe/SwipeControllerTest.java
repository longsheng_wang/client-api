/* Copyright 2014 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.swipe;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Functional testing for swipe controller
 * 
 * @author kiana.baradaran
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/spring-security.xml",
		"file:src/test/webapp/WEB-INF/mvc-dispatcher-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class SwipeControllerTest {

	private static final Logger logger = Logger.getLogger(SwipeControllerTest.class);

	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private FilterChainProxy springSecurityFilter;
	private MockMvc mockMvc;
	private MockHttpSession session;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		mockMvc = webAppContextSetup(wac).addFilters(springSecurityFilter).alwaysDo(print()).build();
		// enable authentication for all tests
		Authentication authentication = new UsernamePasswordAuthenticationToken("reunifyuser", "user@reunify278");
		SecurityContext securityContext = SecurityContextHolder.getContext();
		securityContext.setAuthentication(authentication);
		session = new MockHttpSession();
		session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, securityContext);
	}

	@After
	public void tearDown() throws Exception {
		mockMvc = null;
	}

	@Test
	public void testGetMemberSwipesByIdValidID() throws Exception {
		//@formatter:off
		MockHttpServletResponse response = mockMvc.perform(get("/memberswipes/2272620/from/2015-02-10 00:00:00/to/2015-02-11 00:00:00")
				.session(session))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8")).andReturn().getResponse();
		
		String expected = "{\"swipes\":[{\"locationId\":118,\"memberId\":2272620,\"timestamp\":\"2015-02-10 12:29:27\"},{\"locationId\":118,\"memberId\":2272620,\"timestamp\":\"2015-02-10 16:07:38\"},{\"locationId\":118,\"memberId\":2272620,\"timestamp\":\"2015-02-10 19:31:47\"}]}";
		 assertEquals(expected, response.getContentAsString());
		
		//@formatter:on
	}

	@Test
	public void testGetMemberSwipesByIdEmptyResponse() throws Exception {
		//@formatter:off
		MockHttpServletResponse response = mockMvc.perform(get("/memberswipes/102052649/from/2014-11-25 00:00:00/to/2014-11-26 00:00:00")
				.session(session))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8")).andReturn().getResponse();
		
		String expected = "{\"swipes\":[]}";
		 assertEquals(expected, response.getContentAsString());
		
		//@formatter:on
	}

	@Test
	public void testGetMemberSwipesByIdInvalidID() throws Exception {
		//@formatter:off
		mockMvc.perform(get("/memberswipes/a/from/2014-11-25 00:00:00/to/2014-11-26 00:00:00")
				.session(session))
				.andExpect(status().isNotFound());
		//@formatter:on
	}

	@Test
	public void testGetMemberSwipesByIdInvalidDate() throws Exception {
		//@formatter:off
		mockMvc.perform(get("/memberswipes/106787777/from/2014-15 00:00:00/to/2014-11-26 00:00:00")
				.session(session))
				.andExpect(status().isBadRequest());
		//@formatter:on
	}

	@Test
	public void testGetLocationSwipes() throws Exception {
		//@formatter:off
		MockHttpServletResponse response = mockMvc.perform(get("/swipes/118/from/2015-02-10 00:00:00/to/2015-02-11 00:00:00")
				.session(session))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8")).andReturn().getResponse();

		JsonParser parser = new JsonParser();
		JsonObject o = (JsonObject)parser.parse(response.getContentAsString());
		assertEquals(o.getAsJsonArray("swipes").size(),2148);
		
		//@formatter:on	}

	}

	@Test
	public void testGetLocationSwipesInvalidId() throws Exception {
		//@formatter:off
		mockMvc.perform(get("/swipes/a/from/2014-11-25 00:00:00/to/2014-11-26 00:00:00")
				.session(session))
				.andExpect(status().isNotFound());
		//@formatter:on	}

	}
}
