/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.traffic;

import java.util.List;

/**
 * Response object for TrafficService
 * 
 * @author kiana.baradaran
 * 
 */
public class TrafficResponse {
	private List<TrafficZone> zones;

	public TrafficResponse(List<TrafficZone> zones) {
		// TODO Auto-generated constructor stub
		this.zones = zones;
	}

	public TrafficResponse() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @return zones
	 */
	public List<TrafficZone> getZones() {
		return zones;
	}

	/**
	 * 
	 * @param zones
	 */
	public void setZones(List<TrafficZone> zones) {
		this.zones = zones;
	}
}
