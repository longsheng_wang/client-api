/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var default_offer = [
		"myPlan tools & trackers",
		"Open Gym Times",
		"Member Advantage: Discounts on movies, nationwide and local retailers and restaurants",
		"myHealth Advisor", "Tuesday night social runs" ];
var default_bucks = [ "myCoach", "Yoga workshop" ];
var gender;

var interception_tag="";

$(document).ready(function() {

	initialize_member_individual_page();
	stop_page_specific_timeouts();

	/*
	 * Member Page Checkboxes --!!IMPORTANT!!--
	 */
	$('#need-attention-input-checkbox').iCheck({
		checkboxClass : 'icheckbox_square-red',
		radioClass : 'icheckbox_square-red',
		increaseArea : '20%' // optional
	});
	$('#need-followup-input-checkbox').iCheck({
		checkboxClass : 'icheckbox_square-yellow',
		radioClass : 'icheckbox_square-yellow',
		increaseArea : '20%' // optional
	});
	$('#good-to-go-input-checkbox').iCheck({
		checkboxClass : 'icheckbox_square-green',
		radioClass : 'icheckbox_square-green',
		increaseArea : '20%' // optional
	});
	isMemberInClub();
	getMemberData();
	getMemberInfo();
	viewCurrentListHandler("random click");
//	Show Search field if any note is available
	if($("#note_0").val !== ""){
				$("#notepad-views-search-field").css("display","block");
	};
	
});

$(document).on('ifUnchecked', "#need-attention-input-checkbox", function() {
	interception_tag = "";

});

$(document).on('ifUnchecked', "#need-followup-input-checkbox", function() {
	interception_tag = "";
});

$(document).on('ifUnchecked', "#good-to-go-input-checkbox", function() {
	interception_tag = "";
});

$(document).on("ifChecked", "#need-attention-input-checkbox", function() {
	$("#good-to-go-input-checkbox").iCheck('uncheck');
	$("#need-followup-input-checkbox").iCheck('uncheck');
	interception_tag = $(this).val();

});

$(document).on("ifChecked", "#need-followup-input-checkbox", function() {
	$("#good-to-go-input-checkbox").iCheck('uncheck');
	$("#need-attention-input-checkbox").iCheck('uncheck');
	interception_tag = $(this).val();

});

$(document).on("ifChecked", "#good-to-go-input-checkbox", function() {
	$("#need-followup-input-checkbox").iCheck('uncheck');
	$("#need-attention-input-checkbox").iCheck('uncheck');
	interception_tag = $(this).val();

});

$(document).on(
		" tap",
		"#notepad_post_button",
		function(e) {
			if (interception_tag === ""
					&& $("#notepad_post_content").val().trim() === "") {
				$("#enter-note-alert").css("visibility","visible");
				$("#select-status-alert").css("visibility","visible");
			} else if ($("#notepad_post_content").val().trim() === "") {
				$("#enter-note-alert").css("visibility","visible");
				$("#select-status-alert").css("visibility","hidden");

			} else if (interception_tag === "") {
				$("#select-status-alert").css("visibility","visible");
				$("#enter-note-alert").css("visibility","hidden");

			} else {
				saveInterceptionAction(interception_tag);
				$("#enter-note-alert").css("visibility","hidden");
				$("#select-status-alert").css("visibility","hidden");
			}
			if($("#note_0").val !== ""){
				$("#notepad-views-search-field").css("display","block");
	};
		});

$(document).on("keyup", "#notepad-views-search-field", function(event) {
	$(".highlight").removeClass("highlight");

		var a = $(event.target).val();
		if (a.length > 1){
			$('#notepad-insert-and-view-div').animate({
				scrollTop : 0
			}, 700);
			searchNotes(a);
			$("#clear_search_note").css("display","block");
			
				}
		else{
			$(".notesDiv").css("display","block");
			$(".highlight").removeClass("highlight");
			$(".searched").removeClass("searched");
			showShortMessages();
			$("#clear_search_note").css("display","none");
		}
		
		
});

$(document).on('touchend', function(event) {

	if (!$(event.target).hasClass('form-control')) {
		$('#notepad-views-search-field').blur();
		$('#notepad_post_content').blur();
	}

});

$(document).on(
		'click tap',
		function(event) {

			if ($(event.target).hasClass('filterinput')) {
				$('#notepad_post_content').blur();
			}

			if (!$(event.target).hasClass('form-control')
					&& !$(event.target).hasClass('filterinput')) {
				$('#notepad-views-search-field').blur();
				$('#notepad_post_content').blur();
			}

		});

$(document).on("keyup", "#notepad_post_content", function() {
	$(this).css('height', 'auto');
	$(this).height(this.scrollHeight);
});

function saveInterceptionAction(interception_tag) {
	var member_id = getURLParameter("member_id");
	var msg = $("#notepad_post_content").val();
	$
			.ajax({
				data : {
					member_id : member_id,
					tag : interception_tag,
					msg : msg
				},
				type : 'POST',
				async : false,
				url : ctx + '/addintercept',
				dataType : 'json',
				success : function(data) {
					if (data) {
						$("#notepad_post_content").val('');
						$('#notepad-post-chars-left').text(
								'1000 character(s) left.');
						$("#good-to-go-input-checkbox").iCheck('uncheck');
						$("#need-attention-input-checkbox").iCheck('uncheck');
						$("#need-followup-input-checkbox").iCheck('uncheck');
					} else {
						alert("Oops! Something went wrong. Please submit the note again!");
					}
				},
				error : function() {
					alert("Oops! Something went wrong. Please submit the note again!");
				}
			});

	viewCurrentListHandler();
	$('#notepad-insert-and-view-div').animate({
		scrollTop : 0
	}, 700);
}

function getMemberInfo() {
	var memberId = getURLParameter("memberId");

	$
			.ajax({
				type : 'GET',
				async : true,
				url : ctx + '/members/' + memberId,
				dataType : 'json',
				beforeSend : function() {
					// This callback function will trigger before data is sent
					$.mobile.showPageLoadingMsg(true); // This will show ajax
					// spinner
				},
				complete : function() {
					// This callback function will trigger on data sent/received
					// complete
					$.mobile.hidePageLoadingMsg(); // This will hide ajax
					// spinner
				},
				success : function(data) {
					if (jQuery.isEmptyObject(data)) {

					} else {

						if (data.gender !== null
								&& !jQuery.isEmptyObject(data.gender)) {
							gender = data.gender;

							var position = localStorage.getItem('position');
							if (position === null) {
								if (gender === 'F') {
									$("#member-individual-picture-holder")
											.html(
													"<img src=\""
															+ ctx
															+ "/static/pulse/img/profilePictures/Female_Silhoutte.png\" style=\"width: 160px; height: 160px; margin-top:11px; margin-left:-5px;\">");
								} else if (gender === 'M') {
									$("#member-individual-picture-holder")
											.html(
													"<img src=\""
															+ ctx
															+ "/static/pulse/img/profilePictures/Male_Silhoutte.png\" style=\"width: 160px; height: 160px; margin-top:11px; margin-left:-5px;\">");
								}
							}

						} else {
							gender = "";
						}

						if (data.isNewComer) {
							$("#member_page_risk_new_photo_icon")
									.html(
											"<img width=\"30\"  src=\""
													+ ctx
													+ "/static/pulse/img/misc/NEWCOMER_PHOTOICON_2x.png\">");
						}

						/* Set Info */
						var firstName = data.memberFirstName;
						var lastName = data.memberLastName;
						if (firstName === null || firstName === "") {
							$('#individual_member_name').html(
									"Not Available (" + member_id + ")");
						} else {
							$('#individual_member_first_name').html(firstName);
							$('#individual_member_lasst_name').html(lastName);
						}
						var isBirthdayWeek = data.birthdayWeek;
						var memberBirthDate = data.memberBirthDate;
						function getAge(memberBirthDate) {
							var today = new Date();
							var birthDate = new Date(memberBirthDate);
							var age = today.getFullYear()
									- birthDate.getFullYear();
							var m = today.getMonth() - birthDate.getMonth();
							var d = today.getDate() - birthDate.getDate();

							if (isBirthdayWeek) {
								$("#member_individual_birthday_notification")
										.html(
												"<img src=\""
														+ ctx
														+ "/static/pulse/img/occasions/birthday-member-icon-green.png\" height=\"55\">");
								$("#individual_member_age").html(
										data.age + " - " + today.getMonth()
												+ today.getDate());
							} else {
								$("#individual_member_age").html(data.age);
							}

							if (m === 0 && d === 0) {
								$("#member_individual_birthday_notification")
										.html(
												"<img src=\""
														+ ctx
														+ "/static/pulse/img/occasions/birthday-member-icon-green.png\" height=\"55\">");

							}

						}

						var joinDate = data.joinDate;
						function anniversary(joinDate) {
							var today = new Date();
							var anniv = new Date(joinDate);
							var y = today.getFullYear() - anniv.getFullYear();
							var m = today.getMonth() - anniv.getMonth();
							var d = today.getDate() - anniv.getDate();

							if (m === 0 && d === 0) {
								// anniversary + year
							} else {
								$("#individual_member_join_date")
										.html(joinDate);
								// (y Y and m M) duration if m<0 then m* -1 and
								// y-1
							}

						}

						$("#individual_member_account_type").html(
								data.membershipType);
						if (data.primaryHolder) {

							$("#individual_member_primary_account").html("Yes");
						} else {
							$("#individual_member_primary_account").html("No");
						}
						$("#individual_member_primary_club").html(data.clubId);
					}
				}
			});
}

function initialize_all_member_page_fields() {
	$("#member-individual-picture-holder")
			.html(
					"<img alt=\"\" src=\""
							+ ctx
							+ "/static/pulse/img/loader/ajax-loader.gif\"  style=\"width: 40px; height: 40px; z-index: -1; margin-top: 45%;\">");

	$('#individual_member_first_name').html("");
	$('#individual_member_last_name').html("");
	$("#individual_member_age").html("");
	$("#individual_member_join_date").html("");
	$("#member-individual-health-score")
			.html(
					"<img alt=\"\" src=\""
							+ ctx
							+ "/static/pulse/img/memberprofile/RD_HEALTHSCORE_GREY-B3_2.0x.png\" height=\"42\">");
	$("#member-individual-personal-trainer")
			.html(
					"<img alt=\"\" src=\""
							+ ctx
							+ "/static/pulse/img/memberprofile/RD_TRAININGSESSION_Grey-b3_2.0x.png\" height=\"42\">");
	$("#individual_member_account_type").html("");
	$("#individual_member_primary_club").html("");
	$('#individual_member_id').html("");
	$("#member-individual-time-of-visit").html("");
	$("#member-individual-companionship").html("");
	$("#member_individual_birthday_notification").html("");
	$("#member_individual_star_status")
			.html(
					"<img src=\""
							+ ctx
							+ "/static/pulse/img/memberprofile/RD_STARGrey_2.0x.png\" height=\"42\">");
	$("#member_page_risk_photo_icon")
			.html(
					"<img src=\""
							+ ctx
							+ "/static/pulse/img/memberprofile/RD_AtRisk_GREY_2.0x.png\" height=\"42\">");
	$("#member_page_risk_value_icon").css("color", "#4d4d4d");
	$("#member_page_risk_value_icon").html("RISK");
	$("#member_page_new_photo_icon").html("");
	$("#offer-page-member-offer-allowance").html("");
	$("#notepad_post_content").val("");
	$("#individual_member_availability").html("");
	$("#individual_member_availability").css("background", "transparent");
	$('#individual_member_lastAction').html("");
	$('#individual_member_lastVisit').html("");
	$("#individual_member_lastVisit_bolt").html("");

	$("#member_page_duration_year_icon").css("visibility", "hidden");
	$("#individual_member_frequency").html("");
	$("#individual_member_frequency_bolt").html("");
	$("#member_individual_action_list").html("");
	$("#member_individual_interest_icon_1").html("");
	$("#member_individual_interest_icon_2").html("");
	$("#member_individual_interest_title_1").html("");
	$("#member_individual_interest_title_2").html("");

}
function initialize_member_individual_page() {

	$('#individual_member_first_name').html("");
	$('#individual_member_last_name').html("");
	$("#member_individual_notepad_content").css("display", "none");
	$("#member_individual_notepad_content").css("visibilty", "hidden");
	$("#member_individual_offer_content").css("visibilty", "visible");
	$("#member_individual_offer_content").css("display", "inline");
	$("#good-to-go-input-checkbox").iCheck('uncheck');
	$("#need-followup-input-checkbox").iCheck('uncheck');
	$("#need-attention-input-checkbox").iCheck('uncheck');
	$('.btn-offer').css("background-color", "#99CA3C");
	$('.btn-notepad').css("background-color", "#4D4D4D");
	$('#notepad-post-chars-left').text('1000 character(s) left.');
}
