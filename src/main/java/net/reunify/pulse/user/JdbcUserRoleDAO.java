/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * JDBC implementation of UserRoleDAO
 * 
 * @author kiana.baradaran
 * 
 */
@Component
public class JdbcUserRoleDAO implements UserRoleDAO {
	private static final Logger logger = Logger.getLogger(JdbcUserRoleDAO.class);

	@Inject
	@Named("pulseJdbcTemplate")
	private JdbcTemplate pulseJdbcTemplate;

	@Override
	public List<UserRole> getAllRoles() {

		List<UserRole> roles = new ArrayList<UserRole>();
		String selectRoles = "select record_id,role_name from user_roles_lookup;";
		List<Map<String, Object>> rows = this.pulseJdbcTemplate.queryForList(selectRoles);

		for (Map<String, Object> row : rows) {
			UserRole role = new UserRole((Integer) row.get("record_id"), (String) row.get("role_name"));

			roles.add(role);
		}
		return roles;

	}

}
