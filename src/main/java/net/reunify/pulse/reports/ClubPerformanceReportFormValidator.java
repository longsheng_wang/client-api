/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.reports;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

/**
 * Validator for ClubPerformanceReportForm
 * 
 * @author Vivek Hungund
 *
 */
public class ClubPerformanceReportFormValidator extends DateRangeReportFormValidator {
	private static final Logger logger = Logger.getLogger(ClubPerformanceReportFormValidator.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object,
	 * org.springframework.validation.Errors)
	 */
	@Override
	public void validate(Object target, Errors errors) {
		ClubPerformanceReportForm form = (ClubPerformanceReportForm) target;

		DateRangeReportFormValidationUtils.validateDate(form.getStartDate(), errors, "start");
		DateRangeReportFormValidationUtils.validateDate(form.getEndDate(), errors, "end");
		DateRangeReportFormValidationUtils.validateDateRange(form, errors);
		validateLocation(form, errors, "locationId");

	}
	
	/**
	 * @param form
	 * @param errors
	 * @param locationFieldPath
	 */
	private void validateLocation(ClubPerformanceReportForm form, Errors errors, String locationFieldPath) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, locationFieldPath, "location_required",
				"Please choose a location");

		if (Integer.parseInt(form.getLocationId()) == 0)
			errors.rejectValue(locationFieldPath, "invalid_location", "Please choose a valid location");

	}

}
