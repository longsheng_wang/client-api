<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<strong>Life Time Reporting Tools</strong> <br />
<a href="<%= pageContext.getServletContext().getContextPath() %>/reports/interceptprogressreport">Intercept Progress Report</a> <br />
<a href="<%= pageContext.getServletContext().getContextPath() %>/reports/clubperformance">Club Performance</a><br />
<a href="<%= pageContext.getServletContext().getContextPath() %>/reports/mesperformance">MES Performance</a><br />
<a href="<%= pageContext.getServletContext().getContextPath() %>/memberSwipeSearch-internal">Member Swipe Search Tool</a>
</body>
</html>