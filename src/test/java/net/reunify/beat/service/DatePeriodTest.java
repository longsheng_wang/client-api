/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.beat.service;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author longsheng.wang
 *
 */
public class DatePeriodTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testToday(){
		TimeZone losAngelesTimeZone = TimeZone.getTimeZone("America/Los_Angeles");
		Date testDate = getDate(2014, 5, 21, losAngelesTimeZone);
		Date actual = DatePeriod.TODAY.getStartDate(testDate, losAngelesTimeZone);
		Date expected = getStartDate(2014, 5, 21, losAngelesTimeZone);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testTodayDayLightSaving(){
		Date testDate = getDate(2014, 3, 9, TimeZone.getTimeZone("US/Pacific"));
		Date actual = DatePeriod.TODAY.getStartDate(testDate, TimeZone.getTimeZone("PST"));
		Date expected = getStartDate(2014, 3, 9, TimeZone.getTimeZone("PST"));
		assertEquals(expected, actual);
		actual = DatePeriod.TODAY.getStartDate(testDate, TimeZone.getTimeZone("America/Los_Angeles"));
		assertEquals(expected, actual);
		actual = DatePeriod.TODAY.getStartDate(testDate, TimeZone.getTimeZone("US/Pacific"));
		assertEquals(expected, actual);
		
		testDate = getDate(2014, 11, 2, TimeZone.getTimeZone("US/Pacific"));
		actual = DatePeriod.TODAY.getStartDate(testDate, TimeZone.getTimeZone("PST"));
		expected = getStartDate(2014, 11, 2, TimeZone.getTimeZone("US/Pacific"));
		assertEquals(expected, actual);
		actual = DatePeriod.TODAY.getStartDate(testDate, TimeZone.getTimeZone("America/Los_Angeles"));
		assertEquals(expected, actual);
		actual = DatePeriod.TODAY.getStartDate(testDate, TimeZone.getTimeZone("US/Pacific"));
		assertEquals(expected, actual);
	}
	
	@Test
	public void testYesterday(){
		TimeZone pacificTimeZone = TimeZone.getTimeZone("US/Pacific");
		Date testDate = getDate(2014, 11, 9, pacificTimeZone);
		Date actual = DatePeriod.YESTERDAY.getStartDate(testDate, pacificTimeZone);
		Date expected = getStartDate(2014, 11, 8, pacificTimeZone);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testYesterdayOverYear(){
		TimeZone mountainTimeZone = TimeZone.getTimeZone("US/Mountain");
		Date testDate = getDate(2000, 1, 1, mountainTimeZone);
		Date actual = DatePeriod.YESTERDAY.getStartDate(testDate, mountainTimeZone);
		Date expected = getStartDate(1999, 12, 31, mountainTimeZone);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testYesterdayDayLightSaving(){
		Date testDate = getDate(2014, 3, 10, TimeZone.getTimeZone("US/Pacific"));
		Date actual = DatePeriod.YESTERDAY.getStartDate(testDate, TimeZone.getTimeZone("PST"));
		Date expected = getStartDate(2014, 3, 9, TimeZone.getTimeZone("PST"));
		assertEquals(expected, actual);
		actual = DatePeriod.YESTERDAY.getStartDate(testDate, TimeZone.getTimeZone("America/Los_Angeles"));
		assertEquals(expected, actual);
		actual = DatePeriod.YESTERDAY.getStartDate(testDate, TimeZone.getTimeZone("US/Pacific"));
		assertEquals(expected, actual);
		testDate = getDate(2014, 11, 3, TimeZone.getTimeZone("US/Pacific"));
		actual = DatePeriod.YESTERDAY.getStartDate(testDate, TimeZone.getTimeZone("PST"));
		expected = getStartDate(2014, 11, 2, TimeZone.getTimeZone("US/Pacific"));
		assertEquals(expected, actual);
		actual = DatePeriod.YESTERDAY.getStartDate(testDate, TimeZone.getTimeZone("America/Los_Angeles"));
		assertEquals(expected, actual);
		actual = DatePeriod.YESTERDAY.getStartDate(testDate, TimeZone.getTimeZone("US/Pacific"));
		assertEquals(expected, actual);
	}
	
	@Test
	public void testWtd() {
		TimeZone losAngelesTimeZone = TimeZone.getTimeZone("America/Los_Angeles");
		Date testDate = getDate(2015, 1, 5, losAngelesTimeZone);
		Date actual = DatePeriod.WTD.getStartDate(testDate, losAngelesTimeZone);
		Date expected = getStartDate(2015, 1, 4, losAngelesTimeZone);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testWtdOverYear() {
		TimeZone losAngelesTimeZone = TimeZone.getTimeZone("America/Los_Angeles");
		Date testDate = getDate(2015, 1, 3, losAngelesTimeZone);
		Date actual = DatePeriod.WTD.getStartDate(testDate, losAngelesTimeZone);
		Date expected = getStartDate(2014, 12, 28, losAngelesTimeZone);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testWtdSameDay() {
		TimeZone losAngelesTimeZone = TimeZone.getTimeZone("America/Los_Angeles");
		Date testDate = getDate(2015, 1, 4, losAngelesTimeZone);
		Date actual = DatePeriod.WTD.getStartDate(testDate, losAngelesTimeZone);
		Date expected = getStartDate(2015, 1, 4, losAngelesTimeZone);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testWtdDayLightSaving(){
		Date testDate = getDate(2014, 3, 10, TimeZone.getTimeZone("US/Pacific"));
		Date actual = DatePeriod.WTD.getStartDate(testDate, TimeZone.getTimeZone("PST"));
		Date expected = getStartDate(2014, 3, 9, TimeZone.getTimeZone("PST"));
		assertEquals(expected, actual);
		actual = DatePeriod.WTD.getStartDate(testDate, TimeZone.getTimeZone("America/Los_Angeles"));
		assertEquals(expected, actual);
		actual = DatePeriod.WTD.getStartDate(testDate, TimeZone.getTimeZone("US/Pacific"));
		assertEquals(expected, actual);
		testDate = getDate(2014, 11, 3, TimeZone.getTimeZone("US/Pacific"));
		actual = DatePeriod.WTD.getStartDate(testDate, TimeZone.getTimeZone("PST"));
		expected = getStartDate(2014, 11, 2, TimeZone.getTimeZone("US/Pacific"));
		assertEquals(expected, actual);
		actual = DatePeriod.WTD.getStartDate(testDate, TimeZone.getTimeZone("America/Los_Angeles"));
		assertEquals(expected, actual);
		actual = DatePeriod.WTD.getStartDate(testDate, TimeZone.getTimeZone("US/Pacific"));
		assertEquals(expected, actual);
	}
	
	@Test
	public void testMtd() {
		TimeZone losAngelesTimeZone = TimeZone.getTimeZone("America/Los_Angeles");
		Date testDate = getDate(2014, 10, 31, losAngelesTimeZone);
		Date actual = DatePeriod.MTD.getStartDate(testDate, losAngelesTimeZone);
		Date expected = getStartDate(2014, 10, 1, losAngelesTimeZone);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testMtdSameDay() {
		TimeZone losAngelesTimeZone = TimeZone.getTimeZone("America/Los_Angeles");
		Date testDate = getDate(2015, 1, 1, losAngelesTimeZone);
		Date actual = DatePeriod.MTD.getStartDate(testDate, losAngelesTimeZone);
		Date expected = getStartDate(2015, 1, 1, losAngelesTimeZone);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testMtdDayLightSaving(){
		Date testDate = getDate(2014, 3, 10, TimeZone.getTimeZone("US/Pacific"));
		Date actual = DatePeriod.MTD.getStartDate(testDate, TimeZone.getTimeZone("PST"));
		Date expected = getStartDate(2014, 3, 1, TimeZone.getTimeZone("PST"));
		assertEquals(expected, actual);
		actual = DatePeriod.MTD.getStartDate(testDate, TimeZone.getTimeZone("America/Los_Angeles"));
		assertEquals(expected, actual);
		actual = DatePeriod.MTD.getStartDate(testDate, TimeZone.getTimeZone("US/Pacific"));
		assertEquals(expected, actual);
		testDate = getDate(2014, 11, 3, TimeZone.getTimeZone("US/Pacific"));
		actual = DatePeriod.MTD.getStartDate(testDate, TimeZone.getTimeZone("PST"));
		expected = getStartDate(2014, 11, 1, TimeZone.getTimeZone("US/Pacific"));
		assertEquals(expected, actual);
		actual = DatePeriod.MTD.getStartDate(testDate, TimeZone.getTimeZone("America/Los_Angeles"));
		assertEquals(expected, actual);
		actual = DatePeriod.MTD.getStartDate(testDate, TimeZone.getTimeZone("US/Pacific"));
		assertEquals(expected, actual);
	}
	
	@Test
	public void testYtdDuringDayLightSaving() {
		TimeZone losAngelesTimeZone = TimeZone.getTimeZone("America/Los_Angeles");
		Date testDate = getDate(2014, 10, 31, losAngelesTimeZone);
		Date actual = DatePeriod.YTD.getStartDate(testDate, losAngelesTimeZone);
		Date expected = getStartDate(2014, 1, 1, losAngelesTimeZone);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testYtdOutsideDayLightSaving() {
		TimeZone losAngelesTimeZone = TimeZone.getTimeZone("America/Los_Angeles");
		Date testDate = getDate(2014, 2, 2, losAngelesTimeZone);
		Date actual = DatePeriod.YTD.getStartDate(testDate, losAngelesTimeZone);
		Date expected = getStartDate(2014, 1, 1, losAngelesTimeZone);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testYtdSameDay() {
		TimeZone losAngelesTimeZone = TimeZone.getTimeZone("America/Los_Angeles");
		Date testDate = getDate(2015, 1, 1, losAngelesTimeZone);
		Date actual = DatePeriod.YTD.getStartDate(testDate, losAngelesTimeZone);
		Date expected = getStartDate(2015, 1, 1, losAngelesTimeZone);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testSameDayLastWeekOverYear(){
		TimeZone pacificTimeZone = TimeZone.getTimeZone("US/Pacific");
		Date testDate = getDate(2015, 1, 2, pacificTimeZone);
		Date actual = DatePeriod.SAMEDAYLASTWEEK.getStartDate(testDate, pacificTimeZone);
		Date expected = getStartDate(2014, 12, 26, pacificTimeZone);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testSameDayLastWeekDayLightSaving(){
		TimeZone easternTimeZone = TimeZone.getTimeZone("US/Eastern");
		Date testDate = getDate(2014, 3, 10, easternTimeZone);
		Date actual = DatePeriod.SAMEDAYLASTWEEK.getStartDate(testDate, easternTimeZone);
		Date expected = getStartDate(2014, 3, 3, easternTimeZone);
		assertEquals(expected, actual);
		
		testDate = getDate(2014, 11, 3, easternTimeZone);
		actual = DatePeriod.SAMEDAYLASTWEEK.getStartDate(testDate, easternTimeZone);
		expected = getStartDate(2014, 10, 27, easternTimeZone);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testSameDayLastMonthOverYear(){
		TimeZone centralTimeZone = TimeZone.getTimeZone("US/Central");
		Date testDate = getDate(2015, 1, 2, centralTimeZone);
		Date actual = DatePeriod.SAMEDATELASTMONTH.getStartDate(testDate, centralTimeZone);
		Date expected = getStartDate(2014, 12, 2, centralTimeZone);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testSameDayLastMonthDayLightSaving(){
		TimeZone centralTimeZone = TimeZone.getTimeZone("US/Central");
		Date testDate = getDate(2014, 3, 10, centralTimeZone);
		Date actual = DatePeriod.SAMEDATELASTMONTH.getStartDate(testDate, centralTimeZone);
		Date expected = getStartDate(2014, 2, 10, centralTimeZone);
		assertEquals(expected, actual);
		
		testDate = getDate(2014, 11, 3, centralTimeZone);
		actual = DatePeriod.SAMEDATELASTMONTH.getStartDate(testDate, centralTimeZone);
		expected = getStartDate(2014, 10, 3, centralTimeZone);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testSameDayLastYearDuringDayLightSaving(){
		TimeZone centralTimeZone = TimeZone.getTimeZone("US/Central");
		Date testDate = getDate(2015, 5, 1, centralTimeZone);
		Date actual = DatePeriod.SAMEDATELASTYEAR.getStartDate(testDate, centralTimeZone);
		Date expected = getStartDate(2014, 5, 1, centralTimeZone);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testSameDayLastYearOutsideDayLightSaving(){
		TimeZone centralTimeZone = TimeZone.getTimeZone("US/Central");
		Date testDate = getDate(2015, 12, 7, centralTimeZone);
		Date actual = DatePeriod.SAMEDATELASTYEAR.getStartDate(testDate, centralTimeZone);
		Date expected = getStartDate(2014, 12, 7, centralTimeZone);
		assertEquals(expected, actual);
	}
	
	public Date getDate(int year, int month, int day, TimeZone timeZone) {
		Calendar calendar = new GregorianCalendar(timeZone);
	    calendar.set(Calendar.YEAR, year);
	    calendar.set(Calendar.MONTH, month-1); // Calendar.JANURARY == 0
	    calendar.set(Calendar.DATE, day);
	    return calendar.getTime();
	}
	
	public Date getStartDate(int year, int month, int day, TimeZone timeZone) {
	    Calendar calendar = Calendar.getInstance(timeZone);
	    calendar.set(Calendar.YEAR, year);
	    calendar.set(Calendar.MONTH, month-1);
	    calendar.set(Calendar.DAY_OF_MONTH, day);
	    calendar.set(Calendar.HOUR, 0);
	    calendar.set(Calendar.AM_PM, Calendar.AM);
		calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
	    return calendar.getTime();
	}
}
