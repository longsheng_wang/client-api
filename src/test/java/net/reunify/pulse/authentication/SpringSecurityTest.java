/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.authentication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.web.context.WebApplicationContext;

/**
 * Functional tests for spring security and session management
 * 
 * @author Shakti Shrivastava
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/spring-security.xml",
		"file:src/test/webapp/WEB-INF/mvc-dispatcher-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class SpringSecurityTest {

	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private FilterChainProxy springSecurityFilter;
	private MockMvc mockMvc;
	private final String SECURED_URI = "/emp/get/123";
	private final String LOGIN_PAGE_URL = "/login";
	private final String LOGIN_CHECK_URL = "/j_spring_security_check";
	private final String ACCESS_DENIED_URL = "/denied";
	private final String LOGOUT_URL = "/logout";

	@Before
	public void setUp() throws Exception {
		// Enable Spring Security
		mockMvc = webAppContextSetup(wac).addFilters(springSecurityFilter).alwaysDo(print()).build();
	}

	@After
	public void tearDown() throws Exception {
		mockMvc = null;
	}

	@Test
	public void itShouldDenyAnonymousAccess() throws Exception {
		mockMvc.perform(get(SECURED_URI)).andExpect(urlPath(LOGIN_PAGE_URL));
	}

	@Test
	public void itShouldAllowAccessToSecuredPageForPermittedUser() throws Exception {
		Authentication authentication = new UsernamePasswordAuthenticationToken("testuser", "test");
		SecurityContext securityContext = SecurityContextHolder.getContext();
		securityContext.setAuthentication(authentication);

		MockHttpSession session = new MockHttpSession();
		session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, securityContext);

		mockMvc.perform(get(SECURED_URI).session(session)).andExpect(status().isOk());
	}

	@Test
	public void testValidCredentials() throws Exception {
		HttpSession session =
				mockMvc.perform(post(LOGIN_CHECK_URL).param("username", "testuser").param("password", "test"))
						.andExpect(redirectedUrl(SECURED_URI)).andReturn().getRequest().getSession();

		SecurityContext securityContext =
				(SecurityContext) session.getAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY);
		assertNotNull(securityContext.getAuthentication());
		assertTrue(securityContext.getAuthentication().isAuthenticated());
		assertEquals(securityContext.getAuthentication().getName(), "testuser");
	}

	@Test
	public void testInvalidCredentials() throws Exception {
		HttpSession session =
				mockMvc.perform(post(LOGIN_CHECK_URL).param("username", "foo").param("password", "bar"))
						.andExpect(redirectedUrl(ACCESS_DENIED_URL)).andReturn().getRequest().getSession();
		SecurityContext securityContext =
				(SecurityContext) session.getAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY);
		assertNull(securityContext);
	}

	@Test
	public void testLogout() throws Exception {
		// login
		HttpSession session =
				mockMvc.perform(post(LOGIN_CHECK_URL).param("username", "testuser").param("password", "test"))
						.andExpect(redirectedUrl(SECURED_URI)).andReturn().getRequest().getSession();

		SecurityContext securityContext =
				(SecurityContext) session.getAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY);
		assertNotNull(securityContext.getAuthentication());
		assertTrue(securityContext.getAuthentication().isAuthenticated());
		assertEquals(securityContext.getAuthentication().getName(), "testuser");

		// logout
		session =
				mockMvc.perform(get(LOGOUT_URL)).andExpect(urlPath(LOGIN_PAGE_URL)).andReturn().getRequest()
						.getSession();
		securityContext =
				(SecurityContext) session.getAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY);
		assertNull(securityContext);
	}

	@Test
	public void testLoginPage() throws Exception {
		mockMvc.perform(get(LOGIN_PAGE_URL)).andExpect(status().isOk());
	}

	/**
	 * URL path matcher. Currently MockMvc returns a hard coded path using
	 * localhost as host name on failed logins. This matcher allows us to match
	 * the URL paths only ignoring the host name
	 * 
	 * @param urlPath
	 *            the URL path to to verify
	 * @return
	 * 
	 * @see URL#getPath()
	 */
	public static ResultMatcher urlPath(final String urlPath) {
		return new ResultMatcher() {
			@Override
			public void match(MvcResult result) {
				try {
					assertEquals("URL Path", urlPath, new URL(result.getResponse().getRedirectedUrl()).getPath());
				} catch (MalformedURLException e) {
					fail(e.getMessage());
				}
			}
		};
	}

}
