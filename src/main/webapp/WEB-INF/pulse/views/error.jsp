<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isErrorPage="true" import="org.apache.log4j.Logger"%>

<ui:errorview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Trace - Error">
	<jsp:attribute name="headTag">
	<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/error/error.css"
			rel="stylesheet">
	</jsp:attribute>
	<jsp:body>

<div class="main-div">
<h1>500 - Internal Server Error</h1>

</div>
	</jsp:body>
</ui:errorview>

<%
	Logger logger = Logger.getLogger("JSP");
	logger.error(exception.getMessage(), exception);
%>