/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.base;

/**
 * @author Shakti Shrivastava
 */
public class APIErrorResource {

	private final String name;

	private final String message;

	/**
	 * @param name
	 * @param message
	 */
	public APIErrorResource(String name, String message) {
		super();
		this.name = name;
		this.message = message;
	}

	/**
	 * @param name
	 * @param message
	 */
	public APIErrorResource(Throwable throwable) {
		super();
		if (throwable != null) {
			this.name = throwable.getClass().getSimpleName();
			this.message = throwable.getMessage();
		} else {
			this.name = "unknown";
			this.message = "unknown";
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "APIErrorResource [name=" + name + ", message=" + message + "]";
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
}
