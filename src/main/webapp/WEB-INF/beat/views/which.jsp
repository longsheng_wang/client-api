<%@taglib prefix="ui" tagdir="/WEB-INF/tags/beat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<ui:beatbaseview contextPath="" pageTitle="ReunifyBeat"
	pageName="whichPageView">
	<jsp:attribute name="headTag">
	<!--  Add header files here  -->
	</jsp:attribute>
	<jsp:body>
       <div class="row " style="padding-top: 10px; padding-left: 3%">
			<c:forEach items="${surveyAnswerCount}" var="count">
           <!--row 1-->
           <div class="row">
               <div class="col-lg-5 bootcolstyle">
                   <div class="rowbox orangecol">
                       <p class="customp">${count.content}</p>
                   </div>
               </div>
               <!--row 1-->
               <div class="col-lg-1"></div>
               <div class="col-lg-5 bootcolstyle">

                   <!-- row 1 col 1-->
                   <div class="row col-lg-3 bootcolstyle">
                       <div class="rowbox greencol">
                           <p class="customp">${count.positive}</p>
                       </div>
                   </div>

                   <!--row 1 col 2-->
                   <div class="row col-lg-3 bootcolstyle">
                       <div class="rowbox yellowcol">
                           <p class="customp">${count.neutral}</p>
                       </div>
                   </div>

                   <!--row 1 col 3-->
                   <div class="row col-lg-3 bootcolstyle">
                       <div class="rowbox redcol">
                           <p class="customp">${count.negative}</p>
                       </div>
                   </div>
                   
                   <!--row 1 col 4-->
                   <div class="row col-lg-3 bootcolstyle">
                       <div class="rowbox bluecol">
                           <p class="customp">${count.notanswered}</p>
                       </div>
                   </div>
               </div>
               <!--row 1-->
           </div>
           </c:forEach>
       </div>
</jsp:body>
</ui:beatbaseview>