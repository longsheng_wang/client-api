/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.beat.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;

import net.reunify.beat.dao.CommentDao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author longsheng.wang
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/webapp/WEB-INF/beat/beat-servlet.xml" })
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class CommentServiceImplTest {
	
	@Inject
	@Named("commentService")
	private CommentService commentService;
	
	@Inject
	@Named("commentDao")
	private CommentDao commentDao;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testResolveComment() {
		int prepared = commentDao.unresolveCommentAndDeleteResolvedTime(1L);
		assertEquals(1, prepared);
		assertEquals(false, commentDao.checkCommentResolved(1L));
		assertEquals(1, commentService.resolveOneComment(1L));
		Date resolvedTime = commentService.checkCommentResolvedTime(1L);
		long timeDifference = System.currentTimeMillis() - resolvedTime.getTime();
		assertTrue(timeDifference < 1000*60); // resolved time within 1 min. prove that resolve_time is updated.
	}

}
