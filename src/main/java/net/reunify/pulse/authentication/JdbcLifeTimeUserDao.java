/* Copyright 2013 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.authentication;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * JDBC implementation of UserDAO - Client: LifeTime
 * 
 * @author kiana.baradaran
 * 
 */

public class JdbcLifeTimeUserDao implements UserDAO {

	private static final Logger logger = LoggerFactory.getLogger(JdbcLifeTimeUserDao.class);

	UsernameValidator validator = new UsernameValidator();

	@Inject
	@Named("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@Override
	public UserDetails findUserByUsername(String username) {
		if (!validator.validate(username.toLowerCase())) {
			throw new InvalidUsernameException("No such user: " + username);
		}

		String selectUser =
				"select id::int,(firstname||' '||lastname)::text as name, username::text,password, t1.clubcode, t1.location_id "
						+ " from ltm_users t1 left join ltm_clubs t2 on t1.location_id = t2.mmsclubid::int where lower(username)=lower(?);";

		UserDetails user =
				this.jdbcTemplate.queryForObject(selectUser, new Object[] { username }, new RowMapper<UserDetails>() {
					public UserDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
						List<SimpleGrantedAuthority> authorities =
								Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));

						UserDetails user =

								new LifeTimeUserDetails(rs.getString("username"), "ReunifyTest278", authorities,
														rs.getString("name"), rs.getInt("id"),
														rs.getInt("location_id"), rs.getString("clubcode"));

						return user;
					}
				});

		return user;
	}
}
