/* Copyright 2014 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
	Author: Sina
	12-19 
 */


function getAnalyticsData() {
	var achievment;
	var attrition;
	var interception;
	var frequency;
	var spending;
	var totalSwipedMembers = 0;
	var atRiskSwipesThisWeek;
	var atRiskSwipesToday;
	var interceptedMembersThisWeek;
	var interceptedMembersToday;
	interceptedMembersActualCancellationByRisk = new Array();
	interceptedMembersExpectedCancellationByRisk = new Array();
	savedMemberCancellationByRisk = new Array(); 
	interceptedMembersCancellationRateByRisk = new Array();
	swipedMembersCancellationRateByRisk = new Array();
	swipedMembersActualCancellationByRisk = new Array();
    donutData = new Array();
    donutDataToday = new Array();
    donutDataWeek = new Array();
    interceptedMembersFrequencyByWeek = new Array();
    interceptedMembersFrequencyByWeekReverse = new Array();
    swipedMembersFrequencyByWeek = new Array();
    swipedMembersFrequencyByWeekReverse = new Array();
    interceptedMembersSpendingByWeek = new Array();
    interceptedMembersSpendingByWeekReverse = new Array();
    swipedMembersSpendingByWeek = new Array();
    swipedMembersSpendingByWeekReverse = new Array();
	
	$.jqplot.config.enablePlugins = true;

	  $.ajax({
	        type: 'GET',
	        async: true,
	        url: CONTEXT_PATH + "/location-analytics/details/",
	        dataType: 'json',
	        cache : false,
	        success: function(data) {
	            if (jQuery.isEmptyObject(data)) {

	            } else {
	            	//Achievment Data
	            	interceptedMembersActualCancellationByRisk = data.interceptedMembersActualCancellationByRisk ; 
	            	interceptedMembersExpectedCancellationByRisk = data.interceptedMembersExpectedCancellationByRisk;
	            	savedMemberCancellationByRisk = data.savedMemberCancellationByRisk;
	            	
	            	// Attrition Data
	            	swipedMembersActualCancellationByRisk = data.swipedMembersActualCancellationByRisk;
	            	interceptedMembersCancellationRateByRisk = data.interceptedMembersCancellationRateByRisk;
	            	swipedMembersCancellationRateByRisk = data.swipedMembersCancellationRateByRisk;
	            	
	            	//Interception Data
	            	atRiskSwipesThisWeek = data.atRiskSwipesThisWeek;
	            	atRiskSwipesToday = data.atRiskSwipesToday;
	            	interceptedMembersThisWeek = data.interceptedMembersThisWeek;
	            	interceptedMembersToday = data.interceptedMembersToday;
	            	
	            	//Frequency Data
	            	interceptedMembersFrequencyByWeekReverse = data.interceptedMembersFrequencyByWeek;
	            	swipedMembersFrequencyByWeekReverse = data.swipedMembersFrequencyByWeek;
	            	
	            	//Avg. Spending Data
	            	interceptedMembersSpendingByWeekReverse = data.interceptedMembersSpendingByWeek;
	            	swipedMembersSpendingByWeekReverse = data.swipedMembersSpendingByWeek;




//	  Analytics chart -- bar chart 


     var renderAchievment = function (){

    	    var ticks = [10,20,30,40,50,60,70,80,90,100];
    	    var ticks2 = [0,5,10,15,20,25,30,35,40,45,50];
    	    achievment =   $.jqplot('analytical_achievment_chart', [interceptedMembersActualCancellationByRisk,interceptedMembersExpectedCancellationByRisk,savedMemberCancellationByRisk], {
    	seriesDefaults: {
    		renderer:$.jqplot.BarRenderer,
          rendererOptions:{
            barPadding:0,
          fillToZero: false ,
            highlightMouseOver: false,
            
           
          }, pointLabels: {
              show: false,
        	 },
        	 
     
            
        },
        series:[
                {},
                {},
                {
                    renderer:$.jqplot.LineRenderer,
                    lineWidth:4,
                    markerOptions:{
                        size:0
                    }
                }
                
                
                ],
        seriesColors:["#ef402f","#fcdcd9","#28a446"],
        axes: {
            xaxis: {
            	renderer: $.jqplot.CategoryAxisRenderer,
           
        
                
                ticks: ticks,
                tickOptions:{
                    showGridline: false,
                    
                    mark:'outside',
                    
                    markSize:0,
                    
                  
                  
                },
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
        		label:'Risk Score',
        		
        		
            
             
            },
    
        	yaxis:{

        		ticks:ticks2,
        		tickOptions:{
        		
        		formatString:'%d'},
        		labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
        		label:'Number of Members Who Swiped',
        		
        	},
        	

        	
        },
        grid: {
        	drawBorder:false,
        	shadow:false,
        	background:'#FFFFFF',	
        },
        legend: {
            renderer: $.jqplot.EnhancedLegendRenderer,
            show: true, 
            location: 's', 
            placement: 'outside',
            rendererOptions: {
              numberRows: '1',
              numberColumns: '3'
            },
            labels:['Actual Cancellation&nbsp&nbsp','Expected Cancellation&nbsp&nbsp','Saved Cancellation&nbsp&nbsp'],
            xoffset:30,
            yoffset:55,
            seriesToggle: true
          },
            
          
          
       
    
    });
     }
     renderAchievment();

  					var resizeAchievment = function() {
  								if (achievment)
  									achievment.destroy();
  								renderAchievment();
  						}

  						$(window).resize(function() {
  							resizeAchievment();
  							});
  


//    Attrition chart -- bar chart 
       var renderAttrition= function (){
    	   
    	   


           var ticks = [10,20,30,40,50,60,70,80,90,100];
           var ticks2 = [0,5,10,15,20,25];
        
    	   attrition = $.jqplot('analytical_attrition_chart', [interceptedMembersCancellationRateByRisk, swipedMembersCancellationRateByRisk], {
      
       	seriesDefaults: {
       		renderer:$.jqplot.BarRenderer,
             rendererOptions:{
               barPadding:0,
              
             }, pointLabels: {
                 show: false,
           	 },
           	highlightMouseOver: false,
        
               
           },
           seriesColors:["#3c5016","#c4e090"],
           axes: {
               
        	   
               xaxis: {
               	renderer: $.jqplot.CategoryAxisRenderer,
                
                   ticks: ticks,
                   tickOptions:{
                       showGridline: false,
                       markSize:0,

                     
                   },
                   labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
           		label:'Risk Score',
              
                   
               },
             
           	yaxis:{
        	
           		ticks:ticks2 ,
           		tickOptions:{
           			formatString: '%d%%',
                   
            	},
            	labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
        		label:'Cancellation Rate',
        		
               	
           	},
           	
           },
           grid: {
           	drawBorder:false,
           	shadow:false,
           	background:'#FFFFFF',	
           },
           legend: {
               renderer: $.jqplot.EnhancedLegendRenderer,
               show: true, 
               location: 's', 
               placement: 'outside',
               rendererOptions: {
                 numberRows: '1',
                 numberColumns: '2'
               },
               labels:['Members Intercepted&nbsp&nbsp','Members Who Swiped &nbsp&nbsp'],
               xoffset:30,
               yoffset:55,
              
             },
          
       
       });
       }
       renderAttrition();

			var resizeAttrition = function() {
						if (attrition)
							attrition.destroy();
						renderAttrition();
				}

				$(window).resize(function() {
					resizeAttrition();
					});
				
				
				
				
//	    Interception chart -- donutchart
    
    var renderInterception = function (today){
    	

        donutDataToday = [ ['intercept',interceptedMembersToday],['total', atRiskSwipesToday ]   ];
        donutDataWeek = [ ['intercept',interceptedMembersThisWeek],['total', atRiskSwipesThisWeek ]   ];
    	
        var interceptedRateToday;
        var interceptedRateWeek;
        
        if ( atRiskSwipesToday !== 0){
        	interceptedRateToday = interceptedMembersToday / atRiskSwipesToday;
        }else{
        	interceptedRateToday = 0 ;
        }
        
        if ( atRiskSwipesThisWeek !== 0) {
        	interceptedRateWeek = interceptedMembersThisWeek / atRiskSwipesThisWeek ;
        }else{
        	interceptedRateWeek = 0 ;
        }
        
        
      

         if($('#today-button').hasClass('active')){
    		donutData = donutDataToday;
    	    $('#donut-numbers').html(interceptedMembersToday);
    	    $('#donut-percentage').html(interceptedRateToday + '%');
    	}

         else if($('#week-button').hasClass('active')){
    		donutData = donutDataWeek;
    	    $('#donut-numbers').html(interceptedMembersThisWeek);
    	    $('#donut-percentage').html(interceptedRateWeek + '%');
    	}
    	
    	else{
    		donutData = [0,0];
    	}
         
    	interception = jQuery.jqplot ('analytical_interception_chart', [donutData], 
                { 
            	  seriesColors:["#721109","#f15041"],
                  seriesDefaults: {
                  
                    renderer: jQuery.jqplot.DonutRenderer, 
                    rendererOptions: {
                    
                      showDataLabels: false,
                      startAngle: -90,
                      sliceMargin:0,
                      dataLabelPositionFactor: 1.5,
                 
                    },
              showTicks:false,
              pointLabels: {
                  show: false,
            	 },
            	 highlightMouseOver: false,
                  }, 
                  grid: {drawBorder:false, shadow:false},
                  legend: {
                	  show:true,
                	  
                      renderer: $.jqplot.EnhancedLegendRenderer,
                   
                      show: true, 
                      location: 's', 
                      placement: 'inside',
                   
                      labels:['&nbspIntercepted&nbsp','&nbspAt Risk Swiped&nbsp'],
                      xoffset:-20,
                      yoffset:10,
                      
                    },
                }
              );
    }
    
    renderInterception();

	var resizeInterception = function() {
				if (interception)
					interception.destroy();
				renderInterception();
		}

		$(window).resize(function() {
			resizeInterception();
			});
		
		
//				Frequency chart -- line chart
            
            
              
              var renderFrequency= function (){
            	  
            	  
            	  
            	  interceptedMembersFrequencyByWeek[0] = interceptedMembersFrequencyByWeekReverse[3]; 
            	  interceptedMembersFrequencyByWeek[1] = interceptedMembersFrequencyByWeekReverse[2]; 
            	  interceptedMembersFrequencyByWeek[2] = interceptedMembersFrequencyByWeekReverse[1]; 
            	  interceptedMembersFrequencyByWeek[3] = interceptedMembersFrequencyByWeekReverse[0]; 
            	  
            	  
            	  swipedMembersFrequencyByWeek[0] = swipedMembersFrequencyByWeekReverse[3]; 
            	  swipedMembersFrequencyByWeek[1] = swipedMembersFrequencyByWeekReverse[2]; 
            	  swipedMembersFrequencyByWeek[2] = swipedMembersFrequencyByWeekReverse[1]; 
            	  swipedMembersFrequencyByWeek[3] = swipedMembersFrequencyByWeekReverse[0]; 
	            

                  var thisWeekandThreeWeeksAgo = ['week1','week2','week3','week4']; 
                  var ticks2 = [-20,0,20,40,60,80,100];
                  var tick3 =[0,1,2,3,4,5,6,7,8,9,10];
              	
            	  frequency = $.jqplot ('analytical_frequency_chart', [interceptedMembersFrequencyByWeek,swipedMembersFrequencyByWeek], {
          	  
            	      
                   	seriesDefaults: {
                   		
                        showMarker: false,
	                         rendererOptions:{

	                           bandData:1,
	                          
	                         }, pointLabels: {
	                             show: false,
	                       	 },
	                       	markerOptions:{
	                            size:0
	                        }
	                    
                           
                       },
                      

                       
                       seriesColors:["#3c5016","#c4e090"],
                      
                       axes: {
                           
                    	   	
                           xaxis: {
                           	renderer: $.jqplot.CategoryAxisRenderer,
                         
                               ticks: thisWeekandThreeWeeksAgo,
                               tickOptions:{
                                   showGridline: false,
                                   markSize:0,
                                 
                               },
                               pad:0,
                               labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                          		label:'Timeline in weeks',
                              
                           },
                           x2axis:{
                        	   min:5,
                        	   max:10,
                        	   tick:tick3,
                      	   rendererOptions: { forceTickAt0: true, forceTickAt100: true },
                      	 showTick:false,   	
                      	   tickOptions:{
                      		   show:false,
                        	   		showGridline:false,
                        	   		markSize:0,
                        	   		
                        	   	},
                           },
                         
                       	yaxis:{
                    		
                       	
                       		ticks:ticks2 ,
                       		tickOptions:{
                       			formatString: '%d%%',
                               
                        	},
                        	pad:0,
                        	labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                    		label:'Rate Usage Increase',
                    		
                  
                     },
                     
                       	},
                       	
                    
                        grid: {
                           	drawBorder:false,
                           	shadow:false,
                           	background:'#FFFFFF',	
                           },
                       
                       legend: {
                           renderer: $.jqplot.EnhancedLegendRenderer,
                           show: true, 
                           location: 's', 
                           placement: 'outside',
                           rendererOptions: {
                             numberRows: '1',
                             numberColumns: '3'
                           },
                           
                           labels:['Members Intercepted&nbsp&nbsp','Members who Swiped&nbsp'],
                           xoffset:30,
                           yoffset:70,
                           seriesToggle: true
                         },
                      
                   
                });
            	  
              
    }
              
              renderFrequency();

				var resizeFrequency = function() {
							if (frequency)
								frequency.destroy();
							renderFrequency();
					}

					$(window).resize(function() {
						resizeFrequency();
						});
   
//          	Avg. spending chart -- line chart 
              
              var renderSpending= function (){
            	  
            	  
            	  
            	  
            	  interceptedMembersSpendingByWeek[0] = interceptedMembersSpendingByWeekReverse[3]; 
            	  interceptedMembersSpendingByWeek[1] = interceptedMembersSpendingByWeekReverse[2]; 
            	  interceptedMembersSpendingByWeek[2] = interceptedMembersSpendingByWeekReverse[1]; 
            	  interceptedMembersSpendingByWeek[3] = interceptedMembersSpendingByWeekReverse[0]; 
            	  
            	  
            	  swipedMembersSpendingByWeek[0] = swipedMembersSpendingByWeekReverse[3]; 
            	  swipedMembersSpendingByWeek[1] = swipedMembersSpendingByWeekReverse[2]; 
            	  swipedMembersSpendingByWeek[2] = swipedMembersSpendingByWeekReverse[1]; 
            	  swipedMembersSpendingByWeek[3] = swipedMembersSpendingByWeekReverse[0]; 
            	  

                  var thisWeekandThreeWeeksAgo = ['week1','week2','week3','week4']; 
                  var ticks2 = [-20,0,20,40,60,80,100];
            	  spending = $.jqplot ('analytical_avg_spending_chart', [interceptedMembersSpendingByWeek,swipedMembersSpendingByWeek], {
                 
            	      
            	 	seriesDefaults: {
                        rendererOptions:{
                          barPadding:0,
                         
                        }, pointLabels: {
                            show: false,
                      	 },
                      	markerOptions:{
                           size:0
                       }
                   
                      
                  },
                  seriesColors:["#3c5016","#c4e090"],
                 
                  axes: {
                      

                      xaxis: {
                      	renderer: $.jqplot.CategoryAxisRenderer,
                    
                          ticks: thisWeekandThreeWeeksAgo,
                          tickOptions:{
                              showGridline: false,
                              markSize:0,
                            
                          },
                          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                     		label:'Timeline in weeks',
                         
                      },
                    
                  	yaxis:{
               		
                  	
                  		ticks:ticks2 ,
                  		tickOptions:{
                  			formatString: '%d%%',
                          
                   	},
                   	labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
               		label:'Rate Spending Increase',
                      	
                  	},
                  	
                  },

                  grid: {
                     	drawBorder:false,
                     	shadow:false,
                     	background:'#FFFFFF',	
                     },
                  legend: {
                      renderer: $.jqplot.EnhancedLegendRenderer,
                      show: true, 
                      location: 's', 
                      placement: 'outside',
                      rendererOptions: {
                        numberRows: '1',
                        numberColumns: '2'
                      },
                      labels:['Members Intercepted&nbsp&nbsp&nbsp','Members who Swiped&nbsp&nbsp&nbsp'],
                      xoffset:30,
                      yoffset:70,
                      seriesToggle: true
                    },
                 
              
           });
              
}
              
              renderSpending();

				var resizeSpending = function() {
							if (spending)
								spending.destroy();
							renderSpending();
					}

					$(window).resize(function() {
						resizeSpending();
						});

					
					
					
					
	            }
	        }
	        });
}

