-- Creating requied indices 
CREATE INDEX ON members(client_member_id);

CREATE INDEX ON members(lower(firstname::text) varchar_pattern_ops, lower(lastname::text) varchar_pattern_ops);

-- If a birthday is within the given number of days before or after today
 CREATE OR REPLACE FUNCTION public.compute_birthday_week(dob date, num_days integer, tz text)                                                               
  RETURNS boolean                                                                                                                                           
 AS $function$                                                                                                                                              
 BEGIN                                                                                                                                                      
     IF  date_part('month', age( now() at time zone tz, dob )::interval)=11 AND                                                                             
         DATE_PART('days', DATE_TRUNC('month', now() at time zone tz)                                                                                       
             + '1 MONTH'::INTERVAL                                                                                                                          
             - DATE_TRUNC('month', now() at time zone tz)                                                                                                   
             ) -  date_part('day', age( now() at time zone tz, dob )::interval) < num_days then                                                             
             RETURN  true ;                                                                                                                                 
     ELSIF date_part('month', age( now() at time zone tz, dob )::interval)=0 and date_part('day', age(now() at time zone tz, dob )::interval)< num_days THEN
         RETURN true;                                                                                                                                       
     ELSE                                                                                                                                                   
         RETURN false;                                                                                                                                      
     end IF;                                                                                                                                                
 END;                                                                                                                                                        
 $function$ 
 LANGUAGE plpgsql;

