/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.beat.model;

/**
 * @author longsheng.wang
 *
 */
public class SentimentCount {
	
	private long positive;
	private long negative;
	private long neutral;

	/**
	 * 
	 */
	public SentimentCount() {
		positive = 0;
		negative = 0;
		neutral = 0;
	}

	/**
	 * @param positive
	 * @param negative
	 * @param neutral
	 */
	public SentimentCount(long positive, long negative, long neutral) {
		super();
		this.positive = positive;
		this.negative = negative;
		this.neutral = neutral;
	}
	
	/**
	 * Set the count by its string name.
	 * 
	 * @param name
	 * @param count
	 */
	public void setByName(String name, long count){
		if (name.trim().toLowerCase().equals("positive")){
			this.positive = count;
		}
		else if (name.trim().toLowerCase().equals("negative")){
			this.negative = count;
		}
		else if (name.trim().toLowerCase().equals("neutral")){
			this.neutral = count;
		}
		else {
			throw new IllegalArgumentException("Name can only be positive, negative and neutral. Case insensitive.");
		}
	}
	
	/**
	 * Get the count by its string name.
	 * 
	 * @param name
	 * @return
	 */
	public long getByName(String name){
		if (name.trim().toLowerCase().equals("positive")){
			return positive;
		}
		else if (name.trim().toLowerCase().equals("negative")){
			return negative;
		}
		else if (name.trim().toLowerCase().equals("neutral")){
			return neutral;
		}
		else {
			throw new IllegalArgumentException("Name can only be positive, negative and neutral. Case insensitive.");
		}
	}

	/**
	 * @return the positive
	 */
	public long getPositive() {
		return positive;
	}

	/**
	 * @param positive the positive to set
	 */
	public void setPositive(long positive) {
		this.positive = positive;
	}

	/**
	 * @return the negative
	 */
	public long getNegative() {
		return negative;
	}

	/**
	 * @param negative the negative to set
	 */
	public void setNegative(long negative) {
		this.negative = negative;
	}

	/**
	 * @return the neutral
	 */
	public long getNeutral() {
		return neutral;
	}

	/**
	 * @param neutral the neutral to set
	 */
	public void setNeutral(long neutral) {
		this.neutral = neutral;
	}
}
