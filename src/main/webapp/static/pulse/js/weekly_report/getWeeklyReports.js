/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function check_status() {
    var c1 = localStorage.getItem('club_code');
    var c2 = sessionStorage.getItem('club_code');
    //console.log('c1 '+c1);
    //console.log('c2 '+c2);
    if (c1 === null || c2 === null) {
        $.mobile.changePage("#login_page");
    }
}

function login() {

    $.ajax({
        url: 'jsp/auth/CheckUsername.jsp',
        data: {action: 'login', username: $('#username').val(), password: $('#password').val()}, // Convert a form to a JSON string representation
        type: 'POST',
        async: true,
        dataType: 'json',
        beforeSend: function() {
            // This callback function will trigger before data is sent
            $.mobile.showPageLoadingMsg(true); // This will show ajax spinner
        },
        complete: function() {
            // This callback function will trigger on data sent/received complete
            $.mobile.hidePageLoadingMsg(); // This will hide ajax spinner
        },
        success: function(data) {

            if (jQuery.isEmptyObject(data)) {
                $("#errorpopup").popup("open");
            } else {

                sessionStorage.setItem('name', data.name);
                sessionStorage.setItem('club_code', data.club);
                sessionStorage.setItem('location_id', data.id);
                sessionStorage.setItem('clubname', data.clubname);
                localStorage.setItem('club_code', data.club);
                localStorage.setItem('location_id', data.id);
                localStorage.setItem('name', data.name);
                localStorage.setItem('clubname', data.clubname);
                $.mobile.changePage("#homepage");

            }

        },
        error: function(request, error) {
            // This callback function will trigger on unsuccessful action                
            alert('Network error has occurred please try again!');
        }
    });
}

function getWeeklyInterceptionReports() {
    var location_id = localStorage.getItem("location_id");

    $.ajax({
        data: {
            location_id: location_id
        },
        type: 'POST',
        async: true,
        url: 'jsp/weekly_report/GetWeeklyInterceptionReports.jsp',
        dataType: 'json',
        beforeSend: function() {
            // This callback function will trigger before data is sent
            $.mobile.showPageLoadingMsg(true); // This will show ajax spinner
        },
        complete: function() {
            // This callback function will trigger on data sent/received complete
            $.mobile.hidePageLoadingMsg(); // This will hide ajax spinner
        },
        success: function(data) {
            if (jQuery.isEmptyObject(data)) {

            } else {
                $("#retention_overview_action_table tr:nth-child(1) td:nth-child(3)").html(data.LastWeekInterception.talkedTo);
                $("#retention_overview_action_table tr:nth-child(1) td:nth-child(4)").html(data.BeforeLastWeekInterception.talkedTo);
                $("#retention_overview_action_table tr:nth-child(1) td:nth-child(5)").html(data.LastMonthInterception.talkedTo);
                $("#retention_overview_action_table tr:nth-child(1) td:nth-child(6)").html(data.BeforeLastMonthInterception.talkedTo);

                $("#retention_overview_action_table tr:nth-child(2) td:nth-child(3)").html(data.LastWeekInterception.goodToGo);
                $("#retention_overview_action_table tr:nth-child(2) td:nth-child(4)").html(data.BeforeLastWeekInterception.goodToGo);
                $("#retention_overview_action_table tr:nth-child(2) td:nth-child(5)").html(data.LastMonthInterception.goodToGo);
                $("#retention_overview_action_table tr:nth-child(2) td:nth-child(6)").html(data.BeforeLastMonthInterception.goodToGo);

                $("#retention_overview_action_table tr:nth-child(3) td:nth-child(3)").html(data.LastWeekInterception.followUp);
                $("#retention_overview_action_table tr:nth-child(3) td:nth-child(4)").html(data.BeforeLastWeekInterception.followUp);
                $("#retention_overview_action_table tr:nth-child(3) td:nth-child(5)").html(data.LastMonthInterception.followUp);
                $("#retention_overview_action_table tr:nth-child(3) td:nth-child(6)").html(data.BeforeLastMonthInterception.followUp);

                $("#retention_overview_action_table tr:nth-child(4) td:nth-child(3)").html(data.LastWeekInterception.needAttention);
                $("#retention_overview_action_table tr:nth-child(4) td:nth-child(4)").html(data.BeforeLastWeekInterception.needAttention);
                $("#retention_overview_action_table tr:nth-child(4) td:nth-child(5)").html(data.LastMonthInterception.needAttention);
                $("#retention_overview_action_table tr:nth-child(4) td:nth-child(6)").html(data.BeforeLastMonthInterception.needAttention);

            }
        }
    });


}


function getWeeklyTrafficReports() {
    var location_id = localStorage.getItem("location_id");

    $.ajax({
        data: {
            location_id: location_id
        },
        type: 'POST',
        async: true,
        url: 'jsp/weekly_report/GetWeeklyTrafficReports.jsp',
        dataType: 'json',
        beforeSend: function() {
            // This callback function will trigger before data is sent
            $.mobile.showPageLoadingMsg(true); // This will show ajax spinner
        },
        complete: function() {
            // This callback function will trigger on data sent/received complete
        },
        success: function(data) {
            $.mobile.hidePageLoadingMsg(); // This will hide ajax spinner

            if (jQuery.isEmptyObject(data)) {

            } else {
                $("#retention_overview_traffic_table tr:nth-child(1) td:nth-child(3)").html(data.LastWeekTraffic.members);
                $("#retention_overview_traffic_table tr:nth-child(1) td:nth-child(4)").html(data.BeforeLastWeekTraffic.members);
                $("#retention_overview_traffic_table tr:nth-child(1) td:nth-child(5)").html(data.LastMonthTraffic.members);
                $("#retention_overview_traffic_table tr:nth-child(1) td:nth-child(6)").html(data.BeforeLastMonthTraffic.members);

                $("#retention_overview_traffic_table tr:nth-child(2) td:nth-child(3)").html(data.LastWeekTraffic.atRisk);
                $("#retention_overview_traffic_table tr:nth-child(2) td:nth-child(4)").html(data.BeforeLastWeekTraffic.atRisk);
                $("#retention_overview_traffic_table tr:nth-child(2) td:nth-child(5)").html(data.LastMonthTraffic.atRisk);
                $("#retention_overview_traffic_table tr:nth-child(2) td:nth-child(6)").html(data.BeforeLastMonthTraffic.atRisk);

                $("#retention_overview_traffic_table tr:nth-child(3) td:nth-child(3)").html(data.LastWeekTraffic.newMember);
                $("#retention_overview_traffic_table tr:nth-child(3) td:nth-child(4)").html(data.BeforeLastWeekTraffic.newMember);
                $("#retention_overview_traffic_table tr:nth-child(3) td:nth-child(5)").html(data.LastMonthTraffic.newMember);
                $("#retention_overview_traffic_table tr:nth-child(3) td:nth-child(6)").html(data.BeforeLastMonthTraffic.newMember);

                $("#retention_overview_traffic_table tr:nth-child(4) td:nth-child(3)").html(data.LastWeekTraffic.birthdayWeek);
                $("#retention_overview_traffic_table tr:nth-child(4) td:nth-child(4)").html(data.BeforeLastWeekTraffic.birthdayWeek);
                $("#retention_overview_traffic_table tr:nth-child(4) td:nth-child(5)").html(data.LastMonthTraffic.birthdayWeek);
                $("#retention_overview_traffic_table tr:nth-child(4) td:nth-child(6)").html(data.BeforeLastMonthTraffic.birthdayWeek);

                $("#retention_overview_traffic_table tr:nth-child(5) td:nth-child(3)").html(data.LastWeekTraffic.toBeEngaged);
                $("#retention_overview_traffic_table tr:nth-child(5) td:nth-child(4)").html(data.BeforeLastWeekTraffic.toBeEngaged);
                $("#retention_overview_traffic_table tr:nth-child(5) td:nth-child(5)").html(data.LastMonthTraffic.toBeEngaged);
                $("#retention_overview_traffic_table tr:nth-child(5) td:nth-child(6)").html(data.BeforeLastMonthTraffic.toBeEngaged);

            }
        }
    });


}


function getWeeklyTerminationReports() {
    var location_id = localStorage.getItem("location_id");

    $.ajax({
        data: {
            location_id: location_id
        },
        type: 'POST',
        async: true,
        url: 'jsp/weekly_report/GetWeeklyTerminationReports.jsp',
        dataType: 'json',
        beforeSend: function() {
            // This callback function will trigger before data is sent
            $.mobile.showPageLoadingMsg(true); // This will show ajax spinner
        },
        complete: function() {
            // This callback function will trigger on data sent/received complete
        },
        success: function(data) {
            $.mobile.hidePageLoadingMsg(); // This will hide ajax spinner

            if (jQuery.isEmptyObject(data)) {

            } else {
                $("#retention_overview_termination_table tr:nth-child(1) td:nth-child(2)").html(data.LastWeekTermination.total);
                $("#retention_overview_termination_table tr:nth-child(1) td:nth-child(3)").html(data.LastMonthTermination.total);
            }
        }
    });
    }


function setValues() {

    $("#retention_overview_club_name").html(localStorage.getItem('clubname'));
}