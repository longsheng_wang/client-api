/* Copyright 2015 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.test.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.inject.Inject;
import javax.inject.Named;

import net.reunify.pulse.client.ClientDAO;
import net.reunify.pulse.client.ClientDTO;
import net.reunify.pulse.client.ClientService;
import net.reunify.pulse.client.LocationDTO;
import net.reunify.pulse.members.interception.InterceptQueueEntry;
import net.reunify.pulse.members.interception.MemberInterceptionDAO;
import net.reunify.pulse.user.UserDAO;
import net.reunify.pulse.user.UserDTO;
import net.reunify.pulse.user.UserRole;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 * This module inserts fake database records for testing
 * 
 * @author kiana.baradaran
 * 
 */
public class MockDatabaseRecordCreator {

	@Inject
	private ClientService clientServiceHandler;

	@Inject
	private ClientDAO clientDAO;

	@Inject
	private UserDAO userDAO;

	@Inject
	private MemberInterceptionDAO memberInterceptionDAO;

	@Inject
	@Named("pulseJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	public void cleanUp() {
		String queueCleanUp =
				"delete from member_intercepts_queue where comment = 'Reunify ---- intercept later test ---- Reunify';";
		String clientCleanUp =
				"delete from clients where client_name ='TestCompanyFromReunify' and industry_name = 'Test';";
		String locationCleanUp =
				"delete from client_locations where location_name =  'TestLocationFromReunify' and location_timezone = 'US/PACIFIC';";
		String userCleanUp =
				"delete from users where firstname =  'firstNameForTest' and lastname = 'lastNameForTest' and (email_address='a@test.com' or email_address='b@test.com');";
		String memberCleanUp =
				"delete from members where lower(firstname) =  'firstnamefortest' and lower(lastname) = 'lastnamefortest' and client_member_id = '100100100';";
		jdbcTemplate.update(queueCleanUp);
		jdbcTemplate.update(memberCleanUp);
		jdbcTemplate.update(userCleanUp);
		jdbcTemplate.update(locationCleanUp);
		jdbcTemplate.update(clientCleanUp);
	}

	public int createTestQueueEntryRecord(int memberId, int userId, int locationId) {
		InterceptQueueEntry entry;
		if (memberId > 0 && userId > 0 && locationId > 0) {
			entry =
					new InterceptQueueEntry(memberId, userId, "Reunify ---- intercept later test ---- Reunify",
											"5mins", locationId);
		} else {
			int mockLocationId = createTestLocationRecord(0);
			int mockInterceptorId = createTestUserRecord(mockLocationId);
			int mockMemberId = createTestMemberRecord(mockLocationId);
			entry =
					new InterceptQueueEntry(mockMemberId, mockInterceptorId,
											"Reunify ---- intercept later test ---- Reunify", "5mins", mockLocationId);
		}
		return memberInterceptionDAO.enqueueMemberIntercept(entry);
	}

	public int createTestLocationRecord(int clientId) {
		if (clientId <= 0) {
			clientId = createTestClientRecord();
		}
		LocationDTO locationDTO = new LocationDTO();
		locationDTO.setClientId(clientId);
		locationDTO.setName("TestLocationFromReunify");
		locationDTO.setTimeZone("US/PACIFIC");
		return clientServiceHandler.addLocation(locationDTO);
	}

	public int createTestClientRecord() {
		ClientDTO client = new ClientDTO();
		client.setCompanyName("TestCompanyFromReunify");
		client.setIndustryName("Test");
		return clientDAO.createClientAccount(client);
	}

	public int createTestUserRecord(int locationId) {
		if (locationId <= 0) {
			locationId = createTestLocationRecord(0);
		}
		UserDTO user = new UserDTO();
		user.setFirstName("firstNameForTest");
		user.setLastName("lastNameForTest");
		user.setEmailAddress("a@test.com");
		user.setPassword("Testing1");
		user.setMatchingPassword("Testing1");
		user.setLocationId(locationId);
		user.setRole(new UserRole(1, "Admin"));
		return userDAO.createUserAccount(user);
	}

	public int createTestMemberRecord(int locationId) {
		if (locationId <= 0) {
			locationId = createTestLocationRecord(0);
		}
		String insertMember =
				"INSERT INTO MEMBERS (location_id,firstName, lastName, client_member_id) VALUES (?,'firstNameForTest','lastNameForTest','100100100') returning member_id";
		int memberId = 0;
		memberId =
				this.jdbcTemplate.queryForObject(insertMember, new Object[] { locationId }, new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
						return rs.getInt(1);
					}

				});

		return memberId;

	}

}
