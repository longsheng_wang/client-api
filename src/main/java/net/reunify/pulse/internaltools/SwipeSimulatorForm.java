/* Copyright 2015 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.pulse.internaltools;

import org.apache.log4j.Logger;


/**
 * SwipeSimulatorForm bean used for validating user input
 * 
 * @author Vivek Hungund
 *
 */
public class SwipeSimulatorForm {
	private static final Logger logger = Logger.getLogger(SwipeSimulatorForm.class);
	
	private int clientId;
	
	private int locationId;

	private int numSwipes;
	
	/**
	 * @param clientId
	 * @param locationId
	 * @param numSwipes
	 */
	public SwipeSimulatorForm(int clientId, int locationId, int numSwipesRequested) {
		this.clientId = clientId;
		this.locationId = locationId;
		this.numSwipes = numSwipesRequested;
	}

	/**
	 * Default constructor
	 */
	public SwipeSimulatorForm() {
	}

	/**
	 * @return the clientId
	 */
	public int getClientId() {
		return clientId;
	}

	/**
	 * @param clientId the clientId to set
	 */
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	/**
	 * @return the locationId
	 */
	public int getLocationId() {
		return locationId;
	}

	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	/**
	 * @return the numSwipes
	 */
	public int getNumSwipes() {
		return numSwipes;
	}

	/**
	 * @param numSwipes the numSwipes to set
	 */
	public void setNumSwipes(int numSwipes) {
		this.numSwipes = numSwipes;
	}

}
