<%@taglib prefix="ui" tagdir="/WEB-INF/tags/pulse/ui"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<ui:baseview contextPath="${pageContext.servletContext.contextPath}"
	pageTitle="Trace - Member Profile">
	<jsp:attribute name="headTag">
  		<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/memberprofile/memberprofile.css"
			rel="stylesheet">
			<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/memberprofile/notepad.css"
			rel="stylesheet">
<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/square/green.min.css"
			rel="stylesheet">
<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/square/red.min.css"
			rel="stylesheet">
<link
			href="${pageContext.servletContext.contextPath}/static/pulse/css/square/yellow.min.css"
			rel="stylesheet">

		<script
			src="${pageContext.servletContext.contextPath}/static/pulse/js/memberprofile/memberprofile.js"
			type="text/javascript"></script>
			<script
			src="${pageContext.servletContext.contextPath}/static/pulse/js/memberprofile/notepad.js"
			type="text/javascript"></script>
			<script
			src="${pageContext.servletContext.contextPath}/static/pulse/js/checkbox/icheck.min.js"></script>
<script
			src="${pageContext.servletContext.contextPath}/static/pulse/js/pictureHolder/holder.min.js"
			type="text/javascript"></script>
	</jsp:attribute>
	<jsp:body>
<div id="memberprofile_content" class="col-md-12 col-sm-12"
			style="height: 100%; padding: 0; margin-top: 10px;">
	<div class="col-md-6 col-sm-6">
		<div class="col-md-5 col-sm-5" style="height: 180px; padding: 0;">

			<div id="member-individual-picture-holder" class="img-thumbnail"
						style="width: 160px; height: 180px; text-align: center;">
				<img alt=""
							src="${pageContext.servletContext.contextPath}/static/pulse/img/loader/ajax-loader.gif"
							style="width: 40px; height: 40px; z-index: -1; margin-top: 45%;">

			</div>
			<div id="individual_member_availability" class="in-club-status"></div>
		</div>



		<div class="col-md-7 col-sm-7"
					style="height: 180px; border: #dddddd 1px solid; padding: 0;">
			<div class="row" style="height: 50px;">
				<div class="col-md-10 col-sm-10" style="margin-top: 7px;">
					<p id="individual_member_first_name" class="member-name"></p>
					<p id="individual_member_last_name" class="member-name"
								style="margin-top: -5px;"></p>

				</div>
				<div class="col-md-2 col-sm-2" style="margin-top: 7px;">
					<div class="row" id="member_page_new_photo_icon"></div>
				</div>
			</div>
			<div class="row" style="height: 46px;">

				<div class="col-md-7 col-sm-7">
					<div id="individual_member_age"
								class="member-individual-info-top-box"></div>
				</div>
				<div class="col-md-5 col-sm-5">
					<div class="row" style="height: 46px;">
						<div id="member_page_duration_year_icon" class="col-md-6 col-sm-6"
									style="height: 46px; background: url('${pageContext.servletContext.contextPath}/static/pulse/img/memberprofile/RD_Anniversary_2.0x.png') no-repeat; background-position-x: center; background-size: 42px; visibility: hidden; text-align: center;"></div>
						<div class="col-md-6 col-sm-6"
									id="member_individual_birthday_notification"></div>

					</div>
				</div>


			</div>

			<div class="row" style="height: 42px;">
				<div id="individual_member_id" class="col-md-5 col-sm-5 green-text"
							style="line-height: 35px;"></div>
				<div class="col-md-4 col-sm-4 green-text-bold"
							id="individual_member_primary_club" style="line-height: 35px;"></div>
			</div>

			<div class="row" style="height: 42px;">

				<div class="col-md-12 col-sm-12">
					<div id="individual_member_account_type"
								class="member-individual-info-top-box"></div>
				</div>


			</div>


		</div>
	
	<div class=" col-md-12 col-sm-12"
					style="height: 90px; border: solid 1px #dddddd; margin-top: 10px; padding: 0;">

		<div class="col-md-2 col-sm-2 col-md-offset-1 col-sm-offset-1"
						style="margin-top: 1px;">
			<div style="text-align: center; height: 40px;">
				<div id="member_page_risk_photo_icon" style="">
					<img alt=""
									src="${pageContext.servletContext.contextPath}/static/pulse/img/memberprofile/RD_AtRisk_GREY_2.0x.png"
									height="42">
				</div>
			</div>
			<div id="member_page_risk_value_icon"
							class="member_page_risk_value_icon_caption">RISK</div>
		</div>
		<div class="col-md-2 col-sm-2" style="height: 90px;">
			<div id="member_individual_star_status"
							style="text-align: center; margin-top: 2px; height: 40px;">
				<img alt=""
								src="${pageContext.servletContext.contextPath}/static/pulse/img/memberprofile/RD_STARGrey_2.0x.png"
								height="42">
			</div>
			<div class="member-individual-icon-caption">STAR</div>
		</div>

		<div class="col-md-2 col-sm-2" style="height: 90px;">

			<div style="text-align: center; margin-top: 2px; height: 40px;"
							id="member-individual-health-score">
				<img alt=""
								src="${pageContext.servletContext.contextPath}/static/pulse/img/memberprofile/RD_HEALTHSCORE_GREY-B3_2.0x.png"
								height="42">
			</div>
			<div class="member-individual-icon-caption">
				Health</br>Score
			</div>
		</div>
		<div class="col-md-2 col-sm-2" style="height: 90px;">
			<div style="text-align: center; margin-top: 2px; height: 40px;"
							id="member-individual-personal-trainer">
				<img alt=""
								src="${pageContext.servletContext.contextPath}/static/pulse/img/memberprofile/RD_TRAININGSESSION_Grey-b3_2.0x.png"
								height="42">
			</div>
			<div class="member-individual-icon-caption">
				Needs</br>TS
			</div>
		</div>

		<div class="col-md-2 col-sm-2" style="height: 90px;">

			<div style="text-align: center; height: 50px;">
				<div class="member-individual-offer-limit"
								id="offer-page-member-offer-allowance"></div>
			</div>
			<div class="member-individual-icon-caption"
							style="margin-left: 10px;">
				LT</br>Buck$
			</div>
		</div>
	</div>


	<div class=" col-md-12 col-sm-12"
					style="height: 230px; border: solid 1px #dddddd; margin-top: 10px;">

		<div class="row " style="width: 100%; height: 38px;">

			<div class="col-md-1 col-sm-1"></div>
			<div class="col-md-4 col-sm-4 ">
				<div class="light-grey-text member-individual-title">Tenure</div>
			</div>

			<div class="col-md-7 col-sm-7 ">
				<div id="individual_member_join_date" class="member-individual-info"></div>
			</div>
		</div>

		<div class="row " style="width: 100%; height: 38px;">
			<div class="col-md-1 col-sm-1 member-individual-bolt-icon"
							id="individual_member_lastVisit_bolt"></div>
			<div class="col-md-4 col-sm-4">
				<div class="light-grey-text member-individual-title">Last
					Visit</div>
			</div>
			<div class="col-md-7 col-sm-7">
				<div class="member-individual-info">
					<span id="individual_member_lastVisit"></span>
				</div>


			</div>
		</div>
		<div class="row " style="width: 100%; height: 38px;">
			<div class="col-md-1 col-sm-1"></div>

			<div class="col-md-4 col-sm-4">
				<div class="light-grey-text member-individual-title">Last
					Contact</div>
			</div>
			<div class="col-md-7 col-sm-7">
				<div class="member-individual-info">
					<span id="individual_member_lastAction"></span>
				</div>

			</div>
		</div>
		<div class="row " style="width: 100%; height: 38px;">
			<div class="col-md-1 col-sm-1 member-individual-bolt-icon"
							id="individual_member_frequency_bolt"></div>

			<div class="col-md-4 col-sm-4">
				<div class="light-grey-text member-individual-title">Frequency</div>
			</div>
			<div class="col-md-7 col-sm-7">
				<div id="individual_member_frequency" class="member-individual-info"></div>
			</div>

		</div>
		<div class="row " style="width: 100%; height: 38px;">
			<div class="col-md-1 col-sm-1"></div>

			<div class="col-md-4 col-sm-4">
				<div class="light-grey-text member-individual-title">Time of
					Visit</div>


			</div>
			<div class="col-md-7  col-sm-7">
				<div class="member-individual-info"
								id="member-individual-time-of-visit"></div>
			</div>

		</div>
		<div class="row" style="width: 100%; height: 38px;">
			<div class="col-md-1 col-sm-1"></div>

			<div class="col-md-4 col-sm-4">
				<div class="light-grey-text member-individual-title">Companionship</div>

			</div>
			<div class="col-md-7 col-sm-7">
				<div class="member-individual-info"
								id="member-individual-companionship"></div>
			</div>

		</div>
	</div>


	<div class=" col-md-12 col-sm-12"
					style="height: 120px; margin-top: 10px; padding: 0;">
		<div class="panel panel-default sharp-edge" style="">
			<div class="panel-heading sharp-edge"
							style="color: #4d4d4d; text-shadow: none;">
				<span class="badge pull-right"
								id="member_individual_interest_total_number"></span>
				<h3 class="panel-title">Interests</h3>
			</div>
			<div class="panel-body sharp-edge" style="height: 85px;">
				<div class="row" style="height: 100px; margin-top: -15px;">
					<div class="col-md-6 col-sm-6" style="height: 70px;">
						<div class="row">
							<div class="col-md-5 col-sm-5 interest-icon"
											id="member_individual_interest_icon_1"></div>
							<div class="col-md-7 col-sm-7 interest-title"
											id="member_individual_interest_title_1"></div>
						</div>


					</div>
					<div class="col-md-6 col-sm-6" style="height: 70px;">
						<div class="row">
							<div class="col-md-5 col-sm-5 interest-icon"
											id="member_individual_interest_icon_2"></div>
							<div class="col-md-7 col-sm-7 interest-title"
											id="member_individual_interest_title_2"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="col-md-6 col-sm-6" style="height:670px" >

	<div id="member_individual_offer_content">

		<div class="row" style="max-height:670px">
			<div class=" panel panel-default sharp-edge"
							style="width: 97%; margin-left: 2%; max-height:670px; margin-bottom:0px; padding-bottom:0px">
				<div class="panel-heading sharp-edge"
								style="color: #4d4d4d; text-shadow: none;">
					<span class="pull-right"> </span>
					<h3 class="panel-title" style="">
						<span style="display: inline-block"><img
										src="${pageContext.servletContext.contextPath}/static/pulse/img/interception/interception-flag-grey.png"
										width="20" alt=""> Interception</span>
					</h3>

				</div>
				<div class=" panel-body sharp-edge" style="max-height: 670px;margin-bottom:0px;padding-bottom:0px;">
					<div class=" col-md-10 col-sm-10" style="padding: 0; ">
						<div class=" col-md-12 col-sm-12"
										style="height:50px; padding: 0;">
							
								<div id="enter-note-alert" style="margin-top:-3%;padding:0px;text-align:center;color:red;visibility:hidden"> 
										Please enter a note						
									</div>
							
							<textarea data-role="none" class="form-control" rows="1"
											maxlength="1000"
											style=" border-radius: 0; box-shadow: none; outline: none; -webkit-box-shadow: none; -moz-box-shadow: none;max-height:50px"
											id="notepad_post_content"
											placeholder="Tap to add notes about this member."></textarea>

						</div>
						<div class=" col-md-12 col-sm-12"
										style="height: 25px; padding: 5px;">
										
										<div class="col-sm-8" id="select-status-alert" style="padding:0px;padding-left:20%;color:red;visibility:hidden"> 
										Please select a status						
									</div>
										
							<div
									class="col-sm-4"		style="font-size: 12px; text-align: right; height: 20px;  color: #dddddd; text-shadow: none; line-height: 20px;margin:0px;padding:0px"
											id="notepad-post-chars-left"></div>
						</div>



						<div class=" col-md-12 col-sm-12"
										style="height: 30px; padding: 0;">


							<div class=" col-md-4 col-sm-4  "
											style="padding-left: 5px; padding-right: 0;">
								<div class=" col-md-2 col-sm-2" style="padding: 0;">
									<label> <input type="checkbox" data-role='none'
													class="checkbox" id="good-to-go-input-checkbox"
													value="0001" name="fooby[1][]" />
									</label>
								</div>

								<div
												class=" col-md-10 col-sm-10 member-page-interception-checkbox-text-container">
									<span class="member-individual-checkbox-caption"> Good
										to Go</span>
								</div>
							</div>
							<div class="col-md-4 col-sm-4 " style="padding: 0">
								<div class=" col-md-2 col-sm-2" style="padding: 0;">
									<label> <input type="checkbox" data-role='none'
													class="checkbox" id="need-followup-input-checkbox"
													value="0010" name="fooby[1][]" />
									</label>
								</div>

								<div
												class=" col-md-10 col-sm-10 member-page-interception-checkbox-text-container">
									<span class="member-individual-checkbox-caption">Need
										Follow-up </span>
								</div>
							</div>
							<div class="col-md-4 col-sm-4 " style="padding: 0">
								<div class=" col-md-2 col-sm-2" style="padding: 0">
									<label> <input type="checkbox" data-role='none'
													class="checkbox" id="need-attention-input-checkbox"
													value="0011" name="fooby[1][]" />
									</label>
								</div>
								<div
												class=" col-md-10 col-sm-10 member-page-interception-checkbox-text-container">
									<span class=" member-individual-checkbox-caption">Need
										Attention</span>
								</div>
							</div>
						</div>
					</div>
					<div class=" col-md-2 col-sm-2" style="padding: 0;">
						<button class="btn btn-default" id="notepad_post_button"
										type="button" data-role="none"
										style="height: 35px; width: 90%; margin-left: 10%;margin-top:10px">Submit</button>

					</div>
					<div class=" col-md-12 col-sm-12" style="padding: 0; margin-top:3%">
					
						
						<div class="col-md-6 col-sm-6" style="margin-bottom: 5%;">
							<input id="notepad-views-search-field" class="form-control"
											style="width: 100%; margin-top: 0;display:none" data-role="none" 
											type="text" placeholder="Search notes..." maxlength="20" >
											
						
						</div>
						<div class="col-sm-6"></div>
						 <div class="col-md-2 col-sm-2" style="height: 35px; ">
							<button class="btn btn-small" style="height:33px ; display:none" id="clear_search_note">Clear Search</button>
						</div> 
						

					</div>

					<div class=" col-md-12 col-sm-12" id="notepad-insert-and-view-div"
									style="width: 100%; max-height: 425px; overflow:auto"></div>


				</div>

			</div>
		</div>

	</div>

</div>
</div>
	</jsp:body>
</ui:baseview>