/* Copyright 2014 Reunify LLC. All Rights Reserved.
 * Proprietary and confidential information of Reunify LLC. Disclosure, use,
 * or reproduction without the written authorization of Reunify LLC is prohibited.
 */
package net.reunify.pulse.authentication;

import javax.servlet.http.HttpServletRequest;

import net.reunify.pulse.base.BaseController;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Shakti Shrivastava
 *
 */
@Controller
public class AuthenticationController extends BaseController {
	private static final Logger logger = Logger.getLogger(AuthenticationController.class);

	@RequestMapping(value = "/authentication", method = RequestMethod.POST)
	public ResponseEntity<String> login(HttpServletRequest request) {		
		return new ResponseEntity<String>("{\"id\":\"175\",\"username\":\"TestUser\",\"club\":\"EPR\",\"name\":\"User Test\",\"clubname\":\"Eden Prairie (Flagship), MN\"}", HttpStatus.OK);
	}
	
	
}
