/* Copyright 2013 Reunify. All Rights Reserved.
 * Proprietary and confidential information of Reunify. Disclosure, use,
 * or reproduction without the written authorization of Reunify is prohibited.
 */
package net.reunify.beat.model;

/**
 * @author longsheng.wang
 *
 */
public class SurveyAnswerCount {
	
	private long id;
	private String content;
	private long positive;
	private long neutral;
	private long negative;
	private long notanswered;

	/**
	 * 
	 */
	public SurveyAnswerCount() {
		id = 0L;
		content = "";
		positive = 0L;
		negative = 0L;
		negative = 0L;
		notanswered = 0L;
	}

	/**
	 * @param id
	 * @param question
	 * @param positive
	 * @param neutral
	 * @param negative
	 * @param notanswered
	 */
	public SurveyAnswerCount(long id, String question, long positive, long neutral, long negative, long notanswered) {
		super();
		this.id = id;
		this.content = question;
		this.positive = positive;
		this.neutral = neutral;
		this.negative = negative;
		this.notanswered = notanswered;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the question
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the question to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the positive
	 */
	public long getPositive() {
		return positive;
	}

	/**
	 * @param positive the positive to set
	 */
	public void setPositive(long positive) {
		this.positive = positive;
	}

	/**
	 * @return the neutral
	 */
	public long getNeutral() {
		return neutral;
	}

	/**
	 * @param neutral the neutral to set
	 */
	public void setNeutral(long neutral) {
		this.neutral = neutral;
	}

	/**
	 * @return the negative
	 */
	public long getNegative() {
		return negative;
	}

	/**
	 * @param negative the negative to set
	 */
	public void setNegative(long negative) {
		this.negative = negative;
	}

	/**
	 * @return the notanswered
	 */
	public long getNotanswered() {
		return notanswered;
	}

	/**
	 * @param notanswered the notanswered to set
	 */
	public void setNotanswered(long notanswered) {
		this.notanswered = notanswered;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + (int) (negative ^ (negative >>> 32));
		result = prime * result + (int) (neutral ^ (neutral >>> 32));
		result = prime * result + (int) (positive ^ (positive >>> 32));
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SurveyAnswerCount other = (SurveyAnswerCount) obj;
		if (id != other.id)
			return false;
		if (negative != other.negative)
			return false;
		if (neutral != other.neutral)
			return false;
		if (positive != other.positive)
			return false;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "QuestionAnswerCount [id=" + id + ", question=" + content + ", positive=" + positive + ", neutral="
				+ neutral + ", negative=" + negative + "]";
	}

}