/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



function login() {

    $.ajax({
        url: 'jsp/auth/CheckUsername.jsp',
        data: {action: 'login', username: $('#username').val(), password: $('#password').val()}, // Convert a form to a JSON string representation
        type: 'POST',
        async: true,
        dataType: 'json',
        beforeSend: function() {
            // This callback function will trigger before data is sent
            $.mobile.showPageLoadingMsg(true); // This will show ajax spinner
        },
        complete: function() {
            // This callback function will trigger on data sent/received complete
            $.mobile.hidePageLoadingMsg(); // This will hide ajax spinner
        },
        success: function(data) {

            if (jQuery.isEmptyObject(data)) {
                $("#errorpopup").popup("open");
            } else {

                sessionStorage.setItem('name', data.name);
                sessionStorage.setItem('club_code', data.club);
                sessionStorage.setItem('location_id', data.id);
                sessionStorage.setItem('clubname', data.clubname);
                localStorage.setItem('club_code', data.club);
                localStorage.setItem('location_id', data.id);
                localStorage.setItem('name', data.name);
                localStorage.setItem('clubname', data.clubname);

                $.mobile.changePage("#homepage");

            }

        },
        error: function(request, error) {
            // This callback function will trigger on unsuccessful action                
            alert('Network error has occurred please try again!');
        }
    });
}


function check_status() {
    var c1 = localStorage.getItem('club_code');
    var c2 = sessionStorage.getItem('club_code');
    //console.log('c1 '+c1);
    //console.log('c2 '+c2);
    if (c1 === null || c2 === null) {
        $.mobile.changePage("#login_page");
    }
}


function getWeeklyReports() {
    var location_id = localStorage.getItem("location_id");

    $.ajax({
        data: {
            location_id: location_id
        },
        type: 'POST',
        async: true,
        url: 'jsp/weekly_report/GetWeeklyReportsFromFile.jsp',
        dataType: 'json',
        beforeSend: function() {
            // This callback function will trigger before data is sent
            $.mobile.showPageLoadingMsg(true); // This will show ajax spinner
        },
        complete: function() {
            // This callback function will trigger on data sent/received complete
            $.mobile.hidePageLoadingMsg(); // This will hide ajax spinner
        },
        success: function(data) {

            if (jQuery.isEmptyObject(data)) {

            } else {

                var d = new Date();
                var month = d.getMonth() + 1;
                console.log(month);
                var year = d.getFullYear();
                var daysInThisMonth = Math.round(((new Date(year, month)) - (new Date(year, month - 1))) / 86400000);
                var daysInLastMonth = Math.round(((new Date(year, month - 1)) - (new Date(year, month - 2))) / 86400000);
//                console.log(daysInLastMonth);
//                console.log(daysInThisMonth);

                if (!jQuery.isEmptyObject(data.TimeIntervals)) {
                    $("#retention_overview_termination_table th:nth-child(2)").html(data.TimeIntervals.LastDay);
                    $("#retention_overview_termination_table th:nth-child(3)").html("Week of " + data.TimeIntervals.LastWeek);
                    $("#retention_overview_termination_table th:nth-child(4)").html(data.TimeIntervals.ThisMonth);
                    $("#retention_overview_termination_table th:nth-child(5)").html(data.TimeIntervals.LastMonth);

                }



                if (!jQuery.isEmptyObject(data.LastDayInterception)) {
                    $("#retention_overview_action_table tr:nth-child(1) td:nth-child(3)").html(data.LastDayInterception.talkedTo);
                    $("#retention_overview_action_table tr:nth-child(2) td:nth-child(3)").html(data.LastDayInterception.goodToGo);
                    $("#retention_overview_action_table tr:nth-child(3) td:nth-child(3)").html(data.LastDayInterception.followUp);
                    $("#retention_overview_action_table tr:nth-child(4) td:nth-child(3)").html(data.LastDayInterception.needAttention);

                }
                if (!jQuery.isEmptyObject(data.LastWeekInterception)) {
                    $("#retention_overview_action_table tr:nth-child(1) td:nth-child(4)").html(data.LastWeekInterception.talkedTo);
                    $("#retention_overview_action_table tr:nth-child(2) td:nth-child(4)").html(data.LastWeekInterception.goodToGo);
                    $("#retention_overview_action_table tr:nth-child(3) td:nth-child(4)").html(data.LastWeekInterception.followUp);
                    $("#retention_overview_action_table tr:nth-child(4) td:nth-child(4)").html(data.LastWeekInterception.needAttention);

                }
                if (!jQuery.isEmptyObject(data.ThisMonthInterception)) {
                    $("#retention_overview_action_table tr:nth-child(1) td:nth-child(5)").html(data.ThisMonthInterception.talkedTo);
                    $("#retention_overview_action_table tr:nth-child(2) td:nth-child(5)").html(data.ThisMonthInterception.goodToGo);
                    $("#retention_overview_action_table tr:nth-child(3) td:nth-child(5)").html(data.ThisMonthInterception.followUp);
                    $("#retention_overview_action_table tr:nth-child(4) td:nth-child(5)").html(data.ThisMonthInterception.needAttention);

                }
                if (!jQuery.isEmptyObject(data.LastMonthInterception)) {
                    $("#retention_overview_action_table tr:nth-child(1) td:nth-child(6)").html(data.LastMonthInterception.talkedTo);
                    $("#retention_overview_action_table tr:nth-child(2) td:nth-child(6)").html(data.LastMonthInterception.goodToGo);
                    $("#retention_overview_action_table tr:nth-child(3) td:nth-child(6)").html(data.LastMonthInterception.followUp);
                    $("#retention_overview_action_table tr:nth-child(4) td:nth-child(6)").html(data.LastMonthInterception.needAttention);

                }

                if (!jQuery.isEmptyObject(data.LastWeekTermination)) {
                    $("#retention_overview_termination_table tr:nth-child(1) td:nth-child(3)").html(data.LastWeekTermination.total);

                }
                if (!jQuery.isEmptyObject(data.LastMonthTermination)) {
                    $("#retention_overview_termination_table tr:nth-child(1) td:nth-child(5)").html(data.LastMonthTermination.total);

                }
                if (!jQuery.isEmptyObject(data.LastDayTermination)) {
                    $("#retention_overview_termination_table tr:nth-child(1) td:nth-child(2)").html(data.LastDayTermination.total);

                }
                if (!jQuery.isEmptyObject(data.ThisMonthTermination)) {
                    $("#retention_overview_termination_table tr:nth-child(1) td:nth-child(4)").html(data.ThisMonthTermination.total);

                }





                if (!jQuery.isEmptyObject(data.LastMonthTraffic)) {
                    $("#retention_overview_traffic_table tr:nth-child(1) td:nth-child(6)").html(data.LastMonthTraffic.members);
                    $("#retention_overview_traffic_table tr:nth-child(2) td:nth-child(6)").html(data.LastMonthTraffic.atRisk);
                    $("#retention_overview_traffic_table tr:nth-child(3) td:nth-child(6)").html(data.LastMonthTraffic.newMember);
                    $("#retention_overview_traffic_table tr:nth-child(4) td:nth-child(6)").html(data.LastMonthTraffic.birthdayWeek);
                    $("#retention_overview_traffic_table tr:nth-child(5) td:nth-child(6)").html(data.LastMonthTraffic.toBeEngaged);


                }
                if (!jQuery.isEmptyObject(data.ThisMonthTraffic)) {
                    $("#retention_overview_traffic_table tr:nth-child(1) td:nth-child(5)").html(data.ThisMonthTraffic.members);

                    $("#retention_overview_traffic_table tr:nth-child(2) td:nth-child(5)").html(data.ThisMonthTraffic.atRisk);

                    $("#retention_overview_traffic_table tr:nth-child(3) td:nth-child(5)").html(data.ThisMonthTraffic.newMember);
                    $("#retention_overview_traffic_table tr:nth-child(4) td:nth-child(5)").html(data.ThisMonthTraffic.birthdayWeek);
                    $("#retention_overview_traffic_table tr:nth-child(5) td:nth-child(5)").html(data.ThisMonthTraffic.toBeEngaged);



                }
                if (!jQuery.isEmptyObject(data.LastWeekTraffic)) {
                    $("#retention_overview_traffic_table tr:nth-child(1) td:nth-child(4)").html(data.LastWeekTraffic.members);

                    $("#retention_overview_traffic_table tr:nth-child(2) td:nth-child(4)").html(data.LastWeekTraffic.atRisk);

                    $("#retention_overview_traffic_table tr:nth-child(3) td:nth-child(4)").html(data.LastWeekTraffic.newMember);

                    $("#retention_overview_traffic_table tr:nth-child(4) td:nth-child(4)").html(data.LastWeekTraffic.birthdayWeek);
                    $("#retention_overview_traffic_table tr:nth-child(5) td:nth-child(4)").html(data.LastWeekTraffic.toBeEngaged);

                }
                if (!jQuery.isEmptyObject(data.LastDayTraffic)) {
                    $("#retention_overview_traffic_table tr:nth-child(1) td:nth-child(3)").html(data.LastDayTraffic.members);

                    $("#retention_overview_traffic_table tr:nth-child(2) td:nth-child(3)").html(data.LastDayTraffic.atRisk);

                    $("#retention_overview_traffic_table tr:nth-child(3) td:nth-child(3)").html(data.LastDayTraffic.newMember);

                    $("#retention_overview_traffic_table tr:nth-child(4) td:nth-child(3)").html(data.LastDayTraffic.birthdayWeek);

                    $("#retention_overview_traffic_table tr:nth-child(5) td:nth-child(3)").html(data.LastDayTraffic.toBeEngaged);
                }
                if (!jQuery.isEmptyObject(data.LastDayAssistance)) {

                    $("#survey_overview_total_assistance_table tr:nth-child(1) td:nth-child(2)").html(data.LastDayAssistance.assistanceRequests);
                    $("#survey_overview_total_assistance_table tr:nth-child(2) td:nth-child(2)").html(data.LastDayAssistance.requestsCanceled + " (" + data.LastDayAssistance.percentage + "%)");
                    $("#survey_overview_total_assistance_table tr:nth-child(3) td:nth-child(2)").html("_");
                }

                if (!jQuery.isEmptyObject(data.LastWeekAssistance)) {

                    $("#survey_overview_total_assistance_table tr:nth-child(1) td:nth-child(3)").html(data.LastWeekAssistance.assistanceRequests);
                    $("#survey_overview_total_assistance_table tr:nth-child(2) td:nth-child(3)").html(data.LastWeekAssistance.requestsCanceled + " (" + data.LastWeekAssistance.percentage + "%)");
                    $("#survey_overview_total_assistance_table tr:nth-child(3) td:nth-child(3)").html((data.LastWeekAssistance.assistanceRequests / 7).toFixed(2));
                }
                if (!jQuery.isEmptyObject(data.ThisMonthAssistance)) {

                    $("#survey_overview_total_assistance_table tr:nth-child(1) td:nth-child(4)").html(data.ThisMonthAssistance.assistanceRequests);
                    $("#survey_overview_total_assistance_table tr:nth-child(2) td:nth-child(4)").html(data.ThisMonthAssistance.requestsCanceled + " (" + data.ThisMonthAssistance.percentage + "%)");
                    $("#survey_overview_total_assistance_table tr:nth-child(3) td:nth-child(4)").html((data.ThisMonthAssistance.assistanceRequests / daysInThisMonth).toFixed(2));
                }
                if (!jQuery.isEmptyObject(data.LastMonthAssistance)) {

                    $("#survey_overview_total_assistance_table tr:nth-child(1) td:nth-child(5)").html(data.LastMonthAssistance.assistanceRequests);
                    $("#survey_overview_total_assistance_table tr:nth-child(2) td:nth-child(5)").html(data.LastMonthAssistance.requestsCanceled + " (" + data.LastMonthAssistance.percentage + "%)");
                    $("#survey_overview_total_assistance_table tr:nth-child(3) td:nth-child(5)").html((data.LastMonthAssistance.assistanceRequests / daysInLastMonth).toFixed(2));
                }




                if (!jQuery.isEmptyObject(data.LastDaySurvey)) {

                    $("#survey_overview_total_survey_table tr:nth-child(1) td:nth-child(2)").html(data.LastDaySurvey.surveysTaken);
                    $("#survey_overview_total_survey_table tr:nth-child(2) td:nth-child(2)").html(data.LastDaySurvey.withName + " (" + data.LastDaySurvey.percentage + "%)");
                    $("#survey_overview_total_survey_table tr:nth-child(3) td:nth-child(2)").html("_");
                }
                if (!jQuery.isEmptyObject(data.LastWeekSurvey)) {

                    $("#survey_overview_total_survey_table tr:nth-child(1) td:nth-child(3)").html(data.LastWeekSurvey.surveysTaken);
                    $("#survey_overview_total_survey_table tr:nth-child(2) td:nth-child(3)").html(data.LastWeekSurvey.withName + " (" + data.LastWeekSurvey.percentage + "%)");
                    $("#survey_overview_total_survey_table tr:nth-child(3) td:nth-child(3)").html((data.LastWeekSurvey.surveysTaken / 7).toFixed(2));
                }
                if (!jQuery.isEmptyObject(data.ThisMonthSurvey)) {

                    $("#survey_overview_total_survey_table tr:nth-child(1) td:nth-child(4)").html(data.ThisMonthSurvey.surveysTaken);
                    $("#survey_overview_total_survey_table tr:nth-child(2) td:nth-child(4)").html(data.ThisMonthSurvey.withName + " (" + data.ThisMonthSurvey.percentage + "%)");
                    $("#survey_overview_total_survey_table tr:nth-child(3) td:nth-child(4)").html((data.ThisMonthSurvey.surveysTaken / daysInThisMonth).toFixed(2));
                }
                if (!jQuery.isEmptyObject(data.LastMonthSurvey)) {

                    $("#survey_overview_total_survey_table tr:nth-child(1) td:nth-child(5)").html(data.LastMonthSurvey.surveysTaken);
                    $("#survey_overview_total_survey_table tr:nth-child(2) td:nth-child(5)").html(data.LastMonthSurvey.withName + " (" + data.LastMonthSurvey.percentage + "%)");
                    $("#survey_overview_total_survey_table tr:nth-child(3) td:nth-child(5)").html((data.LastMonthSurvey.surveysTaken / daysInLastMonth).toFixed(2));
                }




                if (!jQuery.isEmptyObject(data.LastDayComments)) {

                    $("#survey_overview_total_comments_table tr:nth-child(1) td:nth-child(2)").html(data.LastDayComments.totalNotes);
                    $("#survey_overview_total_comments_table tr:nth-child(2) td:nth-child(2)").html(data.LastDayComments.withName + " (" + data.LastDayComments.percentage + "%)");
                    $("#survey_overview_total_comments_table tr:nth-child(3) td:nth-child(2)").html("_");
                }
                if (!jQuery.isEmptyObject(data.LastWeekComments)) {

                    $("#survey_overview_total_comments_table tr:nth-child(1) td:nth-child(3)").html(data.LastWeekComments.totalNotes);
                    $("#survey_overview_total_comments_table tr:nth-child(2) td:nth-child(3)").html(data.LastWeekComments.withName + " (" + data.LastWeekComments.percentage + "%)");
                    $("#survey_overview_total_comments_table tr:nth-child(3) td:nth-child(3)").html((data.LastWeekComments.totalNotes / 7).toFixed(2));
                }
                if (!jQuery.isEmptyObject(data.ThisMonthComments)) {

                    $("#survey_overview_total_comments_table tr:nth-child(1) td:nth-child(4)").html(data.ThisMonthComments.totalNotes);
                    $("#survey_overview_total_comments_table tr:nth-child(2) td:nth-child(4)").html(data.ThisMonthComments.withName + " (" + data.ThisMonthComments.percentage + "%)");
                    $("#survey_overview_total_comments_table tr:nth-child(3) td:nth-child(4)").html((data.ThisMonthComments.totalNotes / daysInThisMonth).toFixed(2));
                }
                if (!jQuery.isEmptyObject(data.LastMonthComments)) {

                    $("#survey_overview_total_comments_table tr:nth-child(1) td:nth-child(5)").html(data.LastMonthComments.totalNotes);
                    $("#survey_overview_total_comments_table tr:nth-child(2) td:nth-child(5)").html(data.LastMonthComments.withName + " (" + data.LastMonthComments.percentage + "%)");
                    $("#survey_overview_total_comments_table tr:nth-child(3) td:nth-child(5)").html((data.LastMonthComments.totalNotes / daysInLastMonth).toFixed(2));
                }




                if (!jQuery.isEmptyObject(data.LastDaySatisfaction)) {
                    if (!jQuery.isEmptyObject(data.LastDaySatisfaction.surveyType11000)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(1) td:nth-child(3)").html(data.LastDaySatisfaction.surveyType11000 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(1) td:nth-child(3)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastDaySatisfaction.surveyType11111)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(2) td:nth-child(3)").html(data.LastDaySatisfaction.surveyType11111 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(2) td:nth-child(3)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastDaySatisfaction.surveyType11100)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(3) td:nth-child(3)").html(data.LastDaySatisfaction.surveyType11100 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(3) td:nth-child(3)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastDaySatisfaction.surveyType10000)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(4) td:nth-child(3)").html(data.LastDaySatisfaction.surveyType10000 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(4) td:nth-child(3)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastDaySatisfaction.surveyType11110)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(5) td:nth-child(3)").html(data.LastDaySatisfaction.surveyType11110 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(5) td:nth-child(3)").html("-");

                    }
                }

                if (!jQuery.isEmptyObject(data.LastWeekSatisfaction)) {

                    if (!jQuery.isEmptyObject(data.LastWeekSatisfaction.surveyType11000)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(1) td:nth-child(4)").html(data.LastWeekSatisfaction.surveyType11000 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(1) td:nth-child(4)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastWeekSatisfaction.surveyType11111)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(2) td:nth-child(4)").html(data.LastWeekSatisfaction.surveyType11111 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(2) td:nth-child(4)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastWeekSatisfaction.surveyType11100)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(3) td:nth-child(4)").html(data.LastWeekSatisfaction.surveyType11100 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(3) td:nth-child(4)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastWeekSatisfaction.surveyType10000)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(4) td:nth-child(4)").html(data.LastWeekSatisfaction.surveyType10000 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(4) td:nth-child(4)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastWeekSatisfaction.surveyType11110)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(5) td:nth-child(4)").html(data.LastWeekSatisfaction.surveyType11110 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(5) td:nth-child(4)").html("-");

                    }

                }
                if (!jQuery.isEmptyObject(data.ThisMonthSatisfaction)) {
                    if (!jQuery.isEmptyObject(data.ThisMonthSatisfaction.surveyType11000)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(1) td:nth-child(5)").html(data.ThisMonthSatisfaction.surveyType11000 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(1) td:nth-child(5)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.ThisMonthSatisfaction.surveyType11111)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(2) td:nth-child(5)").html(data.ThisMonthSatisfaction.surveyType11111 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(2) td:nth-child(5)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.ThisMonthSatisfaction.surveyType11100)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(3) td:nth-child(5)").html(data.ThisMonthSatisfaction.surveyType11100 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(3) td:nth-child(5)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.ThisMonthSatisfaction.surveyType10000)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(4) td:nth-child(5)").html(data.ThisMonthSatisfaction.surveyType10000 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(4) td:nth-child(5)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.ThisMonthSatisfaction.surveyType11110)) {
                        $("#survey_overview_satisfaction_table tr:nth-child(5) td:nth-child(5)").html(data.ThisMonthSatisfaction.surveyType11110 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(5) td:nth-child(5)").html("-");

                    }

                }
                if (!jQuery.isEmptyObject(data.LastMonthSatisfaction)) {
                    if (!jQuery.isEmptyObject(data.LastMonthSatisfaction.surveyType11000)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(1) td:nth-child(6)").html(data.LastMonthSatisfaction.surveyType11000 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(1) td:nth-child(6)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastMonthSatisfaction.surveyType11111)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(2) td:nth-child(6)").html(data.LastMonthSatisfaction.surveyType11111 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(2) td:nth-child(6)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastMonthSatisfaction.surveyType11100)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(3) td:nth-child(6)").html(data.LastMonthSatisfaction.surveyType11100 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(3) td:nth-child(6)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastMonthSatisfaction.surveyType10000)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(4) td:nth-child(6)").html(data.LastMonthSatisfaction.surveyType10000 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(4) td:nth-child(6)").html("-");

                    }
                    if (!jQuery.isEmptyObject(data.LastMonthSatisfaction.surveyType11110)) {

                        $("#survey_overview_satisfaction_table tr:nth-child(5) td:nth-child(6)").html(data.LastMonthSatisfaction.surveyType11110 + "%");
                    } else {
                        $("#survey_overview_satisfaction_table tr:nth-child(5) td:nth-child(6)").html("-");

                    }
                }




                if (!jQuery.isEmptyObject(data.LastDayAssistancePerLocation)) {
                    $("#survey_overview_assistance_table tr:nth-child(1) td:nth-child(3)").html(data.LastDayAssistancePerLocation.location111100);
                    $("#survey_overview_assistance_table tr:nth-child(2) td:nth-child(3)").html(data.LastDayAssistancePerLocation.location111111);
                    $("#survey_overview_assistance_table tr:nth-child(3) td:nth-child(3)").html(data.LastDayAssistancePerLocation.location111110);
                    $("#survey_overview_assistance_table tr:nth-child(4) td:nth-child(3)").html(data.LastDayAssistancePerLocation.location111000);
                    $("#survey_overview_assistance_table tr:nth-child(5) td:nth-child(3)").html(data.LastDayAssistancePerLocation.location110000);
                    $("#survey_overview_assistance_table tr:nth-child(6) td:nth-child(3)").html(data.LastDayAssistancePerLocation.location100000);
                }

                if (!jQuery.isEmptyObject(data.LastWeekAssistancePerLocation)) {
                    $("#survey_overview_assistance_table tr:nth-child(1) td:nth-child(4)").html(data.LastWeekAssistancePerLocation.location111100);
                    $("#survey_overview_assistance_table tr:nth-child(2) td:nth-child(4)").html(data.LastWeekAssistancePerLocation.location111111);
                    $("#survey_overview_assistance_table tr:nth-child(3) td:nth-child(4)").html(data.LastWeekAssistancePerLocation.location111110);
                    $("#survey_overview_assistance_table tr:nth-child(4) td:nth-child(4)").html(data.LastWeekAssistancePerLocation.location111000);
                    $("#survey_overview_assistance_table tr:nth-child(5) td:nth-child(4)").html(data.LastWeekAssistancePerLocation.location110000);
                    $("#survey_overview_assistance_table tr:nth-child(6) td:nth-child(4)").html(data.LastWeekAssistancePerLocation.location100000);
                }
                if (!jQuery.isEmptyObject(data.ThisMonthAssistancePerLocation)) {
                    $("#survey_overview_assistance_table tr:nth-child(1) td:nth-child(5)").html(data.ThisMonthAssistancePerLocation.location111100);
                    $("#survey_overview_assistance_table tr:nth-child(2) td:nth-child(5)").html(data.ThisMonthAssistancePerLocation.location111111);
                    $("#survey_overview_assistance_table tr:nth-child(3) td:nth-child(5)").html(data.ThisMonthAssistancePerLocation.location111110);
                    $("#survey_overview_assistance_table tr:nth-child(4) td:nth-child(5)").html(data.ThisMonthAssistancePerLocation.location111000);
                    $("#survey_overview_assistance_table tr:nth-child(5) td:nth-child(5)").html(data.ThisMonthAssistancePerLocation.location110000);
                    $("#survey_overview_assistance_table tr:nth-child(6) td:nth-child(5)").html(data.ThisMonthAssistancePerLocation.location100000);
                }
                if (!jQuery.isEmptyObject(data.LastMonthAssistancePerLocation)) {
                    $("#survey_overview_assistance_table tr:nth-child(1) td:nth-child(6)").html(data.LastMonthAssistancePerLocation.location111100);
                    $("#survey_overview_assistance_table tr:nth-child(2) td:nth-child(6)").html(data.LastMonthAssistancePerLocation.location111111);
                    $("#survey_overview_assistance_table tr:nth-child(3) td:nth-child(6)").html(data.LastMonthAssistancePerLocation.location111110);
                    $("#survey_overview_assistance_table tr:nth-child(4) td:nth-child(6)").html(data.LastMonthAssistancePerLocation.location111000);
                    $("#survey_overview_assistance_table tr:nth-child(5) td:nth-child(6)").html(data.LastMonthAssistancePerLocation.location110000);
                    $("#survey_overview_assistance_table tr:nth-child(6) td:nth-child(6)").html(data.LastMonthAssistancePerLocation.location100000);
                }

                if (!jQuery.isEmptyObject(data.Last30DaysComments)) {

                    if (!jQuery.isEmptyObject(data.Last30DaysComments.location111100)) {

                        if (!jQuery.isEmptyObject(data.Last30DaysComments.location111100[0])) {

                            for (var key in data.Last30DaysComments.location111100) {

                                $("#survey_overview_front_desk_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location111100[key].survey) + "</td><td>" + data.Last30DaysComments.location111100[key].msg + "</td><td>" + data.Last30DaysComments.location111100[key].author + "</td><td>" + data.Last30DaysComments.location111100[key].time + "</td></tr>");
                            }
                        } else {
                            $("#survey_overview_front_desk_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location111100.survey) + "</td><td>" + data.Last30DaysComments.location111100.msg + "</td><td>" + data.Last30DaysComments.location111100.author + "</td><td>" + data.Last30DaysComments.location111100.time + "</td></tr>");

                        }
                    }

                    if (!jQuery.isEmptyObject(data.Last30DaysComments.location110000)) {
                        if (!jQuery.isEmptyObject(data.Last30DaysComments.location110000[0])) {
                            for (var key in data.Last30DaysComments.location110000) {

                                $("#survey_overview_womens_locker_room_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location110000[key].survey) + "</td><td>" + data.Last30DaysComments.location110000[key].msg + "</td><td>" + data.Last30DaysComments.location110000[key].author + "</td><td>" + data.Last30DaysComments.location110000[key].time + "</td></tr>");
                            }
                        } else {
                            $("#survey_overview_womens_locker_room_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location110000.survey) + "</td><td>" + data.Last30DaysComments.location110000.msg + "</td><td>" + data.Last30DaysComments.location110000.author + "</td><td>" + data.Last30DaysComments.location110000.time + "</td></tr>");

                        }
                    }

                    if (!jQuery.isEmptyObject(data.Last30DaysComments.location100000)) {
                        if (!jQuery.isEmptyObject(data.Last30DaysComments.location100000[0])) {

                            for (var key in data.Last30DaysComments.location100000) {

                                $("#survey_overview_mens_locker_room_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location100000[key].survey) + "</td><td>" + data.Last30DaysComments.location100000[key].msg + "</td><td>" + data.Last30DaysComments.location100000[key].author + "</td><td>" + data.Last30DaysComments.location100000[key].time + "</td></tr>");
                            }

                        } else {
                            $("#survey_overview_mens_locker_room_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location100000.survey) + "</td><td>" + data.Last30DaysComments.location100000.msg + "</td><td>" + data.Last30DaysComments.location100000.author + "</td><td>" + data.Last30DaysComments.location100000.time + "</td></tr>");

                        }
                    }

                    if (!jQuery.isEmptyObject(data.Last30DaysComments.location111000)) {
                        if (!jQuery.isEmptyObject(data.Last30DaysComments.location111000[0])) {


                            for (var key in data.Last30DaysComments.location111000) {
                                $("#survey_overview_family_locker_room_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location111000[key].survey) + "</td><td>" + data.Last30DaysComments.location111000[key].msg + "</td><td>" + data.Last30DaysComments.location111000[key].author + "</td><td>" + data.Last30DaysComments.location111000[key].time + "</td></tr>");
                            }
                        } else {
                            $("#survey_overview_family_locker_room_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location111000.survey) + "</td><td>" + data.Last30DaysComments.location111000.msg + "</td><td>" + data.Last30DaysComments.location111000.author + "</td><td>" + data.Last30DaysComments.location111000.time + "</td></tr>");

                        }
                    }

                    if (!jQuery.isEmptyObject(data.Last30DaysComments.location111110)) {
                        if (!jQuery.isEmptyObject(data.Last30DaysComments.location111110[0])) {

                            for (var key in data.Last30DaysComments.location111110) {

                                $("#survey_overview_group_fitness_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location111110[key].survey) + "</td><td>" + data.Last30DaysComments.location111110[key].msg + "</td><td>" + data.Last30DaysComments.location111110[key].author + "</td><td>" + data.Last30DaysComments.location111110[key].time + "</td></tr>");
                            }
                        } else {
                            $("#survey_overview_group_fitness_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location111110.survey) + "</td><td>" + data.Last30DaysComments.location111110.msg + "</td><td>" + data.Last30DaysComments.location111110.author + "</td><td>" + data.Last30DaysComments.location111110.time + "</td></tr>");

                        }
                    }
                    if (!jQuery.isEmptyObject(data.Last30DaysComments.location111111)) {
                        if (!jQuery.isEmptyObject(data.Last30DaysComments.location111111[0])) {

                            for (var key in data.Last30DaysComments.location111111) {

                                $("#survey_overview_fitness_floor_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location111111[key].survey) + "</td><td>" + data.Last30DaysComments.location111111[key].msg + "</td><td>" + data.Last30DaysComments.location111111[key].author + "</td><td>" + data.Last30DaysComments.location111111[key].time + "</td></tr>");
                            }
                        } else {
                            $("#survey_overview_fitness_floor_comments_table tbody").append("<tr><td>" + getSurveyType(data.Last30DaysComments.location111111.survey) + "</td><td>" + data.Last30DaysComments.location111111.msg + "</td><td>" + data.Last30DaysComments.location111111.author + "</td><td>" + data.Last30DaysComments.location111111.time + "</td></tr>");

                        }
                    }

                }



            }
        }
    }
    );
}



function setValues() {

    $("#retention_overview_club_name").html(localStorage.getItem('clubname'));
}

function getSurveyType(survey_type) {
    var survey;
    if (survey_type === '10000') {
        survey = "Locker Room";
    } else if (survey_type === '11000') {
        survey = "Main Entrance";
    } else if (survey_type === '11100') {
        survey = "Group Fitness";
    } else if (survey_type === '11110') {
        survey = "Pool";
    } else if (survey_type === '11111') {
        survey = "Fitness Floor";
    } else if (survey_type === '01001') {
        survey = "Comment Page";
    }
    return survey;
}