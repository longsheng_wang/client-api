<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Life Time MES Performance Report</title>
<link
	href="${pageContext.servletContext.contextPath}/static/pulse/css/error/error.css"
	rel="stylesheet">
</head>
<body>

	<c:set var="startDate" value="${param.startDate == null ? '' : param.startDate}" />
	<c:set var="endDate" value="${param.endDate == null ? '' : param.endDate}" />
	
	<strong>MES Performance Report</strong>
	<form:form method="POST"
		action="${pageContext.servletContext.contextPath}/reports/mesperformance" commandName="DateRangeReportForm">
		<table>
			<tr>
				<td>
					<form:label path="startDate">Start Date (e.g. 2015-01-15)</form:label>
				</td>
				<td>
					<form:input type="text" name="startDate" value="${startDate}" path="startDate" />
				</td>
				<td class="error">
					<form:errors path="startDate" element="div" cssClass="error" />
				</td>
			</tr>
			
			<tr>
				<td>
					<form:label path="endDate">End Date (e.g. 2015-02-01)</form:label>
				</td>
				<td>
					<form:input type="text" name="endDate" value="${endDate}" path="endDate" />
				</td>
				<td class="error">
					<form:errors path="endDate" element="div" cssClass="error" />
				</td>
			</tr>
			
			<tr>
				<td><input type="submit" value="Get Report"></td>
			</tr>
		</table>
	</form:form>
	Reports are not available for the current day. Please search only for past dates.<br /> <br />
	<c:if test="${pageContext.request.method=='POST' && !empty report}">

		<table class="data" border="1">
			<thead>
				<tr>
					<th>Employee ID</th>
					<th>Location ID</th>
					<th>Created By</th>
					<th>Number of Interceptions</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${report}" var="reportRow">
					<tr>
						<td>${reportRow.getEmployeeId()}</td>
						<td>${reportRow.getLocationId()}</td>
						<td>${reportRow.getCreatedBy()}</td>
						<td>${reportRow.getCount()}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</c:if>
</body>
</html>